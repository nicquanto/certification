package net.openid.conformance.openinsurance.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class  AbstractPageSizeCondition extends AbstractCondition {

	protected abstract int getPageSize();

	@Override
	@PostEnvironment(strings = "protected_resource_url")
	public Environment evaluate(Environment env) {

		String resourceUrl = env.getString("protected_resource_url");

		String regexPattern = "page-size=[0-9]{1,2}";

		Pattern pattern = Pattern.compile(regexPattern);
		Matcher matcher = pattern.matcher(resourceUrl);
		if (matcher.results().count() > 1) {
			throw error("resourceUrl provided contains multiple page-size parameters, please review the endpoint response.", args("resourceUrl", resourceUrl));
		}

		String newResourceUrl;
		if(resourceUrl.matches(".*" + regexPattern + ".*")){
			newResourceUrl = resourceUrl.replaceFirst(regexPattern, String.format("page-size=%d", getPageSize()));
		} else {
			newResourceUrl = resourceUrl.concat(String.format("?page-size=%d", getPageSize()));
		}

		logSuccess("page-size parameter has been added", args("url", newResourceUrl));
		env.putString("protected_resource_url", newResourceUrl);

		return env;
	}
}

