package net.openid.conformance.openbanking_brasil.opendata.pension;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.opendata.OpenDataLinksAndMetaValidator;
import net.openid.conformance.openbanking_brasil.productsNServices.ProductNServicesCommonFields;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.IntField;
import net.openid.conformance.util.field.NumberField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringArrayField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;


/**
 * Api: swagger/opendata/swagger-pension.yaml
 * Api endpoint: /risk-coverages
 * Api version: 1.0.0-rc3.0
 * Git hash:
 */

@ApiName("Pension Risk Coverages")
public class RiskCoveragesValidator extends AbstractJsonAssertingCondition {
	private static class Fields extends ProductNServicesCommonFields {
	}
	private final OpenDataLinksAndMetaValidator linksAndMetaValidator = new OpenDataLinksAndMetaValidator(this);
	private final CommonOpenDataParts parts;

	public RiskCoveragesValidator() {
		parts = new CommonOpenDataParts(this);
	}

	public static final Set<String> MODALITY = SetUtils.createSet("FUNERAL, PRESTAMISTA, VIAGEM, EDUCACIONAL, DOTAL, ACIDENTES_PESSOAIS, VIDA, PERDA_CERTIFICADO_HABILITACAO_VOO, DOENCAS_GRAVES_DOENCA_TERMINAL, DESEMPREGO_PERDA_RENDA, EVENTOS_ALEATORIOS, PECULIO, PENSAO_PRAZO_CERTO, PENSAO_MENORES_21, PENSAO_MENORES_24, PENSAO_CONJUGE_VITALICIA, PENSAO_CONJUGE_TEMPORARIA, NA");
	public static final Set<String> TYPE = SetUtils.createSet("MORTE, INVALIDEZ, OUTROS, NA");
	public static final Set<String> UPDATE_INDEX = SetUtils.createSet("IPCA, IGP_M, INPC, NA");
	public static final Set<String> PREMIUM_UPDATE_INDEX = SetUtils.createSet("IPCA, IGPM, INPC, NA");
	public static final Set<String> INDEMNIFIABLE_PERIOD = SetUtils.createSet("QUANTIDADE_DETERMINADA_PARCELAS, FIM_CICLO_DETERMINADO, NA");
	public static final Set<String> UNIT = SetUtils.createSet("DIAS, MESES, NAO_APLICA, NA");
	public static final Set<String> RISKS = SetUtils.createSet("ATO_RECONHECIMENTO_PERIGOSO, ATO_ILICITO_DOLOSO_PRATICADO_SEGURADO, OPERACOES_GUERRA, FURACOES_CICLONES_TERREMOTOS, MATERIAL_NUCLEAR, DOENCAS_LESOES_PREEXISTENTES, EPIDEMIAS_PANDEMIAS, SUICIDIO, ATO_ILICITO_DOLOSO_PRATICADO_CONTROLADOR, OUTROS, NA");
	public static final Set<String> PROFIT_MODALITY = SetUtils.createSet("PAGAMENTO_UNICO, FORMA_RENDA, NA");
	public static final Set<String> ASSISTANCE_TYPE = SetUtils.createSet("ACOMPANHANTE_CASO_HOSPITALIZACAO_PROLONGADA, ARQUITETO_VIRTUAL, ASSESSORIA_FINANCEIRA, AUTOMOVEL, AUXILIO_NATALIDADE, AVALIACAO_CLINICA_PREVENTIVA, BOLSA_PROTEGIDA, CESTA_BASICA, CHECKUP_ODONTOLOGICO, CLUBE_VANTAGENS_BENEFICIOS, CONVALESCENCIA, DECESSO, DESCONTO_FARMACIAS_MEDICAMENTOS, DESPESAS_FARMACEUTICAS_VIAGEM, DIGITAL, EDUCACIONAL, EMPRESARIAL, ENCANADOR, ENTRETENIMENTO, EQUIPAMENTOS_MEDICOS, FIANCAS_DESPESAS_LEGAIS, FISIOTERAPIA, FUNERAL, HELP_LINE, HOSPEDAGEM_ACOMPANHANTE, INTERRUPCAO_VIAGEM, INVENTARIO, MAIS_VIDA, MAMAE_BEBE, MEDICA_ACIDENTE_DOENCA, MOTOCICLETA, MULHER, NUTRICIONISTA, ODONTOLOGICA, ORIENTACAO_FITNESS, ORIENTACAO_JURIDICA, ORIENTACAO_NUTRICIONAL, PERSONAL_FITNESS, ORIENTACAO_PSICOSSOCIAL_FAMILIAR, PERDA_ROUBO_CARTAO, PET, PRORROGACAO_ESTADIA, PROTECAO_DADOS, RECOLOCACAO_PROFISSIONAL, REDE_DESCONTO_NUTRICIONAL, RESIDENCIAL, RETORNO_MENORES_SEGURADO, SAQUE_COACAO, SAUDE_BEM_ESTAR, SEGUNDA_OPINIAO_MEDICA, SENIOR, SUSTENTAVEL_DESCARTE_ECOLOGICO, TELEMEDICINA, VIAGEM, VITIMA, OUTROS, NA");
	public static final Set<String> ADDITIONAL = SetUtils.createSet("SORTEIO, SERVICOS_ASSISTENCIAS_COMPLEMENTARES_PAGO, SERVICOS_ASSISTENCIA_COMPLEMENTARES_GRATUITO, OUTROS, NAO_HA, NA");
	public static final Set<String> CRITERIAS_AGE_ADJ = SetUtils.createSet("APOS_PERIODO_ANOS, CADA_PERIODO_ANOS, MUDANCA_FAIXA_ETARIA, NAO_APLICAVEL, NA");
	public static final Set<String> FIN_REGIMES = SetUtils.createSet("REPARTICAO_SIMPLES, REPARTICAO_CAPITAIS, CAPITALIZACAO, NA");
	public static final Set<String> OTHER_GUARANTEED_VALUES = SetUtils.createSet("SALDAMENTO, BENEFICIO_PROLONGADO, NAO_APLICA, NA");
	public static final Set<String> INDEMNITY_PAYMENT_METHOD = SetUtils.createSet("UNICO, SOB_FORMA_RENDA, NA");
	public static final Set<String> PAYMENT_METHODS = SetUtils.createSet("CARTAO_CREDITO, DEBITO_CONTA, DEBITO_CONTA_POUPANCA, BOLETO_BANCARIO, PIX, TED_DOC, CONSIGNACAO_FOLHA_PAGAMENTO, PONTOS_PROGRAMA_BENEFICIO, OUTROS, NA");
	public static final Set<String> CONTRACT_TYPE = SetUtils.createSet("COLETIVO, INDIVIDUAL, NA");
	public static final Set<String> TARGET_AUDIENCES = SetUtils.createSet("PESSOA_NATURAL, PESSOA_JURIDICA, PESSOA_NATURAL_JURIDICA, NA");
	public static final Set<String> CONTRIBUTION_PERIODICITY = SetUtils.createSet("MENSAL, UNICA, ANUAL, TRIMESTRAL, SEMESTRAL, BIMESTRAL, OUTROS, NA");

	@Override
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectArrayField.Builder("data")
				.setValidator(this::assertData)
				.mustNotBeEmpty()
				.build());

		linksAndMetaValidator.assertMetaAndLinks(body);
		logFinalStatus();
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new ObjectField
				.Builder("participant")
				.setValidator(parts::assertParticipantIdentification)
				.build());

		assertField(data,
			new ObjectField
				.Builder("society")
				.setValidator(society -> {
					assertField(society, Fields.name().setMaxLength(80).build());
					assertField(society, Fields.cnpjNumber().setPattern("^(\\d{14})$|^(NA)$").build());
				})
				.build());

		assertField(data, Fields.name().setMaxLength(80).build());
		assertField(data, Fields.code().setMaxLength(80).build());

		assertField(data,
			new StringField
				.Builder("modality")
				.setEnums(MODALITY)
				.setMaxLength(33)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("coverages")
				.setValidator(this::assertCoverages)
				.build());

		assertField(data,
			new StringArrayField
				.Builder("assistanceTypes")
				.setEnums(ASSISTANCE_TYPE)
				.setMaxLength(43)
				.setOptional()
				.build());

		assertField(data,
			new StringArrayField
				.Builder("assistanceTypesAdditionalInfos")
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("additional")
				.setEnums(ADDITIONAL)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("termsAndConditions")
				.setValidator(termsAndConditions -> {
					assertField(termsAndConditions,
						new StringField
							.Builder("susepProcessNumber")
							.setMaxLength(20)
							.setMinLength(2)
							.setPattern("^(\\d{5}\\.\\d{6}\\/\\d{4}-\\d{2}$|^\\d{2}\\.\\d{6}\\/\\d{2}-\\d{2}$|^\\d{3}-\\d{5}\\/\\d{2}$|^\\d{5}\\.\\d{6}\\/\\d{2}-\\d{2})$|^(NA)$")
							.build());

					assertField(termsAndConditions,
						new StringField
							.Builder("detail")
							.setMaxLength(1024)
							.build());
				})
				.setMinItems(1)
				.build());

		assertField(data,
			new ObjectField
				.Builder("pmbacRemuneration")
				.setValidator(pmbacRemuneration -> {
					assertField(pmbacRemuneration,
						new StringField
							.Builder("interestRate")
							.setPattern("^(\\d{1}\\.\\d{6})$|^(-1.000000)$")
							.setMinLength(8)
							.setMaxLength(9)
							.build());

					assertField(pmbacRemuneration,
						new StringArrayField
							.Builder("updateIndexes")
							.setEnums(UPDATE_INDEX)
							.setOptional()
							.build());
				})
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("premiumUpdateIndex")
				.setEnums(PREMIUM_UPDATE_INDEX)
				.build());

		assertField(data,
			new ObjectField
				.Builder("ageAdjustment")
				.setValidator(ageAdjustment -> {
					assertField(ageAdjustment,
						new StringArrayField
							.Builder("criterias")
							.setMaxLength(27)
							.setEnums(CRITERIAS_AGE_ADJ)
							.build());

					assertField(ageAdjustment,
						new IntField
							.Builder("frequency")
							.setMaxLength(3)
							.build());
				})
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("financialRegimeContractType")
				.setEnums(FIN_REGIMES)
				.setOptional()
				.build());

		assertField(data,
			new ObjectField
				.Builder("reclaim")
				.setValidator(this::assertReclaim)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("otherGuaranteedValues")
				.setEnums(OTHER_GUARANTEED_VALUES)
				.build());

		assertField(data,
			new ObjectField
				.Builder("contributionPayment")
				.setValidator(contributionPayment -> {
					assertField(contributionPayment,
						new StringField
							.Builder("contributionPaymentMethod")
							.setEnums(PAYMENT_METHODS)
							.build());

					assertField(contributionPayment,
						new StringField
							.Builder("contributionPaymentMethodAdditionalInfo")
							.setMaxLength(140)
							.setPattern("[\\w\\W\\s]*")
							.setOptional()
							.build());

					assertField(contributionPayment,
						new StringField
							.Builder("contributionPeriodicity")
							.setEnums(CONTRIBUTION_PERIODICITY)
							.build());

					assertField(contributionPayment,
						new StringField
							.Builder("contributionPeriodicityAdditionalInfo")
							.setMaxLength(140)
							.setPattern("[\\w\\W\\s]*")
							.setOptional()
							.build());
				})
				.build());

		assertField(data,
			new ObjectField
				.Builder("minimumRequirement")
				.setValidator(minimumRequirements -> {
					assertField(minimumRequirements,
						new StringField
							.Builder("contractType")
							.setEnums(CONTRACT_TYPE)
							.setMaxLength(19)
							.build());

					assertField(minimumRequirements,
						new StringField
							.Builder("contractingMinRequirement")
							.setMaxLength(1024)
							.build());
				})
				.build());

		assertField(data,
			new StringField
				.Builder("targetAudience")
				.setEnums(TARGET_AUDIENCES)
				.setMaxLength(23)
				.build());
	}

	private void assertCoverages(JsonObject coverages) {
		assertField(coverages,
			new StringField
				.Builder("type")
				.setMaxLength(9)
				.setEnums(TYPE)
				.build());

		assertField(coverages,
			new StringArrayField
				.Builder("typeAdditionalInfos")
				.setMaxLength(100)
				.setOptional()
				.build());

		assertField(coverages,
			new ObjectField
				.Builder("attributes")
				.setValidator(this::assertAttributes)
				.setOptional()
				.build());
	}

	private void assertAttributes(JsonObject coverageAttributes) {
		assertField(coverageAttributes,
			new ObjectField
				.Builder("minValue")
				.setValidator(this::assertCurrencyValue)
				.build());

		assertField(coverageAttributes,
			new ObjectField
				.Builder("maxValue")
				.setValidator(this::assertCurrencyValue)
				.build());

		assertField(coverageAttributes,
			new StringField
				.Builder("indemnifiablePeriod")
				.setEnums(INDEMNIFIABLE_PERIOD)
				.setOptional()
				.build());

		assertField(coverageAttributes,
			new IntField
				.Builder("indemnifiableDeadline")
				.build());

		assertField(coverageAttributes,
			new StringField
				.Builder("indemnityPaymentMethod")
				.setEnums(INDEMNITY_PAYMENT_METHOD)
				.build());

		assertField(coverageAttributes,
			new ObjectField
				.Builder("gracePeriod")
				.setValidator(this::assertGracePeriod)
				.build());

		assertField(coverageAttributes,
			new StringArrayField
				.Builder("excludedRisks")
				.setEnums(RISKS)
				.setMaxLength(40)
				.build());

		assertField(coverageAttributes,
			new StringField
				.Builder("excludedRisksURL")
				.setMaxLength(1024)
				.build());

		assertField(coverageAttributes,
			new StringField
				.Builder("profitModality")
				.setEnums(PROFIT_MODALITY)
				.build());
	}

	private void assertCurrencyValue(JsonObject currencyValue) {
		assertField(currencyValue,
			new StringField
				.Builder("amount")
				.setMaxLength(21)
				.setPattern("^(\\d{1,16}\\.\\d{2,4})$|^(NA)$")
				.build());

		assertField(currencyValue,
			new StringField
				.Builder("currency")
				.setPattern("^([A-Z]{3})$|^(NA)$")
				.build());
	}

	private void assertGracePeriod(JsonObject gracePeriod) {
		assertField(gracePeriod,
			new NumberField
				.Builder("amount")
				.setMaxValue(999999999)
				.setOptional()
				.build());

		assertField(gracePeriod,
			new StringField
				.Builder("unit")
				.setEnums(UNIT)
				.setMaxLength(10)
				.setOptional()
				.build());
	}

	private void assertReclaim(JsonObject reclaim) {
		assertField(reclaim,
			new ObjectArrayField
				.Builder("table")
				.setValidator(table -> {
					assertField(table,
						new IntField
							.Builder("initialMonthRange")
							.setMaxLength(2)
							.build());

					assertField(table,
						new IntField
							.Builder("finalMonthRange")
							.setMaxLength(2)
							.build());

					assertField(table,
						new StringField
							.Builder("percentage")
							.setPattern("^(\\d{1}\\.\\d{6})$|^(-1.000000)$")
							.setMaxLength(9)
							.setMinLength(8)
							.build());
				})
				.setMinItems(1)
				.setOptional()
				.build());

		assertField(reclaim,
			new ObjectField
				.Builder("gracePeriod")
				.setValidator(gracePeriod -> {
					assertField(gracePeriod,
						new NumberField
							.Builder("amount")
							.setMaxValue(999999999)
							.build());

					assertField(gracePeriod,
						new StringField
							.Builder("unit")
							.setEnums(UNIT)
							.build());
				})
				.build());

		assertField(reclaim,
			new StringField
				.Builder("differenciatedPercentage")
				.setOptional()
				.build());
	}
}
