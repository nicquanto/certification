package net.openid.conformance.openbanking_brasil.opendata.exchange;

import com.google.common.collect.Sets;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.opendata.OpenDataLinksAndMetaValidator;
import net.openid.conformance.openbanking_brasil.productsNServices.ProductNServicesCommonFields;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api: swagger/opendata/swagger-exchange.yaml
 * Api endpoint: /online-rates
 * Api version: 1.0.0
 * Git hash:
 */

@ApiName("Online Rate V1")
public class ExchangeOnlineRateValidatorV1 extends AbstractJsonAssertingCondition {

	private static class Fields extends ProductNServicesCommonFields { }

	public static final Set<String> DELIVERY_CURRENCY = Sets.newHashSet("ESPECIE", "CARTAO_PRE_PAGO", "TELETRANSMISSAO_SWIFT");
	public static final Set<String> TRANSANCTION_TYPE = Sets.newHashSet("COMPRA", "VENDA");
	public static final Set<String> TARGET_AUDIENCE = SetUtils.createSet("PESSOA_NATURAL, PESSOA_JURIDICA, PESSOA_NATURAL_JURIDICA");
	public static final Set<String> TRANSANCTION_CATEGORY = SetUtils.createSet("COMERCIO_EXTERIOR, TRANSPORTE, SEGUROS, VIAGENS_INTERNACIONAIS, TRANSFERENCIAS_UNILATERAIS, SERVICOS_DIVERSOS, RENDAS_CAPITAIS, CAPITAIS_BRASILEIROS, CAPITAIS_ESTRANGEIROS, PRESTACAO_SERVICO_PAGAMENTO_OU_TRANSFERENCIA_INTERNACIONAL_EFX");
	private final OpenDataLinksAndMetaValidator linksAndMetaValidator = new OpenDataLinksAndMetaValidator(this);

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectArrayField
				.Builder("data")
				.setValidator(data -> {
					assertField(data,
						new ObjectField
							.Builder("participant")
							.setValidator(this::assertParticipantIdentification)
							.build());

					assertField(data,
						new StringField
							.Builder("foreignCurrency")
							.setPattern("^[A-Z]{3}$")
							.setMaxLength(3)
							.build());

					assertField(data,
						new StringField
							.Builder("deliveryForeignCurrency")
							.setEnums(DELIVERY_CURRENCY)
							.build());

					assertField(data,
						new StringField
							.Builder("transactionType")
							.setEnums(TRANSANCTION_TYPE)
							.build());

					assertField(data,
						new StringField
							.Builder("transactionCategory")
							.setEnums(TRANSANCTION_CATEGORY)
							.build());

					assertField(data,
						new StringField
							.Builder("targetAudience")
							.setEnums(TARGET_AUDIENCE)
							.build());

					assertField(data,
						new StringField
							.Builder("value")
							.setMinLength(8)
							.setMaxLength(22)
							.setPattern("^\\d{1,15}\\.\\d{6}$")
							.build());

					assertField(data,
						new StringField
							.Builder("valueUpdateDateTime")
							.setMaxLength(20)
							.setPattern("(^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])T(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)Z$)")
							.build());

					assertField(data,
						new StringField
							.Builder("disclaimer")
							.build());

				}).mustNotBeEmpty().build());

		linksAndMetaValidator.assertMetaAndLinks(body);

		logFinalStatus();
		return environment;
	}

	private void assertParticipantIdentification(JsonObject participantIdentification) {
		assertField(participantIdentification,
			new StringField
				.Builder("brand")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(80)
				.build());

		assertField(participantIdentification, Fields.name().setMaxLength(80).build());
		assertField(participantIdentification, Fields.cnpjNumber().setMaxLength(14).setPattern("^\\d{14}$").build());

		assertField(participantIdentification,
			new StringField
				.Builder("urlComplementaryList")
				.setMaxLength(1024)
				.setPattern("^(https?:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$")
				.setOptional()
				.build());
	}
}
