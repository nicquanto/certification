package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SetResourceMethodToPatch extends AbstractCondition {

	@Override
	@PostEnvironment(required = "resource")
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");
		resource.remove("resourceMethod");
		resource.addProperty("resourceMethod", "PATCH");
		env.putObject("resource", resource);
		logSuccess("Set protected resource access method to PATCH");

		return env;
	}

}
