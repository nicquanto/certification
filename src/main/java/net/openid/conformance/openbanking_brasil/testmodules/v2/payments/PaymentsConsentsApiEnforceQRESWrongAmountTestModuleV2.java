package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRESCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectQRCodeWithWrongAmountIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRESCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetPaymentAmountToKnownValueOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetPaymentAmountToKnownValueOnPaymentInitiation;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasValorInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasValorInvalido;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_qres-wrong-amount-proxy_test-module_v2",
	displayName = "Payments Consents API test module for QRES local instrument with divergent amount",
	summary = "Ensure that payment fails when the amount sent on the Static QR Code differs from the payload (Reference Error 2.2.2.3)\n" +
		"\u2022 Call POST Consent with different amount values on QRES and the payload\n" +
		"\u2022 Expects 422 - DETALHE_PAGAMENTO_INVALIDO\n" +
		"\u2022 Validate Error message\n" +
		"If no errors are identified:\n" +
		"\u2022 Expects 201 - Validate Reponse\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint\n" +
		"\u2022 Expects 422 - VALOR_INVALIDO\n" +
		"\u2022 Validate Error message\n" +
		"If no errors are identified:\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expects a payment with the definitive state (RJCT) status and RejectionReason equal to VALOR_INVALIDO\n" +
		"\u2022 Validate Error message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiEnforceQRESWrongAmountTestModuleV2 extends AbstractPaymentConsentUnhappyPathTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectQRESCodeLocalInstrument.class);
		callAndStopOnFailure(SelectQRESCodePixLocalInstrument.class);
		callAndStopOnFailure(SetPaymentAmountToKnownValueOnConsent.class);
		callAndStopOnFailure(SetPaymentAmountToKnownValueOnPaymentInitiation.class);
		callAndStopOnFailure(InjectQRCodeWithWrongAmountIntoConfig.class);
	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasValorInvalido.class);
	}

	@Override
	protected void validate422ErrorResponseCode() {
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasValorInvalido.class);
	}

	@Override
	protected Class<EnsureResourceResponseCodeWas201Or422> getExpectedPaymentResponseCode() {
		return EnsureResourceResponseCodeWas201Or422.class;
	}

}
