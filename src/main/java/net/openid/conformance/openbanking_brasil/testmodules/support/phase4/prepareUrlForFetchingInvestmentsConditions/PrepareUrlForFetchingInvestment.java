package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingInvestment extends ResourceBuilder {

    @Override
    @PreEnvironment(strings = {"investment-api", "investmentId"})
    public Environment evaluate(Environment env) {
        String api = env.getString("investment-api");
        String investmentId = env.getString("investmentId");

        setApi(api);
        setEndpoint("/investments/" + investmentId);

        return super.evaluate(env);
    }
}
