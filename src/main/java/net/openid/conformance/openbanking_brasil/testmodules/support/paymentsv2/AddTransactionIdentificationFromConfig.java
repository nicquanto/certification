package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class AddTransactionIdentificationFromConfig extends AbstractCondition {

	@Override
	public final Environment evaluate(Environment env) {
		String transactionIdentifierString = OIDFJSON.getString(
			Optional.ofNullable(env.getElementFromObject("config", "resource.transactionIdentifier"))
				.orElseThrow(() -> error("Transaction Identifier field was null")));
		log("Setting transaction identification to a new value");
		JsonObject obj = env.getObject("resource");
		obj = obj.getAsJsonObject("brazilPixPayment");
		obj = obj.getAsJsonObject("data");
		obj.addProperty("transactionIdentification", transactionIdentifierString);
		log(obj);
		return env;
	}


}

