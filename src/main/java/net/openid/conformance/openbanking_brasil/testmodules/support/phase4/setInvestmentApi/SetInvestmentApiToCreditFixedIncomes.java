package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi;

public class SetInvestmentApiToCreditFixedIncomes extends AbstractSetInvestmentApi {

    @Override
    protected String investmentApi() {
        return "credit-fixed-incomes";
    }
}
