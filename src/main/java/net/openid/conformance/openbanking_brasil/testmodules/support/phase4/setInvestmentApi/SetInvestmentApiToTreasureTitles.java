package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi;

public class SetInvestmentApiToTreasureTitles extends AbstractSetInvestmentApi {

    @Override
    protected String investmentApi() {
        return "treasure-titles";
    }
}
