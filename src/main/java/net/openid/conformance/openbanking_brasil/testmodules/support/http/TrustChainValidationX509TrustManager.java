package net.openid.conformance.openbanking_brasil.testmodules.support.http;


import com.nimbusds.jose.util.X509CertUtils;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.extensions.DirectoryRootsService;
import net.openid.conformance.extensions.SpringContext;
import net.openid.conformance.logging.TestInstanceEventLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.X509TrustManager;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.*;
import java.util.*;

public class TrustChainValidationX509TrustManager implements X509TrustManager {

	private static final Logger LOG = LoggerFactory.getLogger(TrustChainValidationX509TrustManager.class);

	private final TestInstanceEventLog eventLog;

	public TrustChainValidationX509TrustManager(TestInstanceEventLog eventLog) {
		this.eventLog = eventLog;
	}

	@Override
	public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {}

	@Override
	public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
		validateTrustChainReturned(x509Certificates, getAcceptedIssuers());
	}

	@Override
	public X509Certificate[] getAcceptedIssuers() {
		DirectoryRootsService directoryRootsService = getDirectoryRootsService();
		return directoryRootsService.getRootCertificates();
	}

	private String certificateX509ArrayToPemString(X509Certificate... certs) {
		List<String> certStrings = new ArrayList<>();
		for (X509Certificate cert : certs) {
			certStrings.add(X509CertUtils.toPEMString(cert));
		}
		return certStrings.toString();
	}

	public void validateTrustChainReturned(X509Certificate[] trustChain, X509Certificate... directoryRoots) throws CertificateException {
		try {
			Set<TrustAnchor> trustAnchors = generateTrustAnchors(trustChain, directoryRoots);
			PKIXBuilderParameters params = generatePixParams(trustChain, trustAnchors);

			CertPathValidator cpv = CertPathValidator.getInstance("PKIX");

			CertificateFactory cf = CertificateFactory.getInstance("X509");
			CertPath path = cf.generateCertPath(List.of(trustChain));
			//validateChain
			cpv.validate(path, params);
			eventLog.log(this.getClass().getSimpleName(), Map.of("msg","Trust Chain Validated Successfully","Trust Chain", certificateX509ArrayToPemString(trustChain),"result", Condition.ConditionResult.SUCCESS));
		} catch (CertPathValidatorException | InvalidAlgorithmParameterException | NoSuchAlgorithmException e){
			LOG.error("Unable to validate trust chain",e);
			eventLog.log(this.getClass().getSimpleName(), Map.of("msg",e.getMessage(),"Trust Chain", certificateX509ArrayToPemString(trustChain),"result", Condition.ConditionResult.FAILURE));
			throw new CertificateException("Unable to validate trust chain",e);
		}
	}

	private Set<TrustAnchor> generateTrustAnchors(X509Certificate[] trustChain, X509Certificate... directoryRoots) throws CertificateException {
		Set<TrustAnchor> trustAnchors = new HashSet<>();
			//Check if they sent the root cert in the chain to use as trust anchor
			X509Certificate rootCert = findRootCertInChain(Arrays.asList(trustChain));
			if (rootCert != null) {
				LOG.info("Root cert sent in chain, verifying it can be used as trust anchor");
				try {
					directoryRootsContains(rootCert, directoryRoots);
					trustAnchors.add(new TrustAnchor(X509CertUtils.parse(rootCert.getEncoded()),null));
				} catch (CertificateEncodingException e) {
					throw new RuntimeException(e);
				}
			} else {
			//Root not sent in chain, use directory roots as trust anchors
				LOG.info("Root cert not sent in chain, using directory roots as trust anchors");
				for (X509Certificate directoryTrustAnchor : directoryRoots) {
					try {
						trustAnchors.add(new TrustAnchor(X509CertUtils.parse(directoryTrustAnchor.getEncoded()),null));
					} catch (CertificateEncodingException e) {
						throw new RuntimeException(e);
					}
				}
			}
		return trustAnchors;
	}

	private PKIXBuilderParameters generatePixParams(X509Certificate[] x509Certificates, Set<TrustAnchor> trustAnchors) throws CertificateException {
		X509CertSelector selector = new X509CertSelector();
		selector.setCertificate(x509Certificates[0]);
		PKIXBuilderParameters params;
		try {
			params = new PKIXBuilderParameters(trustAnchors, selector);
		} catch (InvalidAlgorithmParameterException e) {
			throw new CertificateException(e);
		}
		params.setRevocationEnabled(false);

		return params;
	}
		private void directoryRootsContains(X509Certificate rootCert, X509Certificate... directoryRootsCerts) throws CertificateException {
			for (X509Certificate certificate : directoryRootsCerts) {
				if ((certificate.getIssuerDN().equals(certificate.getSubjectDN())) && certificate.getIssuerDN().equals(rootCert.getIssuerDN())){
					LOG.info("Directory root contains root cert provided in chain");
					return;
				}
			}
			LOG.error("Directory root does not contain root cert provided in chain");
			throw new CertificateException("Directory root does not contain root cert provided in chain");
		}
		private X509Certificate findRootCertInChain(List<X509Certificate> certificates) {
		X509Certificate rootCert = null;

		for(X509Certificate cert : certificates) {
			if (cert.getIssuerDN().equals(cert.getSubjectDN())){
				rootCert = cert;
				break;
			}
		}

		return rootCert;
	}

	private DirectoryRootsService getDirectoryRootsService() {
		return SpringContext.getBean(DirectoryRootsService.class);
	}
}
