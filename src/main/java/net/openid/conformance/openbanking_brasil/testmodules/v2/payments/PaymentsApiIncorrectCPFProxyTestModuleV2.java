package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforcePresenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.InjectCorrectButUnknownCpfOnPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.InjectCorrectButUnknownCpfOnPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.InjectRealCreditorAccountToPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.InjectRealCreditorAccountToPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDPIorPRD;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasDPIorPRD;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_incorrect-cpf-proxy_test-module_v2",
	displayName = "Payments API test module ensuring unknown CPF is rejected",
	summary = "Ensure error when invalid CPF is sent\n" +
		"\u2022 Calls POST Consents Endpoint with invalid CPF, using DICT as the localInstrument\n" +
		"\u2022 Expects 201- Validate Response\n" +
		"\u2022 Redirects the user to authorize the created consent   \n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"    \n" +
		"\u2022 Calls the POST Payments  invalid creditor account\n" +
		"\u2022 Expects 201 or 422 DETALHE_PAGAMENTO_INVALIDO or PAGAMENTO_RECUSADO_DETENTORA\n" +
		"\u2022 If 201 is returned:\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD    \n" +
		"\u2022 Expects 200 with a RJCT status, and rejectionReason as DETALHE_PAGAMENTO_INVALIDO or PAGAMENTO_RECUSADO_DETENTORA",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)

public class PaymentsApiIncorrectCPFProxyTestModuleV2 extends AbstractPaymentUnhappyPathTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EnforcePresenceOfDebtorAccount.class);
		callAndContinueOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndContinueOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndContinueOnFailure(RemoveQRCodeFromConfig.class);
		callAndContinueOnFailure(InjectRealCreditorAccountToPaymentConsent.class);
		callAndContinueOnFailure(InjectRealCreditorAccountToPayment.class);
		callAndContinueOnFailure(InjectCorrectButUnknownCpfOnPaymentConsent.class);
		callAndContinueOnFailure(InjectCorrectButUnknownCpfOnPayment.class);
	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasDPIorPRD.class);
	}

	@Override
	protected void validate422ErrorResponseCode() {
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasDPIorPRD.class);
	}

	@Override
	protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		return EnsureResourceResponseCodeWas201Or422.class;
	}
}
