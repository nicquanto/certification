package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.List;
import java.util.Optional;

public class SetScopeOnTokenEndpointRequestAccordingToDCRResponse extends AbstractCondition {

	@Override
	@PreEnvironment(required = "token_endpoint_request_form_parameters")
	@PostEnvironment(required = "token_endpoint_request_form_parameters")
	public Environment evaluate(Environment env) {
		JsonObject tokenEndpointRequest = env.getObject("token_endpoint_request_form_parameters");

		List<String> scopes = Lists.newArrayList(Splitter.on(" ").split(
			OIDFJSON.getString(Optional.ofNullable(tokenEndpointRequest.get("scope"))
				.orElseThrow(() -> error("scopes not present on token_endpoint_request_form_parameters",
					args("token_endpoint_request_form_parameters", tokenEndpointRequest))))).iterator());

		if (scopes.contains("payments")) {
			tokenEndpointRequest.addProperty("scope", "payments");
		} else if (scopes.contains("consents")) {
			tokenEndpointRequest.addProperty("scope", "consents");
		} else {
			throw error("There is neither 'payments' nor 'consents' scope provided in DCR response", args("scopes provided", scopes));
		}

		logSuccess("Scope parameter has been set according to DCR response", args("token_endpoint_request_form_parameters", tokenEndpointRequest));

		return env;
	}
}
