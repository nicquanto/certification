package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateErrorAndMetaFieldNames;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreateConsentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractPaymentConsentUnhappyPathTestModuleV2 extends AbstractPaymentUnhappyPathTestModuleV2 {

	@Override
	protected void performPreAuthorizationSteps() {
		OpenBankingBrazilPreAuthorizationErrorAgnosticSteps steps = (OpenBankingBrazilPreAuthorizationErrorAgnosticSteps) new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
			.replace(
				ValidateErrorAndMetaFieldNames.class,
				condition(CreateConsentErrorValidatorV2.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE)
					.skipIfStringMissing("validate_errors")
				)
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
			);
		call(steps);
		callAndStopOnFailure(SaveAccessToken.class);
		callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasFormaPagamentoInvalida.class,Condition.ConditionResult.INFO);
			if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
				fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
			}
		int status = Optional.ofNullable(env.getInteger("consent_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find consent_endpoint_response_full"));
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		if (status == HttpStatus.SC_CREATED) {
			callAndContinueOnFailure(PaymentConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);
		} else {
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class);
			env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
			callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class);
			fireTestFinished();
		}
	}
}
