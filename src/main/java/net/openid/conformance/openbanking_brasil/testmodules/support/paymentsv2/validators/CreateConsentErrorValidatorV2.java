package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.CreateConsentErrorEnumV2;
import net.openid.conformance.testmodule.Environment;

import java.util.Set;

public class CreateConsentErrorValidatorV2 extends AbstractErrorValidatorV2 {


	public static final int MAX_CONSENT_ERROR_ITEMS = 3;
	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment environment) {
		return super.evaluate(environment);
	}

	@Override
	protected Set<String> getErrorCodes() {
		return CreateConsentErrorEnumV2.toSet();
	}

	@Override
	protected int getMaxErrorItems() {
		return MAX_CONSENT_ERROR_ITEMS;
	}

	@Override
	protected String getResponseObjectKey() {
		return RESPONSE_ENV_KEY;
	}
}
