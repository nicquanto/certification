package net.openid.conformance.openbanking_brasil.testmodules.support;

import org.springframework.http.HttpStatus;

public class EnsureConsentResponseWas403 extends AbstractEnsureConsentResponseCode
{
	@Override
	protected int getExpectedStatus() {
		return HttpStatus.FORBIDDEN.value();
	}
}
