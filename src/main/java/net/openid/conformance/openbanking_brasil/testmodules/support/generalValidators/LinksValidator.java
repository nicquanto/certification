package net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Map.entry;

public class LinksValidator {

    private final AbstractJsonAssertingCondition validator;
    private int totalPages;
    private int page;
    private String requestUri;
    private int expectedVersion;
    private String self;
    private int firstPage;
    private int prevPage;
    private int nextPage;
    private int lastPage;
	private String linksPattern;
    private String errorMessage = "";
    private final Map<String, Object> args = new HashMap<>();

    public LinksValidator(AbstractJsonAssertingCondition validator) {
        this.validator = validator;
		this.linksPattern = "^(https:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$";
    }

	public LinksValidator(AbstractJsonAssertingCondition validator, String pattern) {
		this.validator = validator;
		this.linksPattern = pattern;
	}

    public void assertLinksObject(JsonElement body, String requestUri, int expectedVersion) {
        setTotalPages(body);
        this.requestUri = requestUri;
        this.expectedVersion = expectedVersion;

        validator.assertField(body,
            new ObjectField
                .Builder("links")
                .setValidator(this::assertLinks)
                .build());
    }

    public void setTotalPages(JsonElement body) {
        totalPages = OIDFJSON.getInt(
            Optional.ofNullable(body.getAsJsonObject().getAsJsonObject("meta").get("totalPages"))
                .orElse(new JsonPrimitive(-1))
        );
    }

    private void assertLinks(JsonObject links) {
        validator.assertField(links,
            new StringField
                .Builder("self")
                .setMaxLength(2000)
                .setPattern(linksPattern)
                .build());

        self = OIDFJSON.getString(links.get("self"));
        page = getPageFromLink(links, "self");

        if (!assertOtherLinks(links)) {
            buildMessageAndArgs("totalPages parameter is greater than 1, but page parameter is not valid",
                "totalPages", "page");
        } else if (!checkPageParameterMatchesRequest()) {
            buildMessageAndArgs("page parameter on 'self' link does not match the one on the request URI.",
                "'self' page parameter", "request URI");
        } else if (!checkVersionOnSelfLink(links)) {
            buildMessageAndArgs("version on 'self' link does not match expected value.",
                "self", "expected version");
        } else {
            assertLinksOrder(links);
        }
    }

    private int getPageFromLink(JsonObject links, String linkName) {
        String pagePattern = "page=(?<page>[0-9]+)";
        String link = OIDFJSON.getString(
            Optional.ofNullable(links.get(linkName)).orElse(new JsonPrimitive(""))
        );
        Pattern pattern = Pattern.compile(pagePattern);
        Matcher matcher = pattern.matcher(link);
        return matcher.find() ? Integer.parseInt(matcher.group("page")) : -1;
    }

    private boolean assertOtherLinks(JsonObject links) {
        StringField.Builder firstField = new StringField
            .Builder("first")
            .setMaxLength(2000)
            .setPattern(linksPattern);

        StringField.Builder prevField = new StringField
            .Builder("prev")
            .setMaxLength(2000)
            .setPattern(linksPattern);

        StringField.Builder nextField = new StringField
            .Builder("next")
            .setMaxLength(2000)
            .setPattern(linksPattern);

        StringField.Builder lastField = new StringField
            .Builder("last")
            .setMaxLength(2000)
            .setPattern(linksPattern);

        if (totalPages > 1) {
            if (page == 1) {
                firstField = firstField.setOptional();
                prevField = prevField.setOptional();
            } else if (page == totalPages) {
                nextField = nextField.setOptional();
                lastField = lastField.setOptional();
            } else if (page < 1 || page > totalPages) {
                return false;
            }
        } else {
            firstField = firstField.setOptional();
            prevField = prevField.setOptional();
            nextField = nextField.setOptional();
            lastField = lastField.setOptional();
        }

        validator.assertField(links, firstField.build());
        validator.assertField(links, prevField.build());
        validator.assertField(links, nextField.build());
        validator.assertField(links, lastField.build());

        return true;
    }

    private boolean checkPageParameterMatchesRequest() {
        String pagePattern = "page=(?<page>[0-9]+)";
        Pattern pattern = Pattern.compile(pagePattern);
        Matcher matcher = pattern.matcher(requestUri);
        int requestPage = matcher.find() ? Integer.parseInt(matcher.group("page")) : -1;
        return (requestPage == -1) || (page == requestPage);
    }

    private boolean checkVersionOnSelfLink(JsonObject links) {
        String selfLink = OIDFJSON.getString(links.get("self"));
        String versionPattern = "/v(?<version>[0-9]+)/";
        Pattern pattern = Pattern.compile(versionPattern);
        Matcher matcher = pattern.matcher(selfLink);
        return matcher.find() && Integer.parseInt(matcher.group("version")) == expectedVersion;
    }

    private void assertLinksOrder(JsonObject links) {
        if (page > 0) {
            firstPage = getPageFromLink(links, "first");
            prevPage = getPageFromLink(links, "prev");
            nextPage = getPageFromLink(links, "next");
            lastPage = getPageFromLink(links, "last");

            if (firstPage != -1 && firstPage != 1) {
                buildMessageAndArgs("'first' link is present and has a page parameter, but it is not equal to 1.",
                    "'first' page parameter");
            } else if (prevPage != -1 && prevPage != page - 1) {
                buildMessageAndArgs("'prev' link is present and has a page parameter, but it is not equal to 'self' - 1.",
                    "'prev' page parameter", "'self' page parameter");
            } else if (nextPage != -1 && nextPage != page + 1) {
                buildMessageAndArgs("'next' link is present and has a page parameter, but it is not equal to 'self' + 1.",
                    "'next' page parameter", "'self' page parameter");
            } else if (lastPage != - 1 && lastPage != totalPages) {
                buildMessageAndArgs("'last' link is present and has a page parameter, but it is not equal to totalPages.",
                    "'last' page parameter", "totalPages");
            }
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Map<String, Object> getArgs() {
        return args;
    }

    private void buildMessageAndArgs(String message, String... args) {
        errorMessage = message;
        Map<String, Object> argMap = Map.ofEntries(
            entry("totalPages", totalPages),
            entry("page", page),
            entry("request URI", requestUri),
            entry("self", self),
            entry("expected version", expectedVersion),
            entry("'self' page parameter", page),
            entry("'first' page parameter", firstPage),
            entry("'prev' page parameter", prevPage),
            entry("'next' page parameter", nextPage),
            entry("'last' page parameter", lastPage)
        );

        this.args.clear();
        for (String arg : args) {
            this.args.put(arg, argMap.get(arg));
        }
    }
}
