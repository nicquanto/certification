package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import java.util.ArrayList;
import java.util.List;

public class ValidateCaChainReturnedPhase2Phase3 extends AbstractValidateCaChainReturned{
	@Override
	protected List<String> getEndpointRegexes() {
		List<String> endpointRegexes = new ArrayList<>();
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/consents\\/v\\d+\\/consents)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/credit-cards-accounts\\/v\\d+\\/accounts)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/loans\\/v\\d+\\/contracts)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/credit-cards-accounts\\/v\\d+\\/accounts)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/customers\\/v\\d+\\/personal\\/identifications)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/financings\\/v\\d+\\/contracts)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/invoice-financings\\/v\\d+\\/contracts)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/resources\\/v\\d+\\/resources)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/unarranged-accounts-overdraft\\/v\\d+\\/contracts)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/payments\\/v\\d+\\/consents)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/payments\\/v\\d+\\/pix\\/payments)$");
		endpointRegexes.add("^(https:\\/\\/)(.*?)(\\/open-banking\\/customers\\/v\\d+\\/business\\/identifications)$");
		return endpointRegexes;
	}

	@Override
	protected String getVersionRegex() {
		return "^(2.[0-9].[0-9])$";
	}
}
