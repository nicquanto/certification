package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareAllInvestmentsRelatedConsentsForHappyPathTest  extends AbstractCondition {

    @Override
    @PostEnvironment(strings = "consent_permissions")
    public Environment evaluate(Environment env) {
        String[] permissions = {"BANK_FIXED_INCOMES_READ", "CREDIT_FIXED_INCOMES_READ", "FUNDS_READ",
            "VARIABLE_INCOMES_READ", "TREASURE_TITLES_READ", "RESOURCES_READ"};
        env.putString("consent_permissions", String.join(" ", permissions));
        logSuccess("Set all permissions for investments", args("permissions", permissions));
        return env;
    }
}
