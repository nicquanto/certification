package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EnsureDcrResponseDoesNotContainWebhook extends AbstractCondition {
	@Override
	@PreEnvironment(required = "dynamic_registration_endpoint_response")
	public Environment evaluate(Environment env) {
		JsonObject registrationClientEndpointResponse = env.getObject("dynamic_registration_endpoint_response");

		JsonObject responseBody = registrationClientEndpointResponse.getAsJsonObject("body_json");

		if (responseBody.has("webhook_uris")) {
			throw error("webhook_uris is present in the dynamic registration response");
		}
		logSuccess("webhook_uris is not present in the dynamic registration response");
		return env;
	}
}
