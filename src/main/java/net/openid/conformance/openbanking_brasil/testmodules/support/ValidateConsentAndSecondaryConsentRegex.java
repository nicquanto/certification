package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class ValidateConsentAndSecondaryConsentRegex extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String consentUrl = getStringFromEnvironment(env,"config","resource.consentUrl","Consent Url");
		String secondaryConsentUrl = getStringFromEnvironment(env,"config","resource.consentUrl2","Secondary Consent Url");
		validateConsentUrl(consentUrl);
		validateConsentUrl(secondaryConsentUrl);
		return env;
	}

	private void validateConsentUrl(String consentUrl){
		if (consentUrl.matches("^(https://)(.*?)(payments/v[0-9]/consents)")){
			logSuccess("Consent Url matches regex", args("consentUrl", consentUrl));
		} else if (consentUrl.matches("^(https://)(.*?)(consents/v[0-9]/consents)")){
			logSuccess("Consent Url matches regex", args("consentUrl", consentUrl));
		} else {
			throw error("Consent URL does not match the defined pattern for either Consents or Payments Consents", args("consentUrl", consentUrl));
		}
	}
}
