package net.openid.conformance.openbanking_brasil.testmodules;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractDictVerifiedPaymentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePaymentDateIsToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.InjectRealCreditorAccountEmailToPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.InjectRealCreditorAccountToPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveTransactionIdentification;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProxyToFakeEmailAddressOnPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProxyToFakeEmailAddressOnPaymentConsent;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_invalid-email-proxy_test-module",
	displayName = "Payments API test module ensuring email address is incorrect",
	summary = "Payments API test module ensuring email address is incorrect" +
		"Flow:" +
		"Makes a bad DICT payment flow with an incorrect email - expects a 422 at either consent or payment" +
		" initiation stage, or a 201 with a PNDG status at the payment initiation stage but a subsequent status of RJCT" +
		"Required:" +
		"Consent url pointing at the consent endpoint." +
		"Config: Debtor account must be present in the config. We manually set the local instrument to DICT, add a creditor account, add an fake email address.",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiWrongEmailAddressProxyTestModule extends AbstractDictVerifiedPaymentTestModule {

	@Override
	protected void configureDictInfo() {
		eventLog.startBlock("Setting date to today");
		callAndStopOnFailure(EnsurePaymentDateIsToday.class);
		callAndContinueOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndContinueOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndContinueOnFailure(RemoveQRCodeFromConfig.class);
		callAndContinueOnFailure(RemoveTransactionIdentification.class);
		callAndContinueOnFailure(InjectRealCreditorAccountEmailToPaymentConsent.class);
		callAndContinueOnFailure(InjectRealCreditorAccountToPayment.class);
		callAndContinueOnFailure(SetProxyToFakeEmailAddressOnPaymentConsent.class);
		callAndContinueOnFailure(SetProxyToFakeEmailAddressOnPayment.class);

		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);

	}
}
