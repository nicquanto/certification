package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason.EnsureCancellationReasonWasCanceladoAgendamento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancelledFrom.EnsureCancelledFromWasIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasCanc;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_pixscheduling-patch-iniciadora_test-module_v2",
	displayName = "PATCH Payments happy path test that will revoke a scheduled payment with cancelation object set as INICIADORA",
	summary = "PATCH Payments happy path test that will revoke a scheduled payment with cancelation object set as INICIADORA\n" +
		"\u2022 Create consent with request payload with both the schedule.single.date field set as D+1\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"\u2022 Call the POST Payments Endpoint\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD or ACCP\n" +
		"\u2022 Expect Payment Scheduled to be reached (SCHD) - Validate Response\n" +
		"\u2022 Call the PATCH Payments Endpoint - cancelledBy must match the loggedUser object\n" +
		"\u2022 Expect 200 - Make Sure cancellation.cancelledFrom is set as \"INICIADORA\". Make Sure cancellation.reason is set as \"CANCELADO_AGENDAMENTO\" - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiPixSchedulingPatchIniciadoraTestModuleV2 extends AbstractPixSchedulingTestModuleV2{

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
	}

	@Override
	protected void executePostPollingSteps() {
		super.executePostPollingSteps();
		callAndStopOnFailure(EnsurePaymentStatusWasCanc.class);
		callAndStopOnFailure(EnsureCancelledFromWasIniciadora.class);
		callAndStopOnFailure(EnsureCancellationReasonWasCanceladoAgendamento.class);
	}

}
