package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasCanc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs.EnsureScheduledDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentsPatchValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@PublishTestModule(
	testName = "dcr_api_dcm-pagto-webhook-schd_test-module",
	displayName = "dcr_api_dcm-pagto-webhook-schd_test-module",
	summary = "Ensure that the tested institution has correctly implemented the webhook notification endpoint and that this endpoint is correctly called when a scheduled payment changes to a schd state . \n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>\n" +
		"Obtain a SSA from the Directory\n" +
		"Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration\n" +
		"Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>”]\n" +
		"Expect a 201 - Validate Response\n" +
		"Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"Set the consents webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>/open-banking/webhook/v1/payments/v2/consents/{consentId}, where the alias is to be obtained from the field alias on the test configuration\n" +
		"Calls POST Consents Endpoint with localInstrument set as DICT and set the payment.schedule.single.date to D+1\n" +
		"Expects 201 - Validate Response\n" +
		"Redirects the user to authorize the created consent\n" +
		"Call GET Consent\n" +
		"Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"Calls the POST Payments with localInstrument as DICT\n" +
		"Expects 201 - Validate response\n" +
		"Set the payments webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>/open-banking/webhook/v1/payments/v2/pix/payments/{paymentId} where the alias is to be obtained from the field alias on the test configuration\n" +
		"Expect an incoming message, one for each defined endoint, payments and consents, both which  must be mtls protected and can be received on any endpoint at any order - Wait 60 seconds for both messages to be returned\n" +
		"For both webhook calls - Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"Call the Get Payments endpoint with the PaymentID \n" +
		"Expects status returned to be (SCHD) - Validate Response\n" +
		"Call the Patch Payments endpoint with the PaymentID \n" +
		"Expect an incoming message for the payments webhook endpoint - Wait at most 60 seconds for the endpoint to be called\n" +
		"Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"Call the Get Payments endpoint with the PaymentID \n" +
		"Expects status returned to be (CANC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.webhookWaitTime"
	}
)
public class PaymentsWebhookSchdDcrTestModule extends AbstractBrazilDCRPaymentsWebhook{
	@Override
	public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
		Boolean paymentWebhookReceived = env.getBoolean("second_payment_webhook_received");
		String paymentId = env.getString("payment_id");
		 if (paymentWebhookReceived != null && (path.contains("webhook/v1/payments/v2/pix/payments/" +paymentId) && !paymentWebhookReceived.booleanValue())) {
			env.putBoolean("second_payment_webhook_received", true);
			validateWebhook(requestParts);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else {
			return super.handleHttpMtls(path, req, res, session, requestParts);
		}
	}
	@Override
	protected void validatePaymentStatus(){
		Boolean paymentWebhookReceived = env.getBoolean("second_payment_webhook_received");
		if (paymentWebhookReceived != null && paymentWebhookReceived.booleanValue()) {
			callAndContinueOnFailure(EnsurePaymentStatusWasCanc.class);
		} else {
			callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class);
		}
	}
	@Override
	protected void updatePaymentConsent() {
	}
	@Override
	protected void setScheduledPaymentDateTime() {
		callAndStopOnFailure(RemovePaymentConsentDate.class);
		callAndStopOnFailure(EnsureScheduledDateIsTomorrow.class);
	}

	@Override
	protected void requestProtectedResource() {
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		ConditionSequence pixSequence = new CallPixPaymentsEndpointSequence();
		call(pixSequence);
		eventLog.endBlock();
		eventLog.startBlock(currentClientString() + "Validate response");
		validateResponse();
		eventLog.endBlock();
		createPaymentWebhook();
		waitForPaymentConsentAndPaymentWebhookResponse();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		call(getPayment());
		eventLog.endBlock();
		eventLog.startBlock(currentClientString() + "Validate response");
		validateResponse();
		validatePaymentStatus();
		eventLog.endBlock();
		env.putBoolean("second_payment_webhook_received", false);
		callAndStopOnFailure(CreatePatchPaymentsRequestFromConsentRequestV2.class);
		call(new CallPatchPixPaymentsEndpointSequenceV2());
		callAndStopOnFailure(PaymentsPatchValidatorV2.class);
		waitForPaymentWebhookResponse();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		call(getPayment());
		eventLog.endBlock();
		validateResponse();
	}


	protected void waitForPaymentWebhookResponse() {
		long delaySeconds = 5;
		eventLog.startBlock(currentClientString() + "The test will now wait for the webhook to be received");
		for (int attempts = 0; attempts < 12; attempts++) {
			eventLog.log(getId(), "Waiting for webhook to be received at with an interval of " + delaySeconds + " seconds");
			setStatus(Status.WAITING);
			try {
				Thread.sleep(delaySeconds * 1000);
			} catch (InterruptedException e) {
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}
			setStatus(Status.RUNNING);
			boolean webhookReceived = env.getBoolean("second_payment_webhook_received");
			if (webhookReceived) {
				return;
			}
		}
		throw new TestFailureException(getId(), "Webhook not received in time");
	}
}
