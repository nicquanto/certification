package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CheckConsentUrlForPayments extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String consentUrl = env.getString("config", "resource.consentUrl");
		String validatorRegex = "^(https://)(.*?)(payments/v[0-9]/consents)";
		if(!consentUrl.matches(validatorRegex)) {
			throw error("consentUrl is not valid, please ensure that url matches " + validatorRegex, args("consentUrl", consentUrl));
		}
		logSuccess("consentUrl matches expected format");
		return env;
	}
}
