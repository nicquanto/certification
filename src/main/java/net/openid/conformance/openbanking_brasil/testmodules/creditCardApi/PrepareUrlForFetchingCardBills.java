package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi;

import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingCardBills extends ResourceBuilder {

	@Override
	public Environment evaluate(Environment env) {

		String accountId = env.getString("accountId");

		setApi("credit-cards-accounts");
		setEndpoint("/accounts/" + accountId + "/bills");

		return super.evaluate(env);
	}
}
