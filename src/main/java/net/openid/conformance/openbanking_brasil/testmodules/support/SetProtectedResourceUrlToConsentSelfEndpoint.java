package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class SetProtectedResourceUrlToConsentSelfEndpoint extends AbstractCondition {

	@Override
	@PostEnvironment(strings = "protected_resource_url")
	public Environment evaluate(Environment env) {
		try {
			JsonObject body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "consent_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);

			JsonObject links = Optional.ofNullable(body.getAsJsonObject("links"))
				.orElseThrow(() -> error("No links object found."));

			String self = OIDFJSON.getString(Optional.ofNullable(links.get("self"))
				.orElseThrow(() -> error("No self link found inside links.")));

			env.putString("protected_resource_url", self);
			log(String.format("Hitting the endpoint: %s", self));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}
}

