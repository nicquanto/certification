package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddScopeToClientConfigurationFromConsentUrl;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.CallDynamicRegistrationEndpointAndVerifySuccessfulResponse;
import net.openid.conformance.testmodule.PublishTestModule;

import java.time.Instant;
@PublishTestModule(
	testName = "dcr_api_dcm-pagto-wrong-webhook_test-module",
	displayName = "dcr_api_dcm-pagto-wrong-webhook_test-module",
	summary = "Ensure that the tested institution validates the field webhook_uris matches the software_api_webhook_uris on the SSA and correctly returns an error if this is not working as expected. \n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>\n" +
		"Obtain a SSA from the Directory\n" +
		"Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration\n" +
		"Call the Registration Endpoint, but send the field \"webhook_uris\" equal to:[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>/open-banking/webhook/v1”]\n" +
		"Expect a 400 error - Validate institution returns error field as “invalid_webhook_uris” ",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.webhookWaitTime"
	}
)
public class PaymentsWebhookWithDcmWrongWebhookTestModule extends AbstractBrazilDCRPaymentsWebhook {
	@Override
	protected void configureClient() {
		env.putString("test_start_timestamp", Instant.now().toString());
		callAndStopOnFailure(AddScopeToClientConfigurationFromConsentUrl.class);
		doDcr();
		eventLog.endBlock();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {}
	@Override
	protected void addWebhookUrisToDynamicRegistrationRequest() {
		callAndStopOnFailure(AddWrongWebhookUriToclientConfigRequest.class);
	}

	@Override
	protected void setupResourceEndpoint() {
	}

	@Override
	protected void performAuthorizationFlow() {
		fireTestFinished();
	}

	@Override
	protected void callRegistrationEndpoint() {
		ConditionSequence conditionSequence = new CallDynamicRegistrationEndpointAndVerifySuccessfulResponse();
		conditionSequence.replace(EnsureHttpStatusCodeIs201.class, condition(EnsureHttpStatusCodeIs400.class));
		conditionSequence.insertAfter(EnsureHttpStatusCodeIs201.class, condition(CheckErrorFromDynamicRegistrationEndpointIsInvalidWebhook.class));
		conditionSequence.skip(CheckNoErrorFromDynamicRegistrationEndpoint.class,"Expecting Error in DCR response");
		conditionSequence.skip(ExtractDynamicRegistrationResponse.class,"Expecting Error in DCR response");
		conditionSequence.skip(VerifyClientManagementCredentials.class,"Expecting Error in DCR response");
		call(conditionSequence);

		eventLog.endBlock();
	}
}
