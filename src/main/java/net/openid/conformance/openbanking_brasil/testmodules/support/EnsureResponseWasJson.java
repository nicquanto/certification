package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonUtils;

public class EnsureResponseWasJson extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {
		String contentTypeStr = env.getString("resource_endpoint_response_full", "headers.content-type");
		if (!contentTypeStr.contains("application/json")) {
			throw error("Was expecting a JSON response. Returned: " + contentTypeStr);
		}

		logSuccess("Content type is application/json");

		Gson gson = JsonUtils.createBigDecimalAwareGson();
		String body = env.getString("resource_endpoint_response_full", "body");
		try {
			gson.fromJson(body, JsonObject.class);
		} catch (JsonSyntaxException e) {
			throw error("Could not parse JSON response due to invalid syntax", args("Response", body, "Reason", e.getMessage()));
		}

		logSuccess("Response is a valid JSON", args("Response", body));
		return env;
	}

}
