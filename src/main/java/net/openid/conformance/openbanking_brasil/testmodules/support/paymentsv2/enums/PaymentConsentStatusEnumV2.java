package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PaymentConsentStatusEnumV2 {
	AWAITING_AUTHORISATION, AUTHORISED, REJECTED, CONSUMED;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
