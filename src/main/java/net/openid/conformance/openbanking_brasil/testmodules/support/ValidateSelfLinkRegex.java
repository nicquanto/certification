package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class ValidateSelfLinkRegex extends AbstractJsonAssertingCondition {

	public static final String RESPONSE_ENV_KEY = "endpoint_response";
	public static final String PAYMENTS_CONSENTS_BASE_REGEX = "^(https://)(.*?)(/open-banking/payments/v2/consents/)%s(.*)";
	public static final String PAYMENTS_BASE_REGEX = "^(https://)(.*?)(/open-banking/payments/v2/pix/payments/)%s(.*)";
	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {

		JsonObject body;
		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.orElse(env.getObject("resource_endpoint_response"))
			);

			JsonObject links = Optional.ofNullable(body.getAsJsonObject("links"))
				.orElseThrow(() -> error("No links object found."));

			String self = OIDFJSON.getString(Optional.ofNullable(links.get("self"))
				.orElseThrow(() -> error("No self link found inside links.")));

			String id;
			String regex = "";
			if (env.getEffectiveKey(RESPONSE_ENV_KEY).contains("consent")) {
				id = OIDFJSON.getString(Optional.ofNullable(findByPath(body, "$.data.consentId"))
					.orElseThrow(() -> error("Could not find consentId inside consent response")));
				regex = String.format(PAYMENTS_CONSENTS_BASE_REGEX, id);
			} else {
				id = OIDFJSON.getString(Optional.ofNullable(findByPath(body, "$.data.paymentId"))
					.orElseThrow(() -> error("Could not find paymentId inside payment response")));
				regex = String.format(PAYMENTS_BASE_REGEX, id);
			}

			if (!self.matches(regex)) {
				throw error("Self link does not match the regex", args("regex", regex));
			}

			logSuccess("Self Link matches the regex", args("regex", regex));

		} catch (ParseException e) {
			error("Could not extract body from response");
		}
		return env;
	}
}

