package net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.creditOperations.financing.v2.FinancingContractInstallmentsResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditOperations.financing.v2.FinancingContractResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditOperations.financing.v2.FinancingGuaranteesResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditOperations.financing.v2.FinancingPaymentsResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditOperations.financing.v2.FinancingResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareAllCreditOperationsPermissionsForHappyPath;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.AddScopesForFinancingsApi;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractInstallmentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractPaymentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractWarrantiesResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsAndResourcesEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;


@PublishTestModule(
	testName = "financings_api_operational-limits_test-module_v2",
	displayName = "This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Financings API are considered correctly",
	summary = "This test will require the user to have set at least two ACTIVE resources on the Financings API. \n" +
		"This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Financings API are considered correctly.\n" +
		"\u2022 Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test have been provided\n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Credit Operations permission group\n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 Redirect User to authorize the Created Consent - Expect a successful authorization\n" +
		"\u2022 With the authorized consent id (1), call the GET Financings List API Once - Expect a 200 - Save the first returned ACTIVE resource id (R_1) and the second saved returned active resource id (R_2)\n" +
		"\u2022 With the authorized consent id (1), call the GET Financings  API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Financings Warranties API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Financings Scheduled Instalments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Financings Payments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Financings API with the saved Resource ID (R_2) 4 times - Expect a 200\n" +
		"\u2022 Repeat the exact same process done with the first tested resources (R_1) but now, execute it against the second returned Resource (R_2) \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.participants",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness"
	}
)
public class FinancingsApiOperationalLimitsTestModuleV2 extends AbstractOperationalLimitsTestModule {

	private static final String API_RESOURCE_ID = "contractId";
	private static final int NUMBER_OF_IDS_TO_FETCH = 2;

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
		call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class ,condition(ValidateConsentsEndpoints.class)));
		callAndStopOnFailure(BuildFinancingsConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(AddScopesForFinancingsApi.class);
		callAndStopOnFailure(PrepareAllCreditOperationsPermissionsForHappyPath.class);
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class);
		clientAuthType = getVariant(ClientAuthType.class);
	}

	@Override
	protected void validatePermissions() {
		env.putString("permission_type", EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType.FINANCINGS.name());
		callAndContinueOnFailure(EnsureSpecificCreditOperationsPermissionsWereReturned.class, Condition.ConditionResult.WARNING);
	}

	@Override
	protected void validateResponse() {
		//validate financings response
		call(getValidationSequence(FinancingResponseValidatorV2.class));
		eventLog.endBlock();

		runInBlock("Preparing Financings Contracts", () -> {
			env.putString("apiIdName", API_RESOURCE_ID);
			callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);

			env.putInteger("number_of_ids_to_fetch", NUMBER_OF_IDS_TO_FETCH);
			callAndStopOnFailure(FetchSpecifiedNumberOfExtractedApiIds.class);
			disableLogging();
			// Call financings GET 3 times
			for (int i = 1; i < 4; i++) {
				preCallProtectedResource(String.format("[%d] Fetching Financings Contracts", i + 1));
			}

			makeOverOlCall(String.format("Fin. Contracts"));


		});

		for (int i = 0; i < NUMBER_OF_IDS_TO_FETCH; i++) {
			int currentResourceId = i + 1;

			// Call Financings specific contract once with validation

			String financingsContractId = OIDFJSON.getString(env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds").get(i));
			runInLoggingBlock(() -> {
				env.putString(API_RESOURCE_ID, financingsContractId);
				callAndStopOnFailure(PrepareUrlForFetchingFinancingContractResource.class);

				preCallProtectedResource(String.format("Fetching Financings Contract using resource_id_%d", currentResourceId));
				validateResponse("Validate Financings Contract response", FinancingContractResponseValidatorV2.class);

			});

			// Call Financings specific contract 3 times
			for (int j = 1; j < 4; j++) {
				preCallProtectedResource(String.format("[%d] Fetching Financings Contract using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Contract [%d]", currentResourceId));


			// Call Financings warranties once with validation
			runInLoggingBlock(() -> {
				callAndStopOnFailure(PrepareUrlForFetchingFinancingContractWarrantiesResource.class);

				preCallProtectedResource(String.format("Fetch Financings Warranties using resource_id_%d", currentResourceId));
				validateResponse("Validate Financings Warranties", FinancingGuaranteesResponseValidatorV2.class);

			});

			// Call Financings warranties 3 times
			for (int j = 1; j < 4; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Financings Warranties using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Warranties[%d]", currentResourceId));


			// Call Financings Scheduled Instalments once with validation

			runInLoggingBlock(() -> {
				callAndStopOnFailure(PrepareUrlForFetchingFinancingContractInstallmentsResource.class);

				preCallProtectedResource(String.format("Fetch Financings Scheduled Instalments using resource_id_%d", currentResourceId));
				validateResponse("Validate Financings Scheduled Instalments Response", FinancingContractInstallmentsResponseValidatorV2.class);

			});

			// Call Financings Scheduled Instalments 29 times
			for (int j = 1; j < 30; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Financings Scheduled Instalments using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Sc.Inst. [%d]", currentResourceId));

			// Call Financings Payments GET once with validation

			runInLoggingBlock(() -> {
				callAndStopOnFailure(PrepareUrlForFetchingFinancingContractPaymentsResource.class);

				preCallProtectedResource(String.format("Fetch Financings Payments using resource_id_%d", currentResourceId));
				validateResponse("Validate Financings Payments Response", FinancingPaymentsResponseValidatorV2.class);

			});

			// Call Financings Payments GET 29 times
			for (int j = 1; j < 30; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Financings Payments using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Payments [%d]", currentResourceId));


			enableLogging();
		}

	}

	protected void validateResponse(String message, Class<? extends Condition> validationClass) {
		runInBlock(message, () -> call(getValidationSequence(validationClass)));
	}


	protected ConditionSequence getValidationSequence(Class<? extends Condition> validationClass) {
		return sequenceOf(
			condition(validationClass),
			condition(ValidateResponseMetaData.class)
		);
	}

}
