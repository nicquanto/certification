package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason.EnsureCancellationReasonWasCanceladoAgendamento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancelledFrom.EnsureCancelledFromWasDetentora;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasCanc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusSchdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_pixscheduling-patch-detentora_test-module_v2",
	displayName = "PATCH Payments happy path test that will revoke a scheduled payment with cancelation object set as INICIADORA",
	summary = "PATCH Payments happy path test that will require the tester to revoke the scheduled Payment on the Financial Instituion application, leading the scheduled payment to have a cancelation object set as DETENTORA \n" +
		"\u2022 Create consent with request payload with both the  schedule.single.date field set as D+1\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"\u2022 Call the POST Payments Endpoint\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD or ACCP\n" +
		"\u2022 Expect Payment Scheduled to be reached (SCHD) - Validate Response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is SCHD for up to 10 minutes - Wait for the payment to reach a CANC state\n" +
		"\u2022 Expect 200 - Make Sure cancellation.cancelledFrom is set as \"DETENTORA\". Make Sure cancellation.reason is set as \"CANCELADO_AGENDAMENTO\" - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiPixSchedulingPatchDetentoraTestModuleV2 extends AbstractPixSchedulingTestModuleV2 {

	private boolean isPostPolling = false;

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
	}

	@Override
	protected void executePostPollingSteps() {
		isPostPolling = true;
		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(20)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		callAndStopOnFailure(EnsurePaymentStatusWasCanc.class);
		callAndStopOnFailure(EnsureCancelledFromWasDetentora.class);
		callAndStopOnFailure(EnsureCancellationReasonWasCanceladoAgendamento.class);
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		if (isPostPolling) {
			return CheckPaymentPollStatusSchdV2.class;
		}
		return super.getPaymentPollStatusCondition();
	}

}
