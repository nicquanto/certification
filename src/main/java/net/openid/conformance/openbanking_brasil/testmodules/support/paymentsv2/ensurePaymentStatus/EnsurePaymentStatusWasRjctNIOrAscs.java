package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentRejectionReasonEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentStatusEnumV2;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public class EnsurePaymentStatusWasRjctNIOrAscs extends AbstractEnsurePaymentStatusWasX {

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		List<String> expectedStatuses = getExpectedStatuses();
		if (!checkPaymentStatus(env)) {
			throw error("Payment status is not what was expected", args("Expected", expectedStatuses, "Actual", getStatus()));
		}
		try {
			JsonObject responseBody = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response")).getAsJsonObject();

			JsonObject data = Optional.ofNullable(responseBody.getAsJsonObject("data"))
				.orElseThrow(() -> error("Could not extract data from body", args("body", responseBody)));

			if(getStatus().equals(PaymentStatusEnumV2.RJCT.toString())){

				JsonObject rejectionReason = Optional.ofNullable(data.getAsJsonObject("rejectionReason"))
					.orElseThrow(() -> error("Could not extract rejectionReason from data", args("data", data)));

				String rejectionCode = OIDFJSON.getString(Optional.ofNullable(rejectionReason.get("code"))
					.orElseThrow(() -> error("Could not extract code from rejectionReason", args("rejectionReason", rejectionReason))));

				if(!rejectionCode.equals(PaymentRejectionReasonEnumV2.NAO_INFORMADO.toString())){
					throw error("Payment rejection code is not what was expected",
						args("Expected", PaymentRejectionReasonEnumV2.NAO_INFORMADO.toString(), "Actual", rejectionCode));
				}
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		logSuccess("Payment status is what was expected", args("Expected", expectedStatuses, "Status", getStatus()));

		return env;
	}

	@Override
	protected List<String> getExpectedStatuses() {
		return List.of(PaymentStatusEnumV2.RJCT.toString(),PaymentStatusEnumV2.ACSC.toString());
	}
}
