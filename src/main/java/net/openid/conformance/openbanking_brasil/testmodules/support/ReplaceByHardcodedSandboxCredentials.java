package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ReplaceByHardcodedSandboxCredentials extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonObject clientObject = (JsonObject) env.getElementFromObject("config", "client");

		if (clientObject == null) {
			throw error("Client has not been specified");
		}

		String ca = extractStringResource("dcr_sandox_test_hardcoded_creds/ca.pem");
		String key = extractStringResource("dcr_sandox_test_hardcoded_creds/key.key");
		String cert = extractStringResource("dcr_sandox_test_hardcoded_creds/cert.pem");

		String jwks = extractStringResource("dcr_sandox_test_hardcoded_creds/JWKS.json");
		String directoryConfig = extractStringResource("dcr_sandox_test_hardcoded_creds/directory.json");



		JsonObject jwksObject = JsonParser.parseString(jwks).getAsJsonObject();
		clientObject.add("jwks", jwksObject);

		JsonObject directoryObject = JsonParser.parseString(directoryConfig).getAsJsonObject();


		JsonObject mtls = new JsonObject();
		mtls.addProperty("cert", cert);
		mtls.addProperty("key", key);
		mtls.addProperty("ca", ca);

		env.putObject("config", "mtls", mtls);
		env.putObject("config", "directory", directoryObject);

		logSuccess("Successfully replaced client JWKS by Sandbox Credentials one", jwksObject);
		logSuccess("Successfully replaced MTLS by Sandbox Credentials one", mtls);
		logSuccess("Successfully replaced directory by Sandbox Credentials one", directoryObject);
		return env;
	}

	private String extractStringResource(String path) {
		try {
			return IOUtils.resourceToString(path, StandardCharsets.UTF_8, Thread.currentThread().getContextClassLoader());
		} catch (IOException e) {
			throw error(String.format("Could not load %s resource", path), e);
		}
	}
}
