package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJWTAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddNoAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenNoAcceptField;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.*;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.sequence.client.RefreshTokenRequestExpectingErrorSteps;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_contenttype-jwt-refreshtoken_test-module_v2",
	displayName = "Payments API v2 test module for refresh token rotation and accept header",
	summary = "This test is a PIX Payments current date test that aims to test both the refresh token rotation expected behavior and also how the instituion must handles the accept header when it's not sent as application/jwt\n" +
		"\u2022 Call POST token endpoint using client-credentials grant, however request the scopes=\"payments\" and \"openid\"\n" +
		"\u2022 Expect 201 - Created - Make sure token has been issued with only payments scope, ignoring the openid additional scope, and returning the scope optional object in line with https://www.rfc-editor.org/rfc/rfc6749#section-3.3\n" +
		"\u2022 Create consent with valid e-mail payload, localInstrument to be set to DICT\n" +
		"\u2022 Call the POST Consents endpoints, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects 201 - Make sure a JWT is returned - Validate response \n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"\u2022 Call the POST token endpoint with refresh token grant\n" +
		"\u2022 Expect a new access token to be generated - Make sure the refresh token hasn’t been rotated\n" +
		"\u2022 Call the POST Payments Endpoint with the token obtained from the refresh token grant, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects 201 - Make sure a JWT is returned - Validate response \n" +
		"\u2022 Call the GET Payments endpoint with token issued with client_credentials grant, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects either a 200 - make sure response is JWT\n" +
		"\u2022 Call the GET Payments endpoint with token issued with client_credentials grant, set header \"accept\": \"application/json\"\n" +
		"\u2022 Expects either a 200 or a 406 response - If 200 make sure response is JWT\n" +
		"\u2022 Call the GET Consents endpoint with token issued with client_credentials grant, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects either a 200 - make sure response is JWT - Make sure that the self link returned is exactly equal to the requested URI\n" +
		"\u2022 Call the GET Consents endpoint with token issued with client_credentials grant, set header \"accept\": \"application/json\"\n" +
		"\u2022 Expects either a 200 or a 406 response - If 200 make sure response is JWT\n" +
		"\u2022 Call the POST token endpoint with refresh token grant\n" +
		"\u2022 Expect a 400 or 401 - As Defined on the FAPI-Brazil - shall only issue access_tokens on presentation of a refresh_token when the consent resource the refresh token is bound to is active and with \"AUTHORIZED\" status",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiContentTypeJwtRefreshTokenTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(EnsureProxyPresentInConfig.class);
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(SetScopeInClientConfigurationToOpenId.class);
		super.validateClientConfiguration();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.replace(SetPaymentsScopeOnTokenEndpointRequest.class, condition(AddScopeToTokenEndpointRequest.class))
			.insertAfter(CheckIfTokenEndpointResponseError.class, condition(EnsureTokenEndpointResponseScopeWasPayments.class).requirements("RFC6749-3.3").dontStopOnFailure())
			.replace(FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class, condition(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenNoAcceptField.class));
	}

	@Override
	protected void requestProtectedResource() {
		if (!validationStarted) {
			validationStarted = true;
			call(new RefreshTokenRequestSteps(false, addTokenEndpointClientAuthentication)
				.skip(EnsureAccessTokenValuesAreDifferent.class, "No need to make this check")
				.insertAfter(CompareIdTokenClaims.class, condition(ValidateRefreshTokenNotRotated.class)));

			eventLog.startBlock("Calling POST Payments endpoint - */* accept header - expects 201 with JWT response");
			ConditionSequence pixSequence = getPixPaymentSequence()
				.replace(SetApplicationJwtAcceptHeaderForResourceEndpointRequest.class, condition(AddNoAcceptHeaderRequest.class));
			call(pixSequence);
			eventLog.startBlock(currentClientString() + "Validate response");
			env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
			call(getPaymentValidationSequence());

			eventLog.startBlock("Calling GET Payments endpoint - */* accept header - expects 200 with JWT response");
			ConditionSequence getPaymentNoAcceptSequence = new CallGetPaymentEndpointSequence()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddNoAcceptHeaderRequest.class));
			call(getPaymentNoAcceptSequence);

			eventLog.startBlock("Calling GET Payments endpoint - application/json accept header - expects 200 with JWT response or 406");
			ConditionSequence getPaymentJsonAcceptSequence = new CallGetPaymentEndpointSequenceErrorAgnostic()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddJsonAcceptHeaderRequest.class));
			call(getPaymentJsonAcceptSequence);

			eventLog.startBlock("Calling GET Consents endpoint - */* accept header - expects 200 with JWT response");
			ConditionSequence getConsentNoAcceptSequence = new CallGetPaymentConsentEndpointSequence()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddNoAcceptHeaderRequest.class));
			call(getConsentNoAcceptSequence);

			eventLog.startBlock("Calling GET Consents endpoint - application/json accept header - expects 200 with JWT response or 406");
			ConditionSequence getConsentJsonAcceptSequence = new CallGetPaymentConsentEndpointSequenceErrorAgnostic()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddJsonAcceptHeaderRequest.class));
			call(getConsentJsonAcceptSequence);

			eventLog.startBlock("Refresh Token Request - expects 400 or 401");
			call(new RefreshTokenRequestExpectingErrorSteps(false, addTokenEndpointClientAuthentication)
				.replace(CheckTokenEndpointHttpStatus400.class, condition(CheckTokenEndpointHttpStatus400or401.class))
				.skip(CheckErrorFromTokenEndpointResponseErrorInvalidGrant.class, "No need to check this"));

			eventLog.endBlock();
		}
	}

	@Override
	protected void requestAuthorizationCode() {
		super.requestAuthorizationCode();
		callAndStopOnFailure(ExtractRefreshTokenFromTokenResponse.class);
	}
}
