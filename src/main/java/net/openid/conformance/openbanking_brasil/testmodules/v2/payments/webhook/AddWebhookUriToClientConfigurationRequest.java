package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddWebhookUriToClientConfigurationRequest extends AbstractCondition {
	@Override
	@PreEnvironment(required = "registration_client_endpoint_request_body")
	@PostEnvironment(required = "registration_client_endpoint_request_body")
	public Environment evaluate(Environment env) {
		JsonObject dynamicRegistrationRequest = env.getObject("dynamic_registration_request");
		String webhookUri = env.getString("software_api_webhook_uri");
		if (webhookUri == null) {
			throw error("Unable to find webhook uri to add to dynamic registration request");
		}
		JsonArray webhookUris = new JsonArray();
		webhookUris.add(webhookUri);
		dynamicRegistrationRequest.add("webhook_uris", webhookUris);
		log("Added webhook_uris to dynamic registration request", args("dynamic_registration_request", dynamicRegistrationRequest));
		return env;
	}
}
