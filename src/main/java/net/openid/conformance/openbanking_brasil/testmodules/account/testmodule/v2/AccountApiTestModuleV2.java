package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.account.v2.*;
import net.openid.conformance.openbanking_brasil.testmodules.account.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.BuildAccountsConfigResourceUrlFromConsentUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.ValidateMetaOnlyRequestDateTimeDeprecated;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsAndResourcesEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractPhase2V2TestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@PublishTestModule(
	testName = "accounts_api_core_test-module_v2",
	displayName = "Validate structure of all accounts API resources V2",
	summary = "Validates the structure of all account API resources V2\n" +
		"\u2022 Cria Consentimento apenas com as Permissions necessárias para acessar os recursos da API de Accounts\n" +
		"\u2022 Valida todos os campos enviados na API de consentimento\n" +
		"\u2022 Chama todos os recursos da API de Accounts V2\n" +
		"\u2022 Valida todos os campos dos recursos da API de Accounts",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.participants",
		"directory.discoveryUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class AccountApiTestModuleV2 extends AbstractPhase2V2TestModule {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
		call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class ,condition(ValidateConsentsEndpoints.class)));
		callAndStopOnFailure(BuildAccountsConfigResourceUrlFromConsentUrl.class);
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromBookingDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toBookingDate", currentDate.format(FORMATTER));
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(AddAccountScope.class);
		callAndStopOnFailure(PrepareAllAccountRelatedConsentsForHappyPathTest.class);
	}

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(AccountListValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(AccountSelector.class);
		callAndStopOnFailure(PrepareUrlForFetchingAccountResource.class);
		preCallProtectedResource("Fetch Account V2");
		callAndContinueOnFailure(AccountIdentificationResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(PrepareUrlForFetchingAccountBalances.class);
		preCallProtectedResource("Fetch Account balance V2");
		callAndContinueOnFailure(AccountBalancesResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(PrepareUrlForFetchingAccountTransactions.class);
		callAndStopOnFailure(LogKnownIssue.class, "BCLOG-F02-172");
		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);
		preCallProtectedResource("Fetch Account transactions V2");
		callAndContinueOnFailure(AccountTransactionsValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateMetaOnlyRequestDateTimeDeprecated.class, Condition.ConditionResult.FAILURE);

		call(new ValidateSelfEndpoint()
			.replace(ValidateResponseMetaData.class, condition(ValidateMetaOnlyRequestDateTimeDeprecated.class)
			));

		call(condition(VerifyAdditionalFieldsWhenMetaOnlyRequestDateTime.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.WARNING));

		callAndStopOnFailure(PrepareUrlForFetchingAccountLimits.class);
		preCallProtectedResource("Fetch Account limits V2");
		callAndContinueOnFailure(AccountLimitsValidatorV2.class, Condition.ConditionResult.FAILURE);
	}

}
