package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpHeaders;

public class FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders extends FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod {

    @Override
    protected HttpHeaders getHeaders(Environment env) {
        HttpHeaders headers = super.getHeaders(env);
        JsonObject requestHeaders = env.getObject("resource_endpoint_request_headers");
        headers = headersFromJson(requestHeaders, headers);
        return headers;
    }
}
