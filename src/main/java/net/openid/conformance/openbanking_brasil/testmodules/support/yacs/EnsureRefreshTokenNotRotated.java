package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EnsureRefreshTokenNotRotated extends AbstractCondition {
	@Override
	@PreEnvironment(strings = {"refresh_token", "first_refresh_token"})
	public Environment evaluate(Environment env) {
		String refreshToken = env.getString("refresh_token");
		String firstRefreshToken = env.getString("first_refresh_token");
		if(refreshToken == null || firstRefreshToken == null) {
			throw error("Unable to find refresh token");
		}
		if (!refreshToken.equals(firstRefreshToken)){
			throw error("Refresh token should not be rotated");
		}
		logSuccess("Validated refresh token not rotated successfully");
		return env;
	}
}
