package net.openid.conformance.openbanking_brasil.testmodules.opendata;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.opendata.capitalizationBonds.CapitalizationBondsValidator;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.opendata.utils.PrepareToGetOpenDataApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "opendata-capitalization-bonds_test-plan",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.CAPITALIZATION_BONDS_API_PHASE_4A_TEST_PLAN,
	summary = PlanNames.CAPITALIZATION_BONDS_API_PHASE_4A_TEST_PLAN
)
public class CapitalizationBondsTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					CapitalizationBondsApiTestModule.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}

	@PublishTestModule(
		testName = "opendata-capitalization-bonds_api_structural_test-module",
		displayName = "Validate structure of Opendata - Capitalization Bonds API Api resources",
		summary = "Validate structure of Opendata - Capitalization Bonds Api resources",
		profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4)
	public static class CapitalizationBondsApiTestModule extends AbstractNoAuthFunctionalTestModule {

		@Override
		protected void runTests() {
			runInBlock("Validate Opendata - Capitalization Bonds response", () -> {
				callAndStopOnFailure(PrepareToGetOpenDataApi.class);
				preCallResource();
				callAndContinueOnFailure(DoNotStopOnFailure.class);
				callAndContinueOnFailure(CapitalizationBondsValidator.class, Condition.ConditionResult.FAILURE);
			});
		}
	}
}
