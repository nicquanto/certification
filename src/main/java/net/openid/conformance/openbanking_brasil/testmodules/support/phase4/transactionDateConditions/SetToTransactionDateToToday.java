package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class SetToTransactionDateToToday extends AbstractCondition {

    private final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Override
    @PostEnvironment(strings = "toTransactionDate")
    public Environment evaluate(Environment env) {
        LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
        env.putString("toTransactionDate", currentDate.format(FORMATTER));
        logSuccess("toTransactionDate has been set to today",
            args("toTransactionDate", env.getString("toTransactionDate")));
        return env;
    }
}
