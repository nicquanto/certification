package net.openid.conformance.openbanking_brasil.testmodules.v2.resources;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.consent.v2.ConsentDetailsIdentifiedByConsentIdValidatorV2;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.ResourcesResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsureConsentStatusIsAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreateConsentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.CustomerDataResources404;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "resources_api_core_test-module_v2",
	displayName = "Validate structure of all resources API V2 resources",
	summary = "Validates the structure of all resources API V2 resources\n" +
		"\u2022 Call the POST Consents API with will all of the existing permissions. \n" +
		"\u2022 Expect a 201 - Checks all of the fields sent on the consent API are specification compliant  \n" +
		"\u2022 Call the GET Consents API with a client_credentials issued token\n" +
		"\u2022 Expect a 200 - Make sure consent is on “AWAITING_AUTHORISATION” - Validate response payload\n" +
		"\u2022 Redirect User to Authorize the Consent and obtain valid access token\n" +
		"\u2022 Call the GET Consents API with a client_credentials issued token\n" +
		"\u2022 Expect a 200 - Make sure consent is on “AUTHORISED” - Validate response payload\n" +
		"\u2022 Calls the GET resources API V2 with the Polling Resources API sequence\n" +
		"\u2022 Expects a 200 - Validate that the all returned fields are aligned with the specification\n" +
		"\u2022 Call the DELETE Consents API\n" +
		"\u2022 Expects a 204",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.participants",
		"directory.discoveryUrl",
		"resource.brazilCnpj",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ResourcesApiTestModuleV2 extends AbstractOBBrasilPhase2V2TestModuleOptionalErrors {

	@Override
	protected void configureClient(){
		call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
		call(new ValidateRegisteredEndpoints());
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddProductTypeToPhase2V2Config.class);
		callAndStopOnFailure(IgnoreResponseError.class);
		callAndStopOnFailure(PrepareAllResourceRelatedConsentsForHappyPathTest.class);
		callAndStopOnFailure(AddResourcesScope.class);
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		runInBlock("Checking the created consent - Expecting AWAITING_AUTHORISATION status", () -> {
			fetchConsent();
			callAndContinueOnFailure(EnsureConsentStatusIsAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(ConsentDetailsIdentifiedByConsentIdValidatorV2.class);
		});
	}

	@Override
	protected void performPostAuthorizationFlow() {
		runInBlock("Checking the created consent - Expecting AUTHORISED status", () -> {
			fetchConsent();
			callAndStopOnFailure(EnsureConsentWasAuthorised.class);
			callAndStopOnFailure(ConsentDetailsIdentifiedByConsentIdValidatorV2.class);
		});
		super.performPostAuthorizationFlow();
	}

	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
		super.requestProtectedResource();
	}

	@Override
	protected void validateResponse() {
		ResourceApiV2PollingSteps pollingSteps = new ResourceApiV2PollingSteps(env, getId(),
			eventLog,testInfo, getTestExecutionManager());
		runInBlock("Polling Resources API", () -> call(pollingSteps));

		String responseError = env.getString("resource_endpoint_error_code");
		if (Strings.isNullOrEmpty(responseError)) {
			String logMessage = "Validate resources V2 api request";
			runInBlock(logMessage, () -> {
				callAndStopOnFailure(ResourcesResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
				callAndStopOnFailure(EnsureResponseHasLinks.class);
				callAndContinueOnFailure(ValidateResponseMetaDataAndLinks.class, Condition.ConditionResult.FAILURE);
				call(sequence(ValidateSelfEndpoint.class));
			});
		} else {
			callAndContinueOnFailure(CreateConsentErrorValidatorV2.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResponseCodeWas404.class);
			callAndStopOnFailure(CustomerDataResources404.class);
			callAndContinueOnFailure(ChuckWarning.class, Condition.ConditionResult.WARNING);
		}
	}

	private void fetchConsent(){
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndStopOnFailure(TransformConsentRequestForProtectedResource.class);
		preCallProtectedResource();
	}

	@Override
	protected void logFinalEnv() {
		//Not needed here
	}
}
