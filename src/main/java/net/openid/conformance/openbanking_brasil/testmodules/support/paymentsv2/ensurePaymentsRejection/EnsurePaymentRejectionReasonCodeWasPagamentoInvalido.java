package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentRejectionReasonEnumV2;

import java.util.List;

public class EnsurePaymentRejectionReasonCodeWasPagamentoInvalido extends AbstractEnsurePaymentRejectionReasonCodeWasX {

	@Override
	protected List<String> getExpectedRejectionCodes() {
		return List.of(PaymentRejectionReasonEnumV2.DETALHE_PAGAMENTO_INVALIDO.toString());
	}
}
