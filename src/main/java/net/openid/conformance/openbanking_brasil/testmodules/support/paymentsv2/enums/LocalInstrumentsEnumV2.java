package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum LocalInstrumentsEnumV2 {

	MANU, DICT, INIC, QRDN, QRES;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}


}
