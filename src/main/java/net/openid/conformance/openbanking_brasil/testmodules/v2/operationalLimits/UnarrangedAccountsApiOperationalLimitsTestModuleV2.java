package net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.creditOperations.advances.v2.*;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.ExtractSpecifiedResourceApiAccountIdsFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareAllCreditOperationsPermissionsForHappyPath;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContractGuarantees;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContractInstallments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContractPayments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContracts;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsAndResourcesEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;


@PublishTestModule(
	testName = "unarranged-accounts-overdraft_api_operational-limits_test-module_v2",
	displayName = "This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Unarragend Accounts Overdrafts API are considered correctly\n",
	summary = "This test will require the user to have set at least two ACTIVE resources on the Unarragend Accounts Overdrafts  API. \n" +
		"This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Unarragend Accounts Overdrafts  API are considered correctly.\n" +
		"\u2022 Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test have been provided\n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Credit Operations permission group \n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 Redirect User to authorize the Created Consent - Expect a successful authorization\n" +
		"\u2022 With the authorized consent id (1), call the GET Unarranged Accounts List API Once - Expect a 200 - Save the first returned ACTIVE resource id (R_1) and the second saved returned active resource id (R_2)\n" +
		"\u2022 With the authorized consent id (1), call the GET Unarranged Accounts  API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Unarranged Accounts Warranties API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Unarranged Accounts Scheduled Instalments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Unarranged Accounts Payments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Unarranged Accounts API with the saved Resource ID (R_2) 4 times - Expect a 200\n" +
		"\u2022 Repeat the exact same process done with the first tested resources (R_1) but now, execute it against the second returned Resource (R_2) \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.participants",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness",
		"resource.first_unarranged_account_overdraft_account_id",
		"resource.second_unarranged_account_overdraft_account_id"
	}
)
public class UnarrangedAccountsApiOperationalLimitsTestModuleV2 extends AbstractOperationalLimitsTestModule {

	private static final String API_RESOURCE_ID = "contractId";
	private static final int NUMBER_OF_IDS_TO_FETCH = 2;


	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
		call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class ,condition(ValidateConsentsEndpoints.class)));
		callAndStopOnFailure(BuildCreditOperationsAdvancesConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		env.putString("api_type", EnumResourcesType.UNARRANGED_ACCOUNT_OVERDRAFT.name());
		callAndContinueOnFailure(ExtractSpecifiedResourceApiAccountIdsFromConfig.class, Condition.ConditionResult.INFO);
		callAndStopOnFailure(AddUnarrangedOverdraftScope.class);
		callAndStopOnFailure(PrepareAllCreditOperationsPermissionsForHappyPath.class);
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class);
		clientAuthType = getVariant(ClientAuthType.class);
	}

	@Override
	protected void validatePermissions() {
		env.putString("permission_type", EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType.UNARRANGED_ACCOUNTS_OVERDRAFT.name());
		callAndContinueOnFailure(EnsureSpecificCreditOperationsPermissionsWereReturned.class, Condition.ConditionResult.WARNING);
	}

	@Override
	protected void validateResponse() {
		//validate Unarranged Accounts response
		call(getValidationSequence(AdvancesResponseValidatorV2.class));
		eventLog.endBlock();

		JsonArray unarrangedAccountsContractIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");
		if (unarrangedAccountsContractIds.isEmpty()) {
			eventLog.startBlock("Preparing Unarranged Accounts Contracts");
			env.putString("apiIdName", API_RESOURCE_ID);
			callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);

			JsonArray extractedApiIds = env.getObject("extracted_api_ids").getAsJsonArray("extractedApiIds");

			if (extractedApiIds.size() >= NUMBER_OF_IDS_TO_FETCH) {
				env.putInteger("number_of_ids_to_fetch", NUMBER_OF_IDS_TO_FETCH);
			} else {
				env.putInteger("number_of_ids_to_fetch", 1);
			}

			callAndStopOnFailure(FetchSpecifiedNumberOfExtractedApiIds.class);
			unarrangedAccountsContractIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");
		}

		disableLogging();
		// Call Unarranged Accounts GET 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Fetching Unarranged Accounts Contracts", i + 1));
		}

		makeOverOlCall("Contracts");


		eventLog.endBlock();

		for (int i = 0; i < NUMBER_OF_IDS_TO_FETCH; i++) {
			int currentResourceId = i + 1;

			// If during the second resource test part, fetchedApiIds size is less than numberOfIdsToFetch it means that the second account ID was not provided by the client or was not fetched.
			// In that case we assume that the multiple accounts for the same users feature is not supported, therefore we skip the part of the test with the second resource ID.
			if(currentResourceId == 2 && unarrangedAccountsContractIds.size() < NUMBER_OF_IDS_TO_FETCH){
				eventLog.log(getName(), "Could not find second unarranged overdraft account ID. Skipping part of the test with the second resource ID");
				continue;
			}


			// Call Unarranged Accounts specific contract once with validation
			String unarrangedAccountsContractId = OIDFJSON.getString(unarrangedAccountsContractIds.get(i));
			runInLoggingBlock(() -> {
				env.putString(API_RESOURCE_ID, unarrangedAccountsContractId);
				callAndStopOnFailure(PrepareUrlForFetchingCreditAdvanceContracts.class);

				preCallProtectedResource(String.format("Fetching Unarranged Accounts Contract using resource_id_%d", currentResourceId));
				validateResponse("Validate Unarranged Accounts Contract Account response", AdvancesContractResponseValidatorV2.class);

			});

			// Call Unarranged Accounts specific contract 3 times
			for (int j = 1; j < 4; j++) {
				preCallProtectedResource(String.format("[%d] Fetching Unarranged Accounts Contract using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Contract [%d]", currentResourceId));


			// Call Unarranged Accounts warranties once with validation
			runInLoggingBlock(() -> {
				callAndStopOnFailure(PrepareUrlForFetchingCreditAdvanceContractGuarantees.class);

				preCallProtectedResource(String.format("Fetch Unarranged Accounts Warranties using resource_id_%d", currentResourceId));
				validateResponse("Validate Unarranged Accounts Warranties", AdvancesGuaranteesResponseValidatorV2.class);

			});

			// Call Unarranged Accounts warranties 3 times
			for (int j = 1; j < 4; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Unarranged Accounts Warranties using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Warranties[%d]", currentResourceId));


			// Call Unarranged Accounts Scheduled Instalments once with validation

			runInLoggingBlock(() -> {
				callAndStopOnFailure(PrepareUrlForFetchingCreditAdvanceContractInstallments.class);

				preCallProtectedResource(String.format("Fetch Unarranged Accounts Scheduled Instalments using resource_id_%d", currentResourceId));
				validateResponse("Validate Unarranged Accounts Scheduled Instalments Response", AdvancesContractInstallmentsResponseValidatorV2.class);

			});

			// Call Unarranged Accounts Scheduled Instalments 29 times
			for (int j = 1; j < 30; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Unarranged Accounts Scheduled Instalments using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Sc.Inst. [%d]", currentResourceId));

			// Call Unarranged Accounts Payments GET once with validation

			runInLoggingBlock(() -> {
				callAndStopOnFailure(PrepareUrlForFetchingCreditAdvanceContractPayments.class);

				preCallProtectedResource(String.format("Fetch Unarranged Accounts Payments using resource_id_%d", currentResourceId));
				validateResponse("Validate Unarranged Accounts Payments Response", AdvancesPaymentsResponseValidatorV2.class);

			});

			// Call Unarranged Accounts Payments GET 29 times
			for (int j = 1; j < 30; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Unarranged Accounts Payments using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Payments [%d]", currentResourceId));

			enableLogging();
		}

	}


	protected void validateResponse(String message, Class<? extends Condition> validationClass) {
		runInBlock(message, () -> call(getValidationSequence(validationClass)));
	}


	protected ConditionSequence getValidationSequence(Class<? extends Condition> validationClass) {
		return sequenceOf(
			condition(validationClass),
			condition(ValidateResponseMetaDataAndLinks.class)
		);
	}

}
