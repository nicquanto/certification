package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.CreatePaymentErrorEnumV2;

import java.util.List;

/**
 * EnsureErrorResponseCodeField for DETALHE_PAGAMENTO_INVALIDO or PAGAMENTO_RECUSADO_DETENTORA
 */
public class EnsureErrorResponseCodeFieldWasDPIorPRD extends AbstractEnsureErrorResponseCodeFieldWas {
	@Override
	protected List<String> getExpectedCodes() {
		return List.of(CreatePaymentErrorEnumV2.DETALHE_PAGAMENTO_INVALIDO.toString(),
				       CreatePaymentErrorEnumV2.PAGAMENTO_RECUSADO_DETENTORA.toString());
	}
}
