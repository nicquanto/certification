package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;
import java.util.Optional;

public class CreatePatchPaymentsRequestFromConsentRequestV2 extends AbstractCondition {


	@Override
	public Environment evaluate(Environment env) {

		JsonObject resource = Optional.ofNullable(env.getObject("resource"))
			.orElseGet(() -> Optional.ofNullable(env.getElementFromObject("config", "resource"))
				.flatMap(res -> Optional.ofNullable(res.getAsJsonObject()))
				.orElseThrow(() -> error("Could not find resource object")));


		String identification = OIDFJSON.getString(Optional.ofNullable(resource.getAsJsonObject("brazilPaymentConsent"))
			.flatMap(consent -> Optional.ofNullable(consent.getAsJsonObject("data")))
			.flatMap(data -> Optional.ofNullable(data.getAsJsonObject("loggedUser")))
			.flatMap(loggedUser -> Optional.ofNullable(loggedUser.getAsJsonObject("document")))
			.flatMap(document -> Optional.ofNullable(document.get("identification")))
			.orElseThrow(() -> error("Could not extract brazilPaymentConsent.data.loggedUser.document.identification from the resource",
				args("resource", resource))));

		JsonObject patchRequest = new JsonObjectBuilder()
			.addFields("data", Map.of("status", "CANC"))
			.addFields("data.cancellation.cancelledBy.document", Map.of("identification", identification, "rel", "CPF"))
			.build();

		resource.add("brazilPatchPixPayment", patchRequest);
		logSuccess("Added brazilPatchPixPayment to the resource", args("brazilPatchPixPayment", patchRequest));

		return env;
	}
}
