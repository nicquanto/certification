package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforcePresenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveTransactionIdentification;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProxyToRealPhoneNumber;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectRealCreditorAccountPhoneToPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectRealCreditorAccountToPaymentPhone;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_phone-number-proxy_test-module_v2",
	displayName = "Ensure paymets flow is sucessful when a valid phone number proxy is sent",
	summary = "Ensure paymets flow is sucessful when a valid phone number proxy is sent\n" +
		"\u2022 Calls POST Consents Endpoint with valid phone number as proxy, using DICT as the localInstrument\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" \n" +
		"\u2022 Calls the POST Payments with valid payload\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expectes Definitive  state (ACSC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiPhoneNumberProxyTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EnforcePresenceOfDebtorAccount.class);
		// Setting consent to DICT / proxy to real phone number
		eventLog.startBlock("Setting payment consent payload to use real phone number + DICT");
		callAndContinueOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndContinueOnFailure(RemoveQRCodeFromConfig.class);
		callAndContinueOnFailure(InjectRealCreditorAccountPhoneToPaymentConsent.class);
		callAndContinueOnFailure(SetProxyToRealPhoneNumber.class);

		// Setting payment to DICT / proxy to real phone number
		eventLog.startBlock("Setting payment payload to use real phone number + DICT");
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndContinueOnFailure(InjectRealCreditorAccountToPaymentPhone.class);
		callAndStopOnFailure(RemoveTransactionIdentification.class);
	}

}
