package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractBuildConfigResourceUrlFromConsentUrl extends AbstractCondition {

    protected abstract String apiReplacement();

    protected abstract String endpointReplacement();

    protected final String VALIDATOR_REGEX = "^(https://)(.*?)(consents/v[0-9]/consents)";

    @Override
    @PreEnvironment(required = "config")
    public Environment evaluate(Environment env) {
        String consentUrl = env.getString("config","resource.consentUrl");

        if(!consentUrl.matches(VALIDATOR_REGEX)) {
            throw error("consentUrl is not valid, please ensure that url matches " + VALIDATOR_REGEX, args("consentUrl", consentUrl));
        }

        String resourceUrl = consentUrl.replaceFirst("consents", apiReplacement()).replaceFirst("consents", endpointReplacement());
        env.putString("config", "resource.resourceUrl", resourceUrl);

        logSuccess(String.format("resourceUrl for %s set up", apiReplacement()), args("resourceUrl", resourceUrl));
        return env;
    }
}
