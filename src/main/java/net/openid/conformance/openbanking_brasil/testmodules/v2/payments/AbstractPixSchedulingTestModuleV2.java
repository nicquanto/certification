package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CallPatchPixPaymentsEndpointSequenceV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs.EnsureScheduledDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentsPatchValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPixSchedulingTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	protected Class<? extends Condition> getPaymentScheduleDate() {
		return EnsureScheduledDateIsTomorrow.class;
	}

	@Override
	protected void updatePaymentConsent() {
		// date is not needed in this test since the payment is scheduled
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(getPaymentScheduleDate());
		callAndStopOnFailure(RemovePaymentConsentDate.class);
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		configureDictInfo();
	}

	@Override
	protected void validateFinalState() {
		callAndStopOnFailure(EnsurePaymentStatusWasSchd.class);
	}


	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrAccpV2.class;
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();
		executePostPollingSteps();
	}

	protected ConditionSequence getCallPatchConditionSequence() {
		return new CallPatchPixPaymentsEndpointSequenceV2();
	}

	protected ConditionSequence getPatchValidationSequence() {
		return sequenceOf(
			condition(PaymentsPatchValidatorV2.class),
			condition(ValidateMetaOnlyRequestDateTime.class)
		);
	}

	protected ConditionSequence getCreatePatchRequestSequence(){
		return sequenceOf(
			condition(CreatePatchPaymentsRequestFromConsentRequestV2.class)
		);
	}

	protected void executePostPollingSteps() {
		eventLog.startBlock(currentClientString() + "Call PATCH payments endpoint");
		call(getCreatePatchRequestSequence());
		call(getCallPatchConditionSequence());
		eventLog.endBlock();
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
		eventLog.startBlock(currentClientString() + "Validate patched payment");
		call(getPatchValidationSequence());
		eventLog.endBlock();
	}



}
