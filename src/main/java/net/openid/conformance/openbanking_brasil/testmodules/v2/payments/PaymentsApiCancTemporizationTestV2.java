package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallGetPaymentEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforceAbsenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CallPatchPixPaymentsEndpointSequenceV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.EnsurePaymentsTemporizationCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.InsertTemporizationTestValuesIntoConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason.EnsureCancellationReasonWasCanceladoPendencia;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancelledFrom.EnsureCancelledFromWasIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasCanc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasPndg;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRcvdOrPndg;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentsPatchValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_canc-temporization-conditional_test-module_v2",
	displayName = "Payments API v2 test module for checking the optional flow for temporization",
	summary = "This test module is optional and should be executed only by institutions that wish to test the different flows for a payment that will fall into a PNDG state, meaning it was halted for additional verification. The test will only be executed if the \"Payment consent - Logged User CPF - Temporization\" is provided on the test configuration.\n" +
		"The test modules validates is a CANC state is reached when the payment is deleted while at PDNG status\n" +
		"\n" +
		"\u2022 Validates if the user has provided the field “Payment consent - Logged User CPF - Temporization”. If field is not provided the whole test scenario must be SKIPPED\n" +
		"\u2022 Set the payload to be customer business if Payment consent - Business Entity CNPJ - Temporization was provided. If left blank, it should be customer personal\n" +
		"\u2022 Creates consent request_body with valid email proxy specific for this test (cliente-a00002@pix.bcb.gov.br), localInstrument as DICT, Payment consent - Logged User CPF - Temporization” on the loggedUser identification field, Value as 12345,67, and IBGE Town Code as 1400704 (Uiramutã), Debtor account should not be sent\n" +
		"\u2022 Call the POST Consents API\n" +
		"\u2022 Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"\u2022 Redirects the user to authorize the created consent - Expect Successful Authorization\n" +
		"\u2022 Calls the POST Payments Endpoint\n" +
		"\u2022 Expects a 201 with status set as either PNDG or RCVD - Validate response_body\n" +
		"\u2022 Poll the Get Payments endpoint\n" +
		"\u2022 End Polling when payment response returns a PNDG status\n" +
		"\u2022 Call the PATCH Payments Endpoint\n" +
		"\u2022 Expect a 200 to be returned\n" +
		"\u2022 Call the Get Payments endpoint\n" +
		"\u2022 Expect 200 on Canceled State (CANC) - Confirm that cancellation.cancelledFrom:\"INICIADORA\" and cancellation.reason:\"CANCELADO_PENDENCIA\"",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount",
		"conditionalResources.brazilCpfTemporization",
		"conditionalResources.brazilCnpjTemporization",
	}
)
public class PaymentsApiCancTemporizationTestV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsurePaymentsTemporizationCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		callAndStopOnFailure(InsertTemporizationTestValuesIntoConsentRequest.class);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no Temporization CPF/CNPJ was informed.");
		}
	}

	@Override
	protected  void configureDictInfo(){
		callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
	}

	@Override
	protected void requestProtectedResource() {

		eventLog.startBlock("Calling POST payments, expecting PNDG or RCVD");
		call(getPixPaymentSequence());
		validateResponse();
		eventLog.endBlock();

		eventLog.startBlock("Calling PATCH payments, expecting response code 200");
		callAndStopOnFailure(CreatePatchPaymentsRequestFromConsentRequestV2.class);
		call(new CallPatchPixPaymentsEndpointSequenceV2());
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
		callAndContinueOnFailure(PaymentsPatchValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();

		eventLog.startBlock("Calling GET payments, expecting response code 200 with a canceled state");
		call(new CallGetPaymentEndpointSequence());
		callAndStopOnFailure(EnsurePaymentStatusWasCanc.class);
		callAndStopOnFailure(EnsureCancelledFromWasIniciadora.class);
		callAndStopOnFailure(EnsureCancellationReasonWasCanceladoPendencia.class);
		eventLog.endBlock();

	}
	@Override
	protected void validateFinalState(){
		callAndStopOnFailure(EnsurePaymentStatusWasPndg.class);
	}
	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.insertAfter(EnsureResponseCodeWas201.class, condition(EnsurePaymentStatusWasRcvdOrPndg.class));
	}

}
