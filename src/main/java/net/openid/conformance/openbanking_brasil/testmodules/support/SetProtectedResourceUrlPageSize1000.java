package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.openinsurance.testmodule.support.AbstractPageSizeCondition;

public class SetProtectedResourceUrlPageSize1000 extends AbstractPageSizeCondition {

	@Override
	protected int getPageSize() {
		return 1000;
	}
}

