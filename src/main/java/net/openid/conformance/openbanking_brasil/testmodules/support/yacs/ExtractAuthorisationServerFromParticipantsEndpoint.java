package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonUtils;

public class ExtractAuthorisationServerFromParticipantsEndpoint extends AbstractCondition {
	@Override
	@PreEnvironment(required = {"directory_participants_response_full", "config"})
	@PostEnvironment(required = "authorisation_server")
	public Environment evaluate(Environment env) {
		String participantsArrayJsonString = env.getString("directory_participants_response_full", "body");
		String organisationId = getStringFromEnvironment(env, "config", "resource.brazilOrganizationId", "Config brazilOrganizationId Field");

		Gson gson = JsonUtils.createBigDecimalAwareGson();
		JsonArray participantsArray = gson.fromJson(participantsArrayJsonString, JsonArray.class);

		String configWellKnownUri = getStringFromEnvironment(env, "config", "server.discoveryUrl", "Config Discovery URL Field");

		for (JsonElement org : participantsArray) {
			JsonObject orgObject = org.getAsJsonObject();
			if (OIDFJSON.getString(orgObject.get("OrganisationId")).equals(organisationId)) {
				if (orgObject.has("AuthorisationServers")) {
					JsonArray servers = orgObject.get("AuthorisationServers").getAsJsonArray();
					for (JsonElement server : servers) {
						JsonObject serverObject = server.getAsJsonObject();
						if (serverObject.has("OpenIDDiscoveryDocument")) {
							JsonElement openIDDiscoveryDocumentObject = serverObject.get("OpenIDDiscoveryDocument");
							if (!openIDDiscoveryDocumentObject.isJsonNull()) {
								String registeredWellKnownUri = OIDFJSON.getString(serverObject.get("OpenIDDiscoveryDocument"));
								if (registeredWellKnownUri.equals(configWellKnownUri)) {
									env.putObject("authorisation_server", serverObject);
									logSuccess("Authorisation Server Extracted",
										args("Well-Known", configWellKnownUri, "AS", serverObject));
									return env;
								}
							}
						} else {
							throw error("Could not find OpenIDDiscoveryDocument JSON element in the AuthorisationServer object",
								args("AS", serverObject));
						}
					}
				} else {
					throw error("Could not find AuthorisationServers JSON array in the organisation object",
						args("Organisation", orgObject));
				}
			}
		}
		throw error("Could not find Authorisation Server with provided Well-Known URL in the Directory Participants List",
			args("Well-Known", configWellKnownUri, "OrganisationID", organisationId));
	}
}
