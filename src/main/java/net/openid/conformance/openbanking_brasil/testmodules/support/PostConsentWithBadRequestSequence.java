package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class PostConsentWithBadRequestSequence extends AbstractConditionSequence {

	@Override
	public void evaluate() {
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class);
		callAndStopOnFailure(EnsureConsentResponseWas400.class);
		callAndStopOnFailure(ConsentErrorMetaValidator.class);

	}

}
