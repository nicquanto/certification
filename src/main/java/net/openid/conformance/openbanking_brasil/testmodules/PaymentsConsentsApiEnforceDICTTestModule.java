package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePaymentDateIsToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureQRCodePresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentErrorTestingSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateErrorAndMetaFieldNames;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_dict_test-module",
	displayName = "Payments Consents API test module for dict local instrument",
	summary = "Payments Consents API test module ensuring a qr code must be absent when the local instrument is DICT" +
		"Flow:" +
		"Makes a payment flow. Expects a 422 due to a present QR code." +
		"Required:" +
		"Consent url pointing at the consent endpoint." +
		"Resource url pointing at the base url. The test appends on the required payment endpoints" +
		"Config: QR code  and proxy will be populated by the tests. Local Instrument is set to DICT",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiEnforceDICTTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
	}

	@Override
	protected void postConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		eventLog.startBlock("Setting date to today");
		callAndStopOnFailure(EnsurePaymentDateIsToday.class);
		callAndContinueOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndContinueOnFailure(EnsureQRCodePresentInConfig.class);
		callAndStopOnFailure(EnsureProxyPresentInConfig.class);
	}

	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void runTests() {
		runInBlock("Validate payment initiation consent", () -> {
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

			call(sequence(PaymentConsentErrorTestingSequence.class));
			callAndStopOnFailure(EnsureConsentResponseCodeWas422.class);
			callAndStopOnFailure(ValidateErrorAndMetaFieldNames.class);

		});
	}
}
