package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JWTUtil;
import net.openid.conformance.util.JsonUtils;

import java.text.ParseException;
import java.util.Optional;
import java.util.regex.Pattern;

public class BodyExtractor {
	private static final Pattern JWT_PATTERN = Pattern.compile("^(e[yw][a-zA-Z0-9_-]+)\\.([a-zA-Z0-9_-]+)\\.([a-zA-Z0-9_-]+)(\\.([a-zA-Z0-9_-]+)\\.([a-zA-Z0-9_-]+))?$");


	public static Optional<JsonElement> bodyFrom(Environment environment, String responseObjectKey) throws ParseException {

		JsonObject apiResponse = environment.getObject(responseObjectKey);
		if (apiResponse == null) {
			return Optional.empty();
		}

		JsonElement bodyEl = apiResponse.get("body");
		if (bodyEl == null) {
			return Optional.empty();
		}
		String bodyStr = OIDFJSON.getString(bodyEl);

		if (isJwt(bodyStr)) {
			JsonObject decodedJwt;
			decodedJwt = JWTUtil.jwtStringToJsonObjectForEnvironment(bodyStr);

			return Optional.ofNullable(decodedJwt.get("claims"));
		} else {
			return Optional.ofNullable(JsonUtils.createBigDecimalAwareGson().fromJson(bodyStr, JsonElement.class));
		}
	}

	public static boolean isJwt(String response) {
		return JWT_PATTERN.matcher(response).matches();
	}

}
