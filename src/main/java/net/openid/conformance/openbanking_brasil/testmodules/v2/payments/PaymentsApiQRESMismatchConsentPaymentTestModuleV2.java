package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectMismatchingQRCodeIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRESCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetProxyToRealEmailAddressOnPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetProxyToRealEmailAddressOnPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreateConsentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Optional;

@PublishTestModule(
	testName = "payments_api_qres-mismatched-consent-payment_test-module_v2",
	displayName = "Payments Consents API test module for QRES local instrument with mismatched QR codes",
	summary = "Ensure payment fails when the payment QR code differs from the consented one (Reference Error 2.2.2.6)\n" +
		"\u2022 Call POST Consent with valid QRES \n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint with a different city on the qrcode\n" +
		"\u2022 Expects 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO \n" +
		"\u2022 Validate Error message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiQRESMismatchConsentPaymentTestModuleV2 extends AbstractPaymentUnhappyPathTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectQRESCodeLocalInstrument.class);
		callAndStopOnFailure(SelectQRESCodePixLocalInstrument.class);
		callAndStopOnFailure(InjectMismatchingQRCodeIntoConfig.class);
		callAndStopOnFailure(SetProxyToRealEmailAddressOnPaymentConsent.class);
		callAndStopOnFailure(SetProxyToRealEmailAddressOnPayment.class);
		callAndStopOnFailure(InjectRealCreditorAccountEmailToPaymentConsent.class);
		callAndStopOnFailure(InjectRealCreditorAccountToPayment.class);

	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		// Not needed since only 422 response is expected
	}

	@Override
	protected void validate422ErrorResponseCode() {
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento.class);
	}

	@Override
	protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		return EnsureResponseCodeWas422.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpenBankingBrazilPreAuthorizationErrorAgnosticSteps steps = (OpenBankingBrazilPreAuthorizationErrorAgnosticSteps) new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
			.replace(
				ValidateErrorAndMetaFieldNames.class,
				condition(CreateConsentErrorValidatorV2.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE)
					.skipIfStringMissing("validate_errors")
			)
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
			);
		return steps;
	}

	@Override
	protected void performPreAuthorizationSteps() {
		env.mapKey("access_token", "saved_client_credentials");
		call(createOBBPreauthSteps());
		callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasFormaPagamentoInvalida.class, Condition.ConditionResult.INFO);
		if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
			fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
		}
		eventLog.startBlock(currentClientString() + "Validate consents response");
		callAndContinueOnFailure(PaymentConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
		env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);
		eventLog.endBlock();
	}

}
