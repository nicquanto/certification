package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentStatusEnumV2;

import java.util.List;

public class EnsurePaymentStatusWasRcvdOrPatc extends AbstractEnsurePaymentStatusWasX {
	@Override
	protected List<String> getExpectedStatuses() {
		return List.of(PaymentStatusEnumV2.RCVD.toString(), PaymentStatusEnumV2.PATC.toString());
	}
}
