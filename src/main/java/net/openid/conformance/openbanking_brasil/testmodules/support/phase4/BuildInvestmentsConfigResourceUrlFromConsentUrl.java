package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import com.google.common.base.Strings;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractBuildConfigResourceUrlFromConsentUrl;
import net.openid.conformance.testmodule.Environment;

public class BuildInvestmentsConfigResourceUrlFromConsentUrl extends AbstractBuildConfigResourceUrlFromConsentUrl {

    private String api;

    @Override
    protected String apiReplacement() {
        return api;
    }

    @Override
    protected String endpointReplacement() {
        return "investments";
    }

    @Override
    public Environment evaluate(Environment env) {
        api = env.getString("investment-api");
        if (Strings.isNullOrEmpty(api)) {
            throw error("Environment variable investment-api is missing");
        }
        return super.evaluate(env);
    }
}
