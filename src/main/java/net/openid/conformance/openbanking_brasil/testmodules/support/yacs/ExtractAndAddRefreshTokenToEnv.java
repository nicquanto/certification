package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class ExtractAndAddRefreshTokenToEnv extends AbstractCondition {
	@Override
	@PreEnvironment(required = {"token_endpoint_response"})
	@PostEnvironment(strings = {"first_refresh_token", "refresh_token"})
	public Environment evaluate(Environment env) {
		String refreshToken = env.getString("token_endpoint_response", "refresh_token");
		if(refreshToken==null) {
			throw error("Token endpoint response does not contain a refresh token");
		}
		env.putString("first_refresh_token", refreshToken);
		env.putString("refresh_token", refreshToken);
		logSuccess("Extracted refresh token from response", args("refresh_token", refreshToken));
		return env;
	}
}
