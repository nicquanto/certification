package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToOldValueOnPaymentInitiation;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreatePaymentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_idempotency_test-module_v2",
	displayName = "Payments API test module for re-using idempotency keys",
	summary = "Ensure Error when payment request is sent with reused invalid iss and idempotency key\n" +
		"\u2022 Calls POST Consents Endpoint\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Redirects the user to authorize the created consent \n" +
		"\u2022 Call GET Consent, with the same idempotency id used at POST Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" \n" +
		"\u2022 Call the POST Payments with an invalid iss\n" +
		"\u2022 Expect 403 - Validate Error Message\n" +
		"\u2022 Calls the POST Payments, with the same idempotency key used at POST Consent\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Call the POST Payments with the same idempotency key but a different payload\n" +
		"\u2022 Expect 401\n" +
		"\u2022 Call the POST Payments, with the same payload as the previous request but a different idempotency id\n" +
		"\u2022 Expect 401",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsReuseIdempotencyKeyTestModuleV2 extends AbstractOBBrasilFunctionalTestModule {

	@Override
	protected void validateGetConsentResponse() {
		runInBlock("Validate GET Consent Response", () -> {
			callAndContinueOnFailure(PaymentConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
			env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);
		});
	}

	@Override
	protected void performPreAuthorizationSteps() {
		call(createOBBPreauthSteps());
		callAndStopOnFailure(PaymentConsentValidatorV2.class);
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);
		env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);

	}

	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(AddResourceUrlToConfig.class);
		super.setupResourceEndpoint();
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(AddPaymentScope.class);
		super.validateClientConfiguration();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		eventLog.startBlock("Setting date to today");
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(SanitiseQrCodeConfig.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
	}

	@Override
	protected void requestProtectedResource() {
		//Make request using an invalid ISS and expect a 403 response
		eventLog.startBlock("Making PIX request with incorrect ISS");
		callPixWrongIss();
		//Make request with the same idempotency key used at POST consent
		eventLog.startBlock("Making correct PIX request");
		callPix();
		//Make request using same idempotency key but using a different payload - Expects a 401
		eventLog.startBlock("Making PIX request with with same idempotency key but different payload");
		callPixChangedPayload();
		//Make request with different idempotency key but with same payload - Expects a 401
		eventLog.startBlock("Making PIX request with the same payload but different idempotency key");
		callPixDifferentIdempotencyKey();
	}

	@Override
	protected void validateResponse() {
		// Not needed for this test
	}


	public void callPixChangedPayload() {
		callAndStopOnFailure(ModifyPixPaymentValue.class);

		callPixFirstBlock();

		callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");

		callPixSecondBlock();

		callAndStopOnFailure(SetPaymentAmountToOldValueOnPaymentInitiation.class);

		callAndContinueOnFailure(EnsureResponseCodeWas401.class, Condition.ConditionResult.FAILURE);
	}

	protected void validateMeta(boolean isOptional){
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, isOptional);
		callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);
		env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);

	}

	public void callPix() {

		callPixFirstBlock();

		callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");

		callPixSecondBlock();

		callAndStopOnFailure(EnsureResponseCodeWas201.class);

		validateMeta(false);

		callAndStopOnFailure(PaymentInitiationPixPaymentsValidatorV2.class);


	}

	private void callPixDifferentIdempotencyKey() {
		env.mapKey("idempotency_key", "different_idempotency_key");

		callAndStopOnFailure(CreateIdempotencyKey.class);

		callPixFirstBlock();

		env.unmapKey("idempotency_key");

		callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");

		callPixSecondBlock();

		callAndContinueOnFailure(EnsureResponseCodeWas401.class, Condition.ConditionResult.FAILURE);
	}

	public void callPixWrongIss() {
		callPixFirstBlock();

		callAndStopOnFailure(InjectWrongISSToJWT.class);

		callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");

		callPixSecondBlock();

		callAndStopOnFailure(EnsureResourceResponseReturnedJsonContentType.class);

		callAndContinueOnFailure(EnsureResponseCodeWas403.class, Condition.ConditionResult.FAILURE);

		validateError();
	}

	private void validateError() {
		callAndStopOnFailure(CreatePaymentErrorValidatorV2.class);
		validateMeta(true);
	}

	private void callPixFirstBlock() {
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);

		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");

		callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");

		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);

		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");

		callAndStopOnFailure(AddIdempotencyKeyHeader.class);
		callAndStopOnFailure(SetApplicationJwtContentTypeHeaderForResourceEndpointRequest.class);
		callAndStopOnFailure(SetApplicationJwtAcceptHeaderForResourceEndpointRequest.class);
		callAndStopOnFailure(SetResourceMethodToPost.class);
		callAndStopOnFailure(CreatePaymentRequestEntityClaims.class);

		call(exec().mapKey("request_object_claims", "resource_request_entity_claims"));

		callAndStopOnFailure(AddAudAsPaymentInitiationUriToRequestObject.class, "BrazilOB-6.1");

		callAndStopOnFailure(AddIssAsCertificateOuToRequestObject.class, "BrazilOB-6.1");

		callAndStopOnFailure(AddJtiAsUuidToRequestObject.class, "BrazilOB-6.1");
	}

	private void callPixSecondBlock() {
		call(exec().unmapKey("request_object_claims"));

		callAndStopOnFailure(FAPIBrazilSignPaymentInitiationRequest.class);

		callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
	}

}
