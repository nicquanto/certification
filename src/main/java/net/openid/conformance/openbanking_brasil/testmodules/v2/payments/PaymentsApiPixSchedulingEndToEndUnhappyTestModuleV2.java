package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetIncorrectEndToEndIdTimeInThePaymentRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreatePaymentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_pixscheduling-endtoend-unhappy_test-module_v2",
	displayName = "This test is scheduled payments test, which will break the defined rules for the EndToEndID field, which should be sent with a time on the day of the payment and an hour value equal to 15 UTC",
	summary = "This test is scheduled payments test, which will break the defined rules for the EndToEndID field, which should be sent with a time on the day of the payment and an hour value equal to 15 UTC\n" +
		"\u2022 Create consent with request payload with both the  schedule.single.date field set as D+1\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"\u2022 Calls the POST Payments Endpoint with E2EID set for future date but with hour as 01 instead of 15\n" +
		"\u2022 Expects 422 - with error PARAMETRO_INVALIDO -  Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiPixSchedulingEndToEndUnhappyTestModuleV2 extends AbstractPixSchedulingTestModuleV2 {


	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
		callAndStopOnFailure(SetIncorrectEndToEndIdTimeInThePaymentRequestBody.class);
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return super.getPixPaymentSequence()
			.replace(EnsureResponseCodeWas201.class, condition(EnsureResponseCodeWas422.class));
	}

	@Override
	protected void validateResponse() {
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
		callAndStopOnFailure(CreatePaymentErrorValidatorV2.class);
		callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class);
	}
}
