package net.openid.conformance.openbanking_brasil.testmodules;

import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddResourceUrlToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.OverrideCNPJ;
import net.openid.conformance.openbanking_brasil.testmodules.support.OverrideClientWithPagtoClient;
import net.openid.conformance.openbanking_brasil.testmodules.support.OverrideClientWithPagtoClientThatHasClientSpecificJwks;
import net.openid.conformance.openbanking_brasil.testmodules.support.OverrideScopeWithOpenIdPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetDirectoryInfo;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "dcr_api_pagto-attempt-client-takeover_test-module",
	displayName = "Payments API DCR test: attempt to take over client",
	summary = "Obtains a software statement from the Brazil directory (using a client hardcoded into the test suite), registers a new client on the target authorization server then, using valid keys/SSA/etc from a different valid client, attempts to take over the original client. Note that this test overrides the 'alias' value in the configuration, so you may see your test being interrupted if other users are testing.\n" +
		"\u2022 Using a hardcoded client with only the PAGTO role, retrieves from the directory its SSA\n" +
		"\u2022 Performs a DCR on the provided authorization server -> Expects a success \n" +
		"\u2022 Performs a PUT on the registration endpoint with the same configuration -> Expects a success\n" +
		"\u2022 Changes the certificates used to the second set of certificates that do not belong to the PAGTO client created but to a client with a PAGTO role from a different organization\n" +
		"\u2022 Attempts 'GET' on client configuration endpoint using MTLS certificate for the second client -> Expects Success\n" +
		"\u2022 Using the second client with only the PAGTO role, retrieves from the directory its SSA\n" +
		"\u2022 Performs a DCR on the provided authorization server with the second client -> Expects a success \n" +
		"\u2022 Performs a PUT on the registration endpoint using the SSA from the second client with the certificate from the first client -> Expects a failure\n" +
		"\u2022 Performs a PUT on the registration endpoint using the SSA from the second client with the certificate from the second client -> Expects a success\n" +
		"\u2022 DELETEs the second registered client from the authorization server\n" +
		"\u2022 DELETEs the first registered client from the authorization server",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.discoveryUrl",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.client_id",
		"resource.brazilOrganizationId"
	}
)
public class PaymentsApiDcrTestModuleAttemptClientTakeover extends AbstractDcrTestModuleAttemptClientTakeover {

	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(AddResourceUrlToConfig.class);
		super.setupResourceEndpoint();
	}

	@Override
	protected void configureClient() {
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(OverrideClientWithPagtoClient.class);
		callAndStopOnFailure(OverrideScopeWithOpenIdPayments.class);
		callAndStopOnFailure(OverrideCNPJ.class);
		callAndStopOnFailure(SetDirectoryInfo.class);
		super.configureClient();
	}

	@Override
	protected void switchToAlternateClient() {
		callAndStopOnFailure(OverrideClientWithPagtoClientThatHasClientSpecificJwks.class);
		callAndStopOnFailure(OverrideScopeWithOpenIdPayments.class);
	}

}
