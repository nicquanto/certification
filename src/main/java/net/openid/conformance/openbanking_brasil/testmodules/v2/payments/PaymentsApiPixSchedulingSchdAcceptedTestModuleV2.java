package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AskForScreenshotWithScheduledDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs.EnsureScheduledDateIsTodayPlus350;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_pixscheduling-schd-accepted_test-module_v2",
	displayName = "Payments API test module ensuring unknown CPF is rejected",
	summary = "This test is the core happy path scheduled payments test, creating a scheduled payment and waiting for this payment to reach a SCHD state. " +
		"The test will require the user to provide a screen capture showing the scheduled date on it's screen.\n" +
		"\u2022 Create consent with request payload with both the  schedule.single.date field set as D+350\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 User must upload a screen capture showing the payment screen with scheduled date for +350 days\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"\u2022 Calls the POST Payments Endpoint\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD or ACCP\n" +
		"\u2022 Expect Payment Scheduled to be reached (SCHD) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)

public class PaymentsApiPixSchedulingSchdAcceptedTestModuleV2 extends AbstractPixSchedulingTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
	}

	@Override
	protected void createPlaceholder() {
		callAndStopOnFailure(AskForScreenshotWithScheduledDate.class);
		env.putString("error_callback_placeholder", env.getString("payments_placeholder"));
	}

	@Override
	protected void performRedirect() {
		performRedirectWithPlaceholder();
	}


	@Override
	protected Class<? extends Condition> getPaymentScheduleDate() {
		return EnsureScheduledDateIsTodayPlus350.class;
	}

	@Override
	protected void executePostPollingSteps() {
		// No extra steps are needed
	}
}
