package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.RememberOriginalScopes;
import net.openid.conformance.openbanking_brasil.testmodules.ResetScopesToConfigured;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.SetPatchPaymentRequestRelToInvalidValue;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoNaoPermiteCancelamento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAscs;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreatePaymentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentsPatchValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_pixscheduling-patch-unhappy_test-module_v2",
	displayName = "Payments API test for different scenarios where a PATCH is called for a scheduled pix that result in failure",
	summary = "1. PATCH Payments unhappy path test that will revoke a scheduled payment with invalid request_body\n" +
		"\u2022 Create consent with request payload with the schedule.single.date field set as D+1\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"\u2022 Call the POST Payments Endpoint\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD or ACCP\n" +
		"\u2022 Expect Payment Scheduled to be reached (SCHD) - Validate Response\n" +
		"\u2022 Call the PATCH Payments Endpoint - cancelledBy must have the same document as the loggedUser however the field rel will be set to \"XXX\"\n" +
		"\u2022 Expect 422 with error set as PARAMETRO_INVALIDO\n" +
		"\n" +
		"2. Ensure error when PATCH payments endpoint is called in a state different from SCHD/PDNG/PATC\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"\u2022 Expects 201\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint \n" +
		"\u2022 Expects 201\n" +
		"\u2022 Calls the PATCH Payments Endpoint\n" +
		"\u2022 Expects 422 - PAGAMENTO_NAO_PERMITE_CANCELAMENTO\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expects Definitive state (ACSC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)

public class PaymentsApiPixSchedulingPatchUnhappyV2 extends AbstractPixSchedulingTestModuleV2 {

	private static final String MOCK_BANK_WELL_KNOWN = "https://auth.mockbank.poc.raidiam.io/.well-known/openid-configuration";

	private boolean secondTest = false;

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		eventLog.log(getName(), "Payments scope present - protected resource assumed to be a payments endpoint");
		ConditionSequence steps = new OpenBankingBrazilPreAuthorizationSteps(
			false,
			false,
			addTokenEndpointClientAuthentication,
			true,
			false,
			false
		);
		if (!secondTest) {
			steps = steps.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class, condition(RememberOriginalScopes.class));
		} else {
			steps = steps.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class, condition(ResetScopesToConfigured.class));
		}
		return steps;
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
	}

	@Override
	protected ConditionSequence getCallPatchConditionSequence() {
		return super.getCallPatchConditionSequence()
			.replace(EnsureResponseCodeWas200.class, condition(EnsureResponseCodeWas422.class));
	}

	@Override
	protected ConditionSequence getCreatePatchRequestSequence() {
		ConditionSequence sequence = super.getCreatePatchRequestSequence();
		if (!secondTest) {
			sequence = sequence
				.insertAfter(CreatePatchPaymentsRequestFromConsentRequestV2.class, condition(SetPatchPaymentRequestRelToInvalidValue.class));
		}
		return sequence;
	}

	@Override
	protected ConditionSequence getPatchValidationSequence() {
		ConditionSequence sequence = super.getPatchValidationSequence()
			.replace(PaymentsPatchValidatorV2.class, condition(CreatePaymentErrorValidatorV2.class));
		if (!secondTest) {
			sequence = sequence
				.insertAfter(ValidateMetaOnlyRequestDateTime.class, condition(EnsureErrorResponseCodeFieldWasParametroInvalido.class));
		} else {
			sequence = sequence
				.insertAfter(ValidateMetaOnlyRequestDateTime.class, condition(EnsureErrorResponseCodeFieldWasPagamentoNaoPermiteCancelamento.class));
		}
		return sequence;
	}

	@Override
	protected void validateResponse() {
		if (!secondTest) {
			super.validateResponse();
		} else {
			env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
			call(getPaymentValidationSequence());
			callAndStopOnFailure(SaveOldValues.class);
			executePostPollingSteps();
			callAndStopOnFailure(LoadOldValues.class);
			callAndStopOnFailure(SetProtectedResourceUrlToSelfEndpoint.class);
			repeatSequence(this::getRepeatSequenceSecondTest)
				.untilTrue("payment_proxy_check_for_reject")
				.trailingPause(30)
				.times(5)
				.validationSequence(this::getPaymentValidationSequence)
				.onTimeout(sequenceOf(
					condition(TestTimedOut.class),
					condition(ChuckWarning.class)))
				.run();

			validateFinalState();
		}
	}

	protected ConditionSequence getRepeatSequenceSecondTest() {
		return getRepeatSequence()
			.skip(SetProtectedResourceUrlToSelfEndpoint.class, "Url already set");
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		if (!secondTest) {
			return super.getPaymentPollStatusCondition();
		} else {
			return CheckPaymentPollStatusRcvdOrAccpOrAcpdV2.class;
		}
	}

	@Override
	protected void validateFinalState() {
		if (!secondTest) {
			super.validateFinalState();
		} else {
			callAndStopOnFailure(EnsurePaymentStatusWasAscs.class);
		}
	}

	protected void configureSecondTest() {
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		if (env.getString("config", "server.discoveryUrl").equals(MOCK_BANK_WELL_KNOWN)) {
			callAndStopOnFailure(InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources.class);
		}
		configureDictInfo();
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		if (!secondTest) {
			secondTest = true;
			validationStarted = false;
			eventLog.startBlock(currentClientString() + "Setting up second part of the test");
			configureSecondTest();
			eventLog.endBlock();
			performAuthorizationFlow();
		} else {
			super.onPostAuthorizationFlowComplete();
		}
	}
}
