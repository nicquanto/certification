package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveProxyFromConsentConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveProxyFromPaymentConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectMANUCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectMANUCodePixLocalInstrument;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_manu-pix-response_test-module_v2",
	displayName = "Payments API test module for MANU local instrument pix response",
	summary = "Ensure response are valid when localInstrument is MANU\n" +
		"\u2022 Calls POST Consents Endpoint with localInstrument as MANU, and no proxy or qrcode provided\n" +
		"\u2022 Expects 201\n" +
		"\u2022 Redirects the user to authorize the created consent   \n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"    \n" +
		"\u2022 Calls the POST Payments with localInstrument as MANU\n" +
		"\u2022 Expects 201 - Validate response    \n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expectes Definitive  state (ACSC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiMANUPixResponseTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	@Override
	protected  void configureDictInfo(){
		callAndStopOnFailure(SelectMANUCodeLocalInstrument.class);
		callAndStopOnFailure(SelectMANUCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
		callAndStopOnFailure(RemoveProxyFromConsentConfig.class);
		callAndStopOnFailure(RemoveProxyFromPaymentConfig.class);
	}
}
