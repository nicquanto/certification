package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CheckPaymentVariantPollStatusV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasSIorVAL;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasSIorVAL;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToOutOfRangeOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToOutOfRangeOnPaymentInitiation;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_multiple-consents-no-funds-conditional_test-module_v2",
	displayName = "Payments API multiple consents no funds conditional test module",
	summary = "This test module should be executed only by institutions that currently support consents with multiple accounts, in case the institution does not support this feature the ‘Payment consent - Logged User CPF - Multiple Consents Test' and 'Payment consent - Business Entity CNPJ - Multiple Consents Test’ should be left empty, which in turn will make the test return a SKIPPED\n" +
		"This test module validates that the payment will reach a RJCT status when canceled by the payment initiator\n" +
		"\u2022 Validates if the user has provided the field “Payment consent - Logged User CPF - Multiple Consents Test”. If field is not provided the whole test scenario must be SKIPPED\n" +
		"\u2022 Set the payload to be customer business if Payment consent - Business Entity CNPJ - Multiple Consents Test was provided. If left blank, it should be customer personal\n" +
		"\u2022 Creates consent request_body with valid email proxy (cliente-a00001@pix.bcb.gov.br) and its standardized payload, set the Payment consent - Logged User CPF - Multiple Consents Test” on the loggedUser identification field, and  the amount to be 9999999999.99, Debtor account should not be sent\n" +
		"\u2022 Call the POST Consents API\n" +
		"\u2022 Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"\u2022 Redirects the user to authorize the created consent - Expect Successful Authorization\n" +
		"\u2022 Calls the POST Payments Endpoint\n" +
		"\u2022 Expects either a 201 or a 422 with SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE response - Validate Response_Body\n" +
		"\u2022 If a 201 is returned, poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD or PATC – Wait for user to provide approval\n" +
		"\u2022 Expect Canceled State (RJCT) - Confirm that RejectionReason equal to SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE is returned",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"conditionalResources.brazilCpfJointAccount",
		"conditionalResources.brazilCnpjJointAccount",
	}
)
public class PaymentsApiMultipleConsentsNoFundsConditionalTestModuleV2 extends AbstractPaymentConsentUnhappyPathTestModuleV2 {

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsurePaymentsJointAccountCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no 'Payment consent - Logged User CPF - Multiple Consents Test' was informed.");
		}
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
		callAndStopOnFailure(SetPaymentAmountToOutOfRangeOnConsent.class);
		callAndStopOnFailure(SetPaymentAmountToOutOfRangeOnPaymentInitiation.class);
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentVariantPollStatusV2.class;
	}

	@Override
	protected Class<EnsureResourceResponseCodeWas201Or422> getExpectedPaymentResponseCode() {
		return EnsureResourceResponseCodeWas201Or422.class;
	}
	@Override
	protected void validatePaymentRejectionReasonCode() {
		callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasSIorVAL.class);
	}
	@Override
	protected void validate422ErrorResponseCode() {
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasSIorVAL.class);
	}

}
