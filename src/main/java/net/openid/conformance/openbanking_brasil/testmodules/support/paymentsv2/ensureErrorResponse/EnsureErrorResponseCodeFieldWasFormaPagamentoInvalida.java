package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AbstractErrorFromJwtResponseCondition;
import net.openid.conformance.testmodule.Environment;

public class EnsureErrorResponseCodeFieldWasFormaPagamentoInvalida extends AbstractErrorFromJwtResponseCondition {

	public static final String EXPECTED_ERROR = "FORMA_PAGAMENTO_INVALIDA";

	@Override
	public Environment evaluate(Environment env) {

		JsonObject response = env.getObject("consent_endpoint_response_full");
		env.putBoolean("error_status_FPI",validateError(response, EXPECTED_ERROR));
		return env;
	}

}
