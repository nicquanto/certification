package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAscs;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;

import java.util.Objects;
import java.util.Optional;

public abstract class AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 extends AbstractOBBrasilFunctionalTestModule {

	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(AddResourceUrlToConfig.class);
		super.setupResourceEndpoint();
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(AddPaymentScope.class);
		super.validateClientConfiguration();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		call(createOBBPreauthSteps()
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class))));
		callAndStopOnFailure(SaveAccessToken.class);
		runInBlock(currentClientString() + "Validate consents response", this::validateConsentResponse);
	}

	@Override
	protected void validateGetConsentResponse() {
		runInBlock("Validate GET Consent Response", this::validateConsentResponse);
	}

	protected void validateConsentResponse() {
		callAndContinueOnFailure(PaymentConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
		env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		configureDictInfo();
	}

	protected abstract void configureDictInfo();

	@Override
	protected void fetchConsentToCheckAuthorisedStatus() {
		super.fetchConsentToCheckAuthorisedStatus();
		env.unmapKey("access_token");
	}

	@Override
	protected void requestProtectedResource() {
		if (!validationStarted) {
			validationStarted = true;
			eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
			ConditionSequence pixSequence = getPixPaymentSequence();
			call(pixSequence);
			eventLog.endBlock();
			eventLog.startBlock(currentClientString() + "Validate response");
			validateResponse();
			eventLog.endBlock();
		}
	}

	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence();
	}

	@Override
	protected void validateResponse() {
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
		call(getPaymentValidationSequence());


		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(5)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		validateFinalState();

	}

	protected void validateFinalState() {
		callAndStopOnFailure(EnsurePaymentStatusWasAscs.class);
	}

	protected ConditionSequence getRepeatSequence() {
		return new ValidateSelfEndpoint()
			.replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeader.class),
				condition(CallProtectedResource.class)
			))
			.skip(ValidateResponseMetaData.class, "Will be checked Later")
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values")
			.insertAfter(ValidateResponseMetaData.class, condition(getPaymentPollStatusCondition()));
	}

	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrAccpOrAcpdV2.class;
	}

	protected ConditionSequence getPaymentValidationSequence(){
		return sequenceOf(
			condition(PaymentInitiationPixPaymentsValidatorV2.class)
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE),
			condition(ValidateMetaOnlyRequestDateTime.class)
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		forceGetCallToUseClientCredentialsToken(builder);

		super.call(builder);

		if (CallProtectedResource.class.equals(builder.getConditionClass())) {
			validateSelfLink("resource_endpoint_response_full");
		}

		if (FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
			FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class.equals(builder.getConditionClass()) ||
			FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders.class.equals(builder.getConditionClass())) {
			validateSelfLink("consent_endpoint_response_full");
		}
	}

	protected void forceGetCallToUseClientCredentialsToken(ConditionCallBuilder builder) {
		if (CallProtectedResource.class.equals(builder.getConditionClass())) {
			String resourceMethod = OIDFJSON.getString(Optional.ofNullable(
				env.getElementFromObject("resource", "resourceMethod")).orElse(new JsonPrimitive("")));
			if (Objects.equals(resourceMethod, "GET") && env.containsObject("old_access_token")) {
				eventLog.log("Load client_credential access token", env.getObject("old_access_token"));
				callAndStopOnFailure(LoadOldAccessToken.class);
				callAndStopOnFailure(SaveAccessToken.class);
			}
		}
	}

	private boolean isEndpointCallSuccessful(int status) {
		if (status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK) {
			return true;
		}
		return false;
	}

	protected void validateSelfLink(String responseFull) {

		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
			.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));

		if (!recursion.equals("true") && isEndpointCallSuccessful(status)) {

			env.putString("recursion", "true");
			call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
			env.mapKey("resource_endpoint_response_full", responseFull);

			ConditionSequence sequence = new ValidateSelfEndpoint().replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeader.class),
				condition(CallProtectedResource.class)
			)).skip(ValidateResponseMetaData.class, "Skipped for later validation");

			if (responseFull.contains("consent")) {
				sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));

				env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			} else {
				env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			}
			callAndContinueOnFailure(ValidateSelfLinkRegex.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY);

			call(sequence);
			env.unmapKey("resource_endpoint_response_full");
			env.putString("recursion", "false");
		}
	}

}
