package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public class InjectInvalidCreditorAccountToPaymentConsent extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonObject obj = env.getObject("resource");
		obj = obj.getAsJsonObject("brazilPaymentConsent");
		obj = obj.getAsJsonObject("data");

		JsonObject creditor = obj.getAsJsonObject("creditor");
		creditor.addProperty("name", DictHomologKeys.PROXY_EMAIL_OWNER_NAME);
		creditor.addProperty("cpfCnpj", DictHomologKeys.PROXY_EMAIL_MISMATCHED_CPF);
		creditor.addProperty("personType", DictHomologKeys.PROXY_EMAIL_PERSON_TYPE);

		obj = obj.getAsJsonObject("payment");
		obj = obj.getAsJsonObject("details");
		obj = obj.getAsJsonObject("creditorAccount");

		obj.addProperty("issuer", DictHomologKeys.PROXY_EMAIL_BRANCH_NUMBER);
		obj.addProperty("number", DictHomologKeys.PROXY_EMAIL_ACCOUNT_MISMATCHED_NUMBER);
		obj.addProperty("accountType", DictHomologKeys.PROXY_EMAIL_ACCOUNT_TYPE);
		obj.addProperty("ispb", DictHomologKeys.PROXY_EMAIL_MISMATCHED_ISPB);
		logSuccess("Added invalid creditor account details to payment consent");

		return env;
	}
}
