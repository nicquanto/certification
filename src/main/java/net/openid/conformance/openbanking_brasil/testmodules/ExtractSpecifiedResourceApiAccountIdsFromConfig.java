package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class ExtractSpecifiedResourceApiAccountIdsFromConfig extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config", strings = "api_type")
	@PostEnvironment(required = "fetched_api_ids")
	public Environment evaluate(Environment env) {

		JsonArray fetchedApiIds = new JsonArray();
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("fetchedApiIds", fetchedApiIds);


		EnumResourcesType resourcesType = EnumResourcesType.valueOf(env.getString("api_type"));
		String resourceEnvVarKey;
		switch (resourcesType) {

			case ACCOUNT:
				resourceEnvVarKey = "account";
				break;
			case CREDIT_CARD_ACCOUNT:
				resourceEnvVarKey = "credit_card_account";
				break;
			case UNARRANGED_ACCOUNT_OVERDRAFT:
				resourceEnvVarKey = "unarranged_account_overdraft_account";
				break;
			default:
				throw error("Incorrect api_type value was provided", args("Expected", EnumResourcesType.values(), "Provided", resourcesType));
		}

		env.putObject("fetched_api_ids", jsonObject);

		Optional.ofNullable(env.getElementFromObject("config", String.format("resource.first_%s_id", resourceEnvVarKey)))
			.ifPresentOrElse(o -> fetchedApiIds.add(OIDFJSON.getString(o)),
				() -> {
					throw error(String.format("Could not find resource.first_%s_id in the resource configuration." +
						" IDs will be fetched after consent creation instead", resourceEnvVarKey));
				});

		Optional.ofNullable(env.getElementFromObject("config", String.format("resource.second_%s_id", resourceEnvVarKey)))
			.ifPresentOrElse(o -> fetchedApiIds.add(OIDFJSON.getString(o)),
				() -> log(String.format("Could not find resource.second_%s_id in the resource configuration." +
					" Assuming multiple accounts for the same users is not supported." +
					" The corresponding part of the test will be skipped", resourceEnvVarKey)));


		logSuccess("Successfully extracted account IDs", args("IDs", fetchedApiIds));

		return env;
	}

}
