package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforceAbsenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.EnsurePaymentsTemporizationCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.InsertTemporizationTestValuesIntoConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasPndg;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRcvdOrPndg;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjctNIOrAscs;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusPdngV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_temporization-conditional_test-module_v2",
	displayName = "Payments API v2 test module for checking the optional flow for temporization",
	summary = "This test module is optional and should be executed only by institutions that wish to test the different flows for a payment that will fall into a PNDG state, meaning it was halted for additional verification. The test will only be executed if the \"Payment consent - Logged User CPF - Temporization Test\" is provided on the test configuration.\n" +
		"\n" +
		"The test modules validate if the payment will reach a final state is reached after a PDNG status" +
		"\n" +
		"\u2022 Validates if the user has provided the field “Payment consent - Logged User CPF - Temporization”. If field is not provided the whole test scenario must be SKIPPED\n" +
		"\u2022 Set the payload to be customer business if Payment consent - Business Entity CNPJ - Temporization was provided. If left blank, it should be customer personal\n" +
		"\u2022 Creates consent request_body with valid email proxy specific for this test (cliente-a00002@pix.bcb.gov.br), localInstrument as DICT, Payment consent - Logged User CPF - Temporization” on the loggedUser identification field, Value as 12345,67, and IBGE Town Code as 1400704 (Uiramutã), Debtor account should not be sent\n" +
		"\u2022 Call the POST Consents API\n" +
		"\u2022 Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"\u2022 Redirects the user to authorize the created consent - Expect Successful Authorization\n" +
		"\u2022 Calls the POST Payments Endpoint\n" +
		"\u2022 Expects a 201 with status set as either PNDG or RCVD - Validate response_body\n" +
		"\u2022 Poll the Get Payments endpoint - End Polling when payment response returns a PNDG status\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is PNDG – Wait for user to provide approval\n" +
		"\u2022 Expect 200 with either:\n" +
		"\n" +
		"\u2022 Rejected State (RJCT) - Confirm that RejectionReason equal to NAO_INFORMADO is returned\n" +
		"\n" +
		"\u2022 Accepted State (ACSC)\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount",
		"conditionalResources.brazilCpfTemporization",
		"conditionalResources.brazilCnpjTemporization",
	}
)
public class PaymentsApiTemporizationConditionalTestV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	private boolean IS_SECOND_POLL = false;

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsurePaymentsTemporizationCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		callAndStopOnFailure(InsertTemporizationTestValuesIntoConsentRequest.class);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no Temporization CPF/CNPJ was informed.");
		}
	}

	@Override
	protected  void configureDictInfo(){
		callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
	}

	@Override
	protected void requestProtectedResource() {
		eventLog.startBlock("Calling POST payments, expecting PNDG or RCVD, Polling GET Payments till PDNG status");
		call(getPixPaymentSequence());
		validateResponse();
		eventLog.endBlock();

		eventLog.startBlock("Polling GET payments while pending. Expects Rejected - Nao Informado or Accepted");
		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(5)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		callAndStopOnFailure(EnsureResponseCodeWas200.class);
		validateFinalState();
		eventLog.endBlock();
	}

	@Override
	protected void validateFinalState(){
		if(IS_SECOND_POLL){
			callAndStopOnFailure(EnsurePaymentStatusWasRjctNIOrAscs.class);
		} else {
			IS_SECOND_POLL = true;
			callAndStopOnFailure(EnsurePaymentStatusWasPndg.class);
		}
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		if(IS_SECOND_POLL){
			return CheckPaymentPollStatusPdngV2.class;
		}
		return CheckPaymentPollStatusRcvdOrAccpOrAcpdV2.class;
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.insertAfter(EnsureResponseCodeWas201.class, condition(EnsurePaymentStatusWasRcvdOrPndg.class));
	}

}
