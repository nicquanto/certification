package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CallPatchPixPaymentsEndpointSequenceV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs.EnsureScheduledDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Objects;

@PublishTestModule(
    testName = "payments-invalid-token-v2",
    displayName = "Payments API v2 test module for invalid token type",
    summary = "This test is a PIX Payments that aims to test if the institution accepts the correct type of Token for each different API method\n" +
        "• Create consent with request payload with both the schedule.single.date field set as D+1\n" +
        "• Call the POST Consents endpoints using token issued via client_credentials grant\n" +
        "• Expects 201 - Validate response\n" +
        "• Redirects the user to authorize the created consent\n" +
        "• Call GET Consent using token issued via client_credentials grant\n" +
        "• Expects 200 - Validate if status is \"AUTHORISED\" and validate the response\n" +
        "• Call the POST Payments Endpoint with a token issued via using token issued via client_credentials grant\n" +
        "• Expect either a 401 to be returned\n" +
        "• Call the POST Payments Endpoint  using a token issued via authorization_code grant\n" +
        "• Expects 201 - Validate Response\n" +
        "• Call the GET Payments endpoint using a token issued via authorization_code grant\n" +
        "• Expect either a 401 to be returned\n" +
        "• Call the PATCH Payments endpoint using a token issued via authorization_code grant\n" +
        "• Expect either a 401 to be returned\n" +
        "• Call the POST Consents endpoint using a token issued via authorization_code grant\n" +
        "• Expect either a 401 to be returned",
    profile = OBBProfile.OBB_PROFILE,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "resource.consentUrl",
        "resource.loggedUserIdentification",
        "resource.businessEntityIdentification",
        "resource.debtorAccountIspb",
        "resource.debtorAccountIssuer",
        "resource.debtorAccountNumber",
        "resource.debtorAccountType",
        "resource.paymentAmount"
    }
)
public class PaymentsApiInvalidTokenTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

    private final String SKIP_MESSAGE = "Not expecting a jwt response";

    @Override
    protected void updatePaymentConsent() {}

    @Override
    protected Class<? extends Condition> getPaymentPollStatusCondition() {
        return CheckPaymentPollStatusRcvdOrAccpV2.class;
    }

    @Override
    protected void validateFinalState() {
        callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void configureDictInfo() {
        callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
        callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
        callAndStopOnFailure(EnsureProxyPresentInConfig.class);
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        callAndStopOnFailure(EnsureScheduledDateIsTomorrow.class);
        callAndStopOnFailure(RemovePaymentConsentDate.class);
        super.onConfigure(config, baseUrl);
    }

    @Override
    protected void requestProtectedResource() {
        if (!validationStarted) {
            validationStarted = true;
            runInBlock("POST payments endpoint using client_credentials token - expects 401", () -> {
                env.mapKey("old_access_token", "authorization_code_access_token");
                callAndStopOnFailure(SaveAccessToken.class);
                eventLog.log("Saved the authorization_code access token", env.getObject("authorization_code_access_token"));
                env.unmapKey("old_access_token");
                callAndStopOnFailure(LoadOldAccessToken.class);
                callAndStopOnFailure(SaveAccessToken.class);
                eventLog.log("Loaded the client_credentials access token", env.getObject("access_token"));

                ConditionSequence pixFailSequence = getPixPaymentSequence()
                    .replace(EnsureResponseCodeWas201.class, condition(EnsureResponseCodeWas401.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .replace(EnsureContentTypeApplicationJwt.class, condition(EnsureContentTypeJson.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .skip(ExtractSignedJwtFromResourceResponse.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseSigningAlg.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseTyp.class, SKIP_MESSAGE)
                    .skip(FetchServerKeys.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseSignature.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseJwtClaims.class, SKIP_MESSAGE);
                call(pixFailSequence);
            });

            runInBlock("POST payments endpoint using authorization_code token - expects 201", () -> {
                loadAuthorizationCode();
                ConditionSequence pixSequence = getPixPaymentSequence();
                call(pixSequence);
                callAndStopOnFailure(SaveOldValues.class);
                validateResponse();
            });

            runInBlock("GET payments endpoint using authorization_code token - expects 401", () -> {
                loadAuthorizationCode();
                ConditionSequence getPixSequence = new CallGetPaymentEndpointSequence()
                    .replace(EnsureResponseCodeWas200.class, condition(EnsureResponseCodeWas401.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .replace(EnsureContentTypeApplicationJwt.class, condition(EnsureContentTypeJson.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .skip(ExtractSignedJwtFromResourceResponse.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseSigningAlg.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseTyp.class, SKIP_MESSAGE)
                    .skip(FetchServerKeys.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseSignature.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseJwtClaims.class, SKIP_MESSAGE)
                    .skip(PaymentInitiationPixPaymentsValidatorV2.class, "Not a 200 response")
                    .skip(ValidateMetaOnlyRequestDateTime.class, "Not a 200 response");
                call(getPixSequence);
            });

            runInBlock("PATCH payments endpoint using authorization_code token - expects 401", () -> {
                loadAuthorizationCode();
                callAndStopOnFailure(CreatePatchPaymentsRequestFromConsentRequestV2.class);
                callAndStopOnFailure(LoadOldValues.class);
                ConditionSequence patchPixSequence = new CallPatchPixPaymentsEndpointSequenceV2()
                    .replace(EnsureResponseCodeWas200.class, condition(EnsureResponseCodeWas401.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .replace(EnsureContentTypeApplicationJwt.class, condition(EnsureContentTypeJson.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .skip(ExtractSignedJwtFromResourceResponse.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseSigningAlg.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseTyp.class, SKIP_MESSAGE)
                    .skip(FetchServerKeys.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseSignature.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseJwtClaims.class, SKIP_MESSAGE);
                call(patchPixSequence);
            });

            runInBlock("POST consents endpoint using authorization_code token - expects 401", () -> {
                loadAuthorizationCode();
                callAndStopOnFailure(PrepareToPostConsentRequest.class);
                callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);
                ConditionSequence postPixConsentSequence = new PaymentConsentErrorTestingSequence()
                    .replace(EnsureContentTypeApplicationJwt.class, condition(EnsureContentTypeJson.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE));
                call(postPixConsentSequence);
                callAndContinueOnFailure(EnsureResponseCodeWas401.class, Condition.ConditionResult.FAILURE);
            });
        }
    }

    @Override
    protected void forceGetCallToUseClientCredentialsToken(ConditionCallBuilder builder) {}

    @Override
    protected void validateSelfLink(String responseFull) {
        if (Objects.equals(responseFull, "resource_endpoint_response_full") && env.containsObject("old_access_token")) {
            eventLog.log("Loaded client_credential access token", env.getObject("old_access_token"));
            callAndStopOnFailure(LoadOldAccessToken.class);
            callAndStopOnFailure(SaveAccessToken.class);
        }
        super.validateSelfLink(responseFull);
    }

    protected void loadAuthorizationCode() {
        env.mapKey("old_access_token", "authorization_code_access_token");
        callAndStopOnFailure(LoadOldAccessToken.class);
        callAndStopOnFailure(SaveAccessToken.class);
        eventLog.log("Loaded the authorization_code access token", env.getObject("access_token"));
        env.unmapKey("old_access_token");
    }
}
