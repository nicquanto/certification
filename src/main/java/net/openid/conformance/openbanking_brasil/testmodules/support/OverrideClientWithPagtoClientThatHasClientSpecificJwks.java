package net.openid.conformance.openbanking_brasil.testmodules.support;

public class OverrideClientWithPagtoClientThatHasClientSpecificJwks extends AbstractOverrideClient {
	// "OrganisationId":"39f08c6a-579e-4cbb-9f83-49708902d908",
	// "SoftwareStatementId":"5c116654-c1b2-4c87-a1b2-102043b79b0a",

	@Override
	String clientCert() {
		return "-----BEGIN CERTIFICATE-----\n" +
			"MIIHFjCCBf6gAwIBAgIUXamF295mCtUUCtr6oTs0V6M4co8wDQYJKoZIhvcNAQEL\n" +
			"BQAwcTELMAkGA1UEBhMCQlIxHDAaBgNVBAoTE09wZW4gQmFua2luZyBCcmFzaWwx\n" +
			"FTATBgNVBAsTDE9wZW4gQmFua2luZzEtMCsGA1UEAxMkT3BlbiBCYW5raW5nIFNB\n" +
			"TkRCT1ggSXNzdWluZyBDQSAtIEcxMB4XDTIyMTIwODE3MzEwMFoXDTIzMTIxMTEw\n" +
			"MDAwMFowggExMQswCQYDVQQGEwJCUjELMAkGA1UECBMCU1AxEjAQBgNVBAcTCVNB\n" +
			"TyBQQVVMTzEsMCoGA1UEChMjT3BlbiBCYW5raW5nIEJyYXNpbCAtIFBvcnRhbCAt\n" +
			"IEh5c3QxLTArBgNVBAsTJDM5ZjA4YzZhLTU3OWUtNGNiYi05ZjgzLTQ5NzA4OTAy\n" +
			"ZDkwODEgMB4GA1UEAxMXbW9jay1wYWd0bzIucmFpZGlhbS5jb20xFzAVBgNVBAUT\n" +
			"DjAyODcyMzY5MDAwMTEwMR4wHAYDVQQPExVOb24tQ29tbWVyY2lhbCBFbnRpdHkx\n" +
			"EzARBgsrBgEEAYI3PAIBAxMCQlIxNDAyBgoJkiaJk/IsZAEBEyQ1YzExNjY1NC1j\n" +
			"MWIyLTRjODctYTFiMi0xMDIwNDNiNzliMGEwggEiMA0GCSqGSIb3DQEBAQUAA4IB\n" +
			"DwAwggEKAoIBAQC0zN5SkvPEFsruz9KK3ho8ipycoLSjd3eUiweY0EztKNUwOWZs\n" +
			"U+LqCcZDnErGXMPqsxlOF2E/lPPt6VYwB3TOoFGGVlaJ/7yVKIsdKEJACTkh47pY\n" +
			"TlHmdoExRlrgGQmBSL6esqyx1bKLuc0zg5Bf0qlfTQ80Aax4PxXDXTpj0KK+9wtv\n" +
			"cfFft+ktszLZTrNPVELvIfMgCFaO1OSpOquBvLfhagTBD/NYytGNquLRD3J0+52c\n" +
			"Z3azlRZ+FGE/gq4B8s/vhUOeWmWtnUW/29CVvmR7r5d0BM8km+jtKdAgXX0tM4p1\n" +
			"qfdnN+g3vAHtaN+oBAwkH3Z3l2APhxb8kZT5AgMBAAGjggLiMIIC3jAMBgNVHRMB\n" +
			"Af8EAjAAMB0GA1UdDgQWBBT5eyv+EbnkCRx21/k43LI+qCHL8jAfBgNVHSMEGDAW\n" +
			"gBSGf1itF/WCtk60BbP7sM4RQ99MvjBMBggrBgEFBQcBAQRAMD4wPAYIKwYBBQUH\n" +
			"MAGGMGh0dHA6Ly9vY3NwLnNhbmRib3gucGtpLm9wZW5iYW5raW5nYnJhc2lsLm9y\n" +
			"Zy5icjBLBgNVHR8ERDBCMECgPqA8hjpodHRwOi8vY3JsLnNhbmRib3gucGtpLm9w\n" +
			"ZW5iYW5raW5nYnJhc2lsLm9yZy5ici9pc3N1ZXIuY3JsMCIGA1UdEQQbMBmCF21v\n" +
			"Y2stcGFndG8yLnJhaWRpYW0uY29tMA4GA1UdDwEB/wQEAwIFoDATBgNVHSUEDDAK\n" +
			"BggrBgEFBQcDAjCCAagGA1UdIASCAZ8wggGbMIIBlwYKKwYBBAGDui9kATCCAYcw\n" +
			"ggE2BggrBgEFBQcCAjCCASgMggEkVGhpcyBDZXJ0aWZpY2F0ZSBpcyBzb2xlbHkg\n" +
			"Zm9yIHVzZSB3aXRoIFJhaWRpYW0gU2VydmljZXMgTGltaXRlZCBhbmQgb3RoZXIg\n" +
			"cGFydGljaXBhdGluZyBvcmdhbmlzYXRpb25zIHVzaW5nIFJhaWRpYW0gU2Vydmlj\n" +
			"ZXMgTGltaXRlZHMgVHJ1c3QgRnJhbWV3b3JrIFNlcnZpY2VzLiBJdHMgcmVjZWlw\n" +
			"dCwgcG9zc2Vzc2lvbiBvciB1c2UgY29uc3RpdHV0ZXMgYWNjZXB0YW5jZSBvZiB0\n" +
			"aGUgUmFpZGlhbSBTZXJ2aWNlcyBMdGQgQ2VydGljaWNhdGUgUG9saWN5IGFuZCBy\n" +
			"ZWxhdGVkIGRvY3VtZW50cyB0aGVyZWluLjBLBggrBgEFBQcCARY/aHR0cDovL3Jl\n" +
			"cG9zaXRvcnkuc2FuZGJveC5wa2kub3BlbmJhbmtpbmdicmFzaWwub3JnLmJyL3Bv\n" +
			"bGljaWVzMA0GCSqGSIb3DQEBCwUAA4IBAQAnQ5RHOqHRV75WdM9omiBJmrxDQtkV\n" +
			"RA6XY0br7gAuDHLHLI1OOjDyIvMvafoL5KUwgMiB4xWDJEc4v1y/Ri6nv/jE2fv3\n" +
			"RPvcdABEBfAK3lsWfUmA9VXDBLM0jA8Arti3LdxlheQm5ZZfLOs72TBSUciEsWAX\n" +
			"gioLUHu89mFvDpLB4xdkAGGDKvPNBRj5iAxpOZt3mxSS3KvxLNtwhy2xR0x/gxeH\n" +
			"iLgvTTrYUE6hh39ED4Y96gghrl61cZZU1uIkX+W6CuMZz0VYt8YjC7rmKRCQ9R3d\n" +
			"XNVHI22Beb2EU7wt8T6ltONla3wxSvn5qOnlb/Zzh2hd8ToJ3efWOMmv\n" +
			"-----END CERTIFICATE-----";
	}

	@Override
	String clientKey() {
		return "-----BEGIN PRIVATE KEY-----\n" +
			"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC0zN5SkvPEFsru\n" +
			"z9KK3ho8ipycoLSjd3eUiweY0EztKNUwOWZsU+LqCcZDnErGXMPqsxlOF2E/lPPt\n" +
			"6VYwB3TOoFGGVlaJ/7yVKIsdKEJACTkh47pYTlHmdoExRlrgGQmBSL6esqyx1bKL\n" +
			"uc0zg5Bf0qlfTQ80Aax4PxXDXTpj0KK+9wtvcfFft+ktszLZTrNPVELvIfMgCFaO\n" +
			"1OSpOquBvLfhagTBD/NYytGNquLRD3J0+52cZ3azlRZ+FGE/gq4B8s/vhUOeWmWt\n" +
			"nUW/29CVvmR7r5d0BM8km+jtKdAgXX0tM4p1qfdnN+g3vAHtaN+oBAwkH3Z3l2AP\n" +
			"hxb8kZT5AgMBAAECggEACxC6zpz11Ft8wg7wGNWtD2WAXkmrpJGMPQghR+X/v0gP\n" +
			"fCsXLKwZqC8+9iZboTZOO1AIzXG63vRT1YzBXKEoMfk35Bvmof2H4i0gDoHHRDoM\n" +
			"SBmy7vKkKpG4KHbg2vRNU59LI1VWInAsxEZz26JCtMqk2RNTSeCMWLOrtlA/YZpp\n" +
			"8/CXOAqtQ+951Mj+X+VKLqGBISKV629GSsMcr4VV5EmDR2T75ndZ1uggJeMpGejG\n" +
			"8+a4lTHMtORyCA+wwPTlDcbHg5s7aGCZHLPRZCGfzOyb03914vH/WEvZaj5Ygklq\n" +
			"kUxC8kQ5V0bktk9TwMC50wX5jLAB9utUbF5EfxnicQKBgQDcYrjyWGkVU2Wg+iyj\n" +
			"Q3GgRKS9snK6vgIyTvLfyuBz4IA62E/pkvvOIgx8aIPSEZyQP4NsBAbRHs6Q5/Uw\n" +
			"W9cdMfxlTutIdr+Iw3BpW30ba2WfyD2NkMPpyG1WRS5sF6tlbc58zhXjSzQmUQdu\n" +
			"wr6CjKFm9ePdvr4ufqiAjwpkTQKBgQDSBINWYouuHockwxP8WXF2GjcI/mjyLLfY\n" +
			"rdOhQxDro5JsgsxpuB1xuRNHjLhUpxNY4QzwDYjtJecTwUjxqWu9kwVVOLYgdACh\n" +
			"uvMYAeNGhcf3b0M7EgtUJAEFR25KZmUQ33IO1WS+74OZO2XgEe9SNYCbLoQv7UsZ\n" +
			"NEemvk45XQKBgFwEzHxD9fx1ieWhD6pLqtHkV3TtYYIffANNe5N9viIkBuaLEzLP\n" +
			"sFvtU5FOglvgBqMkIdekBsSiPFW8xF+guTzzdhNG1G65RlfwYqtbC+269f+1OB43\n" +
			"59VhXmW7FRsE9weuxZzO38y7xdaJ1rSkk68m0O2QlcgssHPxZqHnF8uxAoGATeCa\n" +
			"izfzr9dmKb2k9Zh9OFw0OEOOyu7GJbUfmtbxuU9ohpBiGHnLgVYtzgRifhGOXRDm\n" +
			"6UU7lyTM8UhlMieu6hdQjO+AqbwaV96pMifRDV+ibSB2/7qLNOI89qYsQa6Cn6Ub\n" +
			"+GAtAZhzZijeeB5ubj+ktuai4drOVERMpYpAG4kCgYAKZbf4FDWmOXGHWadS4yX2\n" +
			"oiuMk5a+m5Vg3DM6lkxgW4ZMSfmxf1H3PmemqsAaDlMT+mPQBdfnLHv7OppiTrfL\n" +
			"s1apytoFh1RjIWpwWgvOa0ZilanLofsoqyOz+v2ccim7bnM34fOj70DcNIBK6BDS\n" +
			"WPF7ZDb+uFHZgK8KIZG0eQ==\n" +
			"-----END PRIVATE KEY-----\n";
	}

	@Override
	String orgJwks() {
		return "{\n" +
			"  \"keys\": [\n" +
			"    {\n" +
			"      \"kty\": \"RSA\",\n" +
			"      \"alg\": \"PS256\",\n" +
			"      \"kid\": \"zBOcUM3PQ6tc74R_4dNdZbuUOrqJJRfbu9OlzBV8TSY\",\n" +
			"      \"n\": \"ikHmLu-GUggwBhLf-69QIaeHXLfpX1ErMop9YDG0QvsMxnpyF0hVw3_AHCuXAGvy_q_8vr6b8IJmUM_lDgrVAr7nBjGWE8rTddKD9N19Ojk8fJLWbiKzS7vABcXf0sJJIcl1DqLfVWeM0vdmMqodTMrRiBL5ZDkdNSqRAh1FkLo1vdu0rYrD1prKdk5DClvGr0XKO5zUEO-2KSzwTH_ycZ-wV9uEvyVYD_Z6YYxmttEsHNxPsxvvCYx_hHwgaV_WKROM8X5-b4Gj9tH3COm_MQ1iccaRf5B39blKDmTIn7_7ZCv4Q6ERsjT0gjlNZAa-BekWk3FvoEnSrDoAW3JU2Q\",\n" +
			"      \"e\": \"AQAB\",\n" +
			"      \"d\": \"ApDwD-QgEwg2CfrSNPafTwyavCtpYGdI3Q6J1AiqzvPKxtP8J6H5qOAHZbkfu6Ev-K8KtX34tilZcvu02u1JCwdrkXzHS2qVllzYAv9pxQmjGDB_3I9LfXxIjLg27A9xR42uHPLZddlERCCAQVr7zGfW-pwC4dZsjBwpkQeQObwwFDT7IcVhottaO2JncwFI4EdrfafEnUO0-lzHWoENlThUS0NYYrBt3YS67lHh3fOsfDkAboy0gWfc0dLtyF0UlSJGJDzTBLBIoImJA7_QWdOVoUjbkkPvmsbJyyM4wN8yUJVNyWlAewUBNg2fiJ-aYY3vO2jh2lwXjcNibeM77Q\",\n" +
			"      \"p\": \"wQo3XydDKZutZ_aBQWWVis93yd7kzSXntrQwq0jSBr2UgUItkW5x15SBd0Cuoe0hIMZ2P5MFRSxpWnSCTGdKaL3llMRKLDK5hCnxFkbQQYoyOIEvuEO5X1D39YICRX73QAl9u3dhq2QkdeWFOQsLfMWhau3AoPE322fYqPvBCs0\",\n" +
			"      \"q\": \"t1mkgh9z1b0T_ALDKulrvv3I5MtdAF6gMWPidgaXwImsVhZDllKC8hX_gHF99IaUS4w-HUSXT0HrhRhyblmOZbWj8PLT_A3AdsFtWGBqLc63OGsldivujd_c3hQJcWCq4NOLRt83KX8n7eCpktTOE5EiGMP0V8vlfZW72UWhyj0\",\n" +
			"      \"dp\": \"MHrrWxdWM73oN-LdpVnoy5q9H9K9rZPmdKkeS_YW4SB9ilTfctXE-3pNZXC2Ku6N0lhlXCQFP9EeiFwYWS3bryB55vnBEwaONtX9uTWBmeQmJrCzFljT1k9UZrEG9wMi_08i55Dc05lr2rwQ0Dmo4eYUWvFo3kKWX6Dd9dp8KcU\",\n" +
			"      \"dq\": \"c3FN02BaXsmeO57Bo9M0tBy20Nf6xrDNzEtH22hrRB9rEwkRpSRurl6LcSQEWmIiHS9ALM1zN8QZtsOdyT06G3AyuRMrxhgihqNjZbHPKOhvFGbiP3WJzmqVdn7HM0vaS5TmrMj-wnH9ghliq8CxwEAxZ8Z5oo4PAPO2QydEp0E\",\n" +
			"      \"qi\": \"K1Nu3RrzuMmUtJlF2ESQcfwQdSXBlkPc8Epr0OCWUa48d3V0iObMPVxtUFLNXOYoiKSSbOHqgUo1w5eCypGQpWRlUZmwaFDpFHFhxxfH8vdnMQl8WQvEBwAqMT2hTaMxmo5BkStUjupix5lxSTdgz0NjGBvUfnMC3FXdo2u1bnM\"\n" +
			"    }\n" +
			"  ]\n" +
			"}";
	}

	@Override
	String clientJwks() {
		return "{\n" +
			"  \"keys\": [\n" +
			"    {\n" +
			"      \"kty\": \"RSA\",\n" +
			"      \"kid\": \"j0pZ6CWi2LVZUnMFxrEVtJzEQPf8R3y53810w1nPCHk\",\n" +
			"      \"alg\": \"PS256\",\n" +
			"      \"n\": \"uw3GuWSlkk0mxoj9oytKfaP1KI3yiPJJBkvUAeDkDmk3hzhdDPIMaUB6mEfJYTrMax9gdTgJ22CcglvPwFGY2d5VOi5v88qJMPKpxnacM7oGh94fX-gcizCQo9yMv2Uocnfs40weDgXfZfl0AlW61FYTY19eKZW7XpPoqYI9rnAJkXOJkTPw4T1VBS9mhXj0njDHs-AzOzLatiGrUiJt0M9ioLSmqk8UP6T5-IjPFlQKGYvGWL8WoatNChcWD1mVT-TXvfnqjDZRs6zz74abGptfCm-ksa5W-0aqc7Y6YK0Js_iSD82svNmrB88k-q3MTkR4hpvwCMh6AGDz3nv99w\",\n" +
			"      \"e\": \"AQAB\",\n" +
			"      \"d\": \"GKAzrWymr6AgnrqiSb0FTY0sVW56o7TiEEYjXyvwWkVX3iF5fp7PK3wlp66rwHUxPFkhJc1-3rbVZAQaUcNsUCKJLeO3MW1UqnEIEOzEm7q96V1A3Ct-toRqRmhez0POE2Ped_4pZsc3JgG1WClZM2Mxoj-H8gmYZVcrpkVTQYec3HiyjldEbLXglZUURzSfa-sY2X0tham978ulpAkot6jLJw-VExS1UjmcxUSKntYhuhLTXdmtH6gQinYp-seiBeZUhdYMyeYnJzG1swuOzUB71Qw6cOZwjLADfr8FxllJ9PCjaUsnTu2c6urCoTbrn0UkmF-sJMKZx2-a4iMrAQ\",\n" +
			"      \"p\": \"2qQxBEkiXZLAEvMFNN8ysHKrmvMqBJeQiaiY2aYpq057N5wQvKNSohzwMUVJncXxxfjyb2jfU6mT5I2r5oS8yez_EefF2Wz0omwWyGJmpMGJ8WYQgV6HbczZrOUQoR0gUVQbk5QzCCz7fY9Pvw4T8T04tnPWG6DJFBJEvx0IYKE\",\n" +
			"      \"q\": \"2wPh0pKqmEq95vAsCaUPmBAl9814I9-XSnrLefljjW7wRZuGPUOUlgJ0uYq8pO4XepjPD3g11mTFJ5wlRKObWLFWNnZZfjGQslNPhupBBSjI_9CFIb-aOmL8WsKsleZpOY30Pt7drlnW8UhSXwqU9wLH_nArK4HKCLQIb5Inn5c\",\n" +
			"      \"dp\": \"FUO-2LHcO8mYEL--E-RZY7vjYNChl4y-LAVPyGtWxih952ywXAhucwHpgoFApa2o1B5gReGnRtXJYoM84tCqI-F-9Vjbb0gfiuSEWrznSsLgDbBljo-JEG7KBPzKX0Eb8Y0CmZniVLs7Qnz7vpM58U6JA4XEny9GH0OfmA7Uz8E\",\n" +
			"      \"dq\": \"o_b8EVGMNgd-tG4KCg5w5j7wrdw9nV2_PhtASkjSpwfvCa2tiiAWFVgxWtbq8-7r1PShz8sHQ0Kd91GG9SQnIPdiu9NOnJMu6NJGL5MgqmQmVp4djW6MYDnLA4fK_U5KaLRFruvaurS3nluuj0i0zVhfsbT4HNJGFs3xotWgpHE\",\n" +
			"      \"qi\": \"iaVxa6cEtEkhVw4ygW9qLFy4Mosc398vwP3EGUGOG-u4Kix6KTiCpLhH0FrI0yttZA2xRx7uoxLnbvTu88uuoQLh5gQZ6Bv2aa2iVdPV5Me7UhcSMbky9Id-y7IrEhoYx_oOdX4yyHB0w7xFGLqqaKQ5ggZqrLdX1RmY16VsGiI\"\n" +
			"    }\n" +
			"  ]\n" +
			"}";
	}

	@Override
	String role() {
		return "PAGTO (different organization)";
	}

	@Override
	String directoryClientId() {
		return "oyjbJCw6bR7OvqQAwlJWi";
	}
}
