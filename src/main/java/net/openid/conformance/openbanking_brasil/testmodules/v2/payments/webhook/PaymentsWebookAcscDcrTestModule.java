package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "dcr_api_dcm-pagto-webhook-acsc_test-module",
	displayName = "dcr_api_dcm-pagto-webhook-acsc_test-module",
	summary = "Obtain a SSA from the Directory\n" +
		"Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration\n" +
		"Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>”]\n" +
		"Expect a 201 - Validate Response\n" +
		"Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"Set the consents webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>/open-banking/webhook/v1/payments/v2/consents/{consentId}, where the alias is to be obtained from the field alias on the test configuration\n" +
		"Calls POST Consents Endpoint with localInstrument set as DICT\n" +
		"Expects 201 - Validate Response\n" +
		"Redirects the user to authorize the created consent\n" +
		"Call GET Consent\n" +
		"Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"Calls the POST Payments with localInstrument as DICT\n" +
		"Expects 201 - Validate response\n" +
		"Set the payments webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>/open-banking/webhook/v1/payments/v2/pix/payments/{paymentId} where the alias is to be obtained from the field alias on the test configuration\n" +
		"Expect an incoming message, one for each defined endoint, payments and consents, both which  must be mtls protected and can be received on any endpoint at any order - Wait 60 seconds for both messages to be returned\n" +
		"For both webhook calls - Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"Call the Get Payments endpoint with the PaymentID \n" +
		"Expects status returned to be (ACSC) - Validate Response\n" +
		"Call the Delete Registration Endpoint\n" +
		"Expect a 204 - No Content",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.webhookWaitTime"
	}
)
public class PaymentsWebookAcscDcrTestModule extends AbstractBrazilDCRPaymentsWebhook{
}
