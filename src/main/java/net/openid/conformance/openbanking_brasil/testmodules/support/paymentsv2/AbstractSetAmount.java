package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public abstract class AbstractSetAmount extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {

		JsonObject obj = Optional.ofNullable(env.getElementFromObject("resource", String.format("%s.data.payment", getEnvKey())))
			.orElseThrow(() -> error(String.format("Could not extract %s.data.payment", getEnvKey()))).getAsJsonObject();

		String amountString = OIDFJSON.getString(Optional.ofNullable(obj.get("amount"))
			.orElseThrow(() -> error("Could not parse amount", args("obj", obj))));

		if (amountString.equals(getAmount())) {
			logSuccess("Amount specified is the same, nothing to be done", args("amount", amountString));
			return env;
		}

		obj.addProperty("amount", getAmount());
		env.putObject("resource", String.format("%s.data.payment", getEnvKey()), obj);
		logSuccess(String.format("new amount of %s added to %s", getAmount(), getEnvKey()));

		return env;
	}

	protected abstract String getAmount();

	protected abstract String getEnvKey();
}
