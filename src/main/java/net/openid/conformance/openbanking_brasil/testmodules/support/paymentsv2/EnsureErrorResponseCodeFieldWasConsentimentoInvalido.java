package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.CreatePaymentErrorEnumV2;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasConsentimentoInvalido extends AbstractEnsureErrorResponseCodeFieldWas {
	@Override
	protected List<String> getExpectedCodes() {
		return List.of(CreatePaymentErrorEnumV2.CONSENTIMENTO_INVALIDO.toString());
	}
}
