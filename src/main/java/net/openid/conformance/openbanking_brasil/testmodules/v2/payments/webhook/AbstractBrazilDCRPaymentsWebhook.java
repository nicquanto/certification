package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.condition.common.CheckDistinctKeyIdValueInClientJWKs;
import net.openid.conformance.fapi1advancedfinal.AbstractFAPI1AdvancedFinalBrazilDCR;
import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ValidateWebhookTimeStamp;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.WebhookWithConsentsId;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.WebhookWithPaymentId;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAscs;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantConfigurationFields;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;
import java.util.function.Supplier;

@VariantConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.consentUrl",
	"resource.brazilCpf",
	"resource.brazilCnpj",
	"resource.brazilOrganizationId",
	"resource.brazilQrdnPaymentConsent"
})
public abstract class AbstractBrazilDCRPaymentsWebhook extends AbstractFAPI1AdvancedFinalBrazilDCR {
	@Override
	public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
		boolean paymentWebhookReceived = env.getBoolean("payment_webhook_received");
		boolean paymentConsentWebhookReceived = env.getBoolean("payment_consent_webhook_received");
		String consentId = env.getString("consent_id");
		String paymentId = env.getString("payment_id");
		eventLog.log(getName(),path);
		if (path.contains("open-banking/webhook/v1/payments/v2/consents/" + consentId) && !paymentConsentWebhookReceived) {
			env.putBoolean("payment_consent_webhook_received", true);
			validateWebhook(requestParts);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else if (path.contains("webhook/v1/payments/v2/pix/payments/" + paymentId) && !paymentWebhookReceived) {
			env.putBoolean("payment_webhook_received", true);
			validateWebhook(requestParts);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else {
			return super.handleHttpMtls(path, req, res, session, requestParts);
		}
	}

	public void validateWebhook(JsonObject requestParts){
		setStatus(Status.RUNNING);
		env.putObject("webhook_request", requestParts);
		callAndStopOnFailure(EnsureWebhookInteractionId.class);
		callAndStopOnFailure(ValidateWebhookTimeStamp.class);
		setStatus(Status.WAITING);
	}
	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(AddResourceUrlToConfig.class);
		super.setupResourceEndpoint();
	}

	@Override
	protected void configureClient() {
		env.putString("test_start_timestamp", Instant.now().toString());
		callAndStopOnFailure(AddScopeToClientConfigurationFromConsentUrl.class);
		doDcr();
		eventLog.startBlock("Waiting for webhook to be synced");
		callAndStopOnFailure(WaitForSpecifiedTimeFromConfigInSeconds.class);
		eventLog.endBlock();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		env.putBoolean("payment_webhook_received", false);
		env.putBoolean("payment_consent_webhook_received", false);
		callAndStopOnFailure(AddOpenIdScope.class);
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
		setScheduledPaymentDateTime();
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		configureDictInfo();
		super.onConfigure(config, baseUrl);
	}


	protected void setScheduledPaymentDateTime() {

	}
	protected void validateResponse() {
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
		call(getPaymentValidationSequence());
	}
	protected void validatePaymentStatus(){
		callAndStopOnFailure(EnsurePaymentStatusWasAscs.class);
	}
	@Override
	protected void requestProtectedResource() {
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		ConditionSequence pixSequence = new CallPixPaymentsEndpointSequence();
		call(pixSequence);
		eventLog.endBlock();
		eventLog.startBlock(currentClientString() + "Validate response");
		validateResponse();
		eventLog.endBlock();
		createPaymentWebhook();
		waitForPaymentConsentAndPaymentWebhookResponse();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		call(getPayment());
		eventLog.endBlock();
		eventLog.startBlock(currentClientString() + "Validate response");
		validateResponse();
		validatePaymentStatus();
		eventLog.endBlock();
	}
	public void createPaymentWebhook() {
		callAndStopOnFailure(ExtractPaymentId.class);
		callAndStopOnFailure(WebhookWithPaymentId.class);
		exposeEnvString("webhook_uri_payment_id");
	}
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
		callAndStopOnFailure(EnsureProxyPresentInConfig.class);
	}

	protected void waitForPaymentConsentAndPaymentWebhookResponse() {
		long delaySeconds = 5;
		eventLog.startBlock(currentClientString() + "The test will now wait for the webhook to be received");
		for (int attempts = 0; attempts < 12; attempts++) {
			eventLog.log(getId(), "Waiting for webhook to be received at with an interval of " + delaySeconds + " seconds");
			setStatus(Status.WAITING);
			try {
				Thread.sleep(delaySeconds * 1000);
			} catch (InterruptedException e) {
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}
			setStatus(Status.RUNNING);
			boolean webhookReceived = env.getBoolean("payment_webhook_received");
			boolean paymentConsentWebookReceived = env.getBoolean("payment_consent_webhook_received");
			if (webhookReceived && paymentConsentWebookReceived) {
				eventLog.endBlock();
				return;
			}
		}
		throw new TestFailureException(getId(), "Webhook not received in time");
	}

	protected ConditionSequence getPaymentValidationSequence(){
		return sequenceOf(
			condition(PaymentInitiationPixPaymentsValidatorV2.class)
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE),
			condition(ValidateMetaOnlyRequestDateTime.class)
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
	}

	protected ConditionSequenceRepeater repeatSequence(Supplier<ConditionSequence> conditionSequenceSupplier) {
		return new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager, conditionSequenceSupplier);
	}

	protected ConditionSequence getPayment() {
		return new ValidateSelfEndpoint()
			.replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeader.class),
				condition(CallProtectedResource.class)
			))
			.skip(ValidateResponseMetaData.class, "Will be checked Later")
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values")
			.skip(ValidateResponseMetaData.class, "Will be checked Later");
	}
	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		validateConsentResponse();
		createConsentWebhook();
	}
	protected void validateGetConsentResponse() {
		runInBlock("Validate GET Consent Response", this::validateConsentResponse);
	}
	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckAuthorisedStatus();
			validateGetConsentResponse();
		super.performPostAuthorizationFlow();
	}
	protected void fetchConsentToCheckAuthorisedStatus() {
		eventLog.startBlock("Checking the created consent - Expecting AUTHORISED status");
		callAndStopOnFailure(PaymentConsentIdExtractor.class);
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAuthorised.class, Condition.ConditionResult.WARNING);
		eventLog.endBlock();
	}
	protected void validateConsentResponse() {
		callAndContinueOnFailure(PaymentConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
		env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);
	}

	public void createConsentWebhook(){
		callAndStopOnFailure(WebhookWithConsentsId.class);
		exposeEnvString("webhook_uri_consent_id");
	}
	@Override
	protected void getSsa() {
		super.getSsa();
		validateWebhookOnSoftwareStatement();
	}

	protected void validateWebhookOnSoftwareStatement(){
		callAndContinueOnFailure(EnsureSoftwareStatementContainsWebhook.class);
	}
	@Override
	protected void logFinalEnv(){

	}

	protected void doDcr(){
		ClientAuthType clientAuthType = getVariant(ClientAuthType.class);
		String clientAuth = clientAuthType.toString();
		if (clientAuthType == ClientAuthType.MTLS) {
			// the FAPI auth type variant doesn't use the name required for the registration request as per
			// https://datatracker.ietf.org/doc/html/rfc8705#section-2.1.1
			clientAuth = "tls_client_auth";
		}
		env.putString("client_auth_type", clientAuth);

		callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
		callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);

		// normally our DCR tests create a key on the fly to use, but in this case the key has to be registered
		// manually with the central directory so we must use user supplied keys
		callAndStopOnFailure(ExtractJWKSDirectFromClientConfiguration.class);

		callAndContinueOnFailure(CheckDistinctKeyIdValueInClientJWKs.class, Condition.ConditionResult.FAILURE, "RFC7517-4.5");

		getSsa();

		eventLog.startBlock("Perform Dynamic Client Registration");

		callAndStopOnFailure(StoreOriginalClientConfiguration.class);
		callAndStopOnFailure(ExtractClientNameFromStoredConfig.class);

		setupJwksUri();

		// create basic dynamic registration request
		callAndStopOnFailure(CreateEmptyDynamicRegistrationRequest.class);

		callAndStopOnFailure(AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest.class);
		if (!jarm.isTrue()) {
			// implicit is only required when id_token is returned in frontchannel
			callAndStopOnFailure(AddImplicitGrantTypeToDynamicRegistrationRequest.class);
		}
		callAndStopOnFailure(AddRefreshTokenGrantTypeToDynamicRegistrationRequest.class);
		callAndStopOnFailure(AddClientCredentialsGrantTypeToDynamicRegistrationRequest.class);

		if (clientAuthType == ClientAuthType.MTLS) {
			callAndStopOnFailure(AddTlsClientAuthSubjectDnToDynamicRegistrationRequest.class);
		}

		addJwksToRequest();
		callAndStopOnFailure(AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment.class);
		if (jarm.isTrue()) {
			callAndStopOnFailure(SetResponseTypeCodeInDynamicRegistrationRequest.class);
		} else {
			callAndStopOnFailure(SetResponseTypeCodeIdTokenInDynamicRegistrationRequest.class);
		}
		validateSsa();
		callAndStopOnFailure(AddRedirectUriToDynamicRegistrationRequest.class);

		addSoftwareStatementToRegistrationRequest();

		addWebhookUrisToDynamicRegistrationRequest();

		callRegistrationEndpoint();
	}

	protected void addWebhookUrisToDynamicRegistrationRequest() {
		callAndContinueOnFailure(AddWebhookUrisToDynamicRegistrationRequest.class);
	}

	protected void callClientConfigurationEndpoint() {

		callAndStopOnFailure(CallClientConfigurationEndpoint.class);
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentTypeHttpStatus200.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentType.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckClientIdFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckRedirectUrisFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationUriFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
	}

	protected void runInBlock(String blockText, Runnable actor) {
		eventLog.startBlock(blockText);
		actor.run();
		eventLog.endBlock();
	}
}
