package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.testmodule.Environment;
/**
 * @deprecated
 * proxy is added by default in {@link AddPaymentConsentRequestBodyToConfig}
 */
@Deprecated

public class SetProxyToRealEmailAddressOnPaymentConsent extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject obj = env.getObject("resource");
		obj = obj.getAsJsonObject("brazilPaymentConsent");
		obj = obj.getAsJsonObject("data");
		obj = obj.getAsJsonObject("payment");
		obj = obj.getAsJsonObject("details");
		obj.addProperty("proxy", DictHomologKeys.PROXY_EMAIL);

		logSuccess("Added email address as proxy to payment consent");

		return env;
	}

}
