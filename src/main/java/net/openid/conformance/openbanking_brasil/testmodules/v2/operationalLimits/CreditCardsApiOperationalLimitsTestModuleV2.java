package net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CardAccountsDataResponseResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CardIdentificationResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CreditCardAccountsLimitsResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CreditCardAccountsTransactionCurrentResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CreditCardAccountsTransactionResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.creditCard.v2.CreditCardBillValidatorV2;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.ExtractSpecifiedResourceApiAccountIdsFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingCurrentAccountTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardBills;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardLimits;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCreditCardAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.ValidateMetaOnlyRequestDateTimeDeprecated;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsAndResourcesEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@PublishTestModule(
	testName = "credit-cards_api_operational-limits_test-module_v2",
	displayName = "Credit Cards API Operational Limits test module V2 ",
	summary = "This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Credit Cards API are considered correctly and, if present, that the pagination-key parameter is correctly serving it’s function\n" +
		"This test will require the user to have set at least one ACTIVE resource each with at least 20 Transactions to be returned on the transactions endpoint. The tested resource must be sent on the field FirstCreditCardAccountId (R_1) on the test configuration and on SecondCreditCardAccountId (R_2), in case the company supports multiple accounts for the same user. If the server does not provide the SecondCreditCardAccountId (R_2) the test will skip this part of the test" +
		"This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Credit Cards API are considered correctly and, if present, that the pagination-key parameter is correctly serving it’s function\n" +
		"\u2022 Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL), the CPF for Operational Limits (CPF for OL) and at least the FirstCreditCardAccountId (R_1) have been provided\n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Credit Cards permission group - Expect Server to return a 201 - Save ConsentID (1)\n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards API with the saved Resource ID (R_1) 4 Times  - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Transactions API with the saved Resource ID (R_1) 4 Times, send query parameters fromBookingDate as D-6 and toBookingDate as Today - Expect a 200 response - On the first API Call Make Sure That at least 20 Transactions have been returned \n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Bills API with the saved Resource ID (R_1) 30 Times  - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Limits API with the saved Resource ID (R_1) 240 Times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Transactions-Current API with the saved Resource ID (R_1) 240 Times  - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Transactions-Current API with the saved Resource ID (R_1) Once, send query parameters fromBookingDate as D-6 and toBookingDate as Today, with page-size=1 - Expect a 200 response - Fetch the links.next URI\n" +
		"\u2022 Call the GET Credit Cards Transactions-Current API 19 more times, always using the links.next returned and always forcing the page-size to be equal to 1 - Expect a 200 on every single call\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards API with the saved Resource ID (R_2) once - Expect a 200 response\n" +
		"\u2022 If the field SecondCreditCardAccountId (R_2) has been returned, repeat  the exact same process done with the first tested resources (R_1) but now, execute it against the second returned Resource (R_2) \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.participants",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness",
		"resource.first_credit_card_account_id",
		"resource.second_credit_card_account_id"
	}
)
public class CreditCardsApiOperationalLimitsTestModuleV2 extends AbstractOperationalLimitsTestModule {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	private static final int NUMBER_OF_IDS_TO_FETCH = 2;
	private static final String API_RESOURCE_ID = "creditCardAccountId";

	private static final int REQUIRED_NUMBER_OF_RECORDS = 20;


	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
		call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class ,condition(ValidateConsentsEndpoints.class)));
		callAndStopOnFailure(BuildCreditCardsAccountsConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		env.putString("api_type", EnumResourcesType.CREDIT_CARD_ACCOUNT.name());
		callAndContinueOnFailure(ExtractSpecifiedResourceApiAccountIdsFromConfig.class, Condition.ConditionResult.INFO);
		callAndStopOnFailure(AddCreditCardScopes.class);
		callAndStopOnFailure(PrepareAllCreditCardRelatedConsentsForHappyPathTest.class);
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class);
		clientAuthType = getVariant(ClientAuthType.class);
	}

	@Override
	protected void validateResponse() {
		// Not used in this test module
	}

	@Override
	protected void requestProtectedResource() {

		JsonArray fetchedApiIds = prepareApiIds();

		for (int i = 0; i < NUMBER_OF_IDS_TO_FETCH; i++) {
			int currentResourceId = i + 1;

			// If during the second resource test part, fetchedApiIds size is less than numberOfIdsToFetch it means that the second account ID was not provided by the client or was not fetched.
			// In that case we assume that the multiple accounts for the same users feature is not supported, therefore we skip the part of the test with the second resource ID.
			if (currentResourceId == 2 && fetchedApiIds.size() < NUMBER_OF_IDS_TO_FETCH) {
				eventLog.log(getName(), "Could not find second credit card account ID. Skipping part of the test with the second resource ID");
				continue;
			}
			JsonElement creditCardAccountIdElement = fetchedApiIds.get(i);

			String creditCardAccountId = OIDFJSON.getString(creditCardAccountIdElement);
			// Call to credit card account GET once with validation
			runInLoggingBlock(() -> {
				env.putString("accountId", creditCardAccountId);
				callAndStopOnFailure(PrepareUrlForFetchingCreditCardAccount.class);

				preCallProtectedResource(String.format("Fetching Credit Card Account using resource_id_%d", currentResourceId));
				validateResponse("Validate Credit Card Account response", CardIdentificationResponseValidatorV2.class);
			});


			// Call to credit card account GET 3 times
			for (int j = 1; j < 4; j++) {
				preCallProtectedResource(String.format("[%d] Fetching Credit Card Account using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("CC Account [%d]", currentResourceId));

			// Call to credit card transactions  with dates GET once with validation
			LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

			runInLoggingBlock(() -> {
				env.putString("fromTransactionDate", currentDate.minusDays(6).format(FORMATTER));
				env.putString("toTransactionDate", currentDate.format(FORMATTER));

				callAndStopOnFailure(PrepareUrlForFetchingCardTransactions.class);
				callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);


				env.putInteger("required_number_of_records", REQUIRED_NUMBER_OF_RECORDS);

				preCallProtectedResource(String.format("Fetch Credit Card Transactions using resource_id_%d", currentResourceId));
				validateResponse("Validate Credit Card Transactions Response", CreditCardAccountsTransactionResponseValidatorV2.class, ValidateMetaOnlyRequestDateTimeDeprecated.class);
				callAndStopOnFailure(EnsureAtLeastSpecifiedNumberOfRecordsWereReturned.class);
			});


			// Call to credit card transactions  with dates GET 3 times
			for (int j = 1; j < 4; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Credit Card Transactions using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("CC Trans. [%d]", currentResourceId));


			// Call to credit card bills GET Once with Validation
			runInLoggingBlock(() -> {
				env.putString("fromDueDate", currentDate.minusDays(6).format(FORMATTER));
				env.putString("toDueDate", currentDate.format(FORMATTER));
				callAndStopOnFailure(AddToAndFromDueDateParametersToProtectedResourceUrl.class);
				callAndStopOnFailure(PrepareUrlForFetchingCardBills.class);
				preCallProtectedResource(String.format("Fetch Credit Card Bills using resource_id_%d", currentResourceId));
				validateResponse("Validate Credit Card Bills Response", CreditCardBillValidatorV2.class);
			});

			// Call to credit card bills GET 29 times
			for (int j = 1; j < 30; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Credit Card Bills using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("CC Bills [%d]", currentResourceId));


			// Call to credit card limits GET once with validation

			runInLoggingBlock(() -> {
				callAndStopOnFailure(PrepareUrlForFetchingCardLimits.class);

				preCallProtectedResource(String.format("Fetch Credit Card Limits using resource_id_%d", currentResourceId));
				validateResponse("Validate Credit Card Limits Response", CreditCardAccountsLimitsResponseValidatorV2.class);

			});

			// Call to credit card limits GET 239 times
			for (int j = 1; j < 240; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Credit Card Limits using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("CC Limits [%d]", currentResourceId));

			// Call to credit card current transactions GET once with validation
			runInLoggingBlock(() -> {
				callAndStopOnFailure(PrepareUrlForFetchingCurrentAccountTransactions.class);

				preCallProtectedResource(String.format("Fetch Credit Card Transactions Current using resource_id_%d", currentResourceId));
				validateResponse("Validate Credit Card Transactions Current Response", CreditCardAccountsTransactionCurrentResponseValidatorV2.class, ValidateMetaOnlyRequestDateTimeDeprecated.class);

			});
			// Call to credit card current transactions GET 239 times
			for (int j = 1; j < 240; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Credit Card Transactions Current using resource_id_%d", j + 1, currentResourceId));
			}


			// Call to credit card current transactions with dates and page size fetched from next once with validation
			runInLoggingBlock(() -> {
				env.putString("fromTransactionDate", currentDate.minusDays(6).format(FORMATTER));
				env.putString("toTransactionDate", currentDate.format(FORMATTER));

				callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
				env.putInteger("required_page_size", 1);
				callAndStopOnFailure(AddSpecifiedPageSizeParameterToProtectedResourceUrl.class);

				preCallProtectedResource(String.format("Fetch Credit Card Transactions Current next link using resource_id_%d", currentResourceId));
				validateResponse("Validate Credit Card Transactions Current Response", CreditCardAccountsTransactionCurrentResponseValidatorV2.class, ValidateMetaOnlyRequestDateTimeDeprecated.class);
				callAndStopOnFailure(ValidateNumberOfRecordsPage1.class);
				validateNextLinkResponse();
			});

			// Call to credit card current transactions with dates and page size fetched from next link 19 times GET
			for (int j = 1; j < REQUIRED_NUMBER_OF_RECORDS; j++) {
				String message = String.format("[%d] Fetch Credit Card Transactions Current next link using resource_id_%d", j + 1, currentResourceId);
				eventLog.startBlock(message);

				preCallProtectedResource();
				if (j == 1) {
					callAndStopOnFailure(ValidateNumberOfRecordsPage2.class);
				}
				validateNextLinkResponse();

				eventLog.endBlock();
			}

			makeOverOlCall(String.format("CC CurTr [%d]", currentResourceId));


			enableLogging();
		}

	}

	private JsonArray prepareApiIds() {
		JsonArray fetchedApiIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");

		// IF no resource IDs were provided in the config, we will fetch instead
		if (fetchedApiIds.isEmpty()) {

			preCallProtectedResource("Fetching Credit Card Accounts");
			// Validate credit card response
			runInBlock("Validating Credit Card response", () -> {
				call(getValidationSequence(CardAccountsDataResponseResponseValidatorV2.class, ValidateResponseMetaData.class));

				env.putString("apiIdName", API_RESOURCE_ID);
				callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);

				JsonArray extractedApiIds = env.getObject("extracted_api_ids").getAsJsonArray("extractedApiIds");

				if (extractedApiIds.size() >= NUMBER_OF_IDS_TO_FETCH) {
					env.putInteger("number_of_ids_to_fetch", NUMBER_OF_IDS_TO_FETCH);
				} else {
					env.putInteger("number_of_ids_to_fetch", 1);
				}

				callAndStopOnFailure(FetchSpecifiedNumberOfExtractedApiIds.class);
			});

			fetchedApiIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");
		}
		return fetchedApiIds;
	}


	private void validateNextLinkResponse() {

		callAndStopOnFailure(EnsureOnlyOneRecordWasReturned.class);
		callAndStopOnFailure(ExtractNextLink.class);

		env.putString("value", "1");
		env.putString("parameter", "page-size");
		callAndStopOnFailure(SetSpecifiedValueToSpecifiedUrlParameter.class);
		env.putString("protected_resource_url", env.getString("extracted_link"));

	}

	protected void validateResponse(String message, Class<? extends Condition> validationClass) {
		runInBlock(message, () -> call(getValidationSequence(validationClass, ValidateResponseMetaData.class)));
	}

	protected void validateResponse(String message, Class<? extends Condition> validationClass,
									Class<? extends Condition> metaValidatorClass) {
		runInBlock(message, () -> call(getValidationSequence(validationClass, metaValidatorClass)));
	}

	protected ConditionSequence getValidationSequence(Class<? extends Condition> validationClass,
													  Class<? extends Condition> metaValidatorClass) {
		return sequenceOf(
			condition(validationClass),
			condition(metaValidatorClass)
		);
	}

}
