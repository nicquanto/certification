package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractSetInvestmentApi extends AbstractCondition {

    protected abstract String investmentApi();

    @Override
    public Environment evaluate(Environment env) {
        env.putString("investment-api", investmentApi());
        return env;
    }
}
