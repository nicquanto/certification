package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EnsureDataArrayIsEmpty extends AbstractCondition {
	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {

		JsonObject body = JsonParser.parseString(env.getString("resource_endpoint_response_full", "body"))
			.getAsJsonObject();

		if(!body.has("data")) {
			throw error("Data object not found in response body.", args("body", body));
		}

		JsonElement dataElem = body.get("data");
		if (dataElem.isJsonNull()) {
			throw error("Data object cannot be null.", args("body", body));
		}

		JsonArray dataArray = body.getAsJsonArray("data");
		if(!dataArray.isEmpty()) {
			throw error("Data array should be empty, but it was not.", args("data", dataArray));
		}

		logSuccess("Data array is empty.");
		return env;
	}
}
