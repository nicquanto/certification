package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class EnsureWebhookInteractionId extends AbstractCondition {
	@Override
	@PreEnvironment(required = "webhook_request")
	public Environment evaluate(Environment env) {
		JsonObject webhookRequest = env.getObject("webhook_request");
		JsonObject webhookHeader = webhookRequest.getAsJsonObject("headers");
		String interactionId = OIDFJSON.getString(webhookHeader.get("x-webhook-interaction-id"));
		if (interactionId != null) {
			logSuccess("Webhook interaction id found", args("interaction_id", interactionId));
		} else {
			throw error("Webhook interaction id not found");
		}
		return env;
	}
}
