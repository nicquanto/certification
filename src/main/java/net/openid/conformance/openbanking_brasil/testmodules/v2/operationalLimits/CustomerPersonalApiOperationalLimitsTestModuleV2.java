package net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.registrationData.v2.PersonalIdentificationResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.registrationData.v2.PersonalQualificationResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.registrationData.v2.PersonalRelationsResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.AddScopesForCustomerApi;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareAllCustomerPersonalRelatedConsentsForHappyPathTest;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalFinancialRelationships;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsAndResourcesEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "customer-personal_api_operational-limits_test-module_v2",
	displayName = "Make sure that the server is not blocking access to the APIs as long as the operational limits for the Customer Personal API are considered correctly",
	summary =	"This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Customer Personal API are considered correctly.\n" +
		"\u2022 Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test have been provided\n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Customer Personal permission group - Expect Server to return a 201 - Save ConsentID (1)\n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 With the authorized consent id (1), call the GET Customer Personal Identifications API 4 times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Customer Personal Qualifications 4 times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Customer Personal Financial Relations 4 times - Expect a 200 response\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.participants",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational"
	}
)
public class CustomerPersonalApiOperationalLimitsTestModuleV2 extends AbstractOperationalLimitsTestModule {


	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
		call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class ,condition(ValidateConsentsEndpoints.class)));
		callAndContinueOnFailure(BuildPersonalCustomersConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(AddScopesForCustomerApi.class);
		callAndStopOnFailure(PrepareAllCustomerPersonalRelatedConsentsForHappyPathTest.class);
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class);
	}

	@Override
	protected void validateResponse() {
		// Validate Personal Identification response
		callAndStopOnFailure(PersonalIdentificationResponseValidatorV2.class);
		callAndStopOnFailure(ValidateResponseMetaData.class);

		eventLog.endBlock();
		disableLogging();
		// Call Personal Identification 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Personal Identification Endpoint", i + 1));
		}

		makeOverOlCall("Identification");

		// Call Personal Qualifications once with validation
		runInLoggingBlock(() -> {
			callAndStopOnFailure(PrepareToGetPersonalQualifications.class);

			preCallProtectedResource("Calling Personal Qualifications Endpoint");
			validateResponse("Validate Personal Qualifications response", PersonalQualificationResponseValidatorV2.class);
		});


		// Call Personal Qualifications 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Personal Qualifications Endpoint", i + 1));
		}

		makeOverOlCall("Qualifications");

		// Call Customer Personal Financial Relations once with validation
		runInLoggingBlock(() -> {
			callAndStopOnFailure(PrepareToGetPersonalFinancialRelationships.class);

			preCallProtectedResource("Calling Customer Personal Financial Relations Endpoint");
			validateResponse("Validate Customer Personal Financial Relations response", PersonalRelationsResponseValidatorV2.class);
		});


		// Call Customer Personal Financial Relations 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Customer Personal Financial Relations Endpoint", i + 1));
		}

		makeOverOlCall("Fin Relations");

	}

	private void validateResponse(String message, Class<? extends Condition> validator) {
		runInBlock(message, () -> {
			callAndStopOnFailure(validator);
			callAndStopOnFailure(ValidateResponseMetaData.class);
		});
	}

}
