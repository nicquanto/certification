package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PatchPaymentCancellationReasonEnumV2;

import java.util.List;

public class EnsureCancellationReasonWasCanceladoPendencia extends AbstractEnsureCancellationReasonWasX {

    @Override
    protected List<String> getExpectedCancellationReasons() {
        return List.of(PatchPaymentCancellationReasonEnumV2.CANCELADO_PENDENCIA.toString());
    }
}
