package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasSIorVAL;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasSIorVAL;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToOutOfRangeOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToOutOfRangeOnPaymentInitiation;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_nofunds_test-module_v2",
	displayName = "Ensure a payment with an amount of 9999999999.99 will lead to a RJCT Payment with SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE error",
	summary = "Ensure a payment with an amount of 9999999999.99 will lead to a RJCT Payment with SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE error\n" +
		"\u2022 Creates consent request_body with valid email proxy (cliente-a00001@pix.bcb.gov.br) and it’s standardized payload, however set the amount to be 9999999999.99\n" +
		"\u2022 Call the POST Consents API \n" +
		"\u2022 Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"\u2022 Redirects the user to authorize the created consent - Expect Successful Authorization\n" +
		"\u2022 Calls the POST Payments Endpoint\n" +
		"\u2022 Expects either a 201 or a 422 with SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE response - Validate Response_Body\n" +
		"If a 201 is returned, Poll the Get Payments endpoint with the PaymentID Created\n" +
		"\u2022 End Polling when payment response with RJCT status and RejectionReason equal to SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE is returned\n" +
		"\u2022 Validate Error message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsNoFundsTestModuleV2 extends AbstractPaymentUnhappyPathTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SetPaymentAmountToOutOfRangeOnConsent.class);
		callAndStopOnFailure(SetPaymentAmountToOutOfRangeOnPaymentInitiation.class);
	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasSIorVAL.class);
	}

	@Override
	protected void validate422ErrorResponseCode() {
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasSIorVAL.class);
	}

	@Override
	protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		return EnsureResourceResponseCodeWas201Or422.class;
	}

}
