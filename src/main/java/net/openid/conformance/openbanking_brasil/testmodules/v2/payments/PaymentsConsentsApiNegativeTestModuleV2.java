package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectBADPaymentType;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectInvalidPersonType;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetConsentDateInPast;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.SetInvalidPaymentCurrency;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDataPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreateConsentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;

import java.util.Optional;

@PublishTestModule(
	testName = "payments_api_consents_negative_test-module_v2",
	displayName = "Ensure an error status in sent on the POST Payments API in  different inconsistent scenarios",
	summary = "Ensure an error status in sent on the POST Payments API in  different inconsistent scenarios" +
		"\u2022 Ensure error when Payment Type is \"INVALID\"\n" +
		"Calls POST Consents Endpoint with payment type as \"INVALID\"\n" +
		"Expects 422 PARAMETRO_INVALIDO - validate error message\n" +
		"2. Ensure error when an invalid person type is sent\n" +
		"Calls POST Consents Endpoint with person type as \"INVALID\"\n" +
		"Expects 422 PARAMETRO_INVALIDO - Validate error message\n" +
		"3. Ensure error when invalid currency is sent\n" +
		"Calls POST Consents Endpoint with currency as \"ZZZ\"\n" +
		"Expects 422 PARAMETRO_INVALIDO  - Validate error message\n" +
		"4. Ensure error when invalid date is sent\n" +
		"Calls POST Consents Endpoint with past date value\n" +
		"Expects 422 DATA_PAGAMENTO_INVALIDA - Validate error message\n" +
		"5. Ensure POST Consents is successful when valid payload is sent\n" +
		"Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"Expects 201\n" +
		"Redirects the user to authorize the created consent- Call GET Consent\n" +
		"Expects 200 - Validate if status is \"AUTHORISED\"",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiNegativeTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		// Not needed in this test
	}

	@Override
	protected void createAuthorizationCodeRequest() {
		// Not needed in this test
	}

	@Override
	protected void requestAuthorizationCode() {
		// Not needed in this test
	}

	@Override
	protected void requestProtectedResource() {
		// Not needed in this test
	}

	@Override
	public void start() {
		setStatus(Status.RUNNING);

		saveBrazilPaymentConsent();

		call(exec().startBlock("Ensure error when Payment Type is \"INVALID\""));
		executeUnhappyPath(SelectBADPaymentType.class, EnsureErrorResponseCodeFieldWasParametroInvalido.class);

		call(exec().startBlock("Ensure error when an invalid person type is sent"));
		executeUnhappyPath(SelectInvalidPersonType.class, EnsureErrorResponseCodeFieldWasParametroInvalido.class);

		call(exec().startBlock("Ensure error when invalid currency is sent"));
		executeUnhappyPath(SetInvalidPaymentCurrency.class, EnsureErrorResponseCodeFieldWasParametroInvalido.class);

		call(exec().startBlock("Ensure error when invalid date is sent"));
		executeUnhappyPath(SetConsentDateInPast.class, EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);

		call(exec().startBlock("Ensure POST Consents is successful when valid payload is sent"));
		restoreBrazilPaymentConsent();
		performAuthorizationFlow();
	}

	protected void executeUnhappyPath(Class<? extends Condition> selectWrongType, Class<? extends Condition> ensureError) {
		restoreBrazilPaymentConsent();
		callAndStopOnFailure(selectWrongType);
		call(postConsentSequence()
			.replace(EnsureHttpStatusCodeIs201.class, condition(EnsureConsentResponseCodeWas422.class)));
		validateErrorMessage();
		callAndContinueOnFailure(ensureError, Condition.ConditionResult.FAILURE);
	}


	protected ConditionSequence postConsentSequence() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.skip(ExtractConsentIdFromConsentEndpointResponse.class, "Not needed in this test")
			.skip(CheckForFAPIInteractionIdInResourceResponse.class, "Not needed in this test")
			.skip(FAPIBrazilAddConsentIdToClientScope.class, "Not needed in this test");
	}

	protected void saveBrazilPaymentConsent() {
		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not extract brazilPaymentConsent")).getAsJsonObject();

		env.putObject("resource", "brazilPaymentConsentSave", brazilPaymentConsent.deepCopy());
	}

	protected void restoreBrazilPaymentConsent() {
		JsonObject brazilPaymentConsentSave = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsentSave"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not extract brazilPaymentConsentSave")).getAsJsonObject();

		env.putObject("resource", "brazilPaymentConsent", brazilPaymentConsentSave.deepCopy());
	}

	protected void validateErrorMessage() {
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
		callAndContinueOnFailure(CreateConsentErrorValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
	}


}
