package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JWTUtil;

import java.text.ParseException;
import java.time.Instant;

public class ValidateWebhookTimeStamp extends AbstractCondition {
	@Override
	@PreEnvironment(required = "webhook_request", strings = "test_start_timestamp")
	public Environment evaluate(Environment env) {
		JsonObject webhookRequest = env.getObject("webhook_request");
		String webhookBody = OIDFJSON.getString(webhookRequest.get("body"));
		JsonObject decodedJwt;
		try {
			decodedJwt = JWTUtil.jwtStringToJsonObjectForEnvironment(webhookBody);
		}  catch (ParseException e) {
			throw error("Couldn't parse response as JWT", e, args("body", webhookBody));
		}
		JsonObject dataObject = decodedJwt.getAsJsonObject("claims").getAsJsonObject("data");
		String timestamp = OIDFJSON.getString(dataObject.get("timestamp"));
		Instant currentInstant = Instant.now();
		Instant responseTimeStamp = Instant.parse(timestamp);
		String testStartTimestamp = env.getString("test_start_timestamp");
		Instant testStartInstant = Instant.parse(testStartTimestamp);
		if (responseTimeStamp.isBefore(testStartInstant) || responseTimeStamp.isAfter(currentInstant)) {
			throw error("Webhook timestamp is not within the test start and current time", args("timestamp", timestamp, "test_start_timestamp", testStartTimestamp, "current_timestamp", currentInstant.toString()));
		}
		logSuccess("Webhook timestamp is within the test start and current time", args("timestamp", timestamp));
		return env;
	}
}

