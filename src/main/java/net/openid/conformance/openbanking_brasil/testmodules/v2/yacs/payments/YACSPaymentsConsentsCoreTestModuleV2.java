package net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.*;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments-consents-core-test-v2",
	displayName = "The test will make the user authorize a payment consent request on his home app, changing payment status to AUTHORISED, however a payment won’t be concluded leaving the AUTHORISED consent to be expired. ",
	summary = "Obtain the payments-consents endpoint for the server from the directory using the directory participants API and the provided discoveryUrl\n" +
		"\n" +
		"Create the POST payments-consents payload using the provided brazilCpf and, if present, brazilCnpj. Set all the other fields for the standard Production Creditor Account\n" +
		"\n" +
		"Call the POST Token endpoint with the selected authorization method sending scope=payments and grant as client_credentials\n" +
		"\n" +
		"Call the POST payments-consents endpoint with the obtaine Bearer token and the generated payload\n" +
		"\n" +
		"Expects a 201 - Validate that status is “AWAITING_AUTHORISATION”. Validate that all fields all compliant with specifications, including the meta object and that no additional fields were sent\n" +
		"\n" +
		"Redirect the user to authorize his Consent\n" +
		"\n" +
		"Expect an authorization code to be returned\n" +
		"\n" +
		"Call the POST Token endpoint with authorization_code grant and the returned authorization code from the redirect\n" +
		"\n" +
		"Validate that a Token was created and that a refresh token is included\n" +
		"\n" +
		"Call the POST Token endpoint with the refresh_token grant \n" +
		"\n" +
		"Validate that a new Token was created and that the refresh_token has not been rotated",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.brazilCnpj",
		"resource.consentUrl"
	}
)
public class YACSPaymentsConsentsCoreTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {
	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(ExtractAndAddRefreshTokenToEnv.class);
		call(new RefreshTokenRequestSteps(false,addTokenEndpointClientAuthentication,false));
		callAndStopOnFailure(EnsureRefreshTokenNotRotated.class);
	}

	@Override
	protected void configureDictInfo() {
		// Not needed in this test
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(SetScopeInClientConfigurationToOpenId.class);
		super.validateClientConfiguration();
	}
	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndStopOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
	}
	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		ConditionSequence preAuthSteps = new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.replace(SetPaymentsScopeOnTokenEndpointRequest.class, condition(AddScopeToTokenEndpointRequest.class))
			.insertAfter(CheckIfTokenEndpointResponseError.class, condition(EnsureTokenEndpointResponseScopeWasPayments.class).requirements("RFC6749-3.3").dontStopOnFailure())
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,sequenceOf(condition(CreateRandomFAPIInteractionId.class), condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
			);
		return preAuthSteps;
	}
	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
	}
	@Override
	protected void requestAuthorizationCode() {
		// Store the original access token and ID token separately (see RefreshTokenRequestSteps)
		env.mapKey("access_token", "first_access_token");
		env.mapKey("id_token", "first_id_token");

		super.requestAuthorizationCode();

		// Set up the mappings for the refreshed access and ID tokens
		env.mapKey("access_token", "second_access_token");
		env.mapKey("id_token", "second_id_token");

	}

	@Override
	protected void setupResourceEndpoint() {
		call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class, condition(ValidateConsentsAndPaymentsEndpoints.class)));
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		call(sequence(FAPIResourceConfiguration.class));

		callAndStopOnFailure(ExtractTLSTestValuesFromResourceConfiguration.class);
		callAndContinueOnFailure(ExtractTLSTestValuesFromOBResourceConfiguration.class, Condition.ConditionResult.INFO);
	}

	@Override
	protected void logFinalEnv() {
		//Not needed here
	}
}
