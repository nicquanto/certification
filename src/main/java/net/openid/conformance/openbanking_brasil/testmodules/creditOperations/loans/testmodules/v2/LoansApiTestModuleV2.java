package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.creditOperations.loans.v2.*;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareAllCreditOperationsPermissionsForHappyPath;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsAndResourcesEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractPhase2V2TestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "loans_api_core_test-module_v2",
	displayName = "Validate structure of all loans API resources - V2",
	summary = "Validates the structure of all loans API resources - V2\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Credit Operations API  - V2 (\"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Loans Contracts API - V2\n" +
		"\u2022 Expects 201 - Fetches one of the IDs returned\n" +
		"\u2022 Calls GET Loans Contracts API - V2\n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Loans Warranties API - V2\n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Loans Contracts Instalments API - V2 \n" +
		"\u2022 Expects 200",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.participants",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class LoansApiTestModuleV2 extends AbstractPhase2V2TestModule {

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
		call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class ,condition(ValidateConsentsEndpoints.class)));
		callAndStopOnFailure(BuildLoansConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(AddOpenIdScope.class);
		callAndStopOnFailure(AddLoansScope.class);
		callAndStopOnFailure(PrepareAllCreditOperationsPermissionsForHappyPath.class);
	}

	@Override
	protected void validateResponse() {

		runInBlock("Validate loans root response - V2", () -> {
			callAndStopOnFailure(GetLoansResponseValidatorV2.class);
			callAndStopOnFailure(EnsureResponseHasLinks.class);
			callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
			call(sequence(ValidateSelfEndpoint.class));
		});

		runInBlock("Validate loans contract response - V2", () -> {
			callAndStopOnFailure(LoansContractSelector.class);
			callAndStopOnFailure(PrepareUrlForFetchingLoanContractResource.class);
			preCallProtectedResource();
			callAndStopOnFailure(ContractResponseValidatorV2.class);
			callAndStopOnFailure(EnsureResponseHasLinks.class);
			callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
			call(sequence(ValidateSelfEndpoint.class));
		});

		runInBlock("Validate loans contract warranties response - V2", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingLoanContractWarrantiesResource.class);
			preCallProtectedResource();
			callAndStopOnFailure(ContractGuaranteesResponseValidatorV2.class);
		});

		runInBlock("Validate loans contract payments response - V2", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingLoanContractPaymentsResource.class);
			preCallProtectedResource();
			callAndStopOnFailure(ContractPaymentsValidatorV2.class);
		});

		runInBlock("Validate loans contract instalments response - V2", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingLoanContractInstallmentsResource.class);
			preCallProtectedResource();
			callAndStopOnFailure(ContractInstallmentsResponseValidatorV2.class);
		});

	}

}
