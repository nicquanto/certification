package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.CreateConsentErrorEnumV2;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido extends AbstractEnsureErrorResponseCodeFieldWas {
	@Override
	protected List<String> getExpectedCodes() {
		return List.of(CreateConsentErrorEnumV2.DETALHE_PAGAMENTO_INVALIDO.toString());
	}
}
