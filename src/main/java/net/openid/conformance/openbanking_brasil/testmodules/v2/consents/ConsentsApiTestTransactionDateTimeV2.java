package net.openid.conformance.openbanking_brasil.testmodules.v2.consents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.consent.v2.ConsentDetailsIdentifiedByConsentIdValidatorV2;
import net.openid.conformance.openbanking_brasil.consent.v2.CreateNewConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsAndResourcesEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@PublishTestModule(
	testName = "consents_api_transactiondatetime_test-module_v2",
	displayName = "Ensures the server refuses a consent v2 creation request if the fields transactionFromDateTime and transactionToDateTime are present",
	summary = "Checks created consent response v2\n" +
		"\u2022 Call the POST Consents API request including fields transactionFromDateTime and transactionToDateTime on the request body\n" +
		"\u2022 Expect a 201 Created Response - Make sure that the response does not contain the fields transactionFromDateTime and transactionToDateTime\n" +
		"\u2022 Call the GET Consents API request with created Consent\n" +
		"\u2022 Expect a 200 OK Response - Make sure that the response does not contain the fields transactionFromDateTime and transactionToDateTime",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.participants",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ConsentsApiTestTransactionDateTimeV2 extends AbstractClientCredentialsGrantFunctionalTestModule {

	@Override
	protected void runTests(){
		runInBlock("Check create consent request v2", () -> {
			call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
			call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class ,condition(ValidateConsentsEndpoints.class)));
			callAndStopOnFailure(AddProductTypeToPhase2V2Config.class);
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(AddConsentScope.class);
			callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
			callAndStopOnFailure(SetContentTypeApplicationJson.class);

			Instant currentTime = Instant.now().truncatedTo(ChronoUnit.SECONDS);
			env.putString("transactionFromDateTime", currentTime.minus(1, ChronoUnit.DAYS).toString());
			env.putString("transactionToDateTime", currentTime.toString());

			callAndStopOnFailure(AddTransactionFromAndToConsentRequestBody.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class);

			call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
			callAndStopOnFailure(ResourceEndpointResponseFromFullResponse.class);
			callAndStopOnFailure(EnsureConsentResponseCodeWas201.class);

			callAndContinueOnFailure(CreateNewConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.REVIEW);
			callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.REVIEW);
			callAndStopOnFailure(EnsureConsentHasNoTransactionFromToDateTime.class);
		});

		runInBlock("Validating get consent response v2", () -> {
			callAndStopOnFailure(ConsentIdExtractor.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);

			call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
			callAndStopOnFailure(ResourceEndpointResponseFromFullResponse.class);
			callAndStopOnFailure(EnsureResponseCodeWas200.class);

			callAndContinueOnFailure(ConsentDetailsIdentifiedByConsentIdValidatorV2.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.REVIEW);
			callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.REVIEW);
			callAndContinueOnFailure(EnsureConsentHasNoTransactionFromToDateTime.class);
		});

		runInBlock("Deleting consent", () -> {
			callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		});
	}
}
