package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PaymentRejectionReasonEnumV2 {
	SALDO_INSUFICIENTE, VALOR_ACIMA_LIMITE, VALOR_INVALIDO,
	COBRANCA_INVALIDA, NAO_INFORMADO, PAGAMENTO_DIVERGENTE_CONSENTIMENTO, DETALHE_PAGAMENTO_INVALIDO, PAGAMENTO_RECUSADO_DETENTORA,
	PAGAMENTO_RECUSADO_SPI, FALHA_INFRAESTRUTURA;


	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
