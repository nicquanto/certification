package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EnsureConsentRevokedByUser extends AbstractConsentRejectionValidation {
	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment env) {
		String rejectionReason = "CUSTOMER_MANUALLY_REVOKED";
		String rejectedBy = "USER";
		if (!validateResponse(env.getString("resource_endpoint_response"), rejectionReason, rejectedBy)){
			throw error("Rejection object did not match expected results.", args("Expected reason: ", rejectionReason, "Expected rejectedBy: ", rejectedBy));
		} else {
			logSuccess("Rejection object contains expected results.", args("Expected reason: ", rejectionReason, "Expected rejectedBy: ", rejectedBy));
		}
		return env;
	}
}
