package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ExtraField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.StringField;
import org.springframework.http.HttpStatus;

import java.text.ParseException;
import java.util.Optional;
import java.util.Set;

public abstract class AbstractErrorValidatorV2 extends AbstractJsonAssertingCondition {
	public static final int MAX_ERROR_ITEMS = 13;

	@Override
	protected JsonElement bodyFrom(Environment environment) {
		try {
			return BodyExtractor.bodyFrom(environment, getResponseObjectKey())
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	@Override
	public Environment evaluate(Environment environment) {

		JsonElement body = bodyFrom(environment);
		int statusCode = statusCodeFrom(environment);

		assertField(body,
			new ObjectArrayField
				.Builder("errors")
				.setValidator((error) -> assertError(error, statusCode))
				.setMinItems(1)
				.setMaxItems(statusCode == HttpStatus.UNPROCESSABLE_ENTITY.value() ? getMaxErrorItems() : MAX_ERROR_ITEMS)
				.build());

		return environment;
	}


	private void assertError(JsonObject error, int responseCode) {
		parseResponseBody(error, "errors");


		StringField.Builder codeBuilder = new StringField.Builder("code");


		if (responseCode == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			codeBuilder.setEnums(getErrorCodes());
		} else {
			codeBuilder.setPattern("[\\w\\W\\s]*")
				.setMaxLength(255);
		}

		assertField(error, codeBuilder.build());

		assertField(error,
			new StringField
				.Builder("title")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(255)
				.build());

		assertField(error,
			new StringField
				.Builder("detail")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(2048)
				.build());

		assertExtraFields(new ExtraField.Builder()
			.setPattern("^([A-Z]{4})(-)(.*)$")
			.build());
	}


	private int statusCodeFrom(Environment environment) {
		return Optional.ofNullable(environment.getInteger(getResponseObjectKey(), "status"))
			.orElseThrow(() -> error("Could not extract status code"));
	}

	protected abstract String getResponseObjectKey();

	protected abstract Set<String> getErrorCodes();

	protected abstract int getMaxErrorItems();


}
