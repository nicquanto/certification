package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class AddBrazilPixPaymentToTheResource extends AbstractCondition {

	static private final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
	static private final String SCHEDULED_TIME = "1500";

	@Override
	@PostEnvironment(strings = {"endToEndId", "ispb"})
	public Environment evaluate(Environment env) {
		JsonObject consentData;
		JsonObject consentDetails;
		JsonObject consentPayment;
		JsonObject creditorAccount;
		JsonElement localInstrument;
		JsonElement amount;
		JsonElement currency;
		JsonElement type;
		JsonElement proxy;
		JsonElement qrCode;
		JsonElement ibgeTownCode = null;

		JsonObject resource = env.getObject("resource");
		if (resource == null) {
			resource = env.getElementFromObject("config", "resource").getAsJsonObject();
		}
		if (resource == null) {
			throw error("Could not find resource object");
		}

		JsonObject brazilPaymentConsent = resource.getAsJsonObject("brazilPaymentConsent");
		if (brazilPaymentConsent != null) {
			JsonElement consentDataElement = brazilPaymentConsent.get("data");

			if (consentDataElement != null) {
				consentData = consentDataElement.getAsJsonObject();

				JsonElement consentPaymentElement = consentData.get("payment");
				if (consentPaymentElement != null) {
					consentPayment = consentPaymentElement.getAsJsonObject();

					amount = consentPayment.get("amount");
					if (amount == null) {
						throw error("amount field is missing in the payment object", consentPayment);
					}

					currency = consentPayment.get("currency");
					if (currency == null) {
						throw error("currency field is missing in the payment object", consentPayment);
					}

					type = consentPayment.get("type");
					if (type == null) {
						throw error("type field is missing in the payment object", consentPayment);
					}

					ibgeTownCode = consentPayment.get("ibgeTownCode");

					consentDetails = consentPayment.getAsJsonObject("details");

					if(consentDetails != null){

						localInstrument = consentDetails.get("localInstrument");
						if (localInstrument == null){
							throw error("localInstrument field is missing in the details", consentDetails);
						}

						creditorAccount = consentDetails.getAsJsonObject("creditorAccount");
						if (creditorAccount == null) {
							throw error("creditorAccount object is missing in the details", consentDetails);
						}

						qrCode = consentDetails.get("qrCode");
						proxy = consentDetails.get("proxy");


					}else {
						throw error("details object is missing in the payment", consentPayment);
					}

				} else {
					throw error("payment object is missing in the data", consentData);
				}
			} else {
				throw error("data object is missing in the brazilPaymentConsent", brazilPaymentConsent);
			}
		} else {
			throw error("brazilPaymentConsent object is missing in the resource", resource);
		}

		JsonObject pixPayment = new JsonObject();
		JsonObject data = new JsonObject();
		JsonObject payment = new JsonObject();

		OffsetDateTime currentDateTime;
		String formattedCurrentDateTime;
		JsonObject schedule = consentPayment.getAsJsonObject("schedule");
		if (schedule == null) {
			currentDateTime = OffsetDateTime.now(ZoneOffset.UTC);
			formattedCurrentDateTime = currentDateTime.format(FORMATTER);
		} else {
			JsonElement date = schedule.getAsJsonObject("single").get("date");
			if (date == null) {
				throw error("data.payment.schedule.single.date should not be null for scheduled payment",
					args("schedule", schedule));
			}
			formattedCurrentDateTime = OIDFJSON.getString(date)
				.replaceAll("-", "")
				.concat(SCHEDULED_TIME);
		}
		String ispb = DictHomologKeys.PROXY_CNPJ_INITIATOR.substring(0, 8);
		String randomString = RandomStringUtils.randomAlphanumeric(11);
		String endToEndId = String.format("E%s%s%s", ispb, formattedCurrentDateTime, randomString);
		env.putString("endToEndId", endToEndId);
		env.putString("ispb", ispb);

		pixPayment.add("data", data);
		data.addProperty("endToEndId", endToEndId);
		data.add("payment", payment);
		data.add("creditorAccount", creditorAccount);

		data.add("localInstrument", localInstrument);
		data.addProperty("remittanceInformation", DictHomologKeys.PROXY_EMAIL_STANDARD_REMITTANCEINFORMATION);
		if (qrCode != null) {
			data.add("qrCode", qrCode);
		}
		if (proxy != null) {
			data.add("proxy", proxy);
		}
		data.addProperty("cnpjInitiator", DictHomologKeys.PROXY_CNPJ_INITIATOR);

		if(ibgeTownCode != null) {
			data.add("ibgeTownCode", ibgeTownCode);
		}

		payment.add("amount", amount);
		payment.add("currency", currency);

		env.putObject("resource", "brazilPixPayment", pixPayment);
		logSuccess("Hardcoded brazilPixPayment object was added to the resource", pixPayment);
		return env;
	}
}
