package net.openid.conformance.openbanking_brasil.testmodules.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.PrepareUrlForResourcesCallV2;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.ResourcesResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsAndResourcesEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateConsentsEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ValidateRegisteredEndpoints;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
    "client.org_jwks", "resource.consentUrl"
})
public abstract class AbstractApiResourceTestModuleV2 extends AbstractPhase2V2TestModule {

    private static final String RESOURCE_STATUS = EnumResourcesStatus.AVAILABLE.name();

    protected abstract Class<? extends Condition> buildApiConfigResourceUrlFromConsentUrl();
    protected abstract Class<? extends Condition> scopeAddingCondition();
    protected abstract Class<? extends Condition> permissionPreparationCondition();
    protected abstract Class<? extends Condition> apiValidator();
    protected abstract String apiName();
    protected abstract String apiResourceId();
    protected abstract String resourceType();

    @Override
    protected void configureClient() {
        call(new ValidateWellKnownUriSteps().replace(CallDirectoryParticipantsEndpoint.class,condition(CallDirectoryParticipantsEndpointFromConfig.class)));
        call(new ValidateRegisteredEndpoints().replace(ValidateConsentsAndResourcesEndpoints.class ,condition(ValidateConsentsEndpoints.class)));
        callAndStopOnFailure(buildApiConfigResourceUrlFromConsentUrl());
        super.configureClient();
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        super.onConfigure(config, baseUrl);
        callAndStopOnFailure(AddOpenIdScope.class);
        callAndStopOnFailure(scopeAddingCondition());
        callAndStopOnFailure(AddResourcesScope.class);
        callAndStopOnFailure(permissionPreparationCondition());
    }

    @Override
    protected void validateResponse() {
        runInBlock("Validate " + apiName() + " root response v2", () -> {
            callAndStopOnFailure(apiValidator());
            callAndStopOnFailure(EnsureResponseHasLinks.class);
            callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
            call(sequence(ValidateSelfEndpoint.class));
        });

        call(exec().startBlock("Setting page-size=1000 and calling the " + apiName() + " endpoint again"));
        callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);
        preCallProtectedResource();

        runInBlock("Validate " + apiName() + " root response v2", () -> {
            callAndStopOnFailure(apiValidator());
            callAndStopOnFailure(EnsureResponseHasLinks.class);
            callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
            call(sequence(ValidateSelfEndpoint.class));
        });

        env.putString("apiIdName", apiResourceId());
        callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);
        callAndStopOnFailure(PrepareUrlForResourcesCallV2.class);

        ResourceApiV2PollingSteps pollingSteps = new ResourceApiV2PollingSteps(env, getId(),
            eventLog,testInfo, getTestExecutionManager());
        runInBlock("Polling Resources API", () -> {
            call(pollingSteps);
        });

        call(exec().startBlock("Poll has ended, setting page-size=1000 and calling the resource again"));
        callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);
        preCallProtectedResource();

        runInBlock("Validate Resources response V2", () -> {
            callAndStopOnFailure(ResourcesResponseValidatorV2.class, Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResponseHasLinks.class);
            callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
            call(sequence(ValidateSelfEndpoint.class));
        });

        eventLog.startBlock("Compare active resourceId's with API resources");
        env.putString("resource_type", resourceType());
        env.putString("resource_status", RESOURCE_STATUS);
        callAndStopOnFailure(ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus.class);
        callAndStopOnFailure(CompareResourceIdWithAPIResourceId.class);
        eventLog.endBlock();
    }
}
