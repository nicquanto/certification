package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.AbstractEnsurePaymentStatusWasX;
import net.openid.conformance.testmodule.Environment;

import java.util.List;

public abstract class AbstractCheckPaymentPollStatus extends AbstractEnsurePaymentStatusWasX {



	@Override
	public Environment evaluate(Environment env) {

		boolean isStatusAsExpected = checkPaymentStatus(env);
		String status = getStatus();
		List<String> expectedStatuses = getExpectedStatuses();

		if (isStatusAsExpected) {
			env.putBoolean("payment_proxy_check_for_reject", false);
			logSuccess("Status still in the expected state", args("State", status, "Expected", expectedStatuses));
		} else {
			env.putBoolean("payment_proxy_check_for_reject", true);
			logSuccess("Status no longer in the expected state", args("State", status, "Expected", expectedStatuses));
		}
		return env;
	}


}
