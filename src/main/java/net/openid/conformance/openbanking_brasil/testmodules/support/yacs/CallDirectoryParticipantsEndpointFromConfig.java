package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import net.openid.conformance.openbanking_brasil.testmodules.support.CallDirectoryParticipantsEndpoint;
import net.openid.conformance.testmodule.Environment;

public class CallDirectoryParticipantsEndpointFromConfig extends CallDirectoryParticipantsEndpoint {
	@Override
	protected String getParticipantsUri(Environment environment){
		return getStringFromEnvironment(environment,"config","directory.participants","Participants Endpoint for directory");
	}
}
