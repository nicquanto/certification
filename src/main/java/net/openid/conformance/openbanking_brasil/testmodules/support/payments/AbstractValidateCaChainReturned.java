package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.AbstractCallProtectedResource;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.*;
import java.security.cert.*;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

public abstract class AbstractValidateCaChainReturned extends AbstractCallProtectedResource {

	@Override
	@PreEnvironment(required = {"config", "directory_roots_response", "authorisation_server", "server"})
	public Environment evaluate(Environment env) {
		JsonObject authorisationServer = env.getObject("authorisation_server");
		if (authorisationServer == null){
			throw error("Authorisation Server not present in environment test unable to continue");
		}
		String versionRegex = getVersionRegex();

		List<String> endpointRegexes = getEndpointRegexes();

		List<String> endpoints = getEndpointList(authorisationServer,versionRegex,endpointRegexes);
		JsonObject discoveryDocumentObject = env.getObject("server");
		String tokenEndpoint = extractKeyIfPresentOnMtlsObject("token_endpoint", discoveryDocumentObject);
		String registrationEndpoint = extractKeyIfPresentOnMtlsObject("registration_endpoint", discoveryDocumentObject);
		logSuccess("Extracted token_endpoint and registration_endpoint", args("token_endpoint", tokenEndpoint, "registration_endpoint", registrationEndpoint));
		endpoints.add(tokenEndpoint);
		endpoints.add(registrationEndpoint);
		endpoints.forEach(endpoint -> callEndPoint(env, endpoint));
		return env;
	}
	public String extractKeyIfPresentOnMtlsObject(String key, JsonObject discoveryDocumentObject) {
		JsonObject mtlsObject = discoveryDocumentObject.getAsJsonObject("mtls_endpoint_aliases");
		String value;
		if(mtlsObject != null && mtlsObject.has(key)) {
			value = OIDFJSON.getString(mtlsObject.get(key));
			logSuccess("Found key on mtls_endpoint_aliases", args("key", key, "value", value));
			return value;
		} else if (discoveryDocumentObject.has(key)){
			value = OIDFJSON.getString(discoveryDocumentObject.get(key));
			logSuccess("Did not find key on mtls_endpoint_aliases, returning value from root document", args("key", key, "value", value));
			return value;
		}
		throw error("Could not find key on mtls_endpoint_aliases or root document", args("key", key));
	}
	private void callEndPoint(Environment env, String resourceUrl){
		env.putBoolean("trust_chain_validation", true);
		try {
			RestTemplate restTemplate = createRestTemplate(env);
			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public boolean hasError(ClientHttpResponse response) throws IOException {
					// Treat all http status codes as 'not an error', so spring never throws an exception due to the http
					// status code meaning the rest of our code can handle http status codes how it likes
					return false;
				}
			});

			HttpMethod method = HttpMethod.GET;
			HttpHeaders headers = getHeaders(env);

			if (headers.getAccept().isEmpty()) {
				headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			}

			HttpEntity<?> request = new HttpEntity<>(getBody(env), headers);
			restTemplate.exchange(resourceUrl, method, request, String.class);

		} catch (UnrecoverableKeyException | KeyManagementException | CertificateException | InvalidKeySpecException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
			env.putBoolean("trust_chain_validation", false);
			throw error("Error creating HTTP client", e);
		} catch (RestClientException e) {
			String msg = "Call to protected resource " + resourceUrl + " failed";
			if (e.getCause() != null) {
				msg += " - " +e.getCause().getMessage();
			}
			logFailure(msg);
		}
	}

	@Override
	protected Environment handleClientResponse(Environment env, JsonObject responseCode, String responseBody, JsonObject responseHeaders, JsonObject fullResponse) {
		//Not interested in the api response
		return null;
	}

	private List<String> getEndpointList(JsonObject authorisationServer, String versionRegex, List<String> endpointRegexes){
		JsonArray apiResources = authorisationServer.getAsJsonArray("ApiResources");
		List<String> endpointToCallList = new ArrayList<>();

		if (apiResources != null){
			endpointRegexes.forEach((endpointRegex) -> {
				for (JsonElement entry : apiResources) {
					JsonObject apiResource = entry.getAsJsonObject();
					if (OIDFJSON.getString(apiResource.get("ApiVersion")).matches(versionRegex)){
						JsonArray apiDiscoveryEndpoints = apiResource.getAsJsonArray("ApiDiscoveryEndpoints");
						for (JsonElement apiEndpoint: apiDiscoveryEndpoints){
							JsonObject apiEndpointObject = apiEndpoint.getAsJsonObject();
							String endpointString = OIDFJSON.getString(apiEndpointObject.get("ApiEndpoint"));
							if (endpointString.matches(endpointRegex)){
								endpointToCallList.add(endpointString);
								logSuccess("Successfully found api endpoint: " + endpointString);
							}
						}
					}
				}
			});
		} else {
			throw error("Authorisation Servers does not have any endpoints available");
		}
		if (endpointToCallList.isEmpty()){
			throw error("Unable to locate at least one of the defined endpoints", args("Endpoints Expected", endpointRegexes));
		}
		logSuccess("Validated Authorisation Server contains API endpoint(s) supporting the correct version of the API", args("Endpoints", endpointToCallList));
		return endpointToCallList;
	}

	protected abstract List<String> getEndpointRegexes();

	protected abstract String getVersionRegex();
}
