package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentConsentStatusEnumV2;

public class EnsurePaymentConsentStatusWasAwaitingAuthorisation extends AbstractEnsurePaymentConsentStatusWas {

	@Override
	protected String getExpectedStatus() {
		return PaymentConsentStatusEnumV2.AWAITING_AUTHORISATION.toString();
	}
}
