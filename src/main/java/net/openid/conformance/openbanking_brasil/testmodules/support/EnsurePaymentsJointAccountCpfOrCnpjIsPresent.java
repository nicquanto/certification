package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Optional;

public class EnsurePaymentsJointAccountCpfOrCnpjIsPresent extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonElement cpf = env.getElementFromObject("config", "conditionalResources.brazilCpfJointAccount");
		JsonElement cnpj = env.getElementFromObject("config", "conditionalResources.brazilCnpjJointAccount");


		if (cpf == null && cnpj ==  null) {
			env.putBoolean("continue_test", false);
			throw error("Brazil CPF and CNPJ for Joint Account field is empty. Institution is assumed to not have this functionality");
		}

		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("config", "resource.brazilPaymentConsent.data"))
												.orElseThrow(() -> error("Could not find brazilPaymentConsent")).getAsJsonObject();
		JsonObject brazilQrdnPaymentConsent = Optional.ofNullable(env.getElementFromObject("config", "resource.brazilQrdnPaymentConsent.data"))
			.orElseThrow(() -> error("Could not find brazilPaymentConsent")).getAsJsonObject();

		if (cpf != null) {

			JsonObject loggedUser = new JsonObjectBuilder()
				.addField( "document.identification", OIDFJSON.getString(cpf))
				.addField( "document.rel", "CPF")
				.build();

			brazilPaymentConsent.add("loggedUser", loggedUser);
			brazilQrdnPaymentConsent.add("loggedUser", loggedUser);
			logSuccess("Brazil CPF for Joint Account field is present and added", args("loggedUser", loggedUser));
		}

		if (cnpj != null) {

			JsonObject businessEntity = new JsonObjectBuilder()
				.addField( "document.identification", OIDFJSON.getString(cnpj))
				.addField( "document.rel", "CNPJ")
				.build();

			brazilPaymentConsent.add("businessEntity", businessEntity);
			brazilQrdnPaymentConsent.add("businessEntity", businessEntity);
			logSuccess("Brazil CNPJ for Joint Account field is present and added", args("businessEntity", businessEntity));
		}
		return env;
	}
}
