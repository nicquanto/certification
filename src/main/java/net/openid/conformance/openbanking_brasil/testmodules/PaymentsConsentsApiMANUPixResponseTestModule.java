package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.paymentInitiation.EnsureEndToEndIdIsEqual;
import net.openid.conformance.openbanking_brasil.paymentInitiation.PaymentFetchPixPaymentsValidator;
import net.openid.conformance.openbanking_brasil.paymentInitiation.PaymentInitiationPixPaymentsValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddResourceUrlToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePaymentDateIsToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseHasLinks;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseWasJwt;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureSelfLinkEndsInPaymentId;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveProxyFromConsentConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveProxyFromPaymentConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveTransactionIdentification;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectMANUCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectMANUCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateResponseMetaData;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateSelfEndpoint;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_manu-pix-response_test-module",
	displayName = "Payments API test module for MANU local instrument pix response",
	summary = "Payments API test module ensuring that the pix response for MANU local instrument is correct" +
		"Flow:" +
		"Makes a good payment flow with a local instrument of MANU - expects success." +
		"Required:" +
		"Consent url pointing at the consent endpoint." +
		"Resource url pointing at the base url. The test appends on the required payment endpoints" +
		"Config: Proxy field must be present in both payment and consent so we can remove it. We manually set the local instrument for both consent and payment to MANU for this test.",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiMANUPixResponseTestModule extends AbstractOBBrasilFunctionalTestModule {

	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(AddResourceUrlToConfig.class);
		super.setupResourceEndpoint();
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(AddPaymentScope.class);
		super.validateClientConfiguration();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		eventLog.startBlock("Setting date to today");
		callAndStopOnFailure(EnsurePaymentDateIsToday.class);
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		callAndStopOnFailure(SelectMANUCodeLocalInstrument.class);
		callAndStopOnFailure(SelectMANUCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveTransactionIdentification.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
		callAndStopOnFailure(RemoveProxyFromConsentConfig.class);
		callAndStopOnFailure(RemoveProxyFromPaymentConfig.class);
	}

	@Override
	protected void validateResponse() {
		callAndStopOnFailure(PaymentInitiationPixPaymentsValidator.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(EnsureEndToEndIdIsEqual.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(EnsureSelfLinkEndsInPaymentId.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
		eventLog.startBlock("Checking the self endpoint - Expecting 200, validating response");
		call(new ValidateSelfEndpoint()
			.insertAfter(
				EnsureResponseCodeWas200.class, sequenceOf(
					condition(EnsureResponseWasJwt.class),
					condition(PaymentFetchPixPaymentsValidator.class),
					condition(EnsureEndToEndIdIsEqual.class)
				))
			.replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeader.class),
				condition(CallProtectedResource.class)
			)));

	}

}
