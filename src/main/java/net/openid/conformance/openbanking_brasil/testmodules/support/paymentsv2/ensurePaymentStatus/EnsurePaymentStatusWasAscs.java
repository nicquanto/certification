package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentStatusEnumV2;

import java.util.List;

public class EnsurePaymentStatusWasAscs extends AbstractEnsurePaymentStatusWasX {
	@Override
	protected List<String> getExpectedStatuses() {
		return List.of(PaymentStatusEnumV2.ACSC.toString());
	}
}
