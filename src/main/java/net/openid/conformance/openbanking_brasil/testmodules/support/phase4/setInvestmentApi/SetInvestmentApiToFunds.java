package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi;

public class SetInvestmentApiToFunds extends AbstractSetInvestmentApi {

    @Override
    protected String investmentApi() {
        return "funds";
    }
}
