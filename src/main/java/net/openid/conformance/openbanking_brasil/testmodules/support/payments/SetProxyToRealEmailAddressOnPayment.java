package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.testmodule.Environment;
/**
 * @deprecated
 * proxy is added by default to the consent request in {@link AddPaymentConsentRequestBodyToConfig}
 * and then inserted from consent request to the payment in {@link AddBrazilPixPaymentToTheResource}
 */
@Deprecated
public class SetProxyToRealEmailAddressOnPayment extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonObject obj = env.getObject("resource");
		obj = obj.getAsJsonObject("brazilPixPayment");
		obj = obj.getAsJsonObject("data");
		obj.addProperty("proxy", "cliente-a00001@pix.bcb.gov.br");

		logSuccess("Added real email address as proxy to payment");

		return env;
	}

}
