package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectQRCodeWithTransactionIdentifierIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRESCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreateConsentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

import java.util.Optional;

@PublishTestModule(
	testName = "payments_api_qres-with-transaction-identifier_test-module_v2",
	displayName = "Payments API QRES with transaction identifier test module",
	summary = "Ensures that a payment consent created with a QRES and a transaction identifier is rejected" +
		"\u2022 Call POST Consents with localInstrument as QRES\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Call POST Payment Endpoint with a transactionIdentification\n" +
		"\u2022 Expects 422 DETALHE_PAGAMENTO_INVALIDO\n" +
		"\u2022 Validate Error Message\n" +
		"If no errors are identified:\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expects a payment with the definitive state (RJCT) status and RejectionReason equal to DETALHE_PAGAMENTO_INVALIDO\n" +
		"\u2022 Validate Error message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiQresTransactionIdentifierTestModuleV2 extends AbstractPaymentUnhappyPathTestModuleV2 {


	@Override
	protected void configureDictInfo() {
		callAndContinueOnFailure(SelectQRESCodeLocalInstrument.class);
		callAndContinueOnFailure(SelectQRESCodePixLocalInstrument.class);
		callAndContinueOnFailure(InjectQRCodeWithTransactionIdentifierIntoConfig.class);
		callAndContinueOnFailure(AddTransactionIdentification.class);
	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasDetalhePagamentoInvalido.class);
	}

	@Override
	protected void validate422ErrorResponseCode() {
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class);
	}

	@Override
	protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		return EnsureResourceResponseCodeWas201Or422.class;
	}


	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpenBankingBrazilPreAuthorizationErrorAgnosticSteps steps = (OpenBankingBrazilPreAuthorizationErrorAgnosticSteps) new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
			.replace(
				ValidateErrorAndMetaFieldNames.class,
				condition(CreateConsentErrorValidatorV2.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE)
					.skipIfStringMissing("validate_errors")
			);
		return steps;
	}

	@Override
	protected void performPreAuthorizationSteps() {
		env.mapKey("access_token", "saved_client_credentials");
		call(createOBBPreauthSteps().insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
			sequenceOf(condition(CreateRandomFAPIInteractionId.class),
				condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
		));
		callAndStopOnFailure(SaveAccessToken.class);
		callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasFormaPagamentoInvalida.class, Condition.ConditionResult.INFO);
		if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
			fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
		}
		eventLog.startBlock(currentClientString() + "Validate consents response");
		callAndContinueOnFailure(PaymentConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndContinueOnFailure(ValidateMetaOnlyRequestDateTime.class, Condition.ConditionResult.FAILURE);
		env.unmapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY);
		eventLog.endBlock();
	}
}
