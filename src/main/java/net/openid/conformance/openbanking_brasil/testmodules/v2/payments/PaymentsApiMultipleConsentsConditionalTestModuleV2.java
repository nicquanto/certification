package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRcvdOrPatc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrPatcOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_multiple-consents-conditional_test-module_v2",
	displayName = "Payments API multiple consents conditional test",
	summary = "This test module should be executed only by institutions that currently support consents with multiple accounts, in case the institution does not support this feature the ‘Payment consent - Logged User CPF - Multiple Consents Test' and 'Payment consent - Business Entity CNPJ - Multiple Consents Test’  should be left empty, which in turn will make the test return a SKIPPED\n" +
		"\n" +
		"This test module validates that the payment will reach an ACSC state after multiple consents are granted\n" +
		"\u2022 Validates if the user has provided the field \"Payment consent - Logged User CPF - Multiple Consents Test\". If field is not provided the whole test scenario must be SKIPPED\n" +
		"\u2022 Set the payload to be customer business if Payment consent - Business Entity CNPJ - Multiple Consents Test was provided. If left blank, it should be customer personal\n" +
		"\u2022 Creates consent request_body with valid email proxy (cliente-a00001@pix.bcb.gov.br) and its standardized payload, set the 'Payment consent - Logged User CPF - Multiple Consents Test' on the loggedUser identification field, Debtor account should not be sent\n" +
		"\u2022 Call the POST Consents API\n" +
		"\u2022 Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"\u2022 Redirects the user to authorize the created consent - Expect Successful Authorization\n" +
		"\u2022 Calls the POST Payments Endpoint\n" +
		"\u2022 Expects 201 with status set as either RCVD or PATC - Validate response_body\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created for at most 10 minutes - User must now accept the payment for the the second authorizer\n" +
		"\u2022 Expected Accepted state (ACSC)",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"conditionalResources.brazilCpfJointAccount",
		"conditionalResources.brazilCnpjJointAccount",
	}
)
public class PaymentsApiMultipleConsentsConditionalTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsurePaymentsJointAccountCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no 'Payment consent - Logged User CPF - Multiple Consents Test' was informed.");
		}
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
	}

	@Override
	protected void validateResponse() {
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
		call(getPaymentValidationSequence());

		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(20)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		validateFinalState();
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.insertAfter(EnsureResponseCodeWas201.class, condition(EnsurePaymentStatusWasRcvdOrPatc.class));
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrPatcOrAccpOrAcpdV2.class;
	}
}
