package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.concurrent.TimeUnit;

public class WaitForSpecifiedTimeFromConfigInSeconds extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		long waitTime;
		String expectedWaitSeconds = env.getString("config", "resource.webhookWaitTime");
		if (expectedWaitSeconds == null) {
			logSuccess("No wait time specified in config, defaulting to 600 seconds");
			waitTime = 600;
		} else {
			waitTime = Long.parseLong(expectedWaitSeconds);
			if (waitTime < 0 || waitTime > 600) {
				throw error("Invalid wait time specified in config, it must be between 0 and 600 seconds");
			}
		}
		try {
			logSuccess("Pausing for " + waitTime + " seconds");

			TimeUnit.SECONDS.sleep(waitTime);

			logSuccess("Woke up after " + waitTime + " seconds sleep");
		} catch (InterruptedException e) {
			throw error("Interrupted while sleeping", e);
		}
		return env;
	}
}
