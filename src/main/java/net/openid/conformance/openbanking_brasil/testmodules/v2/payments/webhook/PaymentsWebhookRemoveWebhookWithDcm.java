package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@PublishTestModule(
	testName = "dcr_api_dcm-pagto-remove-webhook_test-module",
	displayName = "dcr_api_dcm-pagto-remove-webhook_test-module",
	summary = "Obtain a SSA from the Directory\n" +
		"Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration\n" +
		"Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>”]\n" +
		"Expect a 201 - Validate Response\n" +
		"Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"Calls POST Consents Endpoint with localInstrument set as DICT\n" +
		"Expects 201 - Validate Response\n" +
		"Redirects the user to authorize the created consent\n" +
		"Call GET Consent\n" +
		"Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"Calls the POST Payments with localInstrument as DICT\n" +
		"Expects 201 - Validate response\n" +
		"Set the payments webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>/open-banking/webhook/v1/payments/v2/pix/payments/{paymentId} where the alias is to be obtained from the field alias on the test configuration\n" +
		"Expect an incoming message, for at most 60 seconds,which must be mtls protected, to be received on the webhook endpoint set for this test\n" +
		"Return a 503 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"Expect an incoming message, for at most 60 seconds, which must be mtls protected, to be received on the webhook endpoint set for this test\n" +
		"Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"Call the Get Payments endpoint with the PaymentID \n" +
		"Expects status returned to be (ACSC) - Validate Response\n" +
		"Call the PUT Registration Endpoint without sending the field \"webhook_uris\"\n" +
		"Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"Calls POST Consents Endpoint with localInstrument set as DICT\n" +
		"Expects 201 - Validate Response\n" +
		"Redirects the user to authorize the created consent\n" +
		"Call GET Consent\n" +
		"Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"Calls the POST Payments with localInstrument as DICT\n" +
		"Expects 201 - Validate response\n" +
		"Validate that no webhook is called on the payments webhook endpoints \n" +
		"Call the Delete Registration Endpoint\n" +
		"Expect a 204 - No Content",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.webhookWaitTime"
	}
)
public class PaymentsWebhookRemoveWebhookWithDcm extends AbstractBrazilDCRPaymentsWebhook {
	private boolean secondAuthCodeFlow = false;

	@Override
	public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
		boolean firstPaymentWebhookReceived = env.getBoolean("first_payment_webhook_received");
		boolean secondPaymentWebhookReceived = env.getBoolean("second_payment_webhook_received");
		String paymentId = env.getString("payment_id");
		 if (path.contains("webhook/v1/payments/v2/pix/payments/" +paymentId) && (!firstPaymentWebhookReceived && !secondPaymentWebhookReceived)) {
			env.putBoolean("first_payment_webhook_received", true);
			return new ResponseEntity<Object>("", HttpStatus.SERVICE_UNAVAILABLE);
		} else if (path.contains("webhook/v1/payments/v2/pix/payments/" +paymentId) && (firstPaymentWebhookReceived && !secondPaymentWebhookReceived)){
			 env.putBoolean("second_payment_webhook_received", true);
			 validateWebhook(requestParts);
			 return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		 } else {
			throw new TestFailureException(getId(), "Got an HTTP response we weren't expecting");
		}
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		env.putBoolean("first_payment_webhook_received", false);
		env.putBoolean("second_payment_webhook_received", false);
		callAndStopOnFailure(AddOpenIdScope.class);
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
		setScheduledPaymentDateTime();
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		configureDictInfo();
		super.onConfigure(config, baseUrl);
	}

	@Override
	public void createConsentWebhook(){
		//Do nothing
	}

	@Override
	protected void requestProtectedResource() {
		super.requestProtectedResource();
		if (!secondAuthCodeFlow){
			doDcm();
			callAndStopOnFailure(WaitForSpecifiedTimeFromConfigInSeconds.class);
			ConditionSequence conditionSequence = createOBBPreauthSteps();
			call(conditionSequence);
			performSecondFlow();
		}
	}
	protected void performSecondFlow() {

		eventLog.startBlock(currentClientString() + "Make request to authorization endpoint");

		createAuthorizationRequest();

		createAuthorizationRequestObject();

		if (isPar.isTrue()) {
			callAndStopOnFailure(BuildRequestObjectPostToPAREndpoint.class);
			addClientAuthenticationToPAREndpointRequest();
			performParAuthorizationRequestFlow();
		} else {
			buildRedirect();
			performRedirect();
		}
	}
	@Override
	protected void onPostAuthorizationFlowComplete() {
		if(!secondAuthCodeFlow) {
			secondAuthCodeFlow = true;
			return;
		}

		super.onPostAuthorizationFlowComplete();
	}
	protected void doDcm() {
		eventLog.startBlock("Updating client with webhook");
		callAndStopOnFailure(CreateClientConfigurationRequestFromDynamicClientRegistrationResponse.class);
		callAndStopOnFailure(AddSoftwareStatementToClientConfigurationRequest.class);
		callAndStopOnFailure(RemoveWebhookFromClientConfigurationRequest.class);
		callClientConfigurationEndpoint();
		eventLog.endBlock();
	}
	@Override
	protected void waitForPaymentConsentAndPaymentWebhookResponse() {
		if (!secondAuthCodeFlow){
			waitForSpecifiedWebhookResponse("first_payment_webhook_received");
			waitForSpecifiedWebhookResponse("second_payment_webhook_received");
		}
	}

	public void waitForSpecifiedWebhookResponse(String booleanToGetFromEnv){
		long delaySeconds = 5;
		eventLog.startBlock(currentClientString() + "The test will now wait for the webhook to be received");
		for (int attempts = 0; attempts < 12; attempts++) {
			eventLog.log(getId(), "Waiting for webhook to be received at with an interval of " + delaySeconds + " seconds");
			setStatus(Status.WAITING);
			try {
				Thread.sleep(delaySeconds * 1000);
			} catch (InterruptedException e) {
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}
			setStatus(Status.RUNNING);
			boolean webhookReceived = env.getBoolean(booleanToGetFromEnv);
			if (webhookReceived) {
				return;
			}
		}
		throw new TestFailureException(getId(), "Webhook not received in time");
	}
	@Override
	protected void callClientConfigurationEndpoint() {

		callAndStopOnFailure(CallClientConfigurationEndpoint.class);
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentTypeHttpStatus200.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentType.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckClientIdFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckRedirectUrisFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationUriFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndStopOnFailure(CheckClientConfigurationDoesNotContainWebhookUri.class);
	}
}
