package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePixScheduleDateIsInPast;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePixScheduleDateIsTooFarInFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentErrorTestingSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsurePaymentDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsureScheduledPaymentDateIsToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsureScheduledPaymentDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.RemovePaymentDateFromConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDataPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreateConsentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;

import java.util.Optional;

@PublishTestModule(
	testName = "payments_api_pixscheduling-dates-unhappy_test-module_v2",
	displayName = "5 Negative Test Scenarios that change the payment date and schedule.single.date fields and expect errors on the POST Consents request",
	summary = "5 Negative Test Scenarios that change the payment date and schedule.single.date fields and expect errors on the POST Consents request\n" +
		"\u2022 Attempts to create a payment consent scheduled with the date element present together with schedule.single.date, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA\n" +
		"\u2022 Create consent with request payload with both the date and the schedule.single.date fields\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error DATA_PAGAMENTO_INVALIDA - Validate Error response \n" +
		"\u2022 Attempts to create a payment consent scheduled for today, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA\n" +
		"\u2022 Create consent with request payload set with schedule.single.date field defined as today\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error  DATA_PAGAMENTO_INVALIDA - Validate Error response \n" +
		"\u2022 Attempts to create a payment consent scheduled for a day in the past, with a payment date of yesterday, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA \n" +
		"\u2022 Create consent with request payload set with schedule.single.date field defined as yesterday D-1\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error DATA_PAGAMENTO_INVALIDA  - Validate Error response \n" +
		"\u2022 Attempts to create a payment consent scheduled for a day too far in the future, with a payment date of today + 370 days, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA \n" +
		"\u2022 Create consent with request payload set with schedule.single.date field defined as yesterday D+370\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error DATA_PAGAMENTO_INVALIDA  - Validate Error response \n" +
		"\u2022 Attempts to create a payment consent wihtout the date or date scheduled fields, both which are optional, expecting error PARAMETRO_NAO_INFORMADO\n" +
		"\u2022 Create consent with request payload set without the date field provided\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error PARAMETRO_NAO_INFORMADO  - Validate Error response ",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsPixSchedulingDatesUnhappyV2 extends AbstractClientCredentialsGrantFunctionalTestModule {

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
	}


	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void runTests() {
		runInBlock("[1] Create consent with request payload with both the date and the schedule.single.date fields", () -> {
			saveBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
				sequenceOf(condition(EnsurePaymentDateIsTomorrow.class),
					       condition(EnsureScheduledPaymentDateIsTomorrow.class)));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		});

		runInBlock("[2] Create consent with request payload set with schedule.single.date field defined as today", () -> {
			restoreBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
				sequenceOf(condition(EnsureScheduledPaymentDateIsToday.class),
					condition(RemovePaymentDateFromConsentRequest.class)));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		});

		runInBlock("[3] Create consent with request payload set with schedule.single.date field defined as yesterday D-1", () -> {
			restoreBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
				sequenceOf(condition(EnsurePixScheduleDateIsInPast.class),
					condition(RemovePaymentDateFromConsentRequest.class)));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		});

		runInBlock("[4] Create consent with request payload set with schedule.single.date field defined as yesterday D+370", () -> {
			restoreBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
				sequenceOf(condition(EnsurePixScheduleDateIsTooFarInFuture.class),
					condition(RemovePaymentDateFromConsentRequest.class)));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		});

		runInBlock("[5] Create consent with request payload set without the date field provided", () -> {
			restoreBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
					condition(RemovePaymentDateFromConsentRequest.class));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasParametroNaoInformado.class);
		});
	}


	protected void saveBrazilPaymentConsent() {
		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.orElseThrow(() -> new TestFailureException(getId(),"Could not extract brazilPaymentConsent")).getAsJsonObject();

		env.putObject("resource", "brazilPaymentConsentSave", brazilPaymentConsent.deepCopy());
	}

	protected void restoreBrazilPaymentConsent() {
		JsonObject brazilPaymentConsentSave = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsentSave"))
			.orElseThrow(() -> new TestFailureException(getId(),"Could not extract brazilPaymentConsentSave")).getAsJsonObject();
		env.putObject("resource", "brazilPaymentConsent", brazilPaymentConsentSave.deepCopy());
	}

	private ConditionSequence consentCreationConditionSequence() {
		return sequenceOf(
			condition(PrepareToPostConsentRequest.class),
			condition(FAPIBrazilCreatePaymentConsentRequest.class),
			sequence(PaymentConsentErrorTestingSequence.class)
		);
	}

	private void validateResponse(Class<? extends Condition> ensureErrorResponse) {

		callAndContinueOnFailure(ensureErrorResponse, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CreateConsentErrorValidatorV2.class, Condition.ConditionResult.FAILURE);
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);
	}
}

