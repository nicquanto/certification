package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePaymentDateIsToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas200or406;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseHasLinks;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseWasJwt;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SignedPaymentConsentSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateResponseMetaData;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreateConsentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import org.springframework.http.HttpStatus;

@PublishTestModule(
	testName = "payments_api_json-accept-header-jwt-returned_test-module_v2",
	displayName = "Payments Consents API test module which sends an accept header of JSON and expects a JWT",
	summary = "Ensure a JWT is returned when a JSON accept header is sent" +
		"\u2022 Calls POST Consents Endpoint \n" +
		"\u2022 Expects 201 - Validate Response \n" +
		"\u2022 Calls GET Consents Endpoint  with a JSON accept header \n" +
		"\u2022 Expects 200 or 406 \n" +
		"\u2022 If a 200 is returned, ensure it is a JWT",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsJsonAcceptHeaderJwtReturnedTestModuleV2 extends AbstractClientCredentialsGrantFunctionalTestModule {

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
	}

	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void postConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndContinueOnFailure(SanitiseQrCodeConfig.class);
	}

	@Override
	protected void runTests() {
		runInBlock("Create a payment consent", () -> {
			eventLog.startBlock("Setting date to today");
			callAndStopOnFailure(EnsurePaymentDateIsToday.class);
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

			call(sequence(SignedPaymentConsentSequence.class));

			callAndStopOnFailure(PaymentConsentValidatorV2.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);

			env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
			env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);

			ConditionSequence validationSequence = new ValidateSelfEndpoint()
				.replace(EnsureResponseCodeWas200.class, condition(EnsureResponseCodeWas200or406.class))
				.skip(SaveOldValues.class, "Skipped in the sequence for further check")
				.skip(LoadOldValues.class, "Skipped in the sequence for further check")
				.skip(ValidateResponseMetaData.class, "Skipped in the sequence for further check");

			call(validationSequence);
			if(env.getInteger("resource_endpoint_response_full", "status") == HttpStatus.OK.value()) {

				env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, false);
				env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);

				callAndStopOnFailure(EnsureResponseWasJwt.class);
				callAndStopOnFailure(PaymentConsentValidatorV2.class);
			} else {
				callAndContinueOnFailure(CreateConsentErrorValidatorV2.class, Condition.ConditionResult.FAILURE);
				env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
				env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);

			}

		});
	}
}
