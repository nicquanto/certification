package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class EnsureSoftwareStatementContainsWebhook extends AbstractCondition {
	@Override
	@PreEnvironment(required = "software_statement_assertion")
	public Environment evaluate(Environment env) {
		String baseUrl = env.getString("base_url");
		JsonObject softwareStatementAssertion = env.getObject("software_statement_assertion");
		if (!softwareStatementAssertion.keySet().contains("software_api_webhook_uris")) {
			env.putString("software_api_webhook_uri", baseUrl);
			throw error("Unable to find software_api_webhook_uris in software statement, defaulting to default webhook endpoint", args("endpoint", baseUrl));
		}
		JsonArray webhookEndpoint = softwareStatementAssertion.get("software_api_webhook_uris").getAsJsonArray();
		webhookEndpoint.forEach(endpoint -> {
			if (OIDFJSON.getString(endpoint).equals(baseUrl)) {
				env.putString("software_api_webhook_uri", OIDFJSON.getString(endpoint));
				logSuccess("Webhook endpoint found", args("endpoint", OIDFJSON.getString(endpoint)));
			} else {
				logSuccess("Webhook endpoint(s) returned to not match defined pattern, defaulting to default webhook endpoint", args("endpoint", baseUrl));
				env.putString("software_api_webhook_uri", baseUrl);
			}
		});
		return env;
	}
}
