package net.openid.conformance.openbanking_brasil.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddClientAssertionToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddClientCredentialsGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddClientIdToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddImplicitGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddJwksUriToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddRedirectUriToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddRefreshTokenGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddSoftwareStatementToClientConfigurationRequest;
import net.openid.conformance.condition.client.AddSoftwareStatementToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddTlsClientAuthSubjectDnToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment;
import net.openid.conformance.condition.client.CallClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CallTokenEndpointAndReturnFullResponse;
import net.openid.conformance.condition.client.CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckClientConfigurationUriFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckClientIdFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CheckNoClientIdFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckRedirectUrisFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckRegistrationClientEndpointContentType;
import net.openid.conformance.condition.client.CheckRegistrationClientEndpointContentTypeHttpStatus200;
import net.openid.conformance.condition.client.CheckTokenEndpointHttpStatusNot200;
import net.openid.conformance.condition.client.ClientManagementEndpointAndAccessTokenRequired;
import net.openid.conformance.condition.client.CreateClientAuthenticationAssertionClaims;
import net.openid.conformance.condition.client.CreateClientConfigurationRequestFromDynamicClientRegistrationResponse;
import net.openid.conformance.condition.client.CreateEmptyDynamicRegistrationRequest;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs400or401;
import net.openid.conformance.condition.client.ExtractMTLSCertificatesFromConfiguration;
import net.openid.conformance.condition.client.FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken;
import net.openid.conformance.condition.client.FAPIBrazilExtractClientMTLSCertificateSubject;
import net.openid.conformance.condition.client.FAPIBrazilExtractJwksUriFromSoftwareStatement;
import net.openid.conformance.condition.client.FapiBrazilVerifyRedirectUriContainedInSoftwareStatement;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetResponseTypeCodeIdTokenInDynamicRegistrationRequest;
import net.openid.conformance.condition.client.SetResponseTypeCodeInDynamicRegistrationRequest;
import net.openid.conformance.condition.client.SignClientAuthenticationAssertion;
import net.openid.conformance.fapi1advancedfinal.AbstractFAPI1AdvancedFinalBrazilDCR;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckScopesFromDynamicRegistrationEndpointContainsConsentsOrPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.OverrideClientWith2ndClientFull;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.CallDynamicRegistrationEndpointAndVerifySuccessfulResponse;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;
import net.openid.conformance.variant.VariantNotApplicable;

@PublishTestModule(
	testName = "dcr_api_fvp-client-takeover_test-module",
	displayName = "Resources API DCR test: attempt to take over client",
	summary = "Obtains a software statement from the Brazil directory (using a client hardcoded into the test suite), registers a new client on the target authorization server then, using valid keys/SSA/etc from a different valid client, attempts to take over the original client. Note that this test overrides the 'alias' value in the configuration, so you may see your test being interrupted if other users are testing.\n" +
		"\u2022 Retrieves from the directory its SSA\n" +
		"\u2022 Performs a DCR on the provided authorization server -> Expects a success \n" +
		"\u2022 Performs a PUT on the registration endpoint with the same configuration -> Expects a success\n" +
		"\u2022 Performs a POST to the token endpoint to obtain a valid token -> Expects a success\n" +
		"\u2022 Changes the certificates used to the second set of certificates that belong to a client from a different organization\n" +
		"\u2022 Attempts 'GET' on client configuration endpoint using MTLS certificate for the second client -> Expects Success\n" +
		"\u2022 Attempts a POST to the token endpoint to obtain a valid token using the MTLS certificate for the second client -> Expects Failure\n" +
		"\u2022 Using the second client, retrieves from the directory its SSA\n" +
		"\u2022 Performs a DCR on the provided authorization server with the second client -> Expects a success \n" +
		"\u2022 Performs a PUT on the registration endpoint using the SSA from the second client with the certificate from the first client -> Expects a failure\n" +
		"\u2022 Performs a PUT on the registration endpoint using the SSA from the second client with the certificate from the second client -> Expects a success\n" +
		"\u2022 DELETEs the second registered client from the authorization server\n" +
		"\u2022 DELETEs the first registered client from the authorization server",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.discoveryUrl",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.client_id",
		"directory2.client_id",
		"mtls2.key",
		"mtls2.cert",
		"mtls2.ca",
		"directory.client_id",
		"directory.discoveryUrl",
		"directory.apibase",
		"directory.keystore",
		"resource.brazilOrganizationId"
	}
)
// hide various config values from the FAPI base module we don't need
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks",
	"resource.brazilPaymentConsent",
	"resource.brazilPixPayment",
	"resource.brazilCpf",
	"resource.brazilCnpj"
})
@VariantNotApplicable(parameter = ClientAuthType.class, values = {"private_key_jwt"})// only applicable for mtls client auth
public class DcrAttemptClientTakeoverFvpTestModule extends AbstractFAPI1AdvancedFinalBrazilDCR {

	protected ClientAuthType clientAuthType;

	@Override
	protected void configureClient() {
		clientAuthType = getVariant(ClientAuthType.class);
		super.configureClient();
	}

	@Override
	protected void callRegistrationEndpoint() {
		call(sequence(CallDynamicRegistrationEndpointAndVerifySuccessfulResponse.class));
		callAndContinueOnFailure(ClientManagementEndpointAndAccessTokenRequired.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2");
		callAndStopOnFailure(CheckScopesFromDynamicRegistrationEndpointContainsConsentsOrPayments.class);
		eventLog.endBlock();

		eventLog.startBlock("Make PUT request to client configuration endpoint with no changes expecting success");
		callAndStopOnFailure(CreateClientConfigurationRequestFromDynamicClientRegistrationResponse.class);
		// get a new SSA (technically there should be one in the DCR response, but they may be single use?)
		callAndStopOnFailure(FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken.class);
		callAndStopOnFailure(AddSoftwareStatementToClientConfigurationRequest.class);
		callAndStopOnFailure(CallClientConfigurationEndpoint.class);
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentTypeHttpStatus200.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentType.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckClientIdFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckRedirectUrisFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationUriFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		eventLog.endBlock();

		eventLog.startBlock("Use client_credentials grant to obtain a Valid Token");
		call(getPreTokenCallSequence());
		callAndStopOnFailure(CallTokenEndpoint.class);
		callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
		eventLog.endBlock();

		eventLog.startBlock("Attempt 'GET' on client configuration endpoint using MTLS certificate for different software, which must succeed");

		//switching to the second client
		eventLog.startBlock("Switch to second client");
		env.putObject("config", "client2", env.getElementFromObject("config", "client").getAsJsonObject());
		callAndStopOnFailure(OverrideClientWith2ndClientFull.class);

		callAndStopOnFailure(ExtractMTLSCertificatesFromConfiguration.class);

		// https://github.com/OpenBanking-Brasil/specs-seguranca/issues/199

		env.removeObject("registration_client_endpoint_request_body"); // so a 'GET' is made
		callAndStopOnFailure(CallClientConfigurationEndpoint.class, "OIDCD-4.2");
		env.mapKey("endpoint_response", "registration_client_endpoint_response");
		callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE, "RFC7592-2.1");
		call(exec().unmapKey("endpoint_response"));
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentTypeHttpStatus200.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");

		getSsa();
		eventLog.endBlock();

		eventLog.startBlock("Make sure that the second client cannot request a Valid Token");
		call(getPreTokenCallSequence());
		callAndStopOnFailure(CallTokenEndpointAndReturnFullResponse.class);
		callAndStopOnFailure(CheckTokenEndpointHttpStatusNot200.class);
		eventLog.endBlock();

		eventLog.startBlock("Perform Dynamic Client Registration for second client");
		env.mapKey("client", "alt_client");
		ClientAuthType clientAuthType = getVariant(ClientAuthType.class);
		callAndStopOnFailure(FAPIBrazilExtractJwksUriFromSoftwareStatement.class, "BrazilOBDCR-7.1-5");
		callAndStopOnFailure(CreateEmptyDynamicRegistrationRequest.class);
		callAndStopOnFailure(AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest.class);
		if (!jarm.isTrue()) {
			// implicit is only required when id_token is returned in frontchannel
			callAndStopOnFailure(AddImplicitGrantTypeToDynamicRegistrationRequest.class);
		}
		callAndStopOnFailure(AddRefreshTokenGrantTypeToDynamicRegistrationRequest.class);
		callAndStopOnFailure(AddClientCredentialsGrantTypeToDynamicRegistrationRequest.class);
		if (clientAuthType == ClientAuthType.MTLS) {
			callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
			callAndStopOnFailure(AddTlsClientAuthSubjectDnToDynamicRegistrationRequest.class);
		}
		callAndStopOnFailure(AddJwksUriToDynamicRegistrationRequest.class, "RFC7591-2", "BrazilOBDCR-7.1-5");
		callAndStopOnFailure(AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment.class);
		if (jarm.isTrue()) {
			callAndStopOnFailure(SetResponseTypeCodeInDynamicRegistrationRequest.class);
		} else {
			callAndStopOnFailure(SetResponseTypeCodeIdTokenInDynamicRegistrationRequest.class);
		}
		callAndContinueOnFailure(FapiBrazilVerifyRedirectUriContainedInSoftwareStatement.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1-6");
		callAndStopOnFailure(AddRedirectUriToDynamicRegistrationRequest.class);
		callAndStopOnFailure(AddSoftwareStatementToDynamicRegistrationRequest.class);

		// Call Registration endpoint
		env.mapKey("dynamic_registration_endpoint_response", "dynamic_registration_endpoint_response_alter");
		call(sequence(CallDynamicRegistrationEndpointAndVerifySuccessfulResponse.class));
		callAndContinueOnFailure(ClientManagementEndpointAndAccessTokenRequired.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2");
		env.unmapKey("dynamic_registration_endpoint_response");

		env.unmapKey("mutual_tls_authentication");
		env.unmapKey("client");

		eventLog.startBlock("Calling PUT on the registration endpoint for the first client using the SSA from the second client with the certificate from the first client");

		callAndStopOnFailure(CreateClientConfigurationRequestFromDynamicClientRegistrationResponse.class);
		callAndStopOnFailure(AddSoftwareStatementToClientConfigurationRequest.class);
		callAndStopOnFailure(FAPIBrazilExtractJwksUriFromSoftwareStatement.class, "BrazilOBDCR-7.1-5");
		callAndStopOnFailure(AddJwksUriToDynamicRegistrationRequest.class, "RFC7591-2", "BrazilOBDCR-7.1-5");
		callAndStopOnFailure(CallClientConfigurationEndpoint.class);
		env.mapKey("endpoint_response", "registration_client_endpoint_response");
		callAndContinueOnFailure(EnsureHttpStatusCodeIs400or401.class, Condition.ConditionResult.FAILURE, "RFC7592-2.1");
		callAndContinueOnFailure(CheckNoClientIdFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE);
		call(exec().unmapKey("endpoint_response"));

		eventLog.startBlock("Calling PUT on the registration endpoint for the first client using the SSA from the second client with the certificate from the second client");

		env.mapKey("mutual_tls_authentication", "altmtls");
		callAndStopOnFailure(CallClientConfigurationEndpoint.class);
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentTypeHttpStatus200.class, Condition.ConditionResult.WARNING, "OIDCD-4.3");
		env.unmapKey("mutual_tls_authentication");
	}

	@Override
	public void cleanup() {
		env.unmapKey("mutual_tls_authentication");
		env.unmapKey("client");
		super.cleanup();
		eventLog.startBlock("Unregister dynamically registered client 2");
		env.mapKey("mutual_tls_authentication", "altmtls");
		env.mapKey("client", "alt_client");
		super.cleanup();
	}

	private ConditionSequence getPreTokenCallSequence() {
		ConditionSequence sequence = sequenceOf(
			condition(CreateTokenEndpointRequestForClientCredentialsGrant.class),
			condition(SetConsentsScopeOnTokenEndpointRequest.class),
			condition(AddClientIdToTokenEndpointRequest.class),
			condition(CreateClientAuthenticationAssertionClaims.class),
			condition(SignClientAuthenticationAssertion.class),
			condition(AddClientAssertionToTokenEndpointRequest.class)
		);

		if (env.getString("scopeToBeUsed").equals("payments")) {
			sequence.replace(SetConsentsScopeOnTokenEndpointRequest.class, condition(SetPaymentsScopeOnTokenEndpointRequest.class));
		}

		if (clientAuthType == ClientAuthType.MTLS) {
			sequence.skip(CreateClientAuthenticationAssertionClaims.class, "Not needed for MTLS")
				.skip(SignClientAuthenticationAssertion.class, "Not needed for MTLS")
				.skip(AddClientAssertionToTokenEndpointRequest.class, "Not needed for MTLS");
		}

		return sequence;
	}

	@Override
	public void start() {
		fireTestFinished();
	}

	@Override
	protected void setupResourceEndpoint() {
		// not needed as resource endpoint won't be called
	}

	@Override
	protected boolean scopeContains(String requiredScope) {
		// Not needed as scope field is optional
		return false;
	}

}
