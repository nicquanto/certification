package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public abstract class AbstractEnsureConsentResponseCode extends AbstractCondition {

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		JsonObject response = env.getObject("consent_endpoint_response_full");
		int status = OIDFJSON.getInt(response.get("status"));
		if(status != getExpectedStatus()) {
			throw error("Failed to obtain expected status", args("expected status", getExpectedStatus(), "obtained status", status));
		} else {
			logSuccess("Correct status obtained", args("status", status));
		}
		return env;
	}

	protected abstract int getExpectedStatus();
}
