package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.creditOperations.financing.v2.FinancingResponseValidatorV2;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareAllCreditOperationsPermissionsForHappyPath;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.AddScopesForFinancingsApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractApiResourceTestModuleV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "financings_api_resources_test-module_v2",
	displayName = "Validate structure of financing API and Resources API resources V2",
	summary = "Makes sure that the Resource API and the API that is the scope of this test plan are returning the same available IDs\n" +
		"\u2022Create a consent with all the permissions needed to access the tested API\n" +
		"\u2022 Expects server to create the consent with 201\n" +
		"\u2022 Redirect the user to authorize at the financial institution\n" +
		"\u2022 Call the tested resource API\n" +
		"\u2022 Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the id provided by this API\n" +
		"\u2022 Call the resources API\n" +
		"\u2022 Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.participants",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class FinancingsApiResourcesTestModuleV2 extends AbstractApiResourceTestModuleV2 {

	@Override
	protected Class<? extends Condition> buildApiConfigResourceUrlFromConsentUrl() {
		return BuildFinancingsConfigResourceUrlFromConsentUrl.class;
	}

	@Override
	protected Class<? extends Condition> scopeAddingCondition() {
		return AddScopesForFinancingsApi.class;
	}

	@Override
	protected Class<? extends Condition> permissionPreparationCondition() {
		return PrepareAllCreditOperationsPermissionsForHappyPath.class;
	}

	@Override
	protected Class<? extends Condition> apiValidator() {
		return FinancingResponseValidatorV2.class;
	}

	@Override
	protected String apiName() {
		return "financing";
	}

	@Override
	protected String apiResourceId() {
		return "contractId";
	}

	@Override
	protected String resourceType() {
		return EnumResourcesType.FINANCING.name();
	}

}

