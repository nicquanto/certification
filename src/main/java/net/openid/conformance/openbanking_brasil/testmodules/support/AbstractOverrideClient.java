package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractOverrideClient extends AbstractCondition {
    String orgJwks = "{\n" +
		"    \"keys\" : [\n" +
		"        {\n" +
		"            \"p\": \"2l0h2pco8a3Az0mMDHNmAyXLikF1EkCsCYzY6-GMlc8K8YWN08KWAGe9kfMOZifSoXpfanOP0gKOZHXZTIkaDIugvveFR97SUDXPpQmyedParhZz8lTf-B2PDXLAHtrZbUajnS8SwUjyHc8Y6Omjz-NlM8O_0p7X7uBsNFHBCxE\",\n" +
		"            \"kty\": \"RSA\",\n" +
		"            \"q\": \"2HcOxjEZPWWV1UwsPtI_2-Cz-UVbSn1ceFC11UzHKUF6e7nLeUWBmX3Glm7apfYUqQPdzfaGyTAz660oIOMdstfeaqmqKs4OWBeB9L_xQ8C9mm_tFM82BrOaXxYwnTa6Tmt-PZ2C39XjQypcPH70doZSJ32U4h0a5z_MXYEXrPU\",\n" +
		"            \"d\": \"IhH_XcCgyJCFW6Ip-ID1BFkcWsCRJ-BKOHmYAuzy1JEM8lj7aYqSNrNAtL5VPRebt9k8bDekl49gryO_bM2a7C886cZdNM_Kqe7vDYpHq2o_BdDN1G-MjmJ4kRrXrgcQaRkDlf-jOmW2_0LEvvXOWrSzwBvdxYqf4Smf-TOFXRtFCb6EK8_UErtjjIG--otLZwTKk6vhj13v9MPx2mq7fQ0O5U7MwlXPrEmCiBTPjHsqwsexQQQCMAV8VMgYbcNEY9Se4bC7PqbjdLPHTCqRaRwnhy5ows8dwVUtZ7CoV3laen3K9GCfiy4mW8ZgHh2eDk6LrGYG_RAZOnW4YEquQQ\",\n" +
		"            \"e\": \"AQAB\",\n" +
		"            \"use\": \"sig\",\n" +
		"            \"kid\": \"_PofFrN5DHA6jvha6jnFTbTfh4OZ-RbDKY9HxgKhQT0\",\n" +
		"            \"qi\": \"YipKaRURVmVO0t18dnJtaEL_Xhzm0D5jbt5Wj_12rX7ztqJjbDoV5ti6ns8eytpxws-5qf49CLl_Nh-twYaeI8QTl0Ja8sq-0McU0hv0mKqEGv7BV7lJKXWHEUc1il1VE9IGolqEQ0wLcFtQCm_EJ7XSVseYIDU70GIl7gIkSbk\",\n" +
		"            \"dp\": \"2ULMiibX8A5GxneQxlqS5xLVvwt9pdl-0km3EoppTCyh4MhW4pi6klClhu9kJjP9F_kfslSkhflRH5c5yzKmHlFWcGrpyyZh7rh7juNlFl25OHjSiAv2g3E1gdXoXUq2BknARIYvjOw9KXeCe0rrV84SRTzAQJkWk2_reOrWvWE\",\n" +
		"            \"alg\": \"PS256\",\n" +
		"            \"dq\": \"c9-3ruxVgIJCpL1z6eUOfhtY_BTee4-D0nwR-i5xZmpAsL6SZ0aHy6yCg8CvlquV_6usLjbLjCMbiAtXMdOh8DySOTK9ftBy6UEcNfIRqp0gYe2MUdliKFA4ULJGL8I-FQCBwWXxyvRqBWhFm7SyFEhu7ejRrAC8iBGkymDyVYk\",\n" +
		"            \"n\": \"uKQidVHqTjoWOyMG2hApxf-379ifin-x8aSvoZr5nicx98jpl8Mcqz-EM7eknwqfelCpIYMpKEjT1KeLqlQe8e0DYympJW1e2osEsD3BVR6u_Iu1rgT5f-fCmaJSSD7MHhcsdKsoijaE69zxns1pf1AtfR-aatQalYd8eL0jYx0H-NMUSeuZYNlTpwroJHtBnggnIe5M7yKjEuGEtUBVBefiR2hqzGgr7JygJkij0m3TqzvCqixZ9IkfAi5aY3fqN1Z9h7B5KUViEyv2NiaD7gEwx7TASsJzw_47rGZ1J4XuCHdd53B5wzpDBLFe1H0umI4ui2VVAjYTgeUYgLYDRQ\"\n" +
		"        }\n" +
		"    ]\n" +
		"}";
    String sandboxCa = "-----BEGIN CERTIFICATE-----\n" +
        "MIIEajCCA1KgAwIBAgIUdIYzEFdw7QJcrySyq6IiEwZfTfAwDQYJKoZIhvcNAQEL\n" +
        "BQAwazELMAkGA1UEBhMCQlIxHDAaBgNVBAoTE09wZW4gQmFua2luZyBCcmFzaWwx\n" +
        "FTATBgNVBAsTDE9wZW4gQmFua2luZzEnMCUGA1UEAxMeT3BlbiBCYW5raW5nIFJv\n" +
        "b3QgU0FOREJPWCAtIEcxMB4XDTIwMTIxMTEwMDAwMFoXDTIzMTIxMTEwMDAwMFow\n" +
        "cTELMAkGA1UEBhMCQlIxHDAaBgNVBAoTE09wZW4gQmFua2luZyBCcmFzaWwxFTAT\n" +
        "BgNVBAsTDE9wZW4gQmFua2luZzEtMCsGA1UEAxMkT3BlbiBCYW5raW5nIFNBTkRC\n" +
        "T1ggSXNzdWluZyBDQSAtIEcxMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC\n" +
        "AQEA6fX+272mHX5QAcDaWkVHFWjnDIcORNUJU3OuNyeuOYhlvXJWydrXe3O+cV+P\n" +
        "S39faMj/nfem3GfJBE7Xn0bWA/8ksxSfrg1BUBJDge4YBBw+VflI3A0g1fk9wJ3H\n" +
        "GInsvV4serRJ/ISJTfs0uRNugX+RrbkT/T0tup4vGd3Kl2sbwUdDjokuJNJHANeO\n" +
        "DRkQ+ra+9Wht71FBlc07yPf7qtpaWHm6aS3s47OJD35ixkG4xiZuHsScxcVtlo1V\n" +
        "W98P2cQfH9H2lll4wWlPTVHpPThB2EYrPhwcxDh8kHkkOHNkyHO/fYM47u7H4VeQ\n" +
        "V75LXWKa7iWmZg+WhFb8TXSr/wIDAQABo4H/MIH8MA4GA1UdDwEB/wQEAwIBBjAP\n" +
        "BgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBSGf1itF/WCtk60BbP7sM4RQ99MvjAf\n" +
        "BgNVHSMEGDAWgBSHE+yWPmLsIRwMSlY68iUM45TpyzBMBggrBgEFBQcBAQRAMD4w\n" +
        "PAYIKwYBBQUHMAGGMGh0dHA6Ly9vY3NwLnNhbmRib3gucGtpLm9wZW5iYW5raW5n\n" +
        "YnJhc2lsLm9yZy5icjBLBgNVHR8ERDBCMECgPqA8hjpodHRwOi8vY3JsLnNhbmRi\n" +
        "b3gucGtpLm9wZW5iYW5raW5nYnJhc2lsLm9yZy5ici9pc3N1ZXIuY3JsMA0GCSqG\n" +
        "SIb3DQEBCwUAA4IBAQBy4928pVPeiHItbneeOAsDoc4Obv5Q4tn0QpqTlSeCSBbH\n" +
        "IURfEr/WaS8sv0JTbIPQEfiO/UtaN8Qxh7j5iVqTwTwgVaE/vDkHxGOen5YxAuyV\n" +
        "1Fpm4W4oQyybiA6puHEBcteuiYZHppGSMus3bmFYTPE+9B0+W914VZeHDujJ2Y3Y\n" +
        "Mc32Q+PC+Zmv8RfaXp7+QCNYSXR5Ts3q3IesWGmlvAM5tLQi75JmzdWXJ1uKU4u3\n" +
        "Nrw5jY4UaOlvB5Re2BSmcjxdLT/5pApzkS+tO6lICnPAtk/Y6dOJ0YxQBMImtliY\n" +
        "p02yfwRaqP8WJ4CnwUHil3ZRt8U9I+psU8b4WV/3\n" +
        "-----END CERTIFICATE-----\n" +
        "-----BEGIN CERTIFICATE-----\n" +
        "MIIDpjCCAo6gAwIBAgIUS3mWeRx1uG/SMl/ql55VwRtNz7wwDQYJKoZIhvcNAQEL\n" +
        "BQAwazELMAkGA1UEBhMCQlIxHDAaBgNVBAoTE09wZW4gQmFua2luZyBCcmFzaWwx\n" +
        "FTATBgNVBAsTDE9wZW4gQmFua2luZzEnMCUGA1UEAxMeT3BlbiBCYW5raW5nIFJv\n" +
        "b3QgU0FOREJPWCAtIEcxMB4XDTIwMTIxMTEwMDAwMFoXDTI1MTIxMDEwMDAwMFow\n" +
        "azELMAkGA1UEBhMCQlIxHDAaBgNVBAoTE09wZW4gQmFua2luZyBCcmFzaWwxFTAT\n" +
        "BgNVBAsTDE9wZW4gQmFua2luZzEnMCUGA1UEAxMeT3BlbiBCYW5raW5nIFJvb3Qg\n" +
        "U0FOREJPWCAtIEcxMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp50j\n" +
        "jNh0wu8ioziC1HuWqOfgXwxeiePiRGw5tKDqKIbC7XV1ghEcDiymTHHWWJSQ1LEs\n" +
        "mYpZVwaos5Mrz2xJwytg8K5eqFqa7QvfOOul29bnzEFk+1gX/0nOYws3Lba9E7S+\n" +
        "uPaUmfElF4r2lcCNL2f3F87RozqZf+DQBdGUzAt9n+ipY1JpqfI3KF/5qgRkPoIf\n" +
        "JD+aj2Y1D6eYjs5uMRLU8FMYt0CCfv/Ak6mq4Y9/7CaMKp5qjlrrDux00IDpxoXG\n" +
        "Kx5cK0KgACb2UBZ98oDQxcGrbRIyp8VGmv68BkEQcm7NljP863uBVxtnVTpRwQ1x\n" +
        "wYEbmSSyoonXy575wQIDAQABo0IwQDAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/\n" +
        "BAUwAwEB/zAdBgNVHQ4EFgQUhxPslj5i7CEcDEpWOvIlDOOU6cswDQYJKoZIhvcN\n" +
        "AQELBQADggEBAFoYqwoH7zvr4v0SQ/hWx/bWFRIcV/Rf6rEWGyT/moVAEjPbGH6t\n" +
        "yHhbxh3RdGcPY7Pzn797lXDGRu0pHv+GAHUA1v1PewCp0IHYukmN5D8+Qumem6by\n" +
        "HyONyUASMlY0lUOzx9mHVBMuj6u6kvn9xjL6xsPS+Cglv/3SUXUR0mMCYf963xnF\n" +
        "BIRLTRlbykgJomUptVl/F5U/+8cD+lB/fcZPoQVI0kK0VV51jAODSIhS6vqzQzH4\n" +
        "cpUmcPh4dy+7RzdTTktxOTXTqAy9/Yx+fk18O9qSQw1MKa9dDZ4YLnAQS2fJJqIE\n" +
        "1DXIta0LpqM4pMoRMXvp9SLU0atVZLEu6Sc=\n" +
        "-----END CERTIFICATE-----\n";

	abstract String clientCert();

	abstract String clientKey();

	abstract String role();

	abstract String directoryClientId();

	abstract String clientJwks();

	String orgJwks() {
		return orgJwks;
	}

	@Override
	public Environment evaluate(Environment env) {

		JsonObject client = (JsonObject) env.getElementFromObject("config", "client");
		if (client == null) {
			client = new JsonObject();
			env.putObject("config", "client", client);
		}
		client.add("jwks", new JsonParser().parse(clientJwks()).getAsJsonObject());
		client.add("org_jwks", new JsonParser().parse(orgJwks()).getAsJsonObject());

		JsonObject directory = (JsonObject) env.getElementFromObject("config", "directory");
		if (directory == null) {
			directory = new JsonObject();
			env.putObject("config", "directory", directory);
		}
		directory.addProperty("client_id", directoryClientId());

		JsonObject mtls = (JsonObject) env.getElementFromObject("config", "mtls");
		if (mtls == null) {
			mtls = new JsonObject();
			env.putObject("config", "mtls", mtls);
		}
		mtls.addProperty("cert", clientCert());
		mtls.addProperty("key", clientKey());
		mtls.addProperty("ca", sandboxCa);

		log("Hardcoded client with "+role()+" role loaded into config",
			env.getObject("config"));

		return env;
	}
}
