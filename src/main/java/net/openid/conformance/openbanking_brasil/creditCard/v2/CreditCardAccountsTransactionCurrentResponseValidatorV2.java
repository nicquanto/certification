package net.openid.conformance.openbanking_brasil.creditCard.v2;

import net.openid.conformance.logging.ApiName;


/**
 * This class corresponds to {@link CreditCardAccountsTransactionResponseValidatorV2}
 * Api: swagger/openBanking/swagger-credit-cards-api-V2.yaml
 * Api endpoint: /accounts/{creditCardAccountId}/transactions-current
 * Api version: 2.0.1.final
 **/
@ApiName("Credit Card Bill Transaction V2")
public class CreditCardAccountsTransactionCurrentResponseValidatorV2 extends CreditCardAccountsTransactionResponseValidatorV2 {}
