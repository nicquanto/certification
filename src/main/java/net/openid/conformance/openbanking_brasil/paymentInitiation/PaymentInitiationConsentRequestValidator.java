package net.openid.conformance.openbanking_brasil.paymentInitiation;

import com.google.common.collect.Sets;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.LocalInstrumentsEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentAccountTypesEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PersonTypesEnumV2;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

@ApiName("Payment Initiation Consent Config")
public class PaymentInitiationConsentRequestValidator extends AbstractJsonAssertingCondition {
	public static final Set<String> TYPES = Sets.newHashSet("PIX");

	@Override
	@PreEnvironment(required = "consent_endpoint_request")
	public Environment evaluate(Environment environment) {

		JsonObject body = environment.getObject("consent_endpoint_request");

		if(body == null) {
			throw error("consent_endpoint_request not found, please verify if you have correctly filled in brazilPaymentConsent and brazilQrdnPaymentConsent.");
		}
		assertHasField(body.getAsJsonObject(), ROOT_PATH);
		assertField(body, new ObjectField.Builder(ROOT_PATH).setValidator(this::assertInnerFields).build());

		return environment;
	}

	private void assertInnerFields(JsonObject body) {

		assertField(body,
			new StringField
				.Builder("ibgeTownCode")
				.setMinLength(7)
				.setMaxLength(7)
				.setPattern("^\\d{7}$")
				.setOptional()
				.build());

		assertField(body, new ObjectField.Builder("loggedUser").setValidator(this::assertLoggedUser).build());

		assertField(body,
			new ObjectField
				.Builder("businessEntity")
				.setValidator(this::assertBusinessEntity)
				.setOptional()
				.setNullable()
				.build());

		assertField(body, new ObjectField.Builder("creditor").setValidator(this::assertCreditor).build());
		assertField(body, new ObjectField.Builder("payment").setValidator(this::assertPayment).build());

		assertField(body,
			new ObjectField
				.Builder("debtorAccount")
				.setValidator(this::assertDebtorAccount)
				.build());
	}

	private void assertLoggedUser(JsonObject loggedUser) {
		assertField(loggedUser, new ObjectField.Builder("document").setValidator(this::assertLoggedUserDocument).build());
	}

	private void assertLoggedUserDocument(JsonObject document) {
		assertField(document,
			new StringField
				.Builder("identification")
				.setPattern("^\\d{11}$")
				.setMaxLength(11)
				.build());

		assertField(document,
			new StringField
				.Builder("rel")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}

	private void assertBusinessEntity(JsonObject businessEntity) {
		assertField(businessEntity, new ObjectField.Builder("document").setValidator(this::assertBusinessEntityDocument).build());
	}

	private void assertBusinessEntityDocument(JsonObject document) {
		assertField(document,
			new StringField
				.Builder("identification")
				.setPattern("^\\d{14}$")
				.setMaxLength(14)
				.build());

		assertField(document,
			new StringField
				.Builder("rel")
				.setPattern("^[A-Z]{4}$")
				.setMaxLength(4)
				.build());
	}

	private void assertCreditor(JsonObject creditor) {

		assertField(creditor,
			new StringField
				.Builder("personType")
				.setEnums(PersonTypesEnumV2.toSet())
				.setMaxLength(15)
				.build());

		assertField(creditor,
			new StringField
				.Builder("cpfCnpj")
				.setPattern("^\\d{11}$|^\\d{14}$")
				.setMinLength(11)
				.setMaxLength(14)
				.build());

		assertField(creditor,
			new StringField
				.Builder("name")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(140)
				.build());
	}

	private void assertPayment(JsonObject payment) {

		assertField(payment,
			new StringField
				.Builder("type")
				.setMaxLength(3)
				.setEnums(TYPES)
				.build());

		assertField(payment,
			new DatetimeField
				.Builder("date")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.setOptional()
				.build());

		assertField(payment,
			new StringField
				.Builder("currency")
				.setPattern("^([A-Z]{3})$")
				.setMaxLength(3)
				.build());

		assertField(payment,
			new StringField
				.Builder("amount")
				.setMinLength(4)
				.setMaxLength(19)
				.setPattern("^((\\d{1,16}\\.\\d{2}))$")
				.build());

		assertField(payment,
			new StringField
				.Builder("ibgeTownCode")
				.setMinLength(7)
				.setMaxLength(7)
				.setPattern("^\\d{7}$")
				.setOptional()
				.build());

		assertField(payment,
			new ObjectField
				.Builder("details")
				.setValidator(this::assertDetails)
				.build());
	}

	private void assertDetails(JsonObject details) {

		assertField(details,
			new StringField
				.Builder("localInstrument")
				.setEnums(LocalInstrumentsEnumV2.toSet())
				.setMaxLength(4)
				.build());

		if(OIDFJSON.getString(details.get("localInstrument")).equalsIgnoreCase(LocalInstrumentsEnumV2.QRES.toString()) || OIDFJSON.getString(details.get("localInstrument")).equalsIgnoreCase(LocalInstrumentsEnumV2.QRDN.toString())){
			assertField(details,
				new StringField
					.Builder("qrCode")
					.setPattern("[\\w\\W\\s]*")
					.setMaxLength(512)
					.build());
		}

		if(!OIDFJSON.getString(details.get("localInstrument")).equalsIgnoreCase(LocalInstrumentsEnumV2.MANU.toString())) {
			assertField(details,
				new StringField
					.Builder("proxy")
					.setPattern("[\\w\\W\\s]*")
					.setMaxLength(77)
					.build());
		}

		assertField(details,
			new ObjectField
				.Builder("creditorAccount")
				.setValidator(this::assertDebtorAccount)
				.build());
	}

	private void assertDebtorAccount(JsonObject debtorAccount) {

		assertField(debtorAccount,
			new StringField
				.Builder("ispb")
				.setPattern("^[0-9]{8}$")
				.setMaxLength(8)
				.setMinLength(8)
				.build());

		assertField(debtorAccount,
			new StringField
				.Builder("issuer")
				.setPattern("^\\d{4}$")
				.setMaxLength(4)
				.setOptional()
				.build());

		assertField(debtorAccount,
			new StringField
				.Builder("number")
				.setPattern("^\\d{3,20}$")
				.setMaxLength(20)
				.setMinLength(3)
				.build());

		assertField(debtorAccount,
			new StringField
				.Builder("accountType")
				.setEnums(PaymentAccountTypesEnumV2.toSet())
				.setMaxLength(4)
				.build());
	}
}
