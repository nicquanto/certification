package net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.phase4b.LinksAndMetaOPFValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.StringField;

/**
 * Api: swagger/openinsurance/phase4b/variable-incomes_1.0.0-rc1.0.yml
 * Api endpoint: /investments
 * Api git hash:
 */

@ApiName("Get Variable Incomes - ProductList")
public class GetVariableIncomesProductListValidator extends AbstractJsonAssertingCondition {


	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectArrayField.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.setMinItems(0)
				.build());

		new LinksAndMetaOPFValidator(this).assertMetaAndLinks(body);
		return environment;
	}

	private void assertData(JsonObject body) {
		assertField(body,
			new StringField
				.Builder("brandName")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(80)
				.build());

		assertField(body,
			new StringField
				.Builder("companyCnpj")
				.setPattern("^\\d{14}$")
				.setMaxLength(14)
				.build());

		assertField(body,
			new StringField
				.Builder("investmentId")
				.setPattern("^[a-zA-Z0-9][a-zA-Z0-9-]{0,99}$")
				.setMaxLength(100)
				.build());
	}
}
