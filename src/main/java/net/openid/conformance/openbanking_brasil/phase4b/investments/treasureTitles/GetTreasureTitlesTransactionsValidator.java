package net.openid.conformance.openbanking_brasil.phase4b.investments.treasureTitles;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.phase4b.investments.LinksAndMetaValidatorTransactions;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api: swagger/openinsurance/phase4b/treasure-titles_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}/transactions
 * Api git hash:
 */

@ApiName("Get Treasure Titles - Transactions")
public class GetTreasureTitlesTransactionsValidator extends AbstractJsonAssertingCondition {

	public static final Set<String> TRANSACTION_TYPE = SetUtils.createSet("COMPRA, VENDA, CANCELAMENTO, VENCIMENTO, PAGAMENTO_JUROS, AMORTIZACAO, TRANSFERENCIA_TITULARIDADE, TRANSFERENCIA_CUSTODIA, OUTROS");

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectArrayField.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.setMinItems(0)
				.build());

		new LinksAndMetaValidatorTransactions(this).assertMetaAndLinks(body);
		return environment;
	}

	private void assertData(JsonObject data) {
		String typeValue = OIDFJSON.getString(findByPath(data, "type"));

		assertField(data,
			new StringField
				.Builder("type")
				.setEnums(SetUtils.createSet("ENTRADA, SAIDA"))
				.build());

		assertField(data,
			new StringField
				.Builder("transactionType")
				.setEnums(TRANSACTION_TYPE)
				.build());

		StringField.Builder typeAdditionalInfoBuilder = new StringField
			.Builder("transactionTypeAdditionalInfo")
			.setPattern("[\\w\\W\\s]*")
			.setMaxLength(100);

		if (!OIDFJSON.getString(findByPath(data, "transactionType")).equals("OUTROS")) {
			typeAdditionalInfoBuilder.setOptional();
		}

		assertField(data, typeAdditionalInfoBuilder.build());

		assertField(data,
			new StringField
				.Builder("transactionDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.build());

		assertField(data,
			new ObjectField
				.Builder("transactionUnitPrice")
				.setValidator(this::assertAmountMaxLength24)
				.build());

		assertField(data,
			new StringField
				.Builder("transactionQuantity")
				.setPattern("^\\d{1,15}\\.\\d{2,8}$")
				.setMaxLength(24)
				.build());

		assertField(data,
			new ObjectField
				.Builder("transactionGrossValue")
				.setValidator(this::assertAmountMaxLength20)
				.build());


		ObjectField.Builder incomeTax = new ObjectField
			.Builder("incomeTax")
			.setValidator(this::assertAmountMaxLength20);

		if (!typeValue.equals("SAIDA")) {
			incomeTax.setOptional();
		}
		assertField(data, incomeTax.build());


		ObjectField.Builder financialTransactionTax = new ObjectField
			.Builder("financialTransactionTax")
			.setValidator(this::assertAmountMaxLength20);

		if (!typeValue.equals("SAIDA")) {
			financialTransactionTax.setOptional();
		}
		assertField(data, financialTransactionTax.build());

		assertField(data,
			new ObjectField
				.Builder("transactionNetValue")
				.setValidator(this::assertAmountMaxLength20)
				.build());


		StringField.Builder remunerationTransactionRate = new StringField
			.Builder("remunerationTransactionRate")
			.setPattern("^\\d{1}\\.\\d{6}$")
			.setMaxLength(8);

		if (!typeValue.equals("ENTRADA")) {
			remunerationTransactionRate.setOptional();
		}
		assertField(data, remunerationTransactionRate.build());

		assertField(data,
			new StringField
				.Builder("transactionId")
				.setPattern("^[a-zA-Z0-9][a-zA-Z0-9-]{0,99}$")
				.setMaxLength(100)
				.build());
	}

	private void assertAmountMaxLength24(JsonObject amount) {
		assertField(amount,
			new StringField
				.Builder("amount")
				.setPattern("^\\d{1,15}\\.\\d{2,8}$")
				.setMaxLength(24)
				.build());

		assertField(amount,
			new StringField
				.Builder("currency")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}

	private void assertAmountMaxLength20(JsonObject amount) {
		assertField(amount,
			new StringField
				.Builder("amount")
				.setPattern("^\\d{1,15}\\.\\d{2,4}$")
				.setMaxLength(20)
				.build());

		assertField(amount,
			new StringField
				.Builder("currency")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}
}
