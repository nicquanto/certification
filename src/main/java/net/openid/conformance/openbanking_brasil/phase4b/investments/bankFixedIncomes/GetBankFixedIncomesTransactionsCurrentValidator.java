package net.openid.conformance.openbanking_brasil.phase4b.investments.bankFixedIncomes;

import net.openid.conformance.logging.ApiName;

/**
 * Api: swagger/openinsurance/phase4b/bank-fixed-incomes_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}/transactions-current
 * Api git hash:
 */

@ApiName("Get Bank Fixed Incomes - Transactions Current")
public class GetBankFixedIncomesTransactionsCurrentValidator extends GetBankFixedIncomesTransactionsValidator {
}
