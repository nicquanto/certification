package net.openid.conformance.openbanking_brasil.phase4b.investments.creditFixedIncomes;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.phase4b.LinksAndMetaOPFValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api: swagger/openinsurance/phase4b/credit-fixed-incomes_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}
 * Api git hash:
 */

@ApiName("Get Credit Fixed Incomes - ProductIdentification")
public class GetCreditFixedIncomesProductIdentificationValidator extends AbstractJsonAssertingCondition {

	public static final Set<String> INVESTMENT_TYPE = SetUtils.createSet("DEBENTURES, CRI, CRA");
	public static final Set<String> INDEXER = SetUtils.createSet("CDI, DI, TR, IPCA, IGP_M, IGP_DI, INPC, BCP, TLC, SELIC, PRE_FIXADO, OUTROS");
	public static final Set<String> VOUCHER_PAYMENT = SetUtils.createSet("MENSAL, TRIMESTRAL, SEMESTRAL, ANUAL, IRREGULAR, OUTROS");

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectField.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.build());

		new LinksAndMetaOPFValidator(this).assertMetaAndLinks(body);
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("issuerInstitutionCnpjNumber")
				.setMaxLength(14)
				.setPattern("^\\d{14}$")
				.build());

		assertField(data,
			new StringField
				.Builder("isinCode")
				.setMaxLength(12)
				.setPattern("^[A-Z]{2}([A-Z0-9]){9}\\d{1}$")
				.setOptional()
				.build());

		String investmentTypeValue = OIDFJSON.getString(findByPath(data, "investmentType"));
		assertField(data,
			new StringField
				.Builder("investmentType")
				.setEnums(INVESTMENT_TYPE)
				.build());

		StringField.Builder debtorCnpjNumber = 	new StringField
			.Builder("debtorCnpjNumber")
			.setMaxLength(14)
			.setPattern("^\\d{14}$");

		if (!(investmentTypeValue.equals("CRI") || investmentTypeValue.equals("CRA"))) {
			debtorCnpjNumber.setOptional();
		}
		assertField(data, debtorCnpjNumber.build());

		StringField.Builder debtorName = new StringField
			.Builder("debtorName")
			.setMaxLength(70)
			.setPattern("[\\w\\W\\s]*");

		if (!(investmentTypeValue.equals("CRI") || investmentTypeValue.equals("CRA"))) {
			debtorName.setOptional();
		}
		assertField(data, debtorName.build());

		assertField(data,
			new StringField
				.Builder("taxExemptProduct")
				.setEnums(SetUtils.createSet("SIM, NAO"))
				.build());

		assertField(data,
			new ObjectField
				.Builder("remuneration")
				.setValidator(this::assertRemuneration)
				.build());

		assertField(data,
			new ObjectField
				.Builder("issueUnitPrice")
				.setValidator(unitPrice -> {
					assertField(unitPrice,
						new StringField
							.Builder("amount")
							.setPattern("^\\d{1,15}\\.\\d{2,8}$")
							.setMaxLength(24)
							.build());

					assertField(unitPrice,
						new StringField
							.Builder("currency")
							.setPattern("^[A-Z]{3}$")
							.setMaxLength(3)
							.build());
				})
				.build());

		assertField(data,
			new StringField
				.Builder("issueDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("dueDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("voucherPaymentIndicator")
				.setEnums(SetUtils.createSet("SIM, NAO"))
				.build());

		StringField.Builder voucherPaymentPeriodicity = new StringField
			.Builder("voucherPaymentPeriodicity")
			.setEnums(VOUCHER_PAYMENT);

		if (!OIDFJSON.getString(findByPath(data, "voucherPaymentIndicator")).equals("SIM")) {
			voucherPaymentPeriodicity.setOptional();
		}
		assertField(data, voucherPaymentPeriodicity.build());


		StringField.Builder periodicityAdditionalInfo = new StringField
			.Builder("voucherPaymentPeriodicityAdditionalInfo")
			.setPattern("[\\w\\W\\s]*")
			.setMaxLength(50);

		if (data.has("voucherPaymentPeriodicity")) {
			if (!OIDFJSON.getString(findByPath(data, "voucherPaymentPeriodicity")).equals("OUTROS")) {
				periodicityAdditionalInfo.setOptional();
			}
		} else {
			periodicityAdditionalInfo.setOptional();
		}
		assertField(data, periodicityAdditionalInfo.build());

		assertField(data,
			new StringField
				.Builder("clearingCode")
				.setPattern("[\\w\\W-]*")
				.setMaxLength(30)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("purchaseDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("gracePeriodDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.build());
	}

	private void assertRemuneration(JsonObject remuneration) {
		String indexerValue = OIDFJSON.getString(findByPath(remuneration, "indexer"));
		StringField.Builder preFixedRate = new StringField
			.Builder("preFixedRate")
			.setPattern("^\\d{1}\\.\\d{6}$")
			.setMaxLength(8);

		if (!indexerValue.equals("PRE_FIXADO")) {
			preFixedRate.setOptional();
		}
		assertField(remuneration, preFixedRate.build());

		StringField.Builder postFixedIndexerPercentage = new StringField
			.Builder("postFixedIndexerPercentage")
			.setPattern("^\\d{1}\\.\\d{6}$")
			.setMaxLength(8);

		if (!indexerValue.equals("PRE_FIXADO")) {
			postFixedIndexerPercentage.setOptional();
		}
		assertField(remuneration, postFixedIndexerPercentage.build());

		assertField(remuneration,
			new StringField
				.Builder("rateType")
				.setEnums(SetUtils.createSet("LINEAR, EXPONENCIAL"))
				.build());

		assertField(remuneration,
			new StringField
				.Builder("ratePeriodicity")
				.setEnums(SetUtils.createSet("MENSAL, ANUAL, DIARIO, SEMESTRAL"))
				.build());

		assertField(remuneration,
			new StringField
				.Builder("calculation")
				.setEnums(SetUtils.createSet("DIAS_UTEIS, DIAS_CORRIDOS"))
				.build());

		assertField(remuneration,
			new StringField
				.Builder("indexer")
				.setEnums(INDEXER)
				.build());

		StringField.Builder indexerAdditionalInfo = new StringField
			.Builder("indexerAdditionalInfo")
			.setPattern("[\\w\\W\\s]*")
			.setMaxLength(50);

		if (!indexerValue.equals("OUTROS")) {
			indexerAdditionalInfo.setOptional();
		}
		assertField(remuneration, indexerAdditionalInfo.build());
	}
}
