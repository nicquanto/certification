package net.openid.conformance.openbanking_brasil.phase4b.investments.treasureTitles;

import net.openid.conformance.logging.ApiName;

/**
 * Api: swagger/openinsurance/phase4b/treasure-titles_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}/transactions-current
 * Api git hash:
 */

@ApiName("Get Treasure Titles - Transactions Current")
public class GetTreasureTitlesTransactionsCurrentValidator extends GetTreasureTitlesTransactionsValidator {
}
