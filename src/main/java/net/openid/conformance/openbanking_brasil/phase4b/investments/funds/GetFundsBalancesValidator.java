package net.openid.conformance.openbanking_brasil.phase4b.investments.funds;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.phase4b.LinksAndMetaOPFValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

/**
 * Api: swagger/openinsurance/phase4b/funds_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}/balances
 * Api git hash:
 */

@ApiName("Get Funds - Balances")
public class GetFundsBalancesValidator extends AbstractJsonAssertingCondition {

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectField.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.build());

		new LinksAndMetaOPFValidator(this).assertMetaAndLinks(body);
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("referenceDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.build());

		assertField(data,
			new ObjectField
				.Builder("grossAmount")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("netAmount")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("incomeTaxProvision")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new ObjectField
				.Builder("financialTransactionTaxProvision")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new ObjectField
				.Builder("blockedAmount")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new StringField
				.Builder("quotaQuantity")
				.setPattern("^\\d{1,15}\\.\\d{2,8}$")
				.setMaxLength(24)
				.build());

		assertField(data,
			new ObjectField
				.Builder("quotaGrossPriceValue")
				.setValidator(amount -> {
					assertField(amount,
						new StringField
							.Builder("amount")
							.setPattern("^-?\\d{1,15}\\.\\d{2,8}$")
							.setMaxLength(25)
							.build());

					assertField(amount,
						new StringField
							.Builder("currency")
							.setPattern("^[A-Z]{3}$")
							.setMaxLength(3)
							.build());
				})
				.build());
	}

	private void assertAmountMaxLength21(JsonObject amount) {
		assertField(amount,
			new StringField
				.Builder("amount")
				.setPattern("^-?\\d{1,15}\\.\\d{2,4}$")
				.setMaxLength(21)
				.build());

		assertField(amount,
			new StringField
				.Builder("currency")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}

	private void assertAmountMaxLength20(JsonObject amount) {
		assertField(amount,
			new StringField
				.Builder("amount")
				.setPattern("^\\d{1,15}\\.\\d{2,4}$")
				.setMaxLength(20)
				.build());

		assertField(amount,
			new StringField
				.Builder("currency")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}
}
