package net.openid.conformance.openbanking_brasil.phase4b.investments.funds;

import net.openid.conformance.logging.ApiName;

/**
 * Api: swagger/openinsurance/phase4b/funds_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}/transactions-current
 * Api git hash:
 */

@ApiName("Get Funds - Transactions Current")
public class GetFundsTransactionsCurrentValidator extends GetFundsTransactionsValidator {
}
