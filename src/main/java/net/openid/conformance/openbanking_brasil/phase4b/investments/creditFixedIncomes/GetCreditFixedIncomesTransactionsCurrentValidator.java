package net.openid.conformance.openbanking_brasil.phase4b.investments.creditFixedIncomes;

import net.openid.conformance.logging.ApiName;

/**
 * Api: swagger/openinsurance/phase4b/credit-fixed-incomes_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}/transactions-current
 * Api git hash:
 */

@ApiName("Get Credit Fixed Incomes - Transactions Current")
public class GetCreditFixedIncomesTransactionsCurrentValidator extends GetCreditFixedIncomesTransactionsValidator {
}
