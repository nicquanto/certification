package net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.phase4b.LinksAndMetaOPFValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

/**
 * Api: swagger/openinsurance/phase4b/variable-incomes_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}/broker-notes/{brokerNoteId}
 * Api git hash:
 */

@ApiName("Get Variable Incomes - Broker Note Details")
public class GetVariableIncomesBrokerNoteDetailsValidator extends AbstractJsonAssertingCondition {

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectField.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.build());

		new LinksAndMetaOPFValidator(this).assertMetaAndLinks(body);
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("brokerNoteNumber")
				.setPattern("^\\d{1,16}$")
				.setMaxLength(16)
				.build());

		assertField(data,
			new ObjectField
				.Builder("grossValue")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("brokerageFee")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("clearingSettlementFee")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("clearingRegistrationFee")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("stockExchangeAssetTradeNoticeFee")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("stockExchangeFee")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("clearingCustodyFee")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("taxes")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("incomeTax")
				.setValidator(this::assertAmountMaxLength21)
				.build());

		assertField(data,
			new ObjectField
				.Builder("netValue")
				.setValidator(this::assertAmountMaxLength21)
				.build());
	}

	private void assertAmountMaxLength21(JsonObject amount) {
		assertField(amount,
			new StringField
				.Builder("amount")
				.setPattern("^-?\\d{1,15}\\.\\d{2,4}$")
				.setMaxLength(21)
				.build());

		assertField(amount,
			new StringField
				.Builder("currency")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());

	}
}
