package net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.phase4b.LinksAndMetaOPFValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

/**
 * Api: swagger/openinsurance/phase4b/variable-incomes_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}
 * Api git hash:
 */

@ApiName("Get Variable Incomes- ProductIdentification")
public class GetVariableIncomesProductIdentificationValidator extends AbstractJsonAssertingCondition {


	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectField.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.build());

		new LinksAndMetaOPFValidator(this).assertMetaAndLinks(body);
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("issuerInstitutionCnpjNumber")
				.setMaxLength(14)
				.setPattern("^\\d{14}$")
				.build());

		assertField(data,
			new StringField
				.Builder("isinCode")
				.setMaxLength(12)
				.setPattern("^[A-Z]{2}([A-Z0-9]){9}\\d{1}$")
				.build());

		assertField(data,
			new StringField
				.Builder("ticker")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(35)
				.build());
	}
}
