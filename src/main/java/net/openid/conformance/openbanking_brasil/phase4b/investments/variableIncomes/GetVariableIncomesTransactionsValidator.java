package net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.phase4b.investments.LinksAndMetaValidatorTransactions;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api: swagger/openinsurance/phase4b/variable-incomes_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}/transactions
 * Api git hash:
 */

@ApiName("Get Variable Incomes - Transactions")
public class GetVariableIncomesTransactionsValidator extends AbstractJsonAssertingCondition {

	public static final Set<String> TRANSACTION_TYPE = SetUtils.createSet("COMPRA, VENDA, DIVIDENDOS, JCP, ALUGUEIS, INPLIT, SPLIT_TRANSFERENCIA_CUSTODIA, TRANSFERENCIA_TITULARIDADE, OUTROS");

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectArrayField.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.setMinItems(0)
				.build());

		new LinksAndMetaValidatorTransactions(this).assertMetaAndLinks(body);
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("type")
				.setEnums(SetUtils.createSet("ENTRADA, SAIDA"))
				.build());

		assertField(data,
			new StringField
				.Builder("transactionType")
				.setEnums(TRANSACTION_TYPE)
				.build());

		StringField.Builder typeAdditionalInfoBuilder = new StringField
			.Builder("typeAdditionalInfo")
			.setPattern("[\\w\\W\\s]*")
			.setMaxLength(100);

		if (!OIDFJSON.getString(findByPath(data, "transactionType")).equals("OUTROS")) {
			typeAdditionalInfoBuilder.setOptional();
		}

		assertField(data, typeAdditionalInfoBuilder.build());

		assertField(data,
			new StringField
				.Builder("transactionDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.setMinLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("priceFactor")
				.setPattern("^\\d{1,15}\\.\\d{2,8}$")
				.setMaxLength(24)
				.setOptional()
				.build());

		ObjectField.Builder transactionUnitPrice = new ObjectField
				.Builder("transactionUnitPrice")
				.setValidator(this::assertAmountMaxLength20);

		if (!(OIDFJSON.getString(findByPath(data, "transactionType")).equals("COMPRA") ||
			OIDFJSON.getString(findByPath(data, "transactionType")).equals("VENDA") ||
			OIDFJSON.getString(findByPath(data, "transactionType")).equals("ALUGUEIS"))) {
			typeAdditionalInfoBuilder.setOptional();
		}
		assertField(data, transactionUnitPrice.build());


		StringField.Builder transactionQuantity = new StringField
			.Builder("transactionQuantity")
			.setPattern("^\\d{1,15}\\.\\d{2,8}$")
			.setMaxLength(24);

		if (!(OIDFJSON.getString(findByPath(data, "transactionType")).equals("COMPRA") ||
			OIDFJSON.getString(findByPath(data, "transactionType")).equals("VENDA") ||
			OIDFJSON.getString(findByPath(data, "transactionType")).equals("ALUGUEIS"))) {
			transactionQuantity.setOptional();
		}

		assertField(data, transactionQuantity.build());

		assertField(data,
			new ObjectField
				.Builder("transactionValue")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new StringField
				.Builder("transactionId")
				.setPattern("^[a-zA-Z0-9][a-zA-Z0-9-]{0,99}$")
				.setMaxLength(100)
				.build());

		StringField.Builder brokerNoteId = new StringField
			.Builder("brokerNoteId")
			.setPattern("^[a-zA-Z0-9][a-zA-Z0-9-]{0,99}$")
			.setMaxLength(100);

		if (!(OIDFJSON.getString(findByPath(data, "transactionType")).equals("COMPRA") ||
			OIDFJSON.getString(findByPath(data, "transactionType")).equals("VENDA"))) {
			brokerNoteId.setOptional();
		}

		assertField(data,brokerNoteId.build());
	}

	private void assertAmountMaxLength20(JsonObject amount) {
		assertField(amount,
			new StringField
				.Builder("amount")
				.setPattern("^\\d{1,15}\\.\\d{2,4}$")
				.setMaxLength(20)
				.build());

		assertField(amount,
			new StringField
				.Builder("currency")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}
}
