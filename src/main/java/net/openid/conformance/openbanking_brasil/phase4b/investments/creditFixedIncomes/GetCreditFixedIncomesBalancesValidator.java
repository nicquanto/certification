package net.openid.conformance.openbanking_brasil.phase4b.investments.creditFixedIncomes;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.phase4b.LinksAndMetaOPFValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

/**
 * Api: swagger/openinsurance/phase4b/credit-fixed-incomes_1.0.0-rc1.0.yml
 * Api endpoint: /investments/{investmentId}/balances
 * Api git hash:
 */

@ApiName("Get Credit Fixed Incomes - Balances")
public class GetCreditFixedIncomesBalancesValidator extends AbstractJsonAssertingCondition {

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
			new ObjectField.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.build());

		new LinksAndMetaOPFValidator(this).assertMetaAndLinks(body);
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("referenceDateTime")
				.setPattern("(^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])T(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)Z$)")
				.setMaxLength(20)
				.build());

		assertField(data,
			new ObjectField
				.Builder("updatedUnitPrice")
				.setValidator(this::assertAmountMaxLength24)
				.build());

		assertField(data,
			new StringField
				.Builder("quantity")
				.setPattern("^\\d{1,15}\\.\\d{2,8}$")
				.setMaxLength(24)
				.setMinLength(4)
				.build());

		assertField(data,
			new ObjectField
				.Builder("grossAmount")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new ObjectField
				.Builder("netAmount")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new ObjectField
				.Builder("incomeTax")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new ObjectField
				.Builder("financialTransactionTax")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new ObjectField
				.Builder("blockedBalance")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new ObjectField
				.Builder("purchaseUnitPrice")
				.setValidator(this::assertAmountMaxLength24)
				.build());

		assertField(data,
			new StringField
				.Builder("preFixedRate")
				.setPattern("^\\d{1}\\.\\d{6}$")
				.setMaxLength(8)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("postFixedIndexerPercentage")
				.setPattern("^\\d{1}\\.\\d{6}$")
				.setMaxLength(8)
				.setOptional()
				.build());

		assertField(data,
			new ObjectField
				.Builder("fine")
				.setValidator(this::assertAmountMaxLength20)
				.build());

		assertField(data,
			new ObjectField
				.Builder("latePayment")
				.setValidator(this::assertAmountMaxLength20)
				.build());
	}

	private void assertAmountMaxLength24(JsonObject amount) {
		assertField(amount,
			new StringField
				.Builder("amount")
				.setPattern("^\\d{1,15}\\.\\d{2,8}$")
				.setMaxLength(24)
				.setMinLength(4)
				.build());

		assertField(amount,
			new StringField
				.Builder("currency")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}

	private void assertAmountMaxLength20(JsonObject amount) {
		assertField(amount,
			new StringField
				.Builder("amount")
				.setPattern("^\\d{1,15}\\.\\d{2,4}$")
				.setMaxLength(20)
				.setMinLength(4)
				.build());

		assertField(amount,
			new StringField
				.Builder("currency")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}
}
