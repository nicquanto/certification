package net.openid.conformance.openbanking_brasil.plans.dcr;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v2.resources.ResourcesApiDcrHappyFlowTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.resources.ResourcesApiDcrSubjectDnV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.resources.ResourcesApiDcrTestModuleAttemptClientTakeoverV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.resources.ResourcesApiDcrTestModuleUnauthorizedClientV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;

import java.util.List;

@PublishTestPlan(
	testPlanName = "dcr-dados_test-plan",
	profile = OBBProfile.OBB_PROFILE_DCR,
	displayName = PlanNames.CUSTOMER_DATA_DCR,
	summary = "Functional tests for DCR with Open Banking Brasil customer data APIs"
)
public class CustomerDataDCRFunctionalTestPlan implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					ResourcesApiDcrHappyFlowTestModuleV2.class,
					ResourcesApiDcrTestModuleUnauthorizedClientV2.class,
					ResourcesApiDcrTestModuleAttemptClientTakeoverV2.class,
					ResourcesApiDcrSubjectDnV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil")
				)
			)
		);
	}
}
