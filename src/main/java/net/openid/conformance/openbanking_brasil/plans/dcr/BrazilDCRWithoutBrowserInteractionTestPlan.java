package net.openid.conformance.openbanking_brasil.plans.dcr;

import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.DCRMultipleClientTest;
import net.openid.conformance.openbanking_brasil.testmodules.DcrNoSubjectTypeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.DcrSandboxCredentialsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.DcrSubjectDnFvpTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.dcr.DcmSubjectDnFvpTetsModule;
import net.openid.conformance.openbanking_brasil.testmodules.dcr.DcrBrcac2022SupportFvpTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantSelection;

import java.util.List;


@PublishTestPlan(
	testPlanName = "dcr-fvp_test-plan",
	displayName = PlanNames.OBB_DCR_WITHOUT_BROWSER_INTERACTION_TEST_PLAN,
	profile = OBBProfile.OBB_PROFILE_DCR
)
public class BrazilDCRWithoutBrowserInteractionTestPlan implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {

		return List.of(
			new ModuleListEntry(
				List.of(
					FAPI1AdvancedFinalBrazilDCRHappyFlowNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRHappyFlowVariantNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRHappyFlowVariant2NoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRClientDeletionNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidRegistrationAccessTokenNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidSoftwareStatementSignatureNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRNoSoftwareStatementNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRNoMTLSNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRBadMTLSNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigBadJwksUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidJwksByValueNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidRedirectUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRNoRedirectUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidRedirectUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidJwksUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidJwksByValueNoAuthFlow.class,
//					DcrAttemptClientTakeoverTestModule.class,
					DCRConsentsBadLoggedUser.class,
					DcrSandboxCredentialsTestModule.class,
					DcrNoSubjectTypeTestModule.class,
					DCRMultipleClientTest.class,
					DcrSubjectDnFvpTestModule.class,
					DcmSubjectDnFvpTetsModule.class,
					DcrBrcac2022SupportFvpTestModule.class
				),
				List.of(new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"))
			)
		);

	}
	public static String certificationProfileName(VariantSelection variant) {
		return "BR-OB Adv. OP DCR";
	}
}
