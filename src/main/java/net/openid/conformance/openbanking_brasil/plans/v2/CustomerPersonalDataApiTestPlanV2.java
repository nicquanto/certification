package net.openid.conformance.openbanking_brasil.plans.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2.CustomerPersonalDataApiTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2.CustomerPersonalWrongPermissionsTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckCustomerV2Module;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.CustomerPersonalApiOperationalLimitsTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "customer-personal_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.CUSTOMER_PERSONAL_DATA_API_PLAN_NAME_V2,
	summary = "Structural and logical tests for OpenBanking Brasil-conformant Personal Customer Data API"
)
public class CustomerPersonalDataApiTestPlanV2 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckCustomerV2Module.class,
					CustomerPersonalDataApiTestModuleV2.class,
					CustomerPersonalWrongPermissionsTestModuleV2.class,
					CustomerPersonalApiOperationalLimitsTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString())
				)
			)
		);
	}
}
