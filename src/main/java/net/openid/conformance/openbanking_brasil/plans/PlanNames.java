package net.openid.conformance.openbanking_brasil.plans;

public class PlanNames {
	/** VERSION 1 **/
	/* Phase 1 - Open Data */
	public static final String ADMIN_API_TEST_PLAN  = "Functional Tests for Admin API - Based on Swagger version: 1.0.1 (Beta)";
	public static final String BANKING_AGENTS_API_TEST_PLAN = "Functional Tests for Channels - BankingAgents API - Based on Swagger version: 1.0.3 (Beta)";
	public static final String BRANCHES_API_TEST_PLAN = "Functional Tests for Channels - Branches API - Based on Swagger version: 1.0.3 (Beta)";
	public static final String ELECTRONIC_CHANNELS_API_TEST_PLAN = "Functional Tests for Channels - Electronic Channels API - Based on Swagger version: 1.0.3 (Beta)";
	public static final String PHONE_CHANNELS_API_TEST_PLAN = "Functional Tests for Channels - Phone Channels API - Based on Swagger version: 1.0.3 (Beta)";
	public static final String SHARED_AUTOMATED_TELLER_MACHINES_API_TEST_PLAN = "Functional Tests for Channels - Shared Automated Teller Machines API - Based on Swagger version: 1.0.3 (Beta)";


	public static final String BUSINESS_ACCOUNTS_API_TEST_PLAN = "Functional Tests for ProductsNServices - BusinessAccounts API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String BUSINESS_CREDIT_CARD_API_TEST_PLAN = "Functional Tests for ProductsNServices - BusinessCreditCard API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String BUSINESS_FINANCINGS_API_TEST_PLAN = "Functional Tests for ProductsNServices - BusinessFinancings API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String BUSINESS_INVOICE_FINANCINGS_API_TEST_PLAN = "Functional Tests for ProductsNServices - BusinessInvoiceFinancings API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String BUSINESS_LOANS_API_TEST_PLAN = "Functional Tests for ProductsNServices - BusinessLoans API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String PERSONAL_ACCOUNTS_API_TEST_PLAN = "Functional Tests for ProductsNServices - PersonalAccounts API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String PERSONAL_CREDIT_CARD_API_TEST_PLAN = "Functional Tests for ProductsNServices - PersonalCreditCard API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String PERSONAL_FINANCINGS_API_TEST_PLAN = "Functional Tests for ProductsNServices - PersonalFinancings API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String PERSONAL_INVOICE_FINANCINGS_API_TEST_PLAN = "Functional Tests for ProductsNServices - PersonalInvoiceFinancings API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String PERSONAL_LOANS_API_TEST_PLAN = "Functional Tests for ProductsNServices - PersonalLoans API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String UNARRANGED_ACCOUNT_BUSINESS_OVERDRAFT_API_TEST_PLAN = "Functional Tests for ProductsNServices - UnarrangedAccountBusinessOverdraft API - Based on Swagger version: 1.0.0 (Beta)";
	public static final String UNARRANGED_ACCOUNT_PERSONAL_OVERDRAFT_API_TEST_PLAN = "Functional Tests for ProductsNServices - UnarrangedAccountPersonalOverdraft API - Based on Swagger version: 1.0.0 (Beta)";

	/* Phase 2 - Customer Data */

	public static final String ACCOUNT_API_NAME = "Functional tests for accounts API - based on Swagger version: 1.0.3";
	public static final String CONSENTS_API_NAME = "Functional tests for consents API - based on Swagger version: 1.0.4";
	public static final String CREDIT_CARDS_API_PLAN_NAME = "Functional tests for Credit Card API - based on swagger version: 1.0.4";

	public static final String CUSTOMER_PERSONAL_DATA_API_PLAN_NAME = "Functional tests for personal customer data API - based on Swagger version: 1.0.3";
	public static final String CUSTOMER_BUSINESS_DATA_API_PLAN_NAME = "Functional tests for business customer data API - based on Swagger version: 1.0.3";
	public static final String RESOURCES_API_PLAN_NAME = "Functional tests for resources API - based on Swagger version: 1.0.2";

	public static final String CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME = "Functional tests for unarranged overdraft API - based on Swagger version: 1.0.4";
	public static final String LOANS_API_PLAN_NAME = "Functional tests for loans API - based on Swagger version: 1.0.4";
	public static final String FINANCINGS_API_NAME = "Functional tests for financings API - based on Swagger version: 1.0.4";
	public static final String CUSTOMER_DATA_DCR = "Functional Tests for DCR - DADOS Role";

	/* Phase 3 - Payment Initiation */

	public static final String OBB_DCR = "Functional Tests for DCR - Delete Not Executed";
	public static final String OBB_DCR_WITHOUT_BROWSER_INTERACTION_TEST_PLAN = "Functional Tests for DCR - FVP 1.0 - Live Tests";

	public static final String OBB_DCR_WITHOUT_BROWSER_INTERACTION_HOMOLG_TEST_PLAN = "Functional Tests for DCR - FVP 1.0 - Homologation Tests";
	public static final String NEW_STYLE_BRCAC_DCR = "Functional Tests for DCR - BRCAC new Format";
	public static final String PAYMENTS_API_DCR = "Functional Tests for DCR - PAGTO Role";
	public static final String PAYMENTS_API_DCR_DCM = "Functional Tests for DCR DCM - PAGTO Role - Webhook - Based on Swagger version: 2.0.0 (WIP)";
	public static final String PAYMENTS_API_PHASE_2_TEST_PLAN = "Functional Tests for Payments API - Based on Swagger version: 1.2.0";

	public static final String PAYMENTS_API_PHASE_3_V2_TEST_PLAN = "Functional Tests for Payments API - Based on Swagger version: 2.0.0";
	public static final String PAYMENTS_API_PHASE_3_TEST_PLAN = "Functional tests for payments PIX Scheduling (T3) - Based on Swagger version: 1.1.0 (WIP)";


	public static final String CREDIT_OPERATIONS_DISCOUNTED_CREDIT_RIGHTS_API_PLAN_NAME = "Functional tests for discounted credit rights API - based on Swagger version: 1.0.4";
	public static final String PAYMENTS_API_PHASE_1_TEST_PLAN = "Functional tests for payments API INIC, DICT and MANU (T0/T1) - Based on Swagger version: 1.0.1 - Limit Submission Date 14/01";

	public static final String PAYMENTS_API_ALL_TEST_PLAN = "Functional tests for payments API INIC, DICT, MANU, QRES, QRDN, Scheduling (T0/T1/T2/T3) - Based on Swagger version: 1.1.0 (WIP)";

	/* Phase 4B */

	public static final String VARIABLE_INCOMES_API_PHASE_4B_TEST_PLAN = "Functional Tests for Variable Incomes API - Based on Swagger version: 1.0.0-rc1.0 (Alpha)";
	public static final String BANK_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN = "Functional Tests for Bank Fixed Incomes API - Based on Swagger version: 1.0.0-rc1.0 (Alpha)";

	/** VERSION 2 **/
	public static final String LATEST_VERSION_2 = "2.0.1";
	public static final String LATEST_VERSION_2_WIP = "2.0.1 (WIP)";
	public static final String ACCOUNT_API_NAME_V2 = "Functional Tests for Accounts API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String CONSENTS_API_NAME_V2 = "Functional Tests for Consents API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String CREDIT_CARDS_API_PLAN_NAME_V2 = "Functional Tests for Credit Cards API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String CREDIT_OPERATIONS_DISCOUNTED_CREDIT_RIGHTS_API_PLAN_NAME_V2 = "Functional Tests for Invoice Financings API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String CUSTOMER_PERSONAL_DATA_API_PLAN_NAME_V2 = "Functional Tests for Customer - Personal API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String CUSTOMER_BUSINESS_DATA_API_PLAN_NAME_V2 = "Functional Tests for Customer - Business API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String RESOURCES_API_PLAN_NAME_V2 = "Functional Tests for Resources API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME_V2 = "Functional Tests for Unarranged Accounts Overdraft API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String LOANS_API_PLAN_NAME_V2 = "Functional Tests for Loans API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String FINANCINGS_API_NAME_V2 = "Functional Tests for Financings API - Based on Swagger version: " + LATEST_VERSION_2;
	public static final String OPERATIONAL_LIMITS_PLAN_NAME_V2 = "Functional tests for operational limits - based on Swagger version: " + LATEST_VERSION_2;

	/** FVP Tests **/
	public static final String OBB_PROD_FVP_PHASE_2 = "Production Functional Tests for Consents and Resources API - API Version 2.0.1";
	public static final String OBB_PROD_FVP_PAYMENTS = "Production Functional Tests for Payments API - API Version 2.0.0";
	public static final String OBB_PROD_HOMOLOG_PLAN = "Functional Tests with General Scope - DCR and Consents";

	/** Phase 4 **/
	public static final String TREASURE_TITLES_API_PHASE_4B_TEST_PLAN = "Functional Tests for Treasure Titles API - Based on Swagger version: 1.0.0-rc1.0 (Alpha)";


	/** Phase 4a **/
	public static final String CAPITALIZATION_BONDS_API_PHASE_4A_TEST_PLAN = "Functional Tests for Capitalization Bonds API - Based on Swagger version: 1.0.0-rc3.0";
	public static final String INSURANCE_AUTOMATIVE_API_PHASE_4A_TEST_PLAN = "Functional Tests for Insurance - Automotive Insurance API - Based on Swagger version: 1.0.0-rc3.0";
	public static final String INSURANCE_HOME_API_PHASE_4A_TEST_PLAN = "Functional Tests for Insurance - HomeInsurance API - Based on Swagger version: 1.0.0-rc3.0";
	public static final String INSURANCE_PERSONAL_API_PHASE_4A_TEST_PLAN = "Functional Tests for Insurance - PersonalInsurance API - Based on Swagger version: 1.0.0-rc3.0";
	public static final String PENSION_RISK_COVERAGES_API_PHASE_4A_TEST_PLAN = "Functional Tests for Pension - Risk Coverages - Based on Swagger version: 1.0.0-rc3.0";
	public static final String PENSION_SURVIVAL_COVERAGES_API_PHASE_4A_TEST_PLAN = "Functional Tests for Pension - Survival Coverages - Based on Swagger version: 1.0.0-rc3.0";



}
