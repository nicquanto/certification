package net.openid.conformance.openbanking_brasil.plans.yacs.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.payments.YACSPaymentsConsentsCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.payments.YACSPreFlightCertCheckPaymentsV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;

import java.util.List;
@PublishTestPlan(
	testPlanName = "Open Finance Brasil Functional Production Tests - FVP Phase 3 - Payments",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	displayName = PlanNames.OBB_PROD_FVP_PAYMENTS,
	summary = "Open Finance Brasil Functional Production Tests - FVP - Payments"
)
public class YACSPaymentsProductionTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					YACSPreFlightCertCheckPaymentsV2.class,
					YACSPaymentsConsentsCoreTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil")
				)
			)
		);
	}
}
