package net.openid.conformance.openbanking_brasil.plans.common;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.common.GetStatusValidator;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToGetProductsNChannelsApi;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;
@PublishTestPlan(
	testPlanName = "common-status_test-plan",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = "Functional Tests for Common - Status API - Based on Swagger version: 1.0.2 (Beta)",
	summary = "Structural and logical tests for OpenBanking Common API"
)
public class CommonApiStatusTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					CommonApiStatusTestModule.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}

	@PublishTestModule(
		testName = "common-status_api_structural_test-module",
		displayName = "Validate structure of Common status API resources",
		summary = "Validates the structure of Common status API resources",
		profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4
	)
	public static class CommonApiStatusTestModule extends AbstractNoAuthFunctionalTestModule {

		@Override
		protected void runTests() {
			runInBlock("Validate Common status response", () -> {
				callAndStopOnFailure(PrepareToGetProductsNChannelsApi.class);
				preCallResource();
				callAndContinueOnFailure(DoNotStopOnFailure.class);
				callAndContinueOnFailure(GetStatusValidator.class, Condition.ConditionResult.FAILURE);
			});
		}
	}
}
