package net.openid.conformance.openbanking_brasil.plans.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.*;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "payments_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.PAYMENTS_API_PHASE_3_V2_TEST_PLAN,
	summary = "Functional test for Open Finance Brasil payments API, including QR code and Pix Schedulling tests. " +
		"These tests are designed to validate the payment initiation of types MANU, DICT, INIC, QRES and QRDN ensuring " +
		"structural integrity and content validation. The tests are based on the Phase 3v2 Specifications " +
		"- https://openfinancebrasil.atlassian.net/wiki/spaces/OF/pages/18055169/v2.0.0+-+Pagamentos"
)
public class PaymentsApiPhase3TestPlanV2 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCertCheckPaymentsModuleV2.class,
					PaymentApiNoDebtorProvidedTestModuleV2.class,
					PaymentsApiContentTypeJwtRefreshTokenTestModuleV2.class,
                    PaymentsApiInvalidTokenTestModuleV2.class,
					PaymentsApiE2EIDTestModuleV2.class,
					PaymentsApiIncorrectCPFProxyTestModuleV2.class,
					PaymentsApiNegativeTestModuleV2.class,
					PaymentsApiPixSchedulingPatchDetentoraTestModuleV2.class,
					PaymentsApiPixSchedulingPatchUnhappyV2.class,
					PaymentsApiPixSchedulingSchdAcceptedTestModuleV2.class,
					PaymentsApiQRESMismatchConsentPaymentTestModuleV2.class,
					PaymentsApiRealEmailAddressWrongCreditorProxyTestModuleV2.class,
					PaymentsApiWrongEmailAddressProxyTestModuleV2.class,
					PaymentsConsentsApiDICTPixResponseTestModuleV2.class,
					PaymentsConsentsApiEnforceDICTTestModuleV2.class,
					PaymentsConsentsApiEnforceMANUTestModuleV2.class,
					PaymentsConsentsApiEnforceQRDNTestModuleV2.class,
					PaymentsConsentsApiEnforceQRDNWithQRESCodeTestModuleV2.class,
					PaymentsConsentsApiEnforceQRESNWithEmailAddressTestModuleV2.class,
					PaymentsConsentsApiEnforceQRESTestModuleV2.class,
					PaymentsConsentsApiEnforceQRESWithPhoneNumberTestModuleV2.class,
					PaymentsConsentsApiEnforceQRESWrongAmountTestModuleV2.class,
					PaymentsConsentsApiEnforceQRESWrongProxyTestModuleV2.class,
					PaymentsConsentsApiForceCheckBadSignatureTestV2.class,
					PaymentsConsentsApiINICPixResponseTestModuleV2.class,
					PaymentsConsentsApiMANUPixResponseTestModuleV2.class,
					PaymentsConsentsApiPhoneNumberProxyTestModuleV2.class,
					PaymentsConsentsApiQRDNHappyTestModuleV2.class,
					PaymentsConsentsApiQresTransactionIdentifierTestModuleV2.class,
					PaymentsConsentsJsonAcceptHeaderJwtReturnedTestModuleV2.class,
					PaymentsConsentsReuseIdempotencyKeyTestModuleV2.class,
					PaymentsConsentsReuseJtiTestModuleV2.class,
					PaymentsNoFundsTestModuleV2.class,
					PaymentsPixSchedulingDatesUnhappyV2.class,
					PaymentsApiPixSchedulingEndToEndUnhappyTestModuleV2.class,
					PaymentsConsumedConsentsTestModuleV2.class,
					PaymentsApiPixSchedulingPatchIniciadoraTestModuleV2.class,
					PaymentsConsentsApiNegativeTestModuleV2.class,
					PaymentsApiMultipleConsentsConditionalTestModuleV2.class,
					PaymentsApiMultipleConsentsCANCDetentoraConditionalTestModuleV2.class,
					PaymentsApiMultipleConsentsCANCIniciadoraConditionalTestModuleV2.class,
					PaymentsApiMultipleConsentsNoFundsConditionalTestModuleV2.class,
					PaymentsApiCancTemporizationTestV2.class,
					PaymentsApiTemporizationConditionalTestV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString())
				)
			)
		);
	}
}
