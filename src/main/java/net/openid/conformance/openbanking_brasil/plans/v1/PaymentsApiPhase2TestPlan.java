package net.openid.conformance.openbanking_brasil.plans.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.paymentInitiation.PaymentsApiBadPaymentSignatureFails;
import net.openid.conformance.openbanking_brasil.paymentInitiation.PaymentsApiFapiTesting;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentApiNoDebtorProvidedRealCreditorTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentApiNoDebtorProvidedTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiE2EIDTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiIncorrectCPFProxyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiInvalidCnpjTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiNegativeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiQRESMismatchConsentPaymentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiRealEmailAddressWrongCreditorProxyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiWrongEmailAddressProxyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiBadPaymentTypeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiDICTPixResponseTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiDateTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEmailAddressProxyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEnforceDICTTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEnforceMANUTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEnforceQRDNTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEnforceQRDNWithQRESCodeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEnforceQRESNWithEmailAddressTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEnforceQRESTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEnforceQRESWithPhoneNumberTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEnforceQRESWrongAmountTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiEnforceQRESWrongProxyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiForceCheckBadSignatureTest;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiINICPixResponseTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiMANUPixResponseTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiPhoneNumberProxyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiQRDNHappyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiQresTransactionIdentifierTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsInvalidPersonTypeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsJsonAcceptHeaderJwtReturnedTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsReuseIdempotencyKeyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsConsentsReuseJtiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PreFlightCertCheckPaymentsModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentsConsentsApiInvalidTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentsConsumedConsentsTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "payments_test-plan",
	profile = OBBProfile.DEV_ONLY,
	displayName = PlanNames.PAYMENTS_API_PHASE_2_TEST_PLAN,
	summary = "Structural and logical tests for OpenBanking Brasil payments API, including QR tests. These tests are designed to validate the payment initation of types MANU, DICT, INIC, QRES and QRDN ensuring structural integrity and content validation. "
)
public class PaymentsApiPhase2TestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCertCheckPaymentsModule.class,
					PaymentsConsentsApiEnforceQRDNTestModule.class,
					PaymentsConsentsApiEnforceQRESTestModule.class,
					PaymentsConsentsApiEnforceQRESNWithEmailAddressTestModule.class,
					PaymentsConsentsApiEnforceQRESWithPhoneNumberTestModule.class,
					PaymentsConsentsApiEnforceQRESWrongAmountTestModule.class,
					PaymentsConsentsApiEnforceQRESWrongProxyTestModule.class,
					PaymentsConsentsApiEnforceQRDNWithQRESCodeTestModule.class,
					PaymentsApiQRESMismatchConsentPaymentTestModule.class,
					PaymentsConsentsApiQRDNHappyTestModule.class,
					PaymentsConsentsApiQresTransactionIdentifierTestModule.class,
					// the below are also in the phase 1 test plan
					PaymentsApiTestModule.class,
					PaymentsApiNegativeTestModule.class,
					PaymentApiNoDebtorProvidedTestModule.class,
					PaymentsConsentsApiTestModule.class,
					PaymentsConsumedConsentsTestModule.class,
					PaymentsConsentsApiEnforceMANUTestModule.class,
					PaymentsConsentsApiMANUPixResponseTestModule.class,
					PaymentsConsentsApiEnforceDICTTestModule.class,
					PaymentsConsentsApiDICTPixResponseTestModule.class,
					PaymentsConsentsApiINICPixResponseTestModule.class,
					PaymentsConsentsApiEmailAddressProxyTestModule.class,
					PaymentsApiWrongEmailAddressProxyTestModule.class,
					PaymentsApiRealEmailAddressWrongCreditorProxyTestModule.class,
					PaymentApiNoDebtorProvidedRealCreditorTestModule.class,
					PaymentsConsentsApiPhoneNumberProxyTestModule.class,
					PaymentsApiIncorrectCPFProxyTestModule.class,
					PaymentsConsentsApiBadPaymentTypeTestModule.class,
					PaymentsConsentsApiDateTestModule.class,
					PaymentsConsentsInvalidPersonTypeTestModule.class,
					PaymentsConsentsReuseJtiTestModule.class,
					PaymentsConsentsJsonAcceptHeaderJwtReturnedTestModule.class,
					PaymentsConsentsReuseIdempotencyKeyTestModule.class,
					PaymentsApiFapiTesting.class,
					PaymentsApiBadPaymentSignatureFails.class,
					PaymentsConsentsApiForceCheckBadSignatureTest.class,
					PaymentsApiInvalidCnpjTestModule.class,
					PaymentsConsentsApiInvalidTestModule.class,
					PaymentsApiE2EIDTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString())
				)
			)
		);
	}
}
