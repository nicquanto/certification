package net.openid.conformance.openbanking_brasil.plans.dcr;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiDcrHappyFlowTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiDcrSubjectDn;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiDcrTestModuleAttemptClientTakeover;
import net.openid.conformance.openbanking_brasil.testmodules.PaymentsApiDcrTestModuleUnauthorizedClient;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;

import java.util.List;

@PublishTestPlan(
	testPlanName = "dcr-pagto_test-plan",
	profile = OBBProfile.OBB_PROFILE_DCR,
	displayName = PlanNames.PAYMENTS_API_DCR,
	summary = "Functional tests for DCR with Open Banking Brasil Payments API"
)
public class PaymentsDCRFunctionalTestPlan implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PaymentsApiDcrHappyFlowTestModule.class,
					PaymentsApiDcrTestModuleUnauthorizedClient.class,
					PaymentsApiDcrTestModuleAttemptClientTakeover.class,
					PaymentsApiDcrSubjectDn.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil")
				)
			)
		);
	}
}
