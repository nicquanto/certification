package net.openid.conformance.openbanking_brasil.plans.yacs.customerdata;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v2.resources.ResourcesApiTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.YACSPreFlightCertCheckV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Open Finance Brasil Functional Production Tests - FVP Phase 2 - Customer Data",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	displayName = PlanNames.OBB_PROD_FVP_PHASE_2,
	summary = "Structural and logical tests for OpenBanking Brasil-conformant Resources API"
)
public class YACSProductionTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					YACSPreFlightCertCheckV2Module.class,
					ResourcesApiTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil")
				)
			)
		);
	}
}
