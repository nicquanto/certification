package net.openid.conformance.openbanking_brasil.resourcesAPI;

public enum EnumResourcesType {
	ACCOUNT, CREDIT_CARD_ACCOUNT, LOAN, FINANCING, UNARRANGED_ACCOUNT_OVERDRAFT, INVOICE_FINANCING
}
