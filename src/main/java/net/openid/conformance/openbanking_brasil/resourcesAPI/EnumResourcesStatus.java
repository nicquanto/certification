package net.openid.conformance.openbanking_brasil.resourcesAPI;

public enum EnumResourcesStatus {
	AVAILABLE, UNAVAILABLE, TEMPORARILY_UNAVAILABLE, PENDING_AUTHORISATION
}
