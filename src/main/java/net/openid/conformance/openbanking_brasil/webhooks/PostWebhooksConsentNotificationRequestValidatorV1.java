package net.openid.conformance.openbanking_brasil.webhooks;

import net.openid.conformance.logging.ApiName;

/**
 * Api url: swagger/openBanking/webhooks/swagger-webhook-api-1.0.0.yml
 * Api endpoint: /payments/{versionApi}/consents/{consentId}
 * Api version: 1.0.0
 */

@ApiName("Post Webhooks Consent Notification Request V1")
public class PostWebhooksConsentNotificationRequestValidatorV1 extends RequestBodyWebhookValidatorV1 {
}
