package net.openid.conformance.openbanking_brasil.creditOperations.discountedCreditRights.v2;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.LinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.NumberField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api: swagger/openinsurance/discountedCreditRights/v2/swagger-invoice-financings-v2.yaml
 * Api endpoint: /contracts/{contractId}/scheduled-instalments
 * Api version: 2.0.1.final
 * Git hash:
 */

@ApiName("Invoice Financing Contract Installments V2")
public class InvoiceFinancingContractInstallmentsResponseValidatorV2 extends AbstractJsonAssertingCondition {
	private final LinksAndMetaValidator linksAndMetaValidator = new LinksAndMetaValidator(this);

	public static final Set<String> TYPE_NUMBER_OF_INSTALMENTS = SetUtils.createSet("DIA, SEMANA, MES, ANO, SEM_PRAZO_TOTAL");
	public static final Set<String> TYPE_CONTRACT_REMAINING = SetUtils.createSet("DIA, SEMANA, MES, ANO, SEM_PRAZO_REMANESCENTE");

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		assertField(body,
			new ObjectField
				.Builder(ROOT_PATH)
				.setValidator(this::assertInnerFields)
				.build());
		linksAndMetaValidator.assertMetaAndLinks(body);
		logFinalStatus();
		return environment;
	}

	private void assertInnerFields(JsonElement data) {
		assertField(data,
			new StringField
				.Builder("typeNumberOfInstalments")
				.setEnums(TYPE_NUMBER_OF_INSTALMENTS)
				.build());


		assertField(data,
			new StringField
				.Builder("typeContractRemaining")
				.setEnums(TYPE_CONTRACT_REMAINING)
				.build());


		NumberField.Builder totalNumberOfInstalmentsBuilder = new NumberField
			.Builder("totalNumberOfInstalments")
			.setMaxValue(999999);

		if(OIDFJSON.getString(findByPath(data, "typeNumberOfInstalments")).equals("SEM_PRAZO_TOTAL")){
			totalNumberOfInstalmentsBuilder.setOptional();
		}


		assertField(data, totalNumberOfInstalmentsBuilder.build());


		NumberField.Builder contractRemainingNumberBuilder = new NumberField
			.Builder("contractRemainingNumber")
			.setMaxValue(999999);

		if(OIDFJSON.getString(findByPath(data, "typeContractRemaining")).equals("SEM_PRAZO_REMANESCENTE")){
			totalNumberOfInstalmentsBuilder.setOptional();
		}

		assertField(data, contractRemainingNumberBuilder.build());

		assertField(data,
			new NumberField
				.Builder("paidInstalments")
				.setMaxValue(999999)
				.build());

		assertField(data,
			new NumberField
				.Builder("dueInstalments")
				.setMaxValue(999999)
				.build());

		assertField(data,
			new NumberField
				.Builder("pastDueInstalments")
				.setMaxValue(999999)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("balloonPayments")
				.setValidator(this::assertInnerFieldsBalloonPayments)
				.setMinItems(1)
				.setOptional()
				.build());
	}

	private void assertInnerFieldsBalloonPayments(JsonObject body) {
		assertField(body,
			new StringField
				.Builder("dueDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.setMinLength(2)
				.build());

		assertField(body,
			new ObjectField
				.Builder("amount")
				.setValidator(this::assertAmount)
				.build());
	}

	private void assertAmount(JsonObject body) {
		assertField(body,
			new StringField.
				Builder("amount")
				.setPattern("^\\d{1,15}\\.\\d{2,4}$")
				.setMinLength(4)
				.setMaxLength(20)
				.build());

		assertField(body,
			new StringField
				.Builder("currency")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}
}
