package net.openid.conformance.openbanking_brasil.creditOperations.loans.v2;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.LinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringArrayField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api: swagger/openinsurance/loans/v2/swagger_loans_apis-v2.yaml
 * Api endpoint: /contracts/{contractId}
 * Api version: 2.0.1.final
 * Git hash:
 */
@ApiName("Loans Contract V2")
public class ContractResponseValidatorV2 extends AbstractJsonAssertingCondition {
	private final LinksAndMetaValidator linksAndMetaValidator = new LinksAndMetaValidator(this);

	final Set<String> PRODUCT_TYPE = SetUtils.createSet("EMPRESTIMOS");
	final Set<String> PRODUCT_SUB_TYPE = SetUtils.createSet("HOME_EQUITY, CHEQUE_ESPECIAL, CONTA_GARANTIDA, CAPITAL_GIRO_TETO_ROTATIVO, CREDITO_PESSOAL_SEM_CONSIGNACAO, CREDITO_PESSOAL_COM_CONSIGNACAO, MICROCREDITO_PRODUTIVO_ORIENTADO, CAPITAL_GIRO_PRAZO_VENCIMENTO_ATE_365_DIAS, CAPITAL_GIRO_PRAZO_VENCIMENTO_SUPERIOR_365_DIAS");
	final Set<String> INSTALMENT_PERIODICITY = SetUtils.createSet("SEM_PERIODICIDADE_REGULAR, SEMANAL, QUINZENAL, MENSAL, BIMESTRAL, TRIMESTRAL, SEMESTRAL, ANUAL, OUTROS");
	final Set<String> AMORTIZATION_SCHEDULED = SetUtils.createSet("SAC, PRICE, SAM, SEM_SISTEMA_AMORTIZACAO, OUTROS");
	final Set<String> TAX_TYPE = SetUtils.createSet("NOMINAL, EFETIVA");
	final Set<String> INTEREST_RATE_TYPE = SetUtils.createSet("SIMPLES, COMPOSTO");
	final Set<String> TAX_PERIODICITY = SetUtils.createSet("AM, AA");
	final Set<String> CALCULATION = SetUtils.createSet("21/252, 30/360, 30/365");
	final Set<String> REFERENTIAL_RATE_INDEXER_TYPE = SetUtils.createSet("SEM_TIPO_INDEXADOR, PRE_FIXADO, POS_FIXADO, FLUTUANTES, INDICES_PRECOS, CREDITO_RURAL, OUTROS_INDEXADORES");
	final Set<String> REFERENTIAL_RATE_INDEXER_SUB_TYPE = SetUtils.createSet("SEM_SUB_TIPO_INDEXADOR, PRE_FIXADO, TR_TBF, TJLP, LIBOR, TLP, OUTRAS_TAXAS_POS_FIXADAS, CDI, SELIC, OUTRAS_TAXAS_FLUTUANTES, IGPM, IPCA, IPCC, OUTROS_INDICES_PRECO, TCR_PRE, TCR_POS, TRFC_PRE, TRFC_POS, OUTROS_INDEXADORES");
	final Set<String> FEE_CHARGE_TYPE = SetUtils.createSet("UNICA, POR_PARCELA");
	final Set<String> FEE_CHARGE = SetUtils.createSet("MINIMO, MAXIMO, FIXO, PERCENTUAL");
	final Set<String> CHARGE_TYPE = SetUtils.createSet("JUROS_REMUNERATORIOS_POR_ATRASO, MULTA_ATRASO_PAGAMENTO, JUROS_MORA_ATRASO, IOF_CONTRATACAO, IOF_POR_ATRASO, SEM_ENCARGO, OUTROS");

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		assertField(body,
			new ObjectField
				.Builder(ROOT_PATH)
				.setValidator(this::assertDataFields)
				.build());
		linksAndMetaValidator.assertMetaAndLinks(body);
		logFinalStatus();
		return environment;
	}

	private void assertDataFields(JsonObject body) {
		assertField(body,
			new StringField
				.Builder("contractNumber")
				.setMaxLength(100)
				.setMinLength(1)
				.setPattern("^[\\w\\W]{1,100}$")
				.build());

		assertField(body,
			new StringField
				.Builder("ipocCode")
				.setMaxLength(67)
				.setMinLength(22)
				.setPattern("^[\\w\\W]{22,67}$")
				.build());

		assertField(body,
			new StringField
				.Builder("productName")
				.setMaxLength(140)
				.setPattern("[\\w\\W\\s]*")
				.build());

		assertField(body,
			new StringField
				.Builder("productType")
				.setEnums(PRODUCT_TYPE)
				.build());

		assertField(body,
			new StringField
				.Builder("productSubType")
				.setEnums(PRODUCT_SUB_TYPE)
				.build());

		assertField(body,
			new DatetimeField
				.Builder("contractDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.build());

		assertField(body,
			new StringArrayField
				.Builder("disbursementDates")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.setMinItems(1)
				.setOptional()
				.build());

		assertField(body,
			new DatetimeField
				.Builder("settlementDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.setOptional()
				.build());

		assertField(body,
			new StringField
				.Builder("contractAmount")
				.setOptional()
				.setMaxLength(20)
				.setMinLength(4)
				.setPattern("^\\d{1,15}\\.\\d{2,4}$")
				.build());

		assertField(body,
			new StringField
				.Builder("currency")
				.setPattern("^(\\w{3}){1}$")
				.setMaxLength(3)
				.setOptional()
				.build());

		assertField(body,
			new DatetimeField
				.Builder("dueDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMinLength(2)
				.setMaxLength(10)
				.setOptional()
				.build());

		assertField(body,
			new StringField
				.Builder("instalmentPeriodicity")
				.setEnums(INSTALMENT_PERIODICITY)
				.build());

		assertField(body,
			new StringField
				.Builder("instalmentPeriodicityAdditionalInfo")
				.setMaxLength(50)
				.setPattern("[\\w\\W\\s]*")
				.setOptional()
				.build());

		assertField(body,
			new DatetimeField
				.Builder("firstInstalmentDueDate")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.setOptional()
				.build());

		assertField(body,
			new StringField
				.Builder("CET")
				.setPattern("^\\d{1,2}\\.\\d{6}$")
				.setMaxLength(9)
				.setMinLength(8)
				.setOptional()
				.build());

		assertField(body,
			new StringField
				.Builder("amortizationScheduled")
				.setEnums(AMORTIZATION_SCHEDULED)
				.build());

		if (body.has("amortizationScheduled") && OIDFJSON.getString(body.get("amortizationScheduled")).equals("OUTROS")) {
			assertField(body,
				new StringField
					.Builder("amortizationScheduledAdditionalInfo")
					.setMaxLength(200)
					.setPattern("[\\w\\W\\s]*")
					.build());
		} else {
			assertField(body,
				new StringField
					.Builder("amortizationScheduledAdditionalInfo")
					.setMaxLength(200)
					.setOptional()
					.setPattern("[\\w\\W\\s]*")
					.build());
		}

		StringField.Builder cnpjConsigneeBuilder = new StringField
			.Builder("cnpjConsignee")
			.setMaxLength(14)
			.setPattern("^\\d{14}$")
			.setOptional();

		if (OIDFJSON.getString(body.get("productSubType")).equals("CREDITO_PESSOAL_COM_CONSIGNACAO")) {
			cnpjConsigneeBuilder.setOptional(false);
		}
		assertField(body, cnpjConsigneeBuilder.build());

		assertField(body,
			new ObjectArrayField
				.Builder("interestRates")
				.setValidator(this::assertInnerFieldsForInterestRate)
				.setMinItems(0)
				.build());

		assertField(body,
			new ObjectArrayField
				.Builder("contractedFees")
				.setValidator(this::assertInnerFieldsContractedFees)
				.setMinItems(0)
				.build());

		assertField(body,
			new ObjectArrayField
				.Builder("contractedFinanceCharges")
				.setValidator(this::assertInnerFieldsCharges)
				.setMinItems(0)
				.build());
	}

	private void assertInnerFieldsForInterestRate(JsonObject body) {
		assertField(body,
			new StringField
				.Builder("taxType")
				.setEnums(TAX_TYPE)
				.build());

		assertField(body,
			new StringField
				.Builder("interestRateType")
				.setEnums(INTEREST_RATE_TYPE)
				.build());

		assertField(body,
			new StringField
				.Builder("taxPeriodicity")
				.setEnums(TAX_PERIODICITY)
				.setMaxLength(2)
				.build());

		assertField(body,
			new StringField
				.Builder("calculation")
				.setEnums(CALCULATION)
				.build());

		assertField(body,
			new StringField
				.Builder("referentialRateIndexerType")
				.setEnums(REFERENTIAL_RATE_INDEXER_TYPE)
				.build());

		assertField(body,
			new StringField
				.Builder("referentialRateIndexerSubType")
				.setEnums(REFERENTIAL_RATE_INDEXER_SUB_TYPE)
				.setOptional()
				.build());

		String referentialRateIndexerType = OIDFJSON.getString(findByPath(body, "referentialRateIndexerType"));


		StringField.Builder referentialRateIndexerAdditionalInfoBuilder = new StringField
			.Builder("referentialRateIndexerAdditionalInfo")
			.setMaxLength(140)
			.setPattern("[\\w\\W\\s]*")
			.setOptional();

		if(referentialRateIndexerType.equals("OUTROS_INDEXADORES") ||
		body.has("referentialRateIndexerSubType") &&
		OIDFJSON.getString(findByPath(body, "referentialRateIndexerSubType")).equals("OUTROS_INDEXADORES")){
			referentialRateIndexerAdditionalInfoBuilder.setOptional(false);
		}

		assertField(body, referentialRateIndexerAdditionalInfoBuilder.build());

		assertField(body,
			new StringField
				.Builder("preFixedRate")
				.setMaxLength(9)
				.setMinLength(8)
				.setPattern("^\\d{1,2}\\.\\d{6}$")
				.build());

		assertField(body,
			new StringField
				.Builder("postFixedRate")
				.setMaxLength(9)
				.setMinLength(8)
				.setPattern("^\\d{1,2}\\.\\d{6}$")
				.build());

		assertField(body,
			new StringField
				.Builder("additionalInfo")
				.setMaxLength(1200)
				.setPattern("[\\w\\W\\s]*")
				.setOptional()
				.build());
	}

	private void assertInnerFieldsContractedFees(JsonObject body) {

		assertField(body,
			new StringField
				.Builder("feeName")
				.setMaxLength(140)
				.setPattern("[\\w\\W\\s]*")
				.build());

		assertField(body,
			new StringField
				.Builder("feeCode")
				.setMaxLength(140)
				.setPattern("[\\w\\W\\s]*")
				.build());

		assertField(body,
			new StringField
				.Builder("feeChargeType")
				.setEnums(FEE_CHARGE_TYPE)
				.build());

		assertField(body,
			new StringField
				.Builder("feeCharge")
				.setEnums(FEE_CHARGE)
				.build());

		StringField.Builder feeAmountBuilder = new StringField
			.Builder("feeAmount")
			.setOptional()
			.setPattern("^\\d{1,15}\\.\\d{2,4}$")
			.setMinLength(4)
			.setMaxLength(20);

		StringField.Builder feeRateBuilder = new StringField
			.Builder("feeRate")
			.setOptional()
			.setPattern("^[01]\\.\\d{6}$")
			.setMinLength(8)
			.setMaxLength(8);

		String feeCharge = OIDFJSON.getString(findByPath(body, "feeCharge"));

		if (feeCharge.equals("PERCENTUAL")) {
			feeRateBuilder.setOptional(false);
		} else {
			feeAmountBuilder.setOptional(false);
		}


		assertField(body, feeAmountBuilder.build());


		assertField(body, feeRateBuilder.build());
	}

	private void assertInnerFieldsCharges(JsonObject body) {
		assertField(body,
			new StringField
				.Builder("chargeType")
				.setEnums(CHARGE_TYPE)
				.build());

		StringField.Builder chargeAdditionalInfoBuilder = new StringField
			.Builder("chargeAdditionalInfo")
			.setMaxLength(140)
			.setPattern("[\\w\\W\\s]*")
			.setOptional();

		if(OIDFJSON.getString(findByPath(body, "chargeType")).equals("OUTROS")){
			chargeAdditionalInfoBuilder.setOptional(false);
		}

		assertField(body, chargeAdditionalInfoBuilder.build());

		assertField(body,
			new StringField
				.Builder("chargeRate")
				.setOptional()
				.setPattern("^[01]\\.\\d{6}$")
				.setMinLength(8)
				.setMaxLength(8)
				.build());
	}
}
