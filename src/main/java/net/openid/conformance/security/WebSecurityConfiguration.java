package net.openid.conformance.security;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import net.openid.conformance.extensions.yacs.TermsAndConditionsAuthenticationFilter;
import org.mitre.oauth2.model.RegisteredClient;
import org.mitre.openid.connect.client.OIDCAuthenticationFilter;
import org.mitre.openid.connect.client.OIDCAuthenticationProvider;
import org.mitre.openid.connect.client.service.RegisteredClientService;
import org.mitre.openid.connect.client.service.impl.DynamicServerConfigurationService;
import org.mitre.openid.connect.client.service.impl.StaticAuthRequestOptionsService;
import org.mitre.openid.connect.client.service.impl.StaticClientConfigurationService;
import org.mitre.openid.connect.client.service.impl.ThirdPartyIssuerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.header.HeaderWriter;
import org.springframework.security.web.header.writers.DelegatingRequestMatcherHeaderWriter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.util.DefaultUriBuilderFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final Logger logger = LoggerFactory.getLogger(DummyUserFilter.class);

	@Autowired(required = false)
	private TermsAndConditionsAuthenticationFilter tocFilter;

	@Value("${fintechlabs.devmode:false}")
	private boolean devmode;

	@Value("${fintechlabs.base_url}")
	private String baseURL;

	// Redirect URI to use
	@Value("${oidc.redirecturi}")
	private String redirectURI;

	@Value("${oidc.google.iss:https://accounts.google.com}")
	private String googleIss;

	// Static Client for gitlab
	@Value("${oidc.gitlab.clientid}")
	private String gitlabClientId;

	@Value("${oidc.gitlab.secret}")
	private String gitlabClientSecret;

	@Value("${oidc.gitlab.iss:https://gitlab.com}")
	private String gitlabIss;

	// Static Client for OBB Directory
	@Value("${oidc.obbrazil.clientid}")
	private String obbrazilClientId;

	@Value("${oidc.obbrazil.secret}")
	private String obbrazilClientSecret;

	@Value("${oidc.obbrazil.iss:${fintechlabs.issuer}}")
	private String obbrazilIss;

	// Config for the admin role
	@Value("${oidc.admin.domains:}")
	private String adminDomains;
	@Value("${oidc.admin.group:}")
	private String adminGroup;
	@Value("${oidc.admin.issuer}")
	private String adminIss;

	@Autowired
	private DummyUserFilter dummyUserFilter;

	@Autowired(required = false)
	private CorsConfigurable additionalCorsConfiguration;

	private RegisteredClient gitlabClientConfig() {
		RegisteredClient rc = new RegisteredClient();
		rc.setClientId(gitlabClientId);
		rc.setClientSecret(gitlabClientSecret);
		// email is only asked for to make it clear to the user which account they're logged into, if they have multiple gitlab ones
		rc.setScope(ImmutableSet.of("openid", "email"));
		rc.setRedirectUris(ImmutableSet.of(redirectURI));
		return rc;
	}

	// Bean to set up the server configuration service. We're only doing dynamic setup.
	@Bean
	public DynamicServerConfigurationService serverConfigurationService(Map<String, RegisteredClient> clients) {
		DynamicServerConfigurationService serverConfig = new DynamicServerConfigurationService();
		serverConfig.setWhitelist(clients.keySet());
		return serverConfig;
	}

	// Service to store/retrieve persisted information for dynamically registered clients.
	@Bean
	public RegisteredClientService registeredClientService() {

		MongoDBRegisteredClientService registeredClientService = new MongoDBRegisteredClientService();
		return registeredClientService;
	}

	@Bean
	public StaticClientConfigurationService staticClientConfigurationService(Map<String, RegisteredClient> clients) {
		StaticClientConfigurationService clientConfigService = new StaticClientConfigurationService();

		clientConfigService.setClients(clients);

		return clientConfigService;
	}

	@Bean
	public LoginUrlAuthenticationEntryPoint authenticationEntryPoint() {
		return new LoginUrlAuthenticationEntryPoint(baseURL + "/openid_connect_login");
	}

	@Bean
	public ThirdPartyIssuerService issuerService() {
		ThirdPartyIssuerService is = new ThirdPartyIssuerService();
		is.setAccountChooserUrl(baseURL + "/login.html");
		return is;
	}

	@Bean
	public AuthRequestUrlBuilderWithFixedScopes authRequestUrlBuilder() {
		return new AuthRequestUrlBuilderWithFixedScopes();
	}

	@Bean
	public OIDCAuthenticationFilter openIdConnectAuthenticationFilter() throws Exception {
		Map<String, RegisteredClient> clients = ImmutableMap.of(obbrazilIss, obbrazilClientConfig(), gitlabIss, gitlabClientConfig());

		OIDCAuthenticationFilter oidcaf = new OIDCAuthenticationFilter();
		oidcaf.setIssuerService(issuerService());
		oidcaf.setServerConfigurationService(serverConfigurationService(clients));
		oidcaf.setClientConfigurationService(staticClientConfigurationService(clients));
		oidcaf.setAuthRequestOptionsService(new StaticAuthRequestOptionsService());
		oidcaf.setAuthRequestUrlBuilder(authRequestUrlBuilder());
		oidcaf.setAuthenticationManager(authenticationManager());
		oidcaf.setAuthenticationFailureHandler(new AuthenticationFailureHandler() {
			@Override
			public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
				String newUrl = new DefaultUriBuilderFactory()
					.uriString("/login.html")
					.queryParam("error", exception.getMessage())
					.build()
					.toString();

				response.sendRedirect(newUrl);
			}
		});

		return oidcaf;
	}

	private RegisteredClient obbrazilClientConfig() {
		RegisteredClient rc = new RegisteredClient();
		rc.setClientId(obbrazilClientId);
		rc.setClientSecret(obbrazilClientSecret);
		rc.setScope(ImmutableSet.of("openid", "email"));
		rc.setRedirectUris(ImmutableSet.of(redirectURI));
		return rc;
	}

	@Bean
	public AuthenticationProvider configureOIDCAuthenticationProvider() {
		OIDCAuthenticationProvider authenticationProvider = new OIDCAuthenticationProvider();

		if (adminIss.equals(googleIss) && !Strings.isNullOrEmpty(adminDomains)) {
			// Create an OIDCAuthoritiesMapper that uses the 'hd' field of a
			// Google account's userInfo. hd = Hosted Domain. Use this to filter to
			// any users of a specific domain
			authenticationProvider.setAuthoritiesMapper(new GoogleHostedDomainAdminAuthoritiesMapper(adminDomains, adminIss));
		} else if (!Strings.isNullOrEmpty(adminGroup)) {
			// use "groups" array from id_token or userinfo for admin access (works with at least gitlab and azure)
			authenticationProvider.setAuthoritiesMapper(new GroupsAdminAuthoritiesMapper(adminGroup, adminIss));
		}

		return authenticationProvider;
	}

	// This sets Spring Security up so that it can use the OIDC tokens etc.
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(configureOIDCAuthenticationProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// @formatter:off

		http.csrf().disable()
				.authorizeRequests()
					.antMatchers("/login.html", "/css/**", "/js/**", "/images/**", "/templates/**", "/favicon.ico", "/test-mtls/**", "/test/**", "/jwks**", "/logout.html", "/robots.txt", "/.well-known/**", "/api/server")
					.permitAll()
				.and().authorizeRequests()
					.requestMatchers(publicRequestMatcher("/log-detail.html", "/logs.html", "/plan-detail.html", "/plans.html"))
					.permitAll()
				.anyRequest()
					.hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
				.and()
					.addFilterBefore(openIdConnectAuthenticationFilter(), AbstractPreAuthenticatedProcessingFilter.class)
				.exceptionHandling()
					.authenticationEntryPoint(authenticationEntryPoint())
				.and()
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
				.and()
					.logout()
					.logoutSuccessUrl("/login.html")
				.and()
					//added to disable x-frame-options only for certain paths
					.headers().frameOptions().disable()
				.and()
					.headers().addHeaderWriter(getXFrameOptionsHeaderWriter())
				.and()
					.cors().configurationSource(getCorsConfigurationSource());

		// @formatter:on

		if (devmode) {
			logger.warn("\n***\n*** Starting application in Dev Mode, injecting dummy user into requests.\n***\n");
			http.addFilterBefore(dummyUserFilter, OIDCAuthenticationFilter.class);
		}
		if(tocFilter != null) {
			http.addFilterAfter(tocFilter, OIDCAuthenticationFilter.class);
		}

	}

	protected HeaderWriter getXFrameOptionsHeaderWriter() {

		AntPathRequestMatcher checkSessionIframeMatcher = new AntPathRequestMatcher("/**/check_session_iframe");
		AntPathRequestMatcher getSessionStateMatcher = new AntPathRequestMatcher("/**/get_session_state");
		RequestMatcher orRequestMatcher = new OrRequestMatcher(checkSessionIframeMatcher, getSessionStateMatcher);

		NegatedRequestMatcher negatedRequestMatcher = new NegatedRequestMatcher(orRequestMatcher);
		//default to SAMEORIGIN except the above endpoints
		XFrameOptionsHeaderWriter xFrameOptionsHeaderWriter = new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.SAMEORIGIN);
		DelegatingRequestMatcherHeaderWriter writer = new DelegatingRequestMatcherHeaderWriter(negatedRequestMatcher, xFrameOptionsHeaderWriter);

		return writer;
	}

	// For more info regarding the CORS handling in the conformance suite, please refer to
	// https://gitlab.com/openid/conformance-suite/-/merge_requests/1175#note_1020913221
	protected AdditiveUrlBasedCorsConfigurationSource getCorsConfigurationSource() {

		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList("*"));
		configuration.setAllowedMethods(Arrays.asList("GET","POST"));

		AdditiveUrlBasedCorsConfigurationSource source = new AdditiveUrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**/check_session_iframe", configuration);
		source.registerCorsConfiguration("/**/get_session_state", configuration);

		if (additionalCorsConfiguration != null) {
			additionalCorsConfiguration.getCorsConfigurations().forEach(source::registerCorsConfiguration);
		}

		return source;
	}

	private RequestMatcher publicRequestMatcher(String... patterns) {

		return new AndRequestMatcher(
				new OrRequestMatcher(
						Arrays.asList(patterns)
								.stream()
								.map(AntPathRequestMatcher::new)
								.collect(Collectors.toList())),
				new PublicRequestMatcher());
	}

}
