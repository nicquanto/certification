package net.openid.conformance.logging;

public enum LeafType {
	PRIVATE_KEY,
	JWKS
}
