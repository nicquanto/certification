package net.openid.conformance.extensions;

import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.*;



public class YACSKmsOperations implements KmsOperations {

	@Override
	public EncryptResponse encrypt(KmsClient kmsClient, EncryptRequest encryptRequest) {
		return kmsClient.encrypt(encryptRequest);
	}

	@Override
	public DecryptResponse decrypt(KmsClient kmsClient, DecryptRequest decryptRequest) {
		return kmsClient.decrypt(decryptRequest);
	}

	@Override
	public GetPublicKeyResponse getPublicKey(KmsClient kmsClient, GetPublicKeyRequest getPublicKeyRequest) {
		return kmsClient.getPublicKey(getPublicKeyRequest);
	}
}
