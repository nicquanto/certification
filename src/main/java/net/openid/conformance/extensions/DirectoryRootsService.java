package net.openid.conformance.extensions;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jose.jwk.JWK;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class DirectoryRootsService {

	private RestTemplate restTemplate = new RestTemplate();

	@Value("${fintechlabs.directoryroots.uri}")
	private String directoryRootsUri;
	private static final Logger LOG = LoggerFactory.getLogger(DirectoryRootsService.class);

	public X509Certificate[] getRootCertificates() {
		JsonObject directoryRoots = getDirectoryRoots();
		JsonArray keys = directoryRoots.getAsJsonArray("keys");
		List<X509Certificate> certificates = new ArrayList<>();
		keys.forEach(key -> {
			List<X509Certificate> chain = extractCertificateFromJwk(key);
			certificates.addAll(chain);
		});
		return certificates.toArray(certificates.toArray(new X509Certificate[0]));
	}
	@Cacheable("directoryRoots")
	public JsonObject getDirectoryRoots() {
		LOG.info("Calling directory roots");
		ResponseEntity<String> forEntity = restTemplate.getForEntity(directoryRootsUri, String.class);
		return new Gson().fromJson(forEntity.getBody(), JsonObject.class);
	}

	@CacheEvict(value = "directoryRoots", allEntries = true)
	@Scheduled(fixedRateString = "1200000")
	public void emptyDirectoryRootsCache() {
		LOG.info("Emptying directory roots cache");
	}
	private List<X509Certificate> extractCertificateFromJwk(JsonElement jwk){
		JWK parsedJwk;
		try {
			parsedJwk = JWK.parse(jwk.toString());
		} catch (ParseException e) {
			LOG.error("Error parsing JWK", e);
			throw new RuntimeException(e);
		}

		return parsedJwk.getParsedX509CertChain();
	}
}
