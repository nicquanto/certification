package net.openid.conformance.extensions;

import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.*;

public interface KmsOperations {



	EncryptResponse encrypt(KmsClient kmsClient, EncryptRequest encryptRequest);

	DecryptResponse decrypt(KmsClient kmsClient, DecryptRequest decryptRequest);

	GetPublicKeyResponse getPublicKey(KmsClient kmsClient, GetPublicKeyRequest getPublicKeyRequest);
}
