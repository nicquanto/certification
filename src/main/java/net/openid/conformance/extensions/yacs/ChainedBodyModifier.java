package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;

import java.util.List;

public class ChainedBodyModifier implements ConfigBodyModifier {

	private final List<ConfigBodyModifier> chain;

	public ChainedBodyModifier(List<ConfigBodyModifier> chain) {
		this.chain = chain;
	}

	@Override
	public void apply(JsonObject config) {
		chain.stream()
			.forEach(c -> c.apply(config));
	}
}
