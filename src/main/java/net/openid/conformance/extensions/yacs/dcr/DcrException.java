package net.openid.conformance.extensions.yacs.dcr;

public class DcrException extends RuntimeException {

	static final long serialVersionUID = 646784461L;

	public DcrException(Exception cause) {
		super(String.format("For Debugging Reasons, the internal failure message when registering the client was the following: %s", cause.getMessage()), cause);
	}

}
