package net.openid.conformance.extensions.yacs;

import net.openid.conformance.info.TestPlanApi;
import net.openid.conformance.runner.TestRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@ConditionalOnProperty(name = "fintechlabs.extensions", havingValue = "YACS")
@RestControllerAdvice
public class ConfigFieldResponseFilteringAdvice implements ResponseBodyAdvice<Collection<Map<String, Object>>> {

	public static final List<String> PERMITTED_FIELDS = List.of(
		"server.discoveryUrl", "resource.brazilCpf", "resource.brazilCnpj"
	);

	@Override
	public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
		Method method = methodParameter.getMethod();
		Class<?> declaringClass = method.getDeclaringClass();
		String name = method.getName();
		boolean isInterestingClass = declaringClass.isAssignableFrom(TestPlanApi.class) || declaringClass.isAssignableFrom(TestRunner.class);
		boolean isInterestingMethod = name.equals("getAvailableTestPlans") || name.equals("getAvailableTests");
		boolean shouldAdvise = isInterestingClass && isInterestingMethod;
		return shouldAdvise;
	}

	@Override
	public Collection<Map<String, Object>> beforeBodyWrite(Collection<Map<String, Object>> body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
		body.stream()
			.forEach(this::clean);
		return body;
	}

	private void clean(Map<String, Object> entry) {
		entry.put("configurationFields", PERMITTED_FIELDS);
	}

}
