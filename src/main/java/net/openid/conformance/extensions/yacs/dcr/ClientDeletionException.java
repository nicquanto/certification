package net.openid.conformance.extensions.yacs.dcr;

public class ClientDeletionException extends Exception {

	static final long serialVersionUID = 1493723L;

	public ClientDeletionException(String clientId) {
		super(String.format("Exception deleting client registered at %s", clientId));
	}

}
