package net.openid.conformance.extensions.yacs;

import net.openid.conformance.security.AuthenticationFacade;
import org.springframework.security.core.Authentication;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

public class TermsAndConditionsAuthenticationFilter implements Filter {

	public static final String PRE_TOC_URI_KEY = "postTocTarget";

	private AuthenticationFacade authenticationFacade;

	public TermsAndConditionsAuthenticationFilter(AuthenticationFacade authenticationFacade) {
		this.authenticationFacade = authenticationFacade;
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) servletRequest;
		Authentication authentication = authenticationFacade.getContextAuthentication();

		String accept = req.getHeader("Accept");

		if(authentication == null || !accept.contains("text/html")) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}
		Optional<? extends TOCGrantedAuthority> tocGrant = authentication.getAuthorities()
			.stream()
			.filter(g -> g instanceof TOCGrantedAuthority)
			.findAny()
			.map(a ->  (TOCGrantedAuthority) a);
		boolean tocAgreementNeeded = tocGrant.map(g -> !g.isAccepted())
			.orElse(false);
		HttpSession session = req.getSession();
		if(!tocAgreementNeeded || req.getServletPath().startsWith("/toc")) {
			filterChain.doFilter(servletRequest, servletResponse);
		} else {
			String targetUrl = req.getRequestURI();
			String params = req.getQueryString();
			String dest = targetUrl;
			if(params != null) {
				dest = dest.concat("?").concat(params);
			}
			session.setAttribute(PRE_TOC_URI_KEY, dest);
			HttpServletResponse res = (HttpServletResponse) servletResponse;
			res.sendRedirect("/toc.html");
		}
	}

}
