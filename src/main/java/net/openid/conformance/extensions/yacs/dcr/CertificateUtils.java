package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;

import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayInputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

public class CertificateUtils {

	public static RSAPrivateKey generatePrivateKeyFromDER(byte[] keyBytes) throws InvalidKeySpecException, NoSuchAlgorithmException {
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);

		KeyFactory factory = KeyFactory.getInstance("RSA");

		return (RSAPrivateKey) factory.generatePrivate(spec);
	}

	public static String sanitise(String raw) {
		return raw.replaceAll("\\n", "")
			.replaceAll("-----BEGIN PRIVATE KEY-----", "")
			.replaceAll("-----BEGIN RSA PRIVATE KEY-----", "")
			.replaceAll("-----END PRIVATE KEY-----", "")
			.replaceAll("-----END RSA PRIVATE KEY-----", "")
			.replaceAll("-----BEGIN CERTIFICATE-----", "")
			.replaceAll("-----END CERTIFICATE-----", "");
	}

	public static X509Certificate generateCertificateFromDER(byte[] certBytes) throws CertificateException {
		CertificateFactory factory = CertificateFactory.getInstance("X.509");

		return (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(certBytes));
	}

	public static List<X509Certificate> generateCertificateChainFromDER(byte[] chainBytes) throws CertificateException {
		CertificateFactory factory = CertificateFactory.getInstance("X.509");

		ArrayList<X509Certificate> chain = new ArrayList<>();
		ByteArrayInputStream in = new ByteArrayInputStream(chainBytes);
		while (in.available() > 0) {
			chain.add((X509Certificate) factory.generateCertificate(in));
		}

		return chain;
	}

	public static JsonObject extractSubject(X509Certificate certificate) {
		X500Principal x500Principal = certificate.getSubjectX500Principal();

		X500Name x500name = X500Name.getInstance(x500Principal.getEncoded());
		String subjectDn = x500Principal.getName();

		RDN[] ouArray = x500name.getRDNs(BCStyle.OU);
		String ouAsString;
		JsonObject o = new JsonObject();
		if (ouArray.length == 0) {
			RDN[] oiArray = x500name.getRDNs(BCStyle.ORGANIZATION_IDENTIFIER);
			if (oiArray.length == 0) {
				throw new RuntimeException("Certificate subjectdn contains neither 'organizational unit name' nor 'organization identifier'");
			}
			String oi = IETFUtils.valueToString(oiArray[0].getFirst().getValue());
			String split[] = oi.split("-", 2);
			if (split.length != 2) {
				throw new RuntimeException("'organization identifier' in the certificate does not contain a '-'");
			}
			o.addProperty("org_type", split[0]); // the OFBBR prefix
			ouAsString = split[1];
		} else {
			RDN ou = ouArray[0];
			ouAsString = IETFUtils.valueToString(ou.getFirst().getValue());
		}
		RDN[] uid = x500name.getRDNs(BCStyle.UID);
		String softwareId;
		if (uid.length == 0) {
			throw new RuntimeException("Empty UID");
		} else {
			softwareId = IETFUtils.valueToString(uid[0].getFirst().getValue());
		}

		o.addProperty("subjectdn", subjectDn);
		o.addProperty("ou", ouAsString);
		o.addProperty("brazil_software_id", softwareId);
		return o;
	}

}
