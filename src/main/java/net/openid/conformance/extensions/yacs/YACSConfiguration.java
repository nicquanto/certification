package net.openid.conformance.extensions.yacs;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import net.openid.conformance.extensions.YACSKmsOperations;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jwt.SignedJWT;
import net.openid.conformance.extensions.yacs.dcr.*;
import net.openid.conformance.info.SavedConfigurationService;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.info.TestPlanService;
import net.openid.conformance.logging.EventLog;
import net.openid.conformance.logging.JsonObjectSanitiser;
import net.openid.conformance.logging.MapSanitiser;
import net.openid.conformance.openbanking_brasil.testmodules.logging.YACSDBExpireLogs;
import net.openid.conformance.security.AuthenticationFacade;
import org.mitre.jwt.signer.service.JWTSigningAndValidationService;
import org.mitre.oauth2.model.RegisteredClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;
import software.amazon.awssdk.services.kms.KmsClient;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@Configuration
@ConditionalOnProperty(name = "fintechlabs.extensions", havingValue = "YACS")
@EnableCaching
@EnableScheduling
public class YACSConfiguration {

	@Value("${oidc.gitlab.clientid}")
	private String gitlabClientId;

	@Value("${oidc.gitlab.secret}")
	private String gitlabClientSecret;

	@Value("${oidc.gitlab.iss:https://gitlab.com}")
	private String adminIss;

	@Value("${oidc.obbrazil.clientid}")
	private String obbrazilClientId;

	@Value("${oidc.obbrazil.secret}")
	private String obbrazilClientSecret;

	@Value("${oidc.obbrazil.iss:${fintechlabs.issuer}}")
	private String obbrazilIss;

	@Value("${oidc.redirecturi}")
	private String redirectURI;

	@Value("${fintechlabs.extensions.kmsKey}")
	private String kmsKeyId;

	@Value("${fintechlabs.directoryroots.uri}")
	private String directoryRootsUri;

	public static final String OBB_BRAZIL_CLIENT = "OBB Brasil Directory Client";

	@Bean
	public JWTSigningAndValidationService signingAndValidationService() {
		return new JWTSigningAndValidationService() {
			@Override
			public Map<String, JWK> getAllPublicKeys() {
				return null;
			}

			@Override
			public boolean validateSignature(SignedJWT jwtString) {
				return false;
			}

			@Override
			public void signJwt(SignedJWT jwt) {

			}

			@Override
			public JWSAlgorithm getDefaultSigningAlgorithm() {
				return null;
			}

			@Override
			public Collection<JWSAlgorithm> getAllSigningAlgsSupported() {
				return null;
			}

			@Override
			public void signJwt(SignedJWT jwt, JWSAlgorithm alg) {

			}

			@Override
			public String getDefaultSignerKeyId() {
				return null;
			}
		};
	}

	@Bean
	public DirectoryDetails directoryDetails(@Value("${yacs.directory.credentials}") String location) throws FileNotFoundException {
		Resource credentials = new FileSystemResource(location);
		if (!credentials.isFile()) {
			throw new RuntimeException(String.format("Credentials file %s does not exist or is not a file", location));
		}
		Properties properties = load(credentials);
		return new DirectoryDetails(
			properties
		);
	}

	@Bean
	public DcrHelper dcrHelper(DirectoryCredentials directoryCredentials, DirectoryDetails directoryDetails){
		return new DcrHelper(directoryCredentials, directoryDetails);
	}

	@Bean
	@Primary
	public TestInfoService yacsTestInfoService(TestInfoService delegate,
											   DcrClientRepository clientRepository,
											   DcrHelper dcrHelper,
											   DirectoryCredentials directoryCredentials) {
		return new DcrPostHocTestInfoService(delegate, clientRepository, dcrHelper,
			directoryCredentials);
	}

	@Bean
	public OrphanCrushingMachine orphanCrushingMachine(DcrClientRepository clientRepository,
													   DcrHelper dcrHelper) {
		OrphanCrushingMachine orphanCrushingMachine = new OrphanCrushingMachine(clientRepository, dcrHelper);
		Runtime.getRuntime().addShutdownHook(new Thread(new OrphanCrushingMachine.ShutdownHook(clientRepository, orphanCrushingMachine)));
		return orphanCrushingMachine;
	}

	@Bean
	@Primary
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "true")
	public TestPlanService yacsTestPlanService(TestPlanService delegate){
		return new YACSDBTestPlanService(delegate);
	}

	@Bean
	@Primary
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "true")
	public SavedConfigurationService yacsSavedConfigurationService(){
		return new YACSDBSavedConfigurationService();
	}

	@Bean
	public DcrClientRepository dcrClientRepository() {
		return new InMemoryDcrClientRepository();
	}

	@Bean
	public ParticipantsService participantsService() {
		return new ParticipantsService();
	}

	@Bean
	public PlanCreationBlockingFilter planCreationBlockingFilter(AuthenticationFacade authenticationFacade, ParticipantsService participantsService
		,@Value("${fintechlabs.devmode:false}")  boolean devmode) {
		EnhancedVerification enhancedVerification = new ChainedEnhancedVerification(List.of(
			new CpfEnhancedVerification(authenticationFacade, devmode),
			new OrgOwnershipEnhancedVerification(authenticationFacade, participantsService, devmode)
		));
		PlanCreationBlockingFilter filter = new PlanCreationBlockingFilter(enhancedVerification);
		return filter;
	}

	@Bean
	public ConfigFilter configFilter(ParticipantsService ps, AuthenticationFacade af, DirectoryDetails directoryDetails) {
		ConfigFilter filter = new ConfigFilter(ps, af, directoryDetails, directoryRootsUri);
		return filter;
	}

	@Bean
	public BeanPostProcessor postProcessor(@Value("${fintechlabs.devmode:false}") boolean devmode) {
		Map<String, RegisteredClient> clients = ImmutableMap.of(obbrazilIss, obbrazilClientConfig(), adminIss, azureClient());
		return new YACSBeanPostProcessor(clients, devmode);
	}

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "true")
	public YACSEncryptAndDecryptOperations yacsEncryptAndDecryptOperations(KmsClient kmsClient){
		return new YACSEncryptAndDecryptOperations(new YACSKmsOperations(), kmsClient, kmsKeyId);
	}

	private RegisteredClient azureClient() {
		RegisteredClient rc = new RegisteredClient();
		rc.setClientId(gitlabClientId);
		rc.setClientSecret(gitlabClientSecret);
		rc.setScope(ImmutableSet.of("openid", "email"));
		rc.setRedirectUris(ImmutableSet.of(redirectURI));
		return rc;
	}

	private RegisteredClient obbrazilClientConfig() {
		RegisteredClient rc = new RegisteredClient();
		rc.setClientName(OBB_BRAZIL_CLIENT);
		rc.setClientId(obbrazilClientId);
		rc.setResponseTypes(Set.of("code", "id_token"));
		rc.setClientSecret(obbrazilClientSecret);
		rc.setScope(ImmutableSet.of("openid", "profile", "trust_framework_profile"));
		rc.setRedirectUris(ImmutableSet.of(redirectURI));
		return rc;
	}

	@Bean
	public TermsAndConditionsAuthenticationFilter tocFilter(AuthenticationFacade authenticationFacade) {
		return new TermsAndConditionsAuthenticationFilter(authenticationFacade);
	}

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "false")
	public DirectoryCredentials directoryCredentials(@Value("${yacs.directory.credentials}") String location) throws FileNotFoundException {
		Resource credentials = new FileSystemResource(location);
		if(!credentials.isFile()) {
			throw new RuntimeException(String.format("Credentials file %s does not exist or is not a file", location));
		}
		Properties properties = load(credentials);
		DirectoryCredentials directoryCredentials = new KeyMaterialBearingDirectoryCredentials(properties);

		return directoryCredentials;
	}

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "true")
	public DirectoryCredentials kmsDirectoryCredentials(@Value("${yacs.directory.credentials}") String location, KmsClient kmsClient) throws FileNotFoundException {
		Resource credentials = new FileSystemResource(location);
		if (!credentials.isFile()) {
			throw new RuntimeException(String.format("Credentials file %s does not exist or is not a file", location));
		}
		Properties properties = load(credentials);
		DirectoryCredentials directoryCredentials = new KmsDirectoryCredentials(properties);

		return directoryCredentials;
	}

	@Bean
	@Primary
	public EventLog encryptingEventLog(JsonObjectSanitiser jsonObjectSanitiser, MapSanitiser mapSanitiser) {
		return new YACSDBExpireLogs(jsonObjectSanitiser, mapSanitiser);
	}

	private Properties load(Resource resource) throws FileNotFoundException {
		try {
			YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
			factory.setResources(resource);
			factory.afterPropertiesSet();

			return factory.getObject();
		} catch (IllegalStateException ex) {
			Throwable cause = ex.getCause();
			if (cause instanceof FileNotFoundException){
				throw (FileNotFoundException) cause;
			}
			throw ex;
		}
	}

}
