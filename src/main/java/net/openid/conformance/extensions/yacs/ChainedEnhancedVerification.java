package net.openid.conformance.extensions.yacs;

import java.util.List;
import java.util.Optional;

public class ChainedEnhancedVerification implements EnhancedVerification {
	private final List<EnhancedVerification> chain;

	public ChainedEnhancedVerification(List<EnhancedVerification> chain) {
		this.chain = chain;
	}

	@Override
	public boolean allowed(BodyRepeatableHttpServletRequest request) {
		Optional<Boolean> allowed = chain.stream()
			.map(e -> e.allowed(request))
			.filter(a -> !a)
			.findAny();
		return allowed.orElse(true);
	}
}
