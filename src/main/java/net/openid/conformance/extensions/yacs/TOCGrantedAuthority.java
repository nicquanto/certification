package net.openid.conformance.extensions.yacs;

import org.springframework.security.core.GrantedAuthority;

/**
 * Added to authenticated users in a YACS context.
 * Mutable, this models whether or not they have accepted the
 * terms and conditions of the platform for a specific session
 */
public class TOCGrantedAuthority implements GrantedAuthority {

	private static final long serialVersionUID = 94855L;

	public static final String ACCEPTED = "TOC_ACCEPTED";
	private boolean accepted = false;

	@Override
	public String getAuthority() {
		return ACCEPTED;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
}
