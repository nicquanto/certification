package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jwt.JWT;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.testmodule.OIDFJSON;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;

import java.text.ParseException;

public class CpfEnhancedVerification implements EnhancedVerification {

	private static final Logger LOG = LoggerFactory.getLogger(CpfEnhancedVerification.class);

	private final AuthenticationFacade authenticationFacade;
	private boolean devmode;

	public CpfEnhancedVerification(AuthenticationFacade authenticationFacade) {
		this(authenticationFacade, false);
	}

	public CpfEnhancedVerification(AuthenticationFacade authenticationFacade, boolean devmode) {
		this.authenticationFacade = authenticationFacade;
		this.devmode = devmode;
	}

	@Override
	public boolean allowed(BodyRepeatableHttpServletRequest request) {
		if(devmode) {
			return true;
		}
		JsonObject jobConfig = request.body();
		JsonObject resource = jobConfig.getAsJsonObject("resource");
		if(resource==null) {
			return false;
		}
		JsonElement cpfElement = resource.get("brazilCpf");
		if(cpfElement == null) {
			LOG.info("No CPF present");
			return false;
		}
		String cpf = OIDFJSON.getString(cpfElement);
		Authentication contextAuthentication = authenticationFacade.getContextAuthentication();
		if(!contextAuthentication.getClass().isAssignableFrom(OIDCAuthenticationToken.class)) {
			LOG.error("Logged in authentication is not OIDC token");
			return false;
		}
		OIDCAuthenticationToken oidcAuth = (OIDCAuthenticationToken) contextAuthentication;
		JWT idToken = oidcAuth.getIdToken();
		try {
			Object nationalId = idToken.getJWTClaimsSet().getClaim("national_id");
			boolean allowed = cpf.equals(nationalId);
			LOG.info("CPF on id token matches that in the config - passing");
			return allowed;
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

}
