package net.openid.conformance.extensions.yacs.dcr;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

@ControllerAdvice
@ConditionalOnProperty(name = "fintechlabs.extensions", havingValue = "YACS")
public class BackgroundDcrExceptionTranslationAdvice {

	@Autowired
	private DirectoryCredentials directoryCredentials;

	private static final Logger LOG = LoggerFactory.getLogger(BackgroundDcrExceptionTranslationAdvice.class);

	private static final String EXPLAIN_LINK_HEADER = "X-FAPI-Error-Explain-Link";
	private static final String EXPLAIN_TEXT_HEADER = "X-FAPI-Error-Explain-Linktext";
	private static final String FVP_DOCS_LINK = "https://gitlab.com/obb1/certification/-/wikis/FVP";
	private static final String FVP_DOCS_MESSAGE = "For details about this process check the FVP documentation here";

	@ExceptionHandler({ DcrException.class })
	public final ResponseEntity<?> handleException(Exception ex, WebRequest request) {
		Map<String, String[]> parameterMap = request.getParameterMap();
		StringBuilder params = new StringBuilder();
		parameterMap.forEach((k, v) -> {
			params.append(k).append("=[").append(String.join(",", v))
				.append("] & ");
		});
		LOG.error("Exception during background DCR: {} {}", ex, params);
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		String message = new StringBuilder()
			.append("An Error was identified when registering a client_id on the provided server well-known and the testing cannot be continued. ")
			.append("Prior to each test, the FVP will use the Software Statement: ")
			.append(directoryCredentials.softwareStatementId())
			.append("to obtain a client_id from the server, which hasn’t been correctly registered due to issues on the server configuration ")
			.append("or because there’s already a client_id registered for this software statement. ")
			.toString();
			return ResponseEntity.status(status)
				.header(EXPLAIN_LINK_HEADER, FVP_DOCS_LINK)
				.header(EXPLAIN_TEXT_HEADER, FVP_DOCS_MESSAGE)
				.body(Map.of(
				"error","DCR Error - Failure on generating a client",
				"message", message,
					"additional", ex.getMessage()));

	}

}
