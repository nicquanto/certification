package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;

import java.time.Instant;
import java.util.Map;

public interface DcrClientRepository {

	void saveClient(String testId, JsonObject clientConfig);
	JsonObject getSavedClient(String testId);

	Map<String, JsonObject> clientsOlderThan(Instant instant);

    void deleteSavedClient(String id) throws ClientDeletionException;
}
