package net.openid.conformance.condition.client.jsonAsserting;

import com.google.gson.JsonElement;

import java.util.HashMap;
import java.util.Map;

public class JsonAssertingResponseBodyParser {


	public Map<String, JsonElement> parseBody(String path, JsonElement element) {
		Map<String, JsonElement> elements = new HashMap<>();

		if (element.isJsonObject()) {
			element.getAsJsonObject().entrySet()
				.forEach(entry -> {
					JsonElement el = entry.getValue();
					String fullPath = path + '.' + entry.getKey();
					parseElement(fullPath, el, elements);
				});
		} else if (element.isJsonArray()) {
			element.getAsJsonArray().forEach(el -> {
				parseElement(path, el, elements);
			});
		} else {
			elements.put(path, element);
		}

		return elements;
	}

	private void parseElement(String fullPath, JsonElement el, Map<String, JsonElement> elements) {
		if (el.isJsonObject() || el.isJsonArray()) {
			elements.putAll(parseBody(fullPath, el));
		} else {
			elements.put(fullPath, el);
		}
	}

}

