package net.openid.conformance.raidiam.validators.organisationDomainUsers;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link GetOrganisationDomainUsersValidator}
 * Api url: ****
 * Api endpoint: GET /organisations/{OrganisationId}/{AuthorisationDomainName}/users/{UserEmailId}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory GET Organisation Domain Users ByEmail")
public class GetOrganisationDomainUsersByEmailValidator extends GetOrganisationDomainUsersValidator {}

