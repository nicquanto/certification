package net.openid.conformance.raidiam.validators.referencesAuthorisationDomain;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link GetAuthorisationDomainByDomainNameValidator}
 * Api url: ****
 * Api endpoint: GET /references/authorisationdomains
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory POST Authorisation Domain")
public class PostAuthorisationDomainValidator extends GetAuthorisationDomainByDomainNameValidator {
}
