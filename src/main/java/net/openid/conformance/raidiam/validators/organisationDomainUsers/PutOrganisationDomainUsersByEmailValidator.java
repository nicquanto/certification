package net.openid.conformance.raidiam.validators.organisationDomainUsers;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostOrganisationDomainUsersValidator}
 * Api url: ****
 * Api endpoint: PUT /organisations/{OrganisationId}/{AuthorisationDomainName}/users/{UserEmailId}/{AuthorisationDomainUserId}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory PUT Organisation Domain Users ByEmail")
public class PutOrganisationDomainUsersByEmailValidator extends PostOrganisationDomainUsersValidator {
}
