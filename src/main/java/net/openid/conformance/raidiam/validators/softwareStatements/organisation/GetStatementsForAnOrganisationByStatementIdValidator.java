package net.openid.conformance.raidiam.validators.softwareStatements.organisation;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: GET /organisations/{OrganisationId}/softwarestatements/{SoftwareStatementId}
 */
@ApiName("Raidiam Directory GET Software Statements for an Organisation By StatementId")
public class GetStatementsForAnOrganisationByStatementIdValidator extends PostStatementsForAnOrganisationValidator {
}
