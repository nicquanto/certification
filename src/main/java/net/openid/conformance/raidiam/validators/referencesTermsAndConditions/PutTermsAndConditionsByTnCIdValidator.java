package net.openid.conformance.raidiam.validators.referencesTermsAndConditions;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostTermsAndConditionsValidator}
 * Api url: ****
 * Api endpoint: PUT /references/termsandconditions/{TnCId}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory PUT References - Terms and Conditions")
public class PutTermsAndConditionsByTnCIdValidator extends PostTermsAndConditionsValidator {
}
