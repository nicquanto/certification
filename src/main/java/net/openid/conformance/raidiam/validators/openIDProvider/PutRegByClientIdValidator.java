package net.openid.conformance.raidiam.validators.openIDProvider;

import net.openid.conformance.logging.ApiName;

/**
 * Api url: ****
 * Api endpoint: PUT /reg/{ClientId}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory PUT Reg By ClientId")
public class PutRegByClientIdValidator extends PostRegValidator {
}
