package net.openid.conformance.raidiam.validators.referencesAuthorisationDomainRole;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostAuthorisationDomainRoleValidator}
 * Api url: ****
 * Api endpoint: GET /references/authorisationdomainroles/{AuthorisationDomainRoleName}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory GET Authorisation Domain Role By DomainRoleName")
public class GetAuthorisationDomainRoleByDomainNameValidator extends PostAuthorisationDomainRoleValidator {

}
