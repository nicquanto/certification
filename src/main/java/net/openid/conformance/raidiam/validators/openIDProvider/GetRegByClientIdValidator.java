package net.openid.conformance.raidiam.validators.openIDProvider;

import net.openid.conformance.logging.ApiName;

/**
 * Api url: ****
 * Api endpoint: GET /reg/{ClientId}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory GET Reg By ClientId")
public class GetRegByClientIdValidator extends PostRegValidator {
}
