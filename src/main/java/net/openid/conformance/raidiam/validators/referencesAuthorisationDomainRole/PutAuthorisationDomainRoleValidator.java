package net.openid.conformance.raidiam.validators.referencesAuthorisationDomainRole;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostAuthorisationDomainRoleValidator}
 * Api url: ****
 * Api endpoint: GET /references/authorisationdomainroles/{AuthorisationDomainRoleName}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory PUT Authorisation Domain Role")
public class PutAuthorisationDomainRoleValidator extends PostAuthorisationDomainRoleValidator {
}
