package net.openid.conformance.raidiam.validators.authorisationServers.certifications;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostServerCertificationsValidator}
 * Api endpoint: GET /organisations/{OrganisationId}/authorisationservers/{AuthorisationServerId}/certifications/{AuthorisationServerCertificationId}
 */
@ApiName("Raidiam Directory GET Authorisation Server Certifications By CertificationId")
public class GetServerCertificationsByCertificationIdValidator extends PostServerCertificationsValidator {
}
