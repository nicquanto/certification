package net.openid.conformance.raidiam.validators.authorisationServers.certifications;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostServerCertificationsValidator}
 * Api endpoint: PUT /organisations/{OrganisationId}/authorisationservers/{AuthorisationServerId}/certifications/{AuthorisationServerCertificationId}
 */
@ApiName("Raidiam Directory PUT Authorisation Server Certifications By CertificationId")
public class PutServerCertificationsByCertificationIdValidator extends PostServerCertificationsValidator {
}
