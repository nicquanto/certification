package net.openid.conformance.raidiam.validators.authorisationServers.discoveryEndpointsAPI;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostDiscoveryEndpointsValidator}
 * Api endpoint: GET /organisations/{OrganisationId}/authorisationservers/{AuthorisationServerId}/apiresources/{ApiResourceId}/apidiscoveryendpoints/{ApiDiscoveryEndpointId}
 */
@ApiName("Raidiam Directory GET Authorisation Servers by EndpointID Ser API Discovery Endpoints")
public class GetDiscoveryEndpointsByEndpointIDValidator extends PostDiscoveryEndpointsValidator {

}
