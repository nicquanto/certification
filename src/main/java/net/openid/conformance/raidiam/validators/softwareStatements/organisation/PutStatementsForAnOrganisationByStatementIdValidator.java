package net.openid.conformance.raidiam.validators.softwareStatements.organisation;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: PUT /organisations/{OrganisationId}/softwarestatements/{SoftwareStatementId}
 */
@ApiName("Raidiam Directory PUT Software Statements for an Organisation By StatementId")
public class PutStatementsForAnOrganisationByStatementIdValidator extends PostStatementsForAnOrganisationValidator {
}
