package net.openid.conformance.raidiam.validators.referencesAuthorityAuthorisationDomain;

import net.openid.conformance.logging.ApiName;

/**
 * Api url: ****
 * Api endpoint: GET /references/authorities/{AuthorityId}/authorisationdomains
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory GET Authority Authorisation Domain By AuthorityId")
public class GetAuthorityAuthorisationDomainByAuthorityIdValidator extends GetAuthorityAuthorisationDomainValidator {
}
