package net.openid.conformance.raidiam.validators.organisationAuthorityClaimsAuthorisation;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostAuthorityClaimsAuthorisationsValidator}
 * Api endpoint: GET /organisations/{OrganisationId}/authorityclaims/{OrganisationAuthorityClaimId}/authorisations/{OrganisationAuthorisationId}
 */
@ApiName("Raidiam Directory GET Organisation Authority Claims Authorisations by AuthorisationId")
public class GetAuthorityClaimsAuthorisationsByAuthorisationIdValidator extends PostAuthorityClaimsAuthorisationsValidator {
}
