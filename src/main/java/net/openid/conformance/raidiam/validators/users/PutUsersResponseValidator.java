package net.openid.conformance.raidiam.validators.users;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link GetUsersResponseValidator}
 * Api url: ****
 * Api endpoint: /users/{UserEmailId}/{TnCId}
 * Api git hash: ****
 *
 */

@ApiName("Raidiam Directory Put Users")
public class PutUsersResponseValidator extends GetUsersResponseValidator {
}
