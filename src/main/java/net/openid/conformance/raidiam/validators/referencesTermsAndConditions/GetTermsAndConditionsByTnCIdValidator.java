package net.openid.conformance.raidiam.validators.referencesTermsAndConditions;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostTermsAndConditionsValidator}
 * Api url: ****
 * Api endpoint: GET /references/termsandconditions/{TnCId}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory GET References - Terms and Conditions by TnCId")
public class GetTermsAndConditionsByTnCIdValidator extends PostTermsAndConditionsValidator {
}
