package net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow;

import net.openid.conformance.fapi1advancedfinal.FAPI1AdvancedFinalBrazilDCRInvalidSoftwareStatementSignature;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_invalid-ss-signature_test-module",
	displayName = "FAPI1-Advanced-Final: Brazil DCR invalidate software statement signature",
	summary = "Perform the DCR flow, but using a software statement where the signature has been invalidated - the server must reject the registration attempt.",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.discoveryUrl",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"directory.client_id",
		"directory.apibase"
	}
)
public class FAPI1AdvancedFinalBrazilDCRInvalidSoftwareStatementSignatureNoAuthFlow
	extends FAPI1AdvancedFinalBrazilDCRInvalidSoftwareStatementSignature {
	@Override
	protected boolean scopeContains(String requiredScope) {
		// Not needed as scope field is optional
		return false;
	}

	@Override
	protected void logFinalEnv() {
		//Not needed here
	}
}
