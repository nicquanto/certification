package net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow;

import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.fapi1advancedfinal.FAPI1AdvancedFinalBrazilDCRHappyFlow;
import net.openid.conformance.openbanking_brasil.consent.v2.ConsentDetailsIdentifiedByConsentIdValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilPaymentConsentProdValues;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.CallDynamicRegistrationEndpointAndVerifySuccessfulResponse;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;


@PublishTestModule(
	testName = "consents_api_bad-logged_test-module_v2",
	displayName = "FAPI1-Advanced-Final: Brazil DCR happy flow without authentication flow",
	summary = "This test will try to use the recently created DCR to access either a Consents or, if the server does not support Phase 2, a Payments Consent Call with a dummy, but well-formated payload to make sure that the server will read the request but won’t be able to process it. If server supports both Consents API, server will try to call both of them and expect a success.\n" +
		"\u2022 Create a client by performing a DCR against the provided server - Expect Success\n" +
		"\u2022 Generate a token with the client_id created using client_credentials grant with either payments or consents scope\n" +
		"\u2022 Validate that the provided URIs follow the expected patter for either payments-consents or consents\n"+
		"\u2022 Use the token to call either the POST Consents or POST Payments Consents API with a pre-defined payload, depending on the directory configuration provided the first provided consent\n" +
		"\u2022 If the Institution returns a 529 instead of a 201, wait 10 seconds and retry the endpoint call - Do this sequence 3 times. Return a failure if all three calls were a 529.\n"+
		"\u2022 Expect the server to accept the message and return a 201 - Validate the response_body and, if the message is a Payments Consents Response make sure the JWT has been correctly signed by the key registered on the A.S.\n" +
		"\u2022 Call either the POST consents or payments-consents API, depending on what has been provided on the \"SecondaryConsentUrl\" field\n" +
		"\u2022 If the Institution returns a 529 instead of a 201, wait 10 seconds and retry the endpoint call - Do this sequence 3 times. Return a failure if all three calls were a 529.\n"+
		"\u2022 Expect the server to accept the message and return a 201 - Validate the response_body and, if the message is a Payments Consents Response make sure the JWT has been correctly signed by the key registered on the A.S.",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.discoveryUrl",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"directory.client_id",
		"directory.apibase",
		"resource.consentUrl",
		"resource.consentUrl2",
		"resource.brazilOrganizationId"
	}
)

public class DCRConsentsBadLoggedUser extends FAPI1AdvancedFinalBrazilDCRHappyFlow {

	protected ClientAuthType clientAuthType;

	@Override
	public void start() {
		setStatus(Status.RUNNING);
		super.onPostAuthorizationFlowComplete();
	}

	@Override
	protected void configureClient() {
		clientAuthType = getVariant(ClientAuthType.class);
		super.configureClient();
	}

	@Override
	protected void setupResourceEndpoint() {
		// not needed as resource endpoint won't be called
	}

	@Override
	protected boolean scopeContains(String requiredScope) {
		// Not needed as scope field is optional
		return false;
	}

	@Override
	protected void callRegistrationEndpoint() {
		call(sequence(CallDynamicRegistrationEndpointAndVerifySuccessfulResponse.class));
		callAndContinueOnFailure(ClientManagementEndpointAndAccessTokenRequired.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2");
		eventLog.endBlock();

		eventLog.startBlock("Configuring dummy data");
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		callAndStopOnFailure(InsertBrazilPaymentConsentProdValues.class);
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		callAndStopOnFailure(InsertProdCpfValueToConfig.class);
		eventLog.endBlock();

		eventLog.startBlock("Checking consentURL");
		callAndStopOnFailure(ValidateConsentAndSecondaryConsentRegex.class);
		String consentUrl = env.getString("config", "resource.consentUrl");
		String secondaryConsentUrl = env.getString("config", "resource.consentUrl2");

		callConsentsEndpoint(consentUrl, "consentUrl");
		callAndContinueOnFailure(SwitchToSecondaryConsentUrl.class);
		callConsentsEndpoint(secondaryConsentUrl, "secondaryConsentUrl");

	}

	private void callConsentsEndpoint(String consentUrl, String message) {
		if (consentUrl.matches("^(https://)(.*?)(consents/v[0-9]/consents)")) {
			eventLog.startBlock("Calling Token Endpoint using Client Credentials");
			eventLog.log(getName(), String.format("%s is identified as Consents consent URL", message));
			callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
			callAndStopOnFailure(SetConsentsScopeOnTokenEndpointRequest.class);

			call(callTokenEndpointShortVersion());
			eventLog.endBlock();
			eventLog.startBlock(String.format("Calling Consents API using %s", message));
			ConditionSequenceRepeater sequenceRepeater = new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager, this::consentsApiSequence)
				.untilTrue("correct_consent_response")
				.times(3)
				.trailingPause(10)
				.onTimeout(sequenceOf(condition(EnsureConsentResponseCodeWas201.class).onFail(Condition.ConditionResult.FAILURE)));
			sequenceRepeater.run();
			call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
			callAndContinueOnFailure(ResourceEndpointResponseFromFullResponse.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(ConsentDetailsIdentifiedByConsentIdValidatorV2.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(ExtractConsentIdFromConsentEndpointResponse.class);
			deleteConsent();
			callAndStopOnFailure(EnsureResponseCodeWas204.class);

		} else if (consentUrl.matches("^(https://)(.*?)(payments/v[0-9]/consents)")) {
			eventLog.startBlock("Calling Token Endpoint using Client Credentials");
			eventLog.log(getName(), String.format("%s is identified as Payments consent URL", message));
			callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
			callAndStopOnFailure(SetPaymentsScopeOnTokenEndpointRequest.class);

			call(callTokenEndpointShortVersion());
			eventLog.endBlock();
			eventLog.startBlock(String.format("Calling Consents API using %s", message));
			ConditionSequence paymentValidationSequence = new SignedPaymentConsentValidationSequence();
			paymentValidationSequence.insertBefore(EnsureContentTypeApplicationJwt.class, condition(EnsureHttpStatusCodeIs201.class).onFail(Condition.ConditionResult.FAILURE));
			ConditionSequenceRepeater sequenceRepeater = new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager, this::getPaymentsConsentSequence)
				.untilTrue("correct_consent_response")
				.times(3)
				.trailingPause(10)
				.onTimeout(sequenceOf(condition(EnsureConsentResponseCodeWas201.class).onFail(Condition.ConditionResult.FAILURE)));
			sequenceRepeater.run();
			call(paymentValidationSequence);
		}
		eventLog.endBlock();
	}

	private ConditionSequence getPaymentsConsentSequence() {
		return new SignedPaymentConsentWithoutValidationSequence()
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class, condition(FAPIBrazilCreatePaymentConsentRequest.class))
			.insertBefore(FAPIBrazilSignPaymentConsentRequest.class, condition(CopyClientJwksToClient.class))
			.replace(EnsureConsentResponseCodeWas201.class, condition(Validate529Or201ReturnedFromConsents.class).onFail(Condition.ConditionResult.FAILURE));
	}
	private ConditionSequence consentsApiSequence() {
		return sequenceOf(
			condition(PrepareToPostConsentRequest.class),
			condition(AddConsentScope.class),
			condition(GetResourceEndpointConfiguration.class),
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(FAPIBrazilOpenBankingCreateConsentRequest.class),
			condition(FAPIBrazilAddExpirationPlus30ToConsentRequest.class),
			condition(SetContentTypeApplicationJson.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(Validate529Or201ReturnedFromConsents.class).onFail(Condition.ConditionResult.FAILURE)
		);
	}

	private ConditionSequence callTokenEndpointShortVersion() {
		ConditionSequence sequence = sequenceOf(
			condition(AddClientIdToTokenEndpointRequest.class),
			condition(CreateClientAuthenticationAssertionClaims.class).dontStopOnFailure(),
			condition(SignClientAuthenticationAssertion.class).dontStopOnFailure(),
			condition(AddClientAssertionToTokenEndpointRequest.class).dontStopOnFailure(),
			condition(CallTokenEndpoint.class),
			condition(CheckIfTokenEndpointResponseError.class),
			condition(ExtractAccessTokenFromTokenResponse.class)
		);

		if (clientAuthType == ClientAuthType.MTLS) {
			sequence.skip(CreateClientAuthenticationAssertionClaims.class, "Not needed for MTLS")
				.skip(SignClientAuthenticationAssertion.class, "Not needed for MTLS")
				.skip(AddClientAssertionToTokenEndpointRequest.class, "Not needed for MTLS");
		}
		return sequence;
	}

	public void deleteConsent() {
		eventLog.startBlock("Deleting consent");
		callAndContinueOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
	}
	@Override
	protected void onPostAuthorizationFlowComplete() {
		// not needed as resource endpoint won't be called
	}

	@Override
	protected void logFinalEnv() {
		//Not needed here
	}
}
