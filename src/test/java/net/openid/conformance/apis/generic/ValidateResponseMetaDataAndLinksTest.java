package net.openid.conformance.apis.generic;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateResponseMetaDataAndLinks;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@UseResurce("jsonResponses/metaData/goodMetaLinksBodyResponse.json")
public class ValidateResponseMetaDataAndLinksTest extends AbstractJsonResponseConditionUnitTest {

    @Test
	public void validateMetaDataAndLinks() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		run(condition);
	}

    @Test
	@UseResurce("jsonResponses/metaData/badMetaLinksBodyResponse.json")
	public void validateStructureWithMissingLinksObject() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "In the presence of a ‘prev’ or ‘next’ link, ‘totalPages’ must be greater than one, since ‘prev’ and ‘next’ are links to previous or next pages respectively, so that, added to the current page, it must total a quantity greater than one page.";
		assertThat(error.getMessage(), containsString(expected));
	}

    @Test
	@UseResurce("jsonResponses/metaData/badItemCountMetaResponse.json")
	public void validateStructureWithIncorrectItemCount() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "Data contains more items than the metadata totalRecords.";
		assertThat(error.getMessage(), containsString(expected));
	}

    @Test
	@UseResurce("jsonResponses/metaData/badSelfLinkResponse.json")
	public void validateStructureWithInvalidSelfLink() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "Value from element self doesn't match the required pattern on the ValidateResponseMetaDataAndLinks API response";
		assertThat(error.getMessage(), containsString(expected));
	}

    @Test
	@UseResurce("jsonResponses/metaData/badResponseWithPrevLink.json")
	public void validateStructureWithPrevLink() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "There should not be a 'prev' link.";
		assertThat(error.getMessage(), containsString(expected));
	}

    @Test
	@UseResurce("jsonResponses/metaData/badResponseWithMissingPrevLink.json")
	public void validateStructureWithMissingPrevLink() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "There should be a 'prev' link.";
		assertThat(error.getMessage(), containsString(expected));
	}

    @Test
	@UseResurce("jsonResponses/metaData/badResponseWithNextLink.json")
	public void validateStructureWithNextLink() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "There should not be a 'next' link.";
		assertThat(error.getMessage(), containsString(expected));
	}

    @Test
	@UseResurce("jsonResponses/metaData/badResponseWithMissingNextLink.json")
	public void validateStructureWithMissingNextLink() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "There should be a 'next' link.";
		assertThat(error.getMessage(), containsString(expected));
	}

	@Test
	@UseResurce("jsonResponses/metaData/goodResponseWithoutMetadata.json")
	public void validateStructureWithoutMetatdata() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		environment.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/metaData/goodResponseWithSelfLinkOnly.json")
	public void validateStructureWithSelfLinkOnly() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/metaData/goodConsentResponseWithoutSelfOrMeta.json")
	public void validateStructureWithoutSelfOrMeta() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		environment.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/metaData/badResponseWithoutSelfLink.json")
	public void validateStructureWithoutSelfLink() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "There should be a 'self' link.";
		assertThat(error.getMessage(), containsString(expected));
	}

	@Test
	@UseResurce("jsonResponses/metaData/badResponseWithMissingTimeZone.json")
	public void validateStructureWithMissingTimeZone() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "Value from element requestDateTime doesn't match the required pattern on the ValidateResponseMetaDataAndLinks API response";
		assertThat(error.getMessage(), containsString(expected));
	}

	@Test
	@UseResurce("jsonResponses/metaData/badResponseWithDateTimeOffset.json")
	public void validateStructureWithDateTimeOffset() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "Value from element requestDateTime doesn't match the required pattern on the ValidateResponseMetaDataAndLinks API response";
		assertThat(error.getMessage(), containsString(expected));
	}

	@Test
	@UseResurce("jsonResponses/metaData/badPaymentConsentResponseWithoutSelf.json")
	public void validatePaymentConsentStructureWithoutLinks() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "Payment consent requires a 'self' link.";
		assertThat(error.getMessage(), containsString(expected));
	}
	@Test
	@UseResurce("jsonResponses/metaData/goodResponseWithEmptyData.json")
	public void validateResponseWithEmptyData() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/metaData/badResponseWithEmptyData.json")
	public void validateBadResponseWithEmptyData() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "totalPages and totalRecords fields have to be 0 when data array is empty";
		assertThat(error.getMessage(), containsString(expected));
	}

	@Test
	@UseResurce("jsonResponses/metaData/badResponseWith3DataitemsAnd0Meta.json")
	public void validateBadResponseWith3DataItemsAnd0Meta() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		ConditionError error = runAndFail(condition);
		String expected = "Data contains more items than the metadata totalRecords.";
		assertThat(error.getMessage(), containsString(expected));
	}

	@Test
	@UseResurce("jsonResponses/metaData/goodResponseWithEmptyDataAndOtherFields.json")
	public void validateGoodResponseWithEmptyDataAndOtherFields() {
		ValidateResponseMetaDataAndLinks condition = new ValidateResponseMetaDataAndLinks();
		run(condition);
	}

}
