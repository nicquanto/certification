package net.openid.conformance.apis.creditCard.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.creditCard.v1.CreditCardAccountsLimitsResponseValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@UseResurce("jsonResponses/creditCard/cardLimits/cardLimitsResponse.json")
public class CreditCardAccountsLimitsResponseValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		CreditCardAccountsLimitsResponseValidator condition = new CreditCardAccountsLimitsResponseValidator();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/creditCard/cardLimits/cardLimitsResponse_with_missed_consolidationType_field.json")
	public void validateStructureWithMissingField() {
		CreditCardAccountsLimitsResponseValidator condition = new CreditCardAccountsLimitsResponseValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("consolidationType", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/cardLimits/cardLimitsResponseWrongEnum.json")
	public void validateStructureWrongEnum() {
		CreditCardAccountsLimitsResponseValidator condition = new CreditCardAccountsLimitsResponseValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldValueNotMatchEnumerationMessage("creditLineLimitType", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/cardLimits/cardLimitsResponseWrongRegexp.json")
	public void validateStructureWrongPattern() {
		CreditCardAccountsLimitsResponseValidator condition = new CreditCardAccountsLimitsResponseValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldValueNotMatchPatternMessage("limitAmount", condition.getApiName())));
	}


}
