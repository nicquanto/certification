package net.openid.conformance.apis.paymentInitiation;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.paymentInitiation.PaymentInitiationConsentRequestValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class PaymentInitiationConsentRequestValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/paymentInitiationConsentRequestOK.json")
	public void validateStructure() {
		environment.putObject("consent_endpoint_request", jsonObject);
		PaymentInitiationConsentRequestValidator condition = new PaymentInitiationConsentRequestValidator();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/paymentInitiationConsentRequest(NoLoggedUser).json")
	public void validateStructureNoLoggedUser() {
		environment.putObject("consent_endpoint_request", jsonObject);
		PaymentInitiationConsentRequestValidator condition = new PaymentInitiationConsentRequestValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("loggedUser", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/paymentInitiationConsentRequest(NoDebtorAccount).json")
	public void validateStructureNoDebtorAccount() {
		environment.putObject("consent_endpoint_request", jsonObject);
		PaymentInitiationConsentRequestValidator condition = new PaymentInitiationConsentRequestValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("debtorAccount", condition.getApiName())));
	}
}
