package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.PlainJWT;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.io.IOException;

import static net.openid.conformance.extensions.yacs.ServletInputStreamFactory.withBody;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CpfValidatingEVTests {

	@Test
	public void rejectsIfCpfMissingFromConfig() throws IOException {

		AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
		EnhancedVerification ev = new CpfEnhancedVerification(authenticationFacade);
		boolean allowed = ev.allowed(withBody(new JsonObject()));

		assertFalse(allowed);

	}

	@Test
	public void rejectsIfCpfInConfigIsNotInIdToken() throws IOException {

		AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
		JWTClaimsSet claimSet = new JWTClaimsSet.Builder()
			.claim("national_id", "anothercpf")
			.build();
		JWT claims = new PlainJWT(claimSet);
		Authentication idToken = new OIDCAuthenticationToken("sub", "iss", null, null, claims, null, null);
		when(authenticationFacade.getContextAuthentication()).thenReturn(idToken);

		EnhancedVerification ev = new CpfEnhancedVerification(authenticationFacade);
		JsonObject config = new JsonObject();
		JsonObject resource = new JsonObjectBuilder().addField("brazilCpf", "notarealcpf").build();
		config.add("resource", resource);
		boolean allowed = ev.allowed(withBody(config));

		assertFalse(allowed);

	}

	@Test
	public void allowsIfCpfInConfigIsInIdToken() throws IOException {

		AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
		JWTClaimsSet claimSet = new JWTClaimsSet.Builder()
			.claim("national_id", "notarealcpf")
			.build();
		JWT claims = new PlainJWT(claimSet);
		Authentication idToken = new OIDCAuthenticationToken("sub", "iss", null, null, claims, null, null);
		when(authenticationFacade.getContextAuthentication()).thenReturn(idToken);

		EnhancedVerification ev = new CpfEnhancedVerification(authenticationFacade);
		JsonObject config = new JsonObject();
		JsonObject resource = new JsonObjectBuilder().addField("brazilCpf", "notarealcpf").build();
		config.add("resource", resource);
		boolean allowed = ev.allowed(withBody(config));

		assertTrue(allowed);

	}

}
