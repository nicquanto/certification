package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.DelegatingServletInputStream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static net.openid.conformance.extensions.yacs.ServletInputStreamFactory.mockRequestBody;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

public class PlanCreationBlockingFilterTests {

	@Test
	public void filterIgnoresRequestsNotCreatingPlan() throws ServletException, IOException {

		Filter filter = new PlanCreationBlockingFilter();
		HttpServletRequest request = mock(HttpServletRequest.class);
		when(request.getMethod()).thenReturn("GET");
		when(request.getServletPath()).thenReturn("/api/nothing");
		HttpServletResponse response = mock(HttpServletResponse.class);
		FilterChain chain = mock(FilterChain.class);
		filter.doFilter(request, response, chain);

		Mockito.verify(chain, times(1)).doFilter(request, response);

	}

	@Test
	public void filterPerformsEnhancedVerificationWhenCreatingPlan() throws ServletException, IOException {

		JsonObject config = new JsonObjectBuilder()
			.addField("cpf", "lewnefwe03")
			.build();
		EnhancedVerification ev = mock(EnhancedVerification.class);
		when(ev.allowed(any())).thenReturn(true);
		Filter filter = new PlanCreationBlockingFilter(ev);
		HttpServletRequest request = mock(HttpServletRequest.class);
		when(request.getMethod()).thenReturn("POST");
		when(request.getServletPath()).thenReturn("/api/plan");
		mockRequestBody(request, config);
		HttpServletResponse response = mock(HttpServletResponse.class);
		FilterChain chain = mock(FilterChain.class);
		filter.doFilter(request, response, chain);

		ArgumentCaptor<? extends HttpServletRequest> requestCaptor = ArgumentCaptor.forClass(HttpServletRequest.class);
		Mockito.verify(chain, times(1)).doFilter(requestCaptor.capture(), isA(ServletResponse.class));

		assertTrue(requestCaptor.getValue().getClass().isAssignableFrom(BodyRepeatableHttpServletRequest.class));

	}

}
