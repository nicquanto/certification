package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasDataPagamentoInvalida;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class AbstractEnsureErrorResponseCodeFieldWasTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseV2.json")
	public void processFindCodeField() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeFieldWasParametroNaoInformado();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseV2.json")
	public void processFindCodeField2() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeWasDataPagamentoInvalida();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/errors/422/bad422WrongCode.json")
	public void processFindCodeField3() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeWasDataPagamentoInvalida();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Could not find error with expected code in the errors JSON array"));

	}

}
