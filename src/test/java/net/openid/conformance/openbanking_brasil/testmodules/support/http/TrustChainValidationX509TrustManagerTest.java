package net.openid.conformance.openbanking_brasil.testmodules.support.http;

import net.openid.conformance.logging.TestInstanceEventLog;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class TrustChainValidationX509TrustManagerTest {
	GeneratedCert rootCertOne;

	GeneratedCert rootCertTwo;

	GeneratedCert rootCertThree;


	X509Certificate[] dirRoots;

	TrustChainValidationX509TrustManager trustManager = new TrustChainValidationX509TrustManager(mock(TestInstanceEventLog.class));
	@Before
	public void setup() {
		rootCertOne = assertDoesNotThrow(() -> createCertificate("Root Certificate CA - 1", "https://example.com/root.cer", null, true, false));
		rootCertTwo = assertDoesNotThrow(() -> createCertificate("Root Certificate CA - 2", "https://example.com/root.cer", null, true, false));
		rootCertThree = assertDoesNotThrow(() -> createCertificate("Root Certificate CA - 3", "https://example.com/root.cer", null, true, false));
		dirRoots = new X509Certificate[]{rootCertOne.certificate, rootCertTwo.certificate};
	}

	@Test
	public void returnNoErrorWhenCorrectTrustChainIsSent() {
		assertDoesNotThrow(() -> trustManager.validateTrustChainReturned(validTrustChain(rootCertOne), dirRoots));
	}

	@Test
	public void returnErrorWhenIntermediateCaIsMissing() {
		assertThrows(CertificateException.class, () -> trustManager.validateTrustChainReturned(trustChainMissingIntermediateCa(rootCertTwo), dirRoots));
	}

	@Test
	public void returnFailureOnExpiredLeaf() {
		assertThrows(CertificateException.class, () -> trustManager.validateTrustChainReturned(trustChainExpiredLeaf(rootCertOne), dirRoots));
	}

	@Test
	public void returnFailureOnExpiredIntermediate() {
		assertThrows(CertificateException.class, () -> trustManager.validateTrustChainReturned(trustChainExpiredIntermediateCa(rootCertOne), dirRoots));
	}

	@Test
	public void returnNoErrorWhenRootIsNotSentButDirRootsContainsIt() {
		assertDoesNotThrow(() -> trustManager.validateTrustChainReturned(rootNotSent(rootCertOne), dirRoots));
	}

	@Test
	public void returnErrorWhenOnlyTheLeafCertIsSent() {
		assertThrows(CertificateException.class, () -> trustManager.validateTrustChainReturned(onlyLeafSent(rootCertOne), dirRoots));
	}

	@Test
	public void returnErrorWhenLeafCertIsSentTwice() {
		assertThrows(CertificateException.class, () -> trustManager.validateTrustChainReturned(leafInChainTwice(rootCertOne), dirRoots));
	}

	@Test
	public void returnErrorWhenRootCertSentIsNotPublishedOnDirRoots() {
		assertThrows(CertificateException.class, () -> trustManager.validateTrustChainReturned(validTrustChain(rootCertThree), dirRoots));
	}

	final static class GeneratedCert {
		public final PrivateKey privateKey;
		public final X509Certificate certificate;

		public GeneratedCert(PrivateKey privateKey, X509Certificate certificate) {
			this.privateKey = privateKey;
			this.certificate = certificate;
		}
	}

	public GeneratedCert createCertificate(String cnName, String domain, GeneratedCert issuer, boolean isCA, boolean expiredCert) throws Exception {
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
		KeyPair certKeyPair = keyGen.generateKeyPair();
		X500Name name = new X500Name("CN=" + cnName);

		BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis());
		Instant validFrom;
		Instant validUntil;
		if (expiredCert) {
			validFrom = Instant.now().minus(10 * 365, ChronoUnit.DAYS);
			validUntil = validFrom.plus(10 * 140, ChronoUnit.DAYS);
		} else {
			validFrom = Instant.now();
			validUntil = validFrom.plus(10 * 360, ChronoUnit.DAYS);
		}


		X500Name issuerName;
		PrivateKey issuerKey;
		if (issuer == null) {
			issuerName = name;
			issuerKey = certKeyPair.getPrivate();
		} else {
			issuerName = new X500Name(issuer.certificate.getSubjectDN().getName());
			issuerKey = issuer.privateKey;
		}
		SubjectPublicKeyInfo subjectPublicKeyInfo = SubjectPublicKeyInfo.getInstance(certKeyPair.getPublic().getEncoded());
		X509v3CertificateBuilder builder = new X509v3CertificateBuilder(
			issuerName,
			serialNumber,
			Date.from(validFrom), Date.from(validUntil),
			name,
			subjectPublicKeyInfo
		);

		if (isCA) {
			builder.addExtension(Extension.basicConstraints, true, new BasicConstraints(isCA));
		}

		if (domain != null) {
			builder.addExtension(Extension.subjectAlternativeName, false,
				new GeneralNames(new GeneralName(GeneralName.dNSName, domain)));
		}

		ContentSigner signer = new JcaContentSignerBuilder("SHA256WithRSA").build(issuerKey);
		X509CertificateHolder certHolder = builder.build(signer);
		X509Certificate cert = new JcaX509CertificateConverter().getCertificate(certHolder);

		return new GeneratedCert(certKeyPair.getPrivate(), cert);
	}

	public X509Certificate[] validTrustChain(GeneratedCert rootCert) throws Exception {
		GeneratedCert intermediateCa = createCertificate("Test Name Int", "https:www.example.com", rootCert, true, false);
		GeneratedCert leafCert = createCertificate("Test Name Leaf", "https:www.example.com", intermediateCa, false, false);
		List<X509Certificate> certificates = List.of(leafCert.certificate, intermediateCa.certificate, rootCert.certificate);
		return certificates.toArray(new X509Certificate[0]);
	}

	public X509Certificate[] trustChainMissingIntermediateCa(GeneratedCert rootCert) throws Exception {
		GeneratedCert intermediateCa = createCertificate("Test Name Int", "https:www.example.com", rootCert, true, false);
		GeneratedCert leafCert = createCertificate("Test Name Leaf", "https:www.example.com", intermediateCa, false, false);
		List<X509Certificate> certificates = List.of(leafCert.certificate, rootCert.certificate);
		return certificates.toArray(new X509Certificate[0]);
	}

	public X509Certificate[] trustChainExpiredIntermediateCa(GeneratedCert rootCert) throws Exception {
		GeneratedCert intermediateCa = createCertificate("Test Name Int", "https:www.example.com", rootCert, true, true);
		GeneratedCert leafCert = createCertificate("Test Name Leaf", "https:www.example.com", intermediateCa, false, false);
		List<X509Certificate> certificates = List.of(leafCert.certificate, intermediateCa.certificate, rootCert.certificate);
		return certificates.toArray(new X509Certificate[0]);
	}

	public X509Certificate[] trustChainExpiredLeaf(GeneratedCert rootCert) throws Exception {
		GeneratedCert intermediateCa = createCertificate("Test Name Int", "https:www.example.com", rootCert, true, false);
		GeneratedCert leafCert = createCertificate("Test Name Leaf", "https:www.example.com", intermediateCa, false, true);
		List<X509Certificate> certificates = List.of(leafCert.certificate, intermediateCa.certificate, rootCert.certificate);
		return certificates.toArray(new X509Certificate[0]);
	}

	public X509Certificate[] onlyLeafSent(GeneratedCert rootCert) throws Exception {
		GeneratedCert intermediateCa = createCertificate("Test Name Int", "https:www.example.com", rootCert, true, false);
		GeneratedCert leafCert = createCertificate("Test Name Leaf", "https:www.example.com", intermediateCa, false, false);
		List<X509Certificate> certificates = List.of(leafCert.certificate);
		return certificates.toArray(new X509Certificate[0]);
	}

	public X509Certificate[] leafInChainTwice(GeneratedCert rootCert) throws Exception {
		GeneratedCert intermediateCa = createCertificate("Test Name Int", "https:www.example.com", rootCert, true, false);
		GeneratedCert leafCert = createCertificate("Test Name Leaf", "https:www.example.com", intermediateCa, false, false);
		List<X509Certificate> certificates = List.of(leafCert.certificate, leafCert.certificate, intermediateCa.certificate, rootCert.certificate);
		return certificates.toArray(new X509Certificate[0]);
	}

	public X509Certificate[] rootNotSent(GeneratedCert rootCert) throws Exception {
		GeneratedCert intermediateCa = createCertificate("Test Name Int", "https:www.example.com", rootCert, true, false);
		GeneratedCert leafCert = createCertificate("Test Name Leaf", "https:www.example.com", intermediateCa, false, false);
		List<X509Certificate> certificates = List.of(leafCert.certificate, intermediateCa.certificate);
		return certificates.toArray(new X509Certificate[0]);
	}
}
