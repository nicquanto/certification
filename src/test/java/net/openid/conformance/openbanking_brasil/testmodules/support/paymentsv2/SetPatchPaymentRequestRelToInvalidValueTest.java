package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

public class SetPatchPaymentRequestRelToInvalidValueTest {

	@Test
	public void setInvalidPatchRelTest() {
		Environment env = new Environment();
		SetPatchPaymentRequestRelToInvalidValue cond = new SetPatchPaymentRequestRelToInvalidValue();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);

		JsonObject resource = new JsonObject();
		JsonObject request = new JsonObjectBuilder()
			.addFields("data", Map.of("status", "CANC"))
			.addFields("data.cancellation.cancelledBy.document", Map.of("identification", "12345678911", "rel", "CPF"))
			.build();

		resource.add("brazilPatchPixPayment", request);
		env.putObject("resource", resource);

		try {
			cond.execute(env);

			JsonObject patchRequest = env.getElementFromObject("resource", "brazilPatchPixPayment").getAsJsonObject();
			assertNotNull(patchRequest);
			JsonObject data = patchRequest.getAsJsonObject("data");
			assertNotNull(data);
			JsonElement status = data.get("status");
			assertNotNull(status);
			assertEquals("CANC", OIDFJSON.getString(status));
			JsonObject cancellation = data.getAsJsonObject("cancellation");
			assertNotNull(cancellation);
			JsonObject cancelledBy = cancellation.getAsJsonObject("cancelledBy");
			assertNotNull(cancelledBy);
			JsonObject document = cancelledBy.getAsJsonObject("document");
			assertNotNull(document);
			JsonElement identification = document.get("identification");
			assertNotNull(identification);
			assertEquals("12345678911", OIDFJSON.getString(identification));
			JsonElement rel = document.get("rel");
			assertNotNull(rel);
			assertEquals("XXX", OIDFJSON.getString(rel));

		} catch (ConditionError error) {
			throw new AssertionError("Condition failed", error);
		}


	}

}
