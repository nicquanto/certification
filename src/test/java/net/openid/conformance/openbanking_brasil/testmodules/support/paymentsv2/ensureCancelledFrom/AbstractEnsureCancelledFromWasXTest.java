package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancelledFrom;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason.EnsureCancellationReasonWasCanceladoAgendamento;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class AbstractEnsureCancelledFromWasXTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/patchPaymentsV2/patchPaymentWithCancellation2.json")
	public void validateGoodCancellationReason() {
		EnsureCancellationReasonWasCanceladoAgendamento condition = new EnsureCancellationReasonWasCanceladoAgendamento();
		run(condition);
	}


	@Test
	@UseResurce("jsonResponses/paymentInitiation/patchPaymentsV2/patchPaymentWithCancellation.json")
	public void validateBadCancellationReason() {
		EnsureCancellationReasonWasCanceladoAgendamento condition = new EnsureCancellationReasonWasCanceladoAgendamento();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Payment cancellationReason type is not what was expected"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/patchPaymentsV2/patchPaymentNoCancellation.json")
	public void validateMissingCancellationReason() {
		EnsureCancellationReasonWasCanceladoAgendamento condition = new EnsureCancellationReasonWasCanceladoAgendamento();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Could not extract cancellationReason from the the resource_endpoint_response_full.data.cancellation"));
	}
}
