package net.openid.conformance.openbanking_brasil.phase4b.investments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.phase4b.investments.treasureTitles.GetTreasureTitlesBalancesValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.treasureTitles.GetTreasureTitlesProductIdentificationValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.treasureTitles.GetTreasureTitlesProductListValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.treasureTitles.GetTreasureTitlesTransactionsCurrentValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.treasureTitles.GetTreasureTitlesTransactionsValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class TreasureTitlesTests extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/GetTreasureTitlesProductListResponse.json")
	public void evaluateProductList() {
		run(new GetTreasureTitlesProductListValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/GetTreasureTitlesProductIdentificationResponse.json")
	public void evaluateProductIdentification() {
		run(new GetTreasureTitlesProductIdentificationValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/additionalOptions/GetTreasureTitlesProductIdentificationResponseIndexerAdditionalInfoRestrição.json")
	public void evaluateProductIdentificationIndexerAdditionalInfoRestrição() {
		GetTreasureTitlesProductIdentificationValidator condition = new GetTreasureTitlesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("indexerAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/additionalOptions/GetTreasureTitlesProductIdentificationResponsePreFixedRateRestrição.json")
	public void evaluateProductIdentificationPreFixedRateRestrição() {
		GetTreasureTitlesProductIdentificationValidator condition = new GetTreasureTitlesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("preFixedRate", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/additionalOptions/GetTreasureTitlesProductIdentificationResponseVoucherPaymentPeriodicityRestrição.json")
	public void evaluateProductIdentificationVoucherPaymentPeriodicityRestrição() {
		GetTreasureTitlesProductIdentificationValidator condition = new GetTreasureTitlesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("voucherPaymentPeriodicity", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/additionalOptions/GetTreasureTitlesProductIdentificationResponseVoucherPaymentPeriodicityAdditionalInfoRestrição.json")
	public void evaluateProductIdentificationVoucherPaymentPeriodicityAdditionalInfoRestrição() {
		GetTreasureTitlesProductIdentificationValidator condition = new GetTreasureTitlesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("voucherPaymentPeriodicityAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/additionalOptions/GetTreasureTitlesProductIdentificationResponsePostFixedIndexerPercentageRestrição.json")
	public void evaluateProductIdentificationPostFixedIndexerPercentageRestrição() {
		GetTreasureTitlesProductIdentificationValidator condition = new GetTreasureTitlesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("postFixedIndexerPercentage", condition.getApiName())));
	}



	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/GetTreasureTitlesBalancesResponse.json")
	public void evaluateBalances() {
		run(new GetTreasureTitlesBalancesValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/GetTreasureTitlesTransactionsResponse.json")
	public void evaluateTransactions() {
		run(new GetTreasureTitlesTransactionsValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/additionalOptions/GetTreasureTitlesTransactionsRemunerationTransactionRateRestrição.json")
	public void evaluateTransactionsRemunerationTransactionRateRestrição() {
		GetTreasureTitlesTransactionsValidator condition = new GetTreasureTitlesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("remunerationTransactionRate", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/additionalOptions/GetTreasureTitlesTransactionsResponseTransactionTypeAdditionalInfoRestrição.json")
	public void evaluateTransactionsTypeAdditionalInfoRestrição() {
		GetTreasureTitlesTransactionsValidator condition = new GetTreasureTitlesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("transactionTypeAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/additionalOptions/GetTreasureTitlesTransactionsResponseIncomeTaxRestrição.json")
	public void evaluateTransactionsIncomeTaxRestrição() {
		GetTreasureTitlesTransactionsValidator condition = new GetTreasureTitlesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("incomeTax", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/additionalOptions/GetTreasureTitlesTransactionsResponseFinancialTransactionTaxRestrição.json")
	public void evaluateProductTransactionsTypeAdditionalInfoRestrição() {
		GetTreasureTitlesTransactionsValidator condition = new GetTreasureTitlesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("financialTransactionTax", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/treasureTitles/GetTreasureTitlesTransactionsResponse.json")
	public void evaluateTransactionsCurrent() {
		run(new GetTreasureTitlesTransactionsCurrentValidator());
	}
}
