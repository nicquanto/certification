package net.openid.conformance.openbanking_brasil.phase4b.investments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.phase4b.investments.funds.GetFundsBalancesValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.funds.GetFundsProductIdentificationValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.funds.GetFundsProductListValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.funds.GetFundsTransactionsCurrentValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.funds.GetFundsTransactionsValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class FundsTests extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/phase4b/investments/funds/GetFundsProductListResponse.json")
	public void evaluateProductList() {
		run(new GetFundsProductListValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/funds/GetFundsProductIdentificationResponse.json")
	public void evaluateProductIdentification() {
		run(new GetFundsProductIdentificationValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/funds/GetFundsBalancesResponse.json")
	public void evaluateBalances() {
		run(new GetFundsBalancesValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/funds/GetFundsTransactionsResponse.json")
	public void evaluateTransactions() {
		run(new GetFundsTransactionsValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/funds/GetFundsTransactionsResponse.json")
	public void evaluateTransactionsCurrent() {
		run(new GetFundsTransactionsCurrentValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/funds/additionalOptions/GetFundsTransactionsResponseTransactionTypeAdditionalInfoRestrição.json")
	public void evaluateTransactionsTypeAdditionalInfoRestrição() {
		GetFundsTransactionsValidator condition = new GetFundsTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("transactionTypeAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/funds/additionalOptions/GetFundsTransactionsResponseIncomeTaxRestrição.json")
	public void evaluateTransactionsIncomeTaxRestrição() {
		GetFundsTransactionsValidator condition = new GetFundsTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("incomeTax", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/funds/additionalOptions/GetFundsTransactionsResponseFinancialTransactionTaxRestrição.json")
	public void
	evaluateProductTransactionsFinancialTransactionTaxRestrição() {
		GetFundsTransactionsValidator condition = new GetFundsTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("financialTransactionTax", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/funds/additionalOptions/GetFundsTransactionsResponseTransactionExitFeeRestrição.json")
	public void evaluateProductTransactionsTransactionExitFeeRestrição() {
		GetFundsTransactionsValidator condition = new GetFundsTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("transactionExitFee", condition.getApiName())));
	}
}
