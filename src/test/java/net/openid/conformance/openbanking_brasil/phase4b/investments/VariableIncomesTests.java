package net.openid.conformance.openbanking_brasil.phase4b.investments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes.GetVariableIncomesBalancesValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes.GetVariableIncomesBrokerNoteDetailsValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes.GetVariableIncomesProductIdentificationValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes.GetVariableIncomesProductListValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes.GetVariableIncomesTransactionsCurrentValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.variableIncomes.GetVariableIncomesTransactionsValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class VariableIncomesTests extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/GetVariableIncomesProductListResponse.json")
	public void evaluateProductList() {
		run(new GetVariableIncomesProductListValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/GetVariableIncomesProductIdentificationResponse.json")
	public void evaluateProductIdentification() {
		run(new GetVariableIncomesProductIdentificationValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/GetVariableIncomesBalancesResponse.json")
	public void evaluateProductBalances() {
		run(new GetVariableIncomesBalancesValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/GetVariableIncomesTransactionsResponse.json")
	public void evaluateProductTransactions() {
		run(new GetVariableIncomesTransactionsValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/GetVariableIncomesTransactionsCurrentResponse.json")
	public void evaluateProductTransactionsCurrent() {
		run(new GetVariableIncomesTransactionsCurrentValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/GetVariableIncomesBrokerNoteDetailsResponse.json")
	public void evaluateBrokerNoteDetails() {
		run(new GetVariableIncomesBrokerNoteDetailsValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/additionalOptions/GetVariableIncomesTypeAdditionalInfoRestrição.json")
	public void evaluateProductTransactionsTypeAdditionalInfoRestrição() {
		GetVariableIncomesTransactionsValidator condition = new GetVariableIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("typeAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/additionalOptions/GetVariableIncomesTransactionUnitPriceRestrição.json")
	public void evaluateProductTransactionsTransactionUnitPriceRestrição() {
		GetVariableIncomesTransactionsValidator condition = new GetVariableIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("transactionUnitPrice", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/additionalOptions/GetVariableIncomesTransactionQuantityRestrição.json")
	public void evaluateProductTransactionsTransactionQuantityRestrição() {
		GetVariableIncomesTransactionsValidator condition = new GetVariableIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("transactionQuantity", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/additionalOptions/GetVariableIncomesBrokerNoteIdRestrição.json")
	public void evaluateProductTransactionsBrokerNoteIdRestrição() {
		GetVariableIncomesTransactionsValidator condition = new GetVariableIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("brokerNoteId", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/additionalOptions/GetVariableIncomesTypeAdditionalInfoRestrição.json")
	public void evaluateProductTransactionsCurrentTypeAdditionalInfoRestrição() {
		GetVariableIncomesTransactionsCurrentValidator condition = new GetVariableIncomesTransactionsCurrentValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("typeAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/additionalOptions/GetVariableIncomesTransactionUnitPriceRestrição.json")
	public void evaluateProductTransactionsCurrentTransactionUnitPriceRestrição() {
		GetVariableIncomesTransactionsCurrentValidator condition = new GetVariableIncomesTransactionsCurrentValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("transactionUnitPrice", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/additionalOptions/GetVariableIncomesTransactionQuantityRestrição.json")
	public void evaluateProductTransactionsCurrentTransactionQuantityRestrição() {
		GetVariableIncomesTransactionsCurrentValidator condition = new GetVariableIncomesTransactionsCurrentValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("transactionQuantity", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/variableIncomes/additionalOptions/GetVariableIncomesBrokerNoteIdRestrição.json")
	public void evaluateProductTransactionsCurrentBrokerNoteIdRestrição() {
		GetVariableIncomesTransactionsCurrentValidator condition = new GetVariableIncomesTransactionsCurrentValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("brokerNoteId", condition.getApiName())));
	}
}
