package net.openid.conformance.openbanking_brasil.phase4b.investments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.phase4b.investments.creditFixedIncomes.GetCreditFixedIncomesBalancesValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.creditFixedIncomes.GetCreditFixedIncomesProductIdentificationValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.creditFixedIncomes.GetCreditFixedIncomesProductListValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.creditFixedIncomes.GetCreditFixedIncomesTransactionsCurrentValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.creditFixedIncomes.GetCreditFixedIncomesTransactionsValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class CreditFixedIncomesTests extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/GetCreditFixedIncomesProductListResponse.json")
	public void evaluateProductList() {
		run(new GetCreditFixedIncomesProductListValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/GetCreditFixedIncomesProductIdentificationResponse.json")
	public void evaluateProductIdentification() {
		run(new GetCreditFixedIncomesProductIdentificationValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesProductIdentificationDebtorCnpjNumberRestrição.json")
	public void evaluateProductIdentificationDebtorCnpjNumberRestrição() {
		GetCreditFixedIncomesProductIdentificationValidator condition = new GetCreditFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("debtorCnpjNumber", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesProductIdentificationDebtorNameRestrição.json")
	public void evaluateProductIdentificationDebtorNameRestrição() {
		GetCreditFixedIncomesProductIdentificationValidator condition = new GetCreditFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("debtorName", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesProductIdentificationPreFixedRateRestrição.json")
	public void evaluateProductIdentificationPreFixedRateRestrição() {
		GetCreditFixedIncomesProductIdentificationValidator condition = new GetCreditFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("preFixedRate", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesProductIdentificationPostFixedIndexerPercentageRestrição.json")
	public void evaluateProductIdentificationPostFixedIndexerPercentageRestrição() {
		GetCreditFixedIncomesProductIdentificationValidator condition = new GetCreditFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("postFixedIndexerPercentage", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesProductIdentificationIndexerAdditionalInfoRestrição.json")
	public void evaluateProductIdentificationIndexerAdditionalInfoRestrição() {
		GetCreditFixedIncomesProductIdentificationValidator condition = new GetCreditFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("indexerAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesProductIdentificationVoucherPaymentPeriodicityRestrição.json")
	public void evaluateProductIdentificationVoucherPaymentPeriodicityRestrição() {
		GetCreditFixedIncomesProductIdentificationValidator condition = new GetCreditFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("voucherPaymentPeriodicity", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesProductIdentificationVoucherPaymentPeriodicityAdditionalInfoRestrição.json")
	public void evaluateProductIdentificationVoucherPaymentPeriodicityAdditionalInfoRestrição() {
		GetCreditFixedIncomesProductIdentificationValidator condition = new GetCreditFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("voucherPaymentPeriodicityAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesProductIdentificationIfOPTIONALVoucherPaymentPeriodicityRestrição.json")
	public void evaluateProductIdentificationIfOPTIONALVoucherPaymentPeriodicityRestrição() {
		run(new GetCreditFixedIncomesProductIdentificationValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/GetCreditFixedIncomesBalancesResponse.json")
	public void evaluateProductBalances() {
		run(new GetCreditFixedIncomesBalancesValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/GetCreditFixedIncomesTransactionsResponse.json")
	public void evaluateProductTransactions() {
		run(new GetCreditFixedIncomesTransactionsValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesTransactionsTypeAdditionalInfoRestrição.json")
	public void evaluateProductTransactionsTypeAdditionalInfoRestrição() {
		GetCreditFixedIncomesTransactionsValidator condition = new GetCreditFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("typeAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesTransactionsIncomeTaxRestrição.json")
	public void evaluateProductTransactionsIncomeTaxRestrição() {
		GetCreditFixedIncomesTransactionsValidator condition = new GetCreditFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("incomeTax", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesTransactionsFinancialTransactionTaxRestrição.json")
	public void evaluateProductTransactionsFinancialTransactionTaxRestrição() {
		GetCreditFixedIncomesTransactionsValidator condition = new GetCreditFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("financialTransactionTax", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesTransactionsRemunerationTransactionRateRestrição.json")
	public void evaluateProductTransactionsRemunerationTransactionRateRestrição() {
		GetCreditFixedIncomesTransactionsValidator condition = new GetCreditFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("remunerationTransactionRate", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/additionalOptions/GetCreditFixedIncomesTransactionsIndexerPercentageRestrição.json")
	public void evaluateProductTransactionsIndexerPercentageRestrição() {
		GetCreditFixedIncomesTransactionsValidator condition = new GetCreditFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("indexerPercentage", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/creditFixedIncomes/GetCreditFixedIncomesTransactionsCurrentResponse.json")
	public void evaluateProductTransactionsCurrent() {
		run(new GetCreditFixedIncomesTransactionsCurrentValidator());
	}
}
