package net.openid.conformance.openbanking_brasil.phase4b.investments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.phase4b.investments.bankFixedIncomes.GetBankFixedIncomesBalancesValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.bankFixedIncomes.GetBankFixedIncomesProductIdentificationValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.bankFixedIncomes.GetBankFixedIncomesProductListValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.bankFixedIncomes.GetBankFixedIncomesTransactionsCurrentValidator;
import net.openid.conformance.openbanking_brasil.phase4b.investments.bankFixedIncomes.GetBankFixedIncomesTransactionsValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class BankFixedIncomesTests extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/GetBankFixedIncomesProductListResponse.json")
	public void evaluateProductList() {
		run(new GetBankFixedIncomesProductListValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/GetBankFixedIncomesProductIdentificationResponse.json")
	public void evaluateProductIdentification() {
		run(new GetBankFixedIncomesProductIdentificationValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/additionalOptions/GetBankFixedIncomesProductIdentificationPreFixedRateRestrição.json")
	public void evaluateProductIdentificationPreFixedRateRestrição() {
		GetBankFixedIncomesProductIdentificationValidator condition = new GetBankFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("preFixedRate", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/additionalOptions/GetBankFixedIncomesProductIdentificationPostFixedIndexerPercentageRestrição.json")
	public void evaluateProductIdentificationPostFixedIndexerPercentageRestrição() {
		GetBankFixedIncomesProductIdentificationValidator condition = new GetBankFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("postFixedIndexerPercentage", condition.getApiName())));

	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/additionalOptions/GetBankFixedIncomesProductIdentificationIndexerAdditionalInfoRestrição.json")
	public void evaluateProductIdentificationIndexerAdditionalInfoRestrição() {
		GetBankFixedIncomesProductIdentificationValidator condition = new GetBankFixedIncomesProductIdentificationValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("indexerAdditionalInfo", condition.getApiName())));

	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/GetBankFixedIncomesBalancesResponse.json")
	public void evaluateProductBalances() {
		run(new GetBankFixedIncomesBalancesValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/GetBankFixedIncomesTransactionsResponse.json")
	public void evaluateProductTransactions() {
		run(new GetBankFixedIncomesTransactionsValidator());
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/additionalOptions/GetBankFixedIncomesTransactionsTypeAdditionalInfoRestrição.json")
	public void evaluateProductTransactionsTypeAdditionalInfoRestrição() {
		GetBankFixedIncomesTransactionsValidator condition = new GetBankFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("typeAdditionalInfo", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/additionalOptions/GetBankFixedIncomesTransactionsIncomeTaxRestrição.json")
	public void evaluateProductTransactionsIncomeTaxRestrição() {
		GetBankFixedIncomesTransactionsValidator condition = new GetBankFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("incomeTax", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/additionalOptions/GetBankFixedIncomesTransactionsFinancialTransactionTaxRestrição.json")
	public void evaluateProductTransactionsFinancialTransactionTaxRestrição() {
		GetBankFixedIncomesTransactionsValidator condition = new GetBankFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("financialTransactionTax", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/additionalOptions/GetBankFixedIncomesTransactionsRemunerationTransactionRateRestrição.json")
	public void evaluateProductTransactionsRemunerationTransactionRateRestrição() {
		GetBankFixedIncomesTransactionsValidator condition = new GetBankFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("remunerationTransactionRate", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/additionalOptions/GetBankFixedIncomesTransactionsIndexerPercentageRestrição.json")
	public void evaluateProductTransactionsIndexerPercentageRestrição() {
		GetBankFixedIncomesTransactionsValidator condition = new GetBankFixedIncomesTransactionsValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("indexerPercentage", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/phase4b/investments/bankFixedIncomes/GetBankFixedIncomesTransactionsCurrentResponse.json")
	public void evaluateProductTransactionsCurrent() {
		run(new GetBankFixedIncomesTransactionsCurrentValidator());
	}

}
