package net.openid.conformance.condition.client;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nimbusds.jose.Algorithm;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jca.JCAContext;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.extensions.AlternateJWSSignerFactory;
import net.openid.conformance.extensions.MultiJWSSignerFactory;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class AbstractSignJWTTests {

	@Test
	public void willSignWithConfiguredJWK() throws Exception {

		Environment env = new Environment();

		TheJWTSigningCondition condition = new TheJWTSigningCondition();

		condition.execute(env);

		String signed = env.getString("signed_jwt");

		assertNotNull(signed);

		SignedJWT signedJWT = SignedJWT.parse(signed);

		boolean verified = signedJWT.verify(new RSASSAVerifier(condition.jwkSet.toPublicJWKSet().getKeyByKeyId(condition.kid).toRSAKey()));
		assertTrue(verified);
	}

	@Test
	public void willSignWithAlternateSigner() throws Exception {

		Environment env = new Environment();

		TheJWTSigningCondition condition = new TheJWTSigningCondition();
		MyJwtSigner jwtSigner = new MyJwtSigner(condition.jwkSet.toPublicJWKSet().getKeyByKeyId(condition.kid).toRSAKey());
		MultiJWSSignerFactory f = (MultiJWSSignerFactory) MultiJWSSignerFactory.getInstance();
		f.registerAlternative(jwtSigner);

		condition.execute(env);

		String signed = env.getString("signed_jwt");

		assertNotNull(signed);

		SignedJWT signedJWT = SignedJWT.parse(signed);

		boolean verified = signedJWT.verify(new RSASSAVerifier(condition.jwkSet.toPublicJWKSet().getKeyByKeyId(condition.kid).toRSAKey()));
		assertFalse(verified);

		verified = signedJWT.verify(new RSASSAVerifier((RSAPublicKey) jwtSigner.keyPair.getPublic()));
		assertTrue(verified);
	}

	@AfterEach
	void tearDown() {
		MultiJWSSignerFactory.getInstance().clearAlternatives();
	}

	static class TheJWTSigningCondition extends AbstractSignJWT {

		final String kid = UUID.randomUUID().toString();
		final JWKSet jwkSet;

		TheJWTSigningCondition() throws NoSuchAlgorithmException {
			setProperties("UNIT-TEST", mock(TestInstanceEventLog.class), Condition.ConditionResult.INFO);
			KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
			gen.initialize(2048);
			KeyPair keyPair = gen.generateKeyPair();
			JWK jwk = new RSAKey.Builder((RSAPublicKey)keyPair.getPublic())
				.privateKey((RSAPrivateKey)keyPair.getPrivate())
				.keyUse(KeyUse.SIGNATURE)
				.algorithm(Algorithm.parse("RS256"))
				.keyID(kid)
				.build();
			JWKSet jwkSet = new JWKSet(jwk);
			this.jwkSet = jwkSet;
		}

		@Override
		protected void logSuccessByJWTType(Environment env, JWTClaimsSet claimSet, JWK jwk, JWSHeader header, String jws, JsonObject verifiableObj) {
			env.putString("signed_jwt", jws);
		}

		@Override
		public Environment evaluate(Environment env) {
			JsonObject claims = new JsonObject();
			claims.addProperty("claim", "value");
			env.putObject("claims", claims);
			JsonObject jwks = JsonParser.parseString(jwkSet.toString(false)).getAsJsonObject();
			signJWT(env, claims, jwks);
			return null;
		}
	}

	static class MyJwtSigner implements JWSSigner, AlternateJWSSignerFactory {

		final KeyPair keyPair;
		private final RSAKey keyToReplace;
		private JWSSigner delegate;

		MyJwtSigner(RSAKey rsaKeyToReplace) throws Exception {
			KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
			gen.initialize(2048);
			KeyPair keyPair = gen.generateKeyPair();
			this.keyPair = keyPair;
			this.delegate = new RSASSASigner(keyPair.getPrivate());
			this.keyToReplace = rsaKeyToReplace;
		}

		@Override
		public Base64URL sign(JWSHeader jwsHeader, byte[] bytes) throws JOSEException {
			return delegate.sign(jwsHeader, bytes);
		}

		@Override
		public Set<JWSAlgorithm> supportedJWSAlgorithms() {
			return Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256);
		}

		@Override
		public JCAContext getJCAContext() {
			return null;
		}

		@Override
		public boolean canSignWith(JWK jwk) {
			String keyID = jwk.getKeyID();
			if(keyID == null) {
				return false;
			}
			return keyID.equals(keyToReplace.getKeyID());
		}

		@Override
		public JWSSigner createJWSSigner(JWK jwk) throws JOSEException {
			return this;
		}

		@Override
		public JWSSigner createJWSSigner(JWK jwk, JWSAlgorithm jwsAlgorithm) throws JOSEException {
			return this;
		}
	}

}
